;; -*- lexical-binding: t -*-
;;; bootstrap.el

(eval-when-compile (require 'cl-lib))

;; Shut up byte-compiler!
(defvar polaris-current-theme)
(defvar polaris-current-font)

(defadvice require (around dotemacs activate)
  (let ((elapsed)
        (loaded (memq feature features))
        (start (current-time)))
    (prog1
        ad-do-it
      (unless loaded
        (with-current-buffer (get-buffer-create "*Require Times*")
          (when (= 0 (buffer-size))
            (insert "| feature | timestamp | elapsed |\n")
            (insert "|---------+-----------+---------|\n"))
          (goto-char (point-max))
          (setq elapsed (float-time (time-subtract (current-time) start)))
          (insert (format "| %s | %s | %f |\n"
                          feature
                          (format-time-string "%Y-%m-%d %H:%M:%S.%3N" (current-time))
                          elapsed)))))))

;; Global constants
(eval-and-compile
  (defconst polaris-default-theme 'polaris)
  (defconst polaris-default-font  nil)

  (defconst polaris-emacs-dir     (expand-file-name "." user-emacs-directory))
  (defconst polaris-core-dir      (concat user-emacs-directory "core/"))
  (defconst polaris-modules-dir   (concat user-emacs-directory "modules/"))
  (defconst polaris-private-dir   (concat user-emacs-directory "private"))
  (defconst polaris-home-dir      (concat (getenv "HOME") "/"))
  (defconst polaris-user-dir      (expand-file-name "usr/"  polaris-home-dir))
  (defconst polaris-mail-dir      (expand-file-name "mail/" polaris-user-dir))
  (setq custom-file (expand-file-name "custom.el" polaris-emacs-dir))

  (defconst polaris-script-dir    (concat user-emacs-directory "scripts"))
  (defconst polaris-ext-dir          (concat user-emacs-directory "ext"))
  (defconst polaris-snippet-dirs  (list (concat polaris-private-dir "/snippets")
                                        (concat polaris-private-dir "/templates")))

  ;; Hostname and emacs version-based elisp temp directories
  (defconst polaris-temp-dir      (format "%s/cache/%s/%s.%s/"
                                          polaris-private-dir (system-name)
                                          emacs-major-version emacs-minor-version))

  (defconst polaris-backup-dir
    (expand-file-name (concat "backups/" (user-real-login-name) "/")
                      polaris-temp-dir)
    "Directory for Emacs backups.")

  (defconst polaris-autosave-dir
    (expand-file-name (concat "autosaves/" (user-real-login-name) "/")
                      polaris-temp-dir)
    "Directory for Emacs auto saves.")

  (defconst IS-MAC     (eq system-type 'darwin))
  (defconst IS-LINUX   (eq system-type 'gnu/linux))
  (defconst IS-WINDOWS (eq system-type 'windows-nt)))

(defun add-to-load-path (dir) (add-to-list 'load-path dir))

(mapc 'add-to-load-path
      `(
        ,(concat user-emacs-directory "core/")
        ,(concat user-emacs-directory "core/defuns/")
        ,(concat user-emacs-directory "private/")
        ,(concat user-emacs-directory "modules/")
        ,(concat user-emacs-directory "modules/defuns/")
        ,(concat user-emacs-directory "modules/contrib/")
        ))

;; Load newer version of .el and .elc if both are available
(when (version<= "24.4" emacs-version)
  (setq load-prefer-newer t))

;;
;; Bootstrap
;;

(defun polaris (packages)
  "Bootstrap POLARIS emacs and initialize PACKAGES"
  ;; stop package.el from being annoying. I rely solely on Cask.
  (setq-default
   package--init-file-ensured t
   package-enable-at-startup nil
   gc-cons-percentage 0.2)

  (load (expand-file-name "packages.el" user-emacs-directory))

  ;; prematurely optimize for faster startup
  (let ((gc-cons-threshold 339430400)
        (gc-cons-percentage 0.6)
        file-name-handler-alist)

    ;; Scan various folders to populate the load-paths
    (setq custom-theme-load-path
          (append (list (expand-file-name "themes/" polaris-private-dir))
                  custom-theme-load-path))

    (require 'f)
    (require 'dash)
    (require 's)

    ;; Load local settings, if available
    (when (file-exists-p "~/.emacs.local.el")
      (load "~/.emacs.local.el"))

    ;; Global settings
    (setq polaris-current-theme polaris-default-theme
          polaris-current-font  polaris-default-font)

    ;; Load 'em up!
    ;; (load (expand-file-name "packages.el" user-emacs-directory))
    (load-theme polaris-current-theme t)
    (mapc 'require packages)))

;;; bootstrap.el ends here

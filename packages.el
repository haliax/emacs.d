
;; -*- lexical-binding: t -*-
;;; packages.el

(require 'package)
(require 'bytecomp)

(eval-when-compile
  (require 'cl-lib))

;; Load newer version of .el and .elc if both are available
(setq load-prefer-newer t
      package-menu-async t)

(setq package-archives '(("org" . "http://orgmode.org/elpa/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("gnu" . "https://elpa.gnu.org/packages/")))
(setq package-enable-at-startup nil)

(defconst polaris|packages
  '(
    ;; Core --- core/core.el
    dash
    f
    s

    use-package
    diminish
    persistent-soft
    async

    ;; UI --- core/core-ui.el
    visual-fill-column
    nlinum
    linum-relative
    rainbow-delimiters
    rainbow-mode
    golden-ratio
    spaceline
    paradox
    beacon
    highlight-numbers
    auto-highlight-symbol
    hl-anything

    ;; Evil --- core/core-evil.el
    evil
    evil-anzu
    evil-args
    evil-cleverparens
    evil-commentary
    evil-easymotion
    evil-embrace
    evil-exchange
    evil-iedit-state
    evil-matchit
    evil-multiedit
    evil-nerd-commenter
    evil-numbers
    evil-snipe
    evil-space
    evil-escape
    evil-surround
    evil-textobj-anyblock
    evil-textobj-column
    evil-visualstar
    evil-indent-plus
    evil-ediff
    link-hint
    sentence-navigation
    vertigo

    ;; Popup -- core/core-popup.el
    shackle

    ;; Editor --- core/core-editor.el
    adaptive-wrap
    ace-window
    ace-link
    avy
    avy-zap
    clean-aindent-mode
    clean-buffers
    help-fns+
    info+
    which-key
    srefactor
    discover-my-major
    counsel
    swiper
    dtrt-indent
    editorconfig
    windsize
    emr
    electric-operator
    expand-region
    fancy-narrow
    focus-autosave-mode
    goto-last-change
    miniedit
    jump-char
    indent-guide
    keychain-environment
    page-break-lines
    smart-forward
    smart-tabs-mode
    smartparens
    on-parens
    outorg
    outshine
    navi-mode
    smex
    subword
    super-save
    vlf
    ws-butler
    iflipb
    imenu-list
    dumb-jump

    ;; Completion --- core/core-company.el
    company
    company-statistics
    company-quickhelp
    company-anaconda
    helm-company
    company-emoji
    company-dict
    company-inf-ruby
    company-go
    company-ghc
    company-ghci
    company-tern
    company-web
    sly-company

    ;; Flycheck --- core/core-flycheck.el
    auto-dictionary
    flycheck
    flycheck-package
    flycheck-pos-tip
    flyspell-popup
    helm-ispell

    ;; Yasnippet --- core/core-yasnippet.el
    auto-yasnippet
    yasnippet

    ;; Project --- core/core-project.el
    flx
    flx-ido
    ido-ubiquitous
    ido-vertical-mode
    lacarte
    neotree
    ranger
    runner
    ibuffer-vc
    ibuffer-projectile

    ;; VCS --- core/core-vcs.el
    git-timemachine
    git-gutter-fringe
    magit
    magit-rockstar
    evil-magit
    magit-popup
    gitconfig-mode
    gitattributes-mode
    gitignore-mode
    helm-gitignore
    with-editor
    browse-at-remote

    ;; Helm -- core/core-helm.el
    helm
    helm-flx
    ;; helm-fuzzier
    helm-ag
    helm-c-yasnippet
    helm-flycheck
    helm-git-grep
    helm-mu
    helm-dash
    helm-projectile
    helm-swoop
    helm-themes
    helm-unicode
    projectile
    ace-jump-helm-line
    helm-describe-modes

    ;; Code evaluation/REPLs/debug -- core/core-eval.el
    quickrun
    repl-toggle
    realgud

    ;; Sessions --- core/core-sessions.el
    workgroups2
    eyebrowse
    persp-mode

    ;; Media --- modules/module-media.el
    mingus
    simple-mpc
    elfeed
    elfeed-goodies
    elfeed-org
    emoji-cheat-sheet-plus
    sauron
    erc
    erc-hl-nicks
    erc-image
    erc-colorize
    erc-social-graph
    erc-view-log
    erc-yt

    ;; Mail --- modules/module-mail.el
    mu4e-maildirs-extension
    mu4e-alert

    ;; Bindings -- core/core-bindings.el
    key-chord
    general

    ;; MODULES

    ;; C/C++
    irony
    irony-eldoc
    flycheck-irony
    company-irony
    company-irony-c-headers
    cmake-ide
    cpputils-cmake
    rtags
    clang-format
    disaster
    helm-gtags
    cmake-mode
    glsl-mode
    cuda-mode
    opencl-mode
    demangle-mode

    ;; C#
    csharp-mode
    omnisharp

    ;; Clojure
    align-cljlet
    clojure-mode
    clojure-snippets
    clojure-cheatsheet
    clj-refactor
    cider
    cider-eval-sexp-fu

    ;; Emacs Lisp
    auto-compile
    aggressive-indent
    elisp-slime-nav
    ;; evil-cleverparens
    highlight-defined
    lispy
    macrostep
    unkillable-scratch
    ;; bury-successful-compilation
    eval-sexp-fu

    ;; D -- modules/module-d.el
    d-mode
    flycheck-dmd-dub

    ;; Data -- modules/module-data.el
    json-mode
    yaml-mode
    toml-mode
    dockerfile-mode
    dactyl-mode
    pkgbuild-mode
    ssh-config-mode
    systemd
    vimrc-mode
    salt-mode
    company-ansible

    ;; Ediff
    ztree

    ;; Elixir
    alchemist
    elixir-mode

    ;; Elm
    elm-mode
    flycheck-elm

    ;; Go
    go-mode
    go-eldoc
    golint
    helm-go-package
    gorepl-mode

    ;; Haskell
    cmm-mode
    company-cabal
    haskell-mode
    ghc
    flycheck-haskell
    ghci-completion
    haskell-snippets
    helm-hoogle
    hindent
    shm

    ;; Java
    emacs-eclim
    groovy-mode
    android-mode

    ;; JS -- modules/module-js.el
    js2-mode
    js2-refactor
    jsx-mode
    tern
    coffee-mode
    tide
    typescript-mode
    skewer-mode
    nodejs-repl

    ;; Julia
    julia-mode

    ;; Latex
    auctex
    company-auctex
    ;; latex-pretty-symbols
    magic-latex-buffer

    ;; Lua
    lua-mode
    company-lua

    ;; Lisp
    sly
    sly-macrostep
    highlight-quoted

    ;; Nim
    nim-mode
    flycheck-nim

    ;; Nix
    nix-mode
    nixos-options
    company-nixos-options
    helm-nixos-options

    ;; Password
    pass

    ;; PDF
    interleave
    org-pdfview
    pdf-tools

    ;; PHP -- modules/module-php.el
    php-mode
    php-refactor-mode
    php-boris
    ac-php

    ;; Processing -- modules/module-processing.el
    processing-mode

    ;; Python -- modules/module-python.el
    anaconda-mode
    flycheck-pyflakes
    flycheck-mypy
    live-py-mode
    helm-pydoc
    pyenv-mode
    pip-requirements
    ein
    nose
    elpy

    ;; R -- modules/module-r.el
    ess
    ess-R-data-view
    ess-R-object-popup
    ess-smart-equals

    ;; Regex -- modules/module-regex.el
    pcre2el

    ;; Ruby
    enh-ruby-mode
    inf-ruby
    robe
    rspec-mode
    ruby-refactor
    yard-mode

    ;; Rust
    rust-mode
    racer
    company-racer
    flycheck-rust

    ;; Scala
    ensime
    scala-mode2

    ;; Scheme
    geiser
    hy-mode

    ;; Sh -- modules/module-sh.el
    company-shell
    fish-mode

    ;; Shell
    esh-help
    eshell-prompt-extras
    eshell-z
    shell-pop

    ;; Stan
    stan-mode

    ;; Text modes -- modules/module-text.el
    markdown-mode
    markdown-toc

    ;; Web development
    emmet-mode
    haml-mode
    less-css-mode
    jade-mode
    restclient
    sass-mode
    stylus-mode
    scss-mode
    slim-mode
    sws-mode
    web-mode

    ;; LIBRARIES ;;

        ;;;; ORGANIZATIONAL/WRITING ;;;;;;;;;;;;
    ;; Org -- modules/module-org.el
    org
    org-plus-contrib
    org-bullets
    org-page
    company-math
    ob-browser
    ob-coffee
    ob-diagrams
    ob-http
    ox-gfm
    ob-ipython
    ob-elixir
    ob-go
    ob-redis
    ob-restclient
    ob-sagemath
    ob-scala
    ob-translate
    org-cliplink
    org-tracktable
    worf

    ;; Org Notebook -- modules/module-org-notebook.el
    org-download
    ox-pandoc

    ;; Writing --- modules/extra-write.el
    helm-bibtex
    writegood-mode
    pandoc-mode
    writeroom-mode
    typo
    focus

    ;; Themes
    apropospriate-theme
    solarized-theme
    majapahit-theme
    spacemacs-theme
    darktooth-theme
    zenburn-theme
    monokai-theme

    ace-popup-menu
    calfw
    color-identifiers-mode
    deft
    easy-kill
    firestarter
    find-file-in-project
    find-file-in-repository
    framemove
    google-translate
    hydra
    highlight-numbers
    highlight-parentheses
    highlight-symbol
    icicles
    multiple-cursors
    shell-switcher
    buffer-flip
    eval-in-repl
    pandoc-mode
    dmenu

    sx
    whole-line-or-region
    esup

        ;;;; EXTRA TOOLS ;;;;;;;;;;;;;;;;;;;;;;;
    ;; Demo --- module-demo.el
    impatient-mode
    puml-mode
    ox-reveal
    htmlize

    ))

(package-initialize)

;; Auto install the required packages
;; https://github.com/bbatsov/prelude/blob/master/core/prelude-packages.el
;;
;; http://toumorokoshi.github.io/emacs-from-scratch-part-2-package-management.html
(defvar polaris/missing-packages '()
  "List populated at each startup that contains the list of packages that need
  to be installed.")

(dolist (p polaris|packages)
  (when (not (package-installed-p p))
    (add-to-list 'polaris/missing-packages p)))

(when polaris/missing-packages
  (message "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  ;; Install the missing packages
  (dolist (p polaris/missing-packages)
    (message "Installing `%s' .." p)
    (package-install p))
  (setq polaris/missing-packages '()))

; (unless package-archive-contents
;   (package-refresh-contents))
;
; (dolist (package package-selected-packages)
;   (when (and (assq package package-archive-contents)
;              (not (package-installed-p package)))
;     (package-install package t)))

(provide 'packages)
;;; packages.el ends here

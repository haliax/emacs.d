;; -*- lexical-binding: t -*-
;;; core-hydra.el

(require 'hydra)
(setq hydra-key-doc-function 'polaris/hydra-key-doc-function
      hydra-head-format "[%s] ")

;; Ivy
(defhydra hydra-ivy (:hint nil :foreign-keys run)
  "
 Move/Resize^^^^      | Select Action^^^^   |  Call^^          |  Cancel^^    | Toggles
--^-^-^-^-------------|--^-^-^-^------------|--^---^-----------|--^-^---------|---------------------
 [_j_/_k_] by line    | [_s_/_w_] next/prev | [_RET_] & done   | [_i_] & ins  | [_C_] calling: %s(if ivy-calling \"on\" \"off\")
 [_g_/_G_] first/last | [_a_]^ ^  list all  | [_TAB_] alt done | [_q_] & quit | [_m_] matcher: %s(ivy--matcher-desc)
 [_d_/_u_] pg down/up |  ^ ^ ^ ^            | [_c_]   & cont   |  ^ ^         | [_f_] case-fold: %`ivy-case-fold-search
 [_<_/_>_] resize     |  ^ ^ ^ ^            | [_o_]   occur    |  ^ ^         | [_t_] truncate: %`truncate-lines
 [_h_/_l_] out/in dir |  ^ ^ ^ ^            |  ^ ^             |  ^ ^         |  ^ ^

Current Action: %s(ivy-action-name)
"
  ("j" ivy-next-line)
  ("k" ivy-previous-line)
  ("l" ivy-alt-done)
  ("h" polaris/counsel-up-directory-no-error)
  ("g" ivy-beginning-of-buffer)
  ("G" ivy-end-of-buffer)
  ("d" ivy-scroll-up-command)
  ("u" ivy-scroll-down-command)
  ;; actions
  ("q" keyboard-escape-quit :exit t)
  ("C-g" keyboard-escape-quit :exit t)
  ("<escape>" keyboard-escape-quit :exit t)
  ("i" nil)
  ("C-o" nil)
  ("TAB" ivy-alt-done :exit nil)
  ;; ("C-j" ivy-alt-done :exit nil)
  ;; ("d" ivy-done :exit t)
  ("RET" ivy-done :exit t)
  ("c" ivy-call)
  ("C-m" ivy-done :exit t)
  ("C" ivy-toggle-calling)
  ("m" ivy-toggle-fuzzy)
  (">" ivy-minibuffer-grow)
  ("<" ivy-minibuffer-shrink)
  ("w" ivy-prev-action)
  ("s" ivy-next-action)
  ("a" ivy-read-action)
  ("t" (setq truncate-lines (not truncate-lines)))
  ("f" ivy-toggle-case-fold)
  ("o" ivy-occur :exit t))
(define-key ivy-minibuffer-map "\C-o" 'hydra-ivy/body)

;; Help
(defhydra hydra-help (:color blue :columns 4)
  "Help"
  ("a" helm-apropos "Apropos")
  ("c" describe-char "Describe Char")
  ("F" find-function "Find Function")
  ("f" describe-function "Describe Function")
  ("k" describe-key "Describe Key")
  ("K" find-function-on-key "Find Key")
  ("m" describe-mode "Describe Modes")
  ("V" find-variable "Find Variable")
  ("v" describe-variable "Describe Variable")
  ("b" helm-descbinds "Describe Bindings"))

;; Info
(defhydra hydra-info (:color blue :hint nil)
  "
Info-mode:

  ^^_]_ forward  (next logical node)       ^^_l_ast (←)        _u_p (↑)                             _f_ollow reference       _T_OC
  ^^_[_ backward (prev logical node)       ^^_r_eturn (→)      _m_enu (↓) (C-u for new window)      _i_ndex                  _d_irectory
  ^^_n_ext (same level only)               ^^_H_istory         _g_oto (C-u for new window)          _,_ next index item      _c_opy node name
  ^^_p_rev (same level only)               _<_/_t_op           _b_eginning of buffer                virtual _I_ndex          _C_lone buffer
  regex _s_earch (_S_ case sensitive)      ^^_>_ final         _e_nd of buffer                      ^^                       _a_propos

  _1_ .. _9_ Pick first .. ninth item in the node's menu.

"
  ("]"   Info-forward-node)
  ("["   Info-backward-node)
  ("n"   Info-next)
  ("p"   Info-prev)
  ("s"   Info-search)
  ("S"   Info-search-case-sensitively)

  ("l"   Info-history-back)
  ("r"   Info-history-forward)
  ("H"   Info-history)
  ("t"   Info-top-node)
  ("<"   Info-top-node)
  (">"   Info-final-node)

  ("u"   Info-up)
  ("^"   Info-up)
  ("m"   Info-menu)
  ("g"   Info-goto-node)
  ("b"   beginning-of-buffer)
  ("e"   end-of-buffer)

  ("f"   Info-follow-reference)
  ("i"   Info-index)
  (","   Info-index-next)
  ("I"   Info-virtual-index)

  ("T"   Info-toc)
  ("d"   Info-directory)
  ("c"   Info-copy-current-node-name)
  ("C"   clone-buffer)
  ("a"   info-apropos)

  ("1"   Info-nth-menu-item)
  ("2"   Info-nth-menu-item)
  ("3"   Info-nth-menu-item)
  ("4"   Info-nth-menu-item)
  ("5"   Info-nth-menu-item)
  ("6"   Info-nth-menu-item)
  ("7"   Info-nth-menu-item)
  ("8"   Info-nth-menu-item)
  ("9"   Info-nth-menu-item)

  ("?"   Info-summary "Info summary")
  ("h"   Info-help "Info help")
  ("q"   Info-exit "Info exit")
  ("C-g" nil "cancel" :color blue))

;; Frame
(defhydra hydra-frame-tools (:color blue :hint nil :idle 0.3)
  "
Frame^^              │   Other Frame
_0_  delete          │   _f_ find file         _f_ find file read only
_1_  delete others   │   _d_ dired             _m_ compose mail
_2_  make            │   _b_ switch buffer     _._ find tag
_o_  other           │   _C-o_ display buffer"
  ("C-f" find-file-other-frame)
  ("C-o" display-buffer-other-frame)
  ("."   find-tag-other-frame)
  ("0"   delete-frame)
  ("1"   delete-other-frames)
  ("2"   make-frame-command)
  ("b"   switch-to-buffer-other-frame)
  ("d"   dired-other-frame)
  ("f"   find-file-other-frame)
  ("m"   compose-mail-other-frame)
  ("o"   other-frame)
  ("r"   find-file-read-only-other-frame))

;; Window
(defhydra hydra-window-tools (:color blue :hint nil :idle 0.3)
  "
Window^^                     │   Other window
_0_  kill buffer & window    │   _f_ find file       _a_ add changelog entry
_c_  clone indirect          │   _d_ dired           _r_ find file read only
_C-j_  dired jump            │   _b_ switch buffer   _m_ compose mail
_C-o_  display buffer        │   _._ find tag"
  ("C-f" find-file-other-window)
  ("C-j" dired-jump-other-window)
  ("C-o" display-buffer)
  ("."   find-tag-other-window)
  ("0"   kill-buffer-and-window)
  ("a"   add-change-log-entry-other-window)
  ("b"   switch-to-buffer-other-window)
  ("c"   clone-indirect-buffer-other-window)
  ("d"   dired-other-window)
  ("f"   find-file-other-window)
  ("m"   compose-mail-other-window)
  ("r"   find-file-read-only-other-window))

;; Flycheck
(defhydra hydra-flycheck-errors
  (:foreign-keys run
                 :pre  (progn (setq hydra-lv t) (flycheck-list-errors))
                 :post (progn (setq hydra-lv nil) (quit-windows-on "*Flycheck errors*"))
                 :hint nil)
  "Errors"
  ("j"  flycheck-next-error            "Next")
  ("k"  flycheck-previous-error        "Previous")
  ("gg" flycheck-first-error           "First")
  ("G"  (progn (goto-char (point-max)) (flycheck-previous-error)) "Last")
  ("f"  flycheck-error-list-set-filter "Filter")
  ("s"  helm-flycheck                  "Search" :color blue)
  ("?"  flycheck-describe-checker      "Describe")
  ("<escape>" nil)
  ("q"  nil))

;; Git timemachine
(defhydra hydra-timemachine
  (:foreign-keys run
                 :pre  (progn
                         (setq hydra-lv t)
                         (let (golden-ratio-mode)
                           (unless (bound-and-true-p git-timemachine-mode)
                             (call-interactively 'git-timemachine))))
                 :post (progn
                         (setq hydra-lv nil)
                         (when (bound-and-true-p git-timemachine-mode)
                           (git-timemachine-quit)))
                 :hint nil)
  "
git time machine

[_p_] previous [_n_] next [_c_] current [_g_] goto nth rev [_Y_] copy hash [_q_] quit

"
("c" git-timemachine-show-current-revision)
("g" git-timemachine-show-nth-revision)
("p" git-timemachine-show-previous-revision)
("n" git-timemachine-show-next-revision)
("Y" git-timemachine-kill-revision)
("q" nil :exit t))

;; Git
(defhydra hydra-git
  (:columns 4
            :pre (setq hydra-lv t)
            :post (progn
                    (setq hydra-lv nil)
                    (condition-case nil
                        (delete-windows-on "*git-gutter:diff*")
                      (error nil)))
            :hint nil)
  "Git"
  ("F" #'magit-pull-popup "Pull" :color blue)
  ("b" #'magit-blame-popup "Blame" :color blue)
  ("z" #'magit-stash-popup "Stash" :color blue)
  ("l" #'magit-log-popup "Log" :color blue)
  ("d" #'magit-diff-popup "Diff" :color blue)
  ("c" #'magit-commit-popup "Commit" :color blue)
  ("m" #'magit-merge-popup "Merge" :color blue)
  ("p" #'magit-push-popup "Push" :color blue)
  ("f" #'magit-fetch-popup "Fetch" :color blue)
  ("v" #'magit-status "Status" :color blue)
  ("t" #'hydra-timemachine/body "Timemachine" :color blue)
  ("d" #'git-gutter:popup-hunk "Diff")
  ("s" #'git-gutter:stage-hunk "Stage")
  ("r" #'git-gutter:revert-hunk "Revert")
  ("j" #'git-gutter:next-hunk "Next")
  ("k" #'git-gutter:previous-hunk "Previous")
  ("gg" (progn
          (evil-goto-first-line)
          (git-gutter:next-hunk 1)) "First")
  ("G" (progn
         (evil-goto-line)
         (git-gutter:previous-hunk 1)) "Last")
  ("<escape>" nil)
  ("q" nil))

;; Toggle
(defhydra hydra-toggle (:color blue :columns 4)
  "Toggle"
  ("a" abbrev-mode "Abbrev")
  ("d" toggle-debug-on-error "Debug")
  ("f" auto-fill-mode "Auto-Fill")
  ("g" golden-ratio-mode "Golden Ratio")
  ("n" narrow-or-widen-dwim "Narrow")
  ("N" neotree-toggle "Neotree")
  ("r" rainbow-mode "Rainbow")
  ("s" flycheck-mode "Flycheck")
  ("S" (if (derived-mode-p 'prog-mode)
           (flyspell-prog-mode)
         (flyspell-mode)) "Flyspell")
  ("t" toggle-truncate-lines "Truncate lines")
  ("T" helm-themes "Themes")
  ("w" whitespace-mode "Whitespace"))

;; Applications
(defhydra hydra-apps (:color blue :hint nil)
  "
Apps:
_e_ eshell        _s_ ansi-term
_p_ packages      _f_ feeds
_i_ freenode      _n_ nixers
_m_ email         _M_ music

"
  ("e" eshell)
  ("p" paradox-list-packages)
  ("s" ansi-term)
  ("f" feeds)
  ("i" freenode)
  ("n" nixers)
  ("m" email)
  ("M" mingus)
  ("q" nil "cancel"))

;; PDF-Tools
(defhydra hydra-pdf-tools (:hint nil)
  "
 Navigation^^^^                Scale/Fit^^                    Annotations^^       Actions^^           Other^^
 ----------^^^^--------------- ---------^^------------------  -----------^^------ -------^^---------- -----^^---
 [_j_/_k_] scroll down/up      [_W_] fit to width             [_al_] list         [_s_] search         [_q_] quit
 [_h_/_l_] scroll left/right   [_H_] fit to height            [_at_] text         [_O_] outline
 [_d_/_u_] pg down/up          [_P_] fit to page              [_aD_] delete       [_p_] print
 [_J_/_K_] next/prev pg        [_m_] slice using mouse        [_am_] markup       [_o_] open link
 ^^^^                          [_b_] slice from bounding box  ^^                  [_r_] revert
 ^^^^                          [_R_] reset slice              ^^                  [_t_] attachments
 ^^^^                          ^^                             ^^                  [_n_] night mode
 "
  ;; Navigation
  ("j"  pdf-view-next-line-or-next-page)
  ("k"  pdf-view-previous-line-or-previous-page)
  ("l"  image-forward-hscroll)
  ("h"  image-backward-hscroll)
  ("J"  pdf-view-next-page)
  ("K"  pdf-view-previous-page)
  ("u"  pdf-view-scroll-down-or-previous-page)
  ("d"  pdf-view-scroll-up-or-next-page)
  ;; Scale/Fit
  ("W" pdf-view-fit-width-to-window)
  ("H" pdf-view-fit-height-to-window)
  ("P" pdf-view-fit-page-to-window)
  ("m" pdf-view-set-slice-using-mouse)
  ("b" pdf-view-set-slice-from-bounding-box)
  ("R" pdf-view-reset-slice)
  ;; Annotations
  ("aD" pdf-annot-delete)
  ("at" pdf-annot-attachment-dired :exit t)
  ("al" pdf-annot-list-annotations :exit t)
  ("am" pdf-annot-add-markup-annotation)
  ;; Actions
  ("s" pdf-occur :exit t)
  ("O" pdf-outline :exit t)
  ("p" pdf-misc-print-document :exit t)
  ("o" pdf-links-action-perform :exit t)
  ("r" pdf-view-revert-buffer)
  ("t" pdf-annot-attachment-dired :exit t)
  ("n" pdf-view-midnight-minor-mode)
  ;; Other
  ("q" nil :exit t))

;; Eyebrowse
(defhydra hydra-workspaces (:hint nil)
  "
 Go to^^^^^^                         Actions^^
 ─────^^^^^^───────────────────────  ───────^^──────────────────────
 [_0_,_9_]^^     nth/new workspace   [_d_] close current workspace
 [_C-0_,_C-9_]^^ nth/new workspace   [_R_] rename current workspace
 [_<tab>_]^^^^   last workspace
 [_l_]^^^^       layouts
 [_n_/_C-l_]^^   next workspace
 [_N_/_p_/_C-h_] prev workspace

"
  ("0" eyebrowse-switch-to-window-config-0 :exit t)
  ("1" eyebrowse-switch-to-window-config-1 :exit t)
  ("2" eyebrowse-switch-to-window-config-2 :exit t)
  ("3" eyebrowse-switch-to-window-config-3 :exit t)
  ("4" eyebrowse-switch-to-window-config-4 :exit t)
  ("5" eyebrowse-switch-to-window-config-5 :exit t)
  ("6" eyebrowse-switch-to-window-config-6 :exit t)
  ("7" eyebrowse-switch-to-window-config-7 :exit t)
  ("8" eyebrowse-switch-to-window-config-8 :exit t)
  ("9" eyebrowse-switch-to-window-config-9 :exit t)
  ("C-0" eyebrowse-switch-to-window-config-0)
  ("C-1" eyebrowse-switch-to-window-config-1)
  ("C-2" eyebrowse-switch-to-window-config-2)
  ("C-3" eyebrowse-switch-to-window-config-3)
  ("C-4" eyebrowse-switch-to-window-config-4)
  ("C-5" eyebrowse-switch-to-window-config-5)
  ("C-6" eyebrowse-switch-to-window-config-6)
  ("C-7" eyebrowse-switch-to-window-config-7)
  ("C-8" eyebrowse-switch-to-window-config-8)
  ("C-9" eyebrowse-switch-to-window-config-9)
  ("<tab>" eyebrowse-last-window-config)
  ("C-h" eyebrowse-prev-window-config)
  ("C-i" eyebrowse-last-window-config)
  ("C-l" eyebrowse-next-window-config)
  ("d" eyebrowse-close-window-config)
  ("l" hydra-persp/body :exit t)
  ("n" eyebrowse-next-window-config)
  ("N" eyebrowse-prev-window-config)
  ("p" eyebrowse-prev-window-config)
  ("R" polaris/workspaces-rename :exit t)
  ("w" eyebrowse-switch-to-window-config :exit t))

(defhydra hydra-persp (:hint nil)
  "
 Go to^^^^^^                                  Actions^^
 ─────^^^^^^──────────────────────────────    ───────^^──────────────────────────────────────────────────
 [_0_,_9_]^^     nth/new layout               [_a_]^^   add buffer
 [_w_]^^^^       workspaces                   [_A_]^^   add all from layout
 [_<tab>_]^^^^   last layout                  [_d_]^^   close current layout
 [_b_]^^^^       buffer in layout             [_D_]^^   close other layout
 [_h_]^^^^       default layout               [_r_]^^   remove current buffer
 [_l_]^^^^       layout w/helm/ivy            [_R_]^^   rename current layout
 [_L_]^^^^       layouts in file              [_s_/_S_] save all layouts/save by names
 [_n_/_C-l_]^^   next layout                  [_t_]^^   show a buffer without adding it to current layout
 [_N_/_p_/_C-h_] prev layout                  [_x_]^^   kill current w/buffers
                                        [_X_]^^   kill other w/buffers

"
  ("1" spacemacs/persp-switch-to-1 :exit t)
  ("2" spacemacs/persp-switch-to-2 :exit t)
  ("3" spacemacs/persp-switch-to-3 :exit t)
  ("4" spacemacs/persp-switch-to-4 :exit t)
  ("5" spacemacs/persp-switch-to-5 :exit t)
  ("6" spacemacs/persp-switch-to-6 :exit t)
  ("7" spacemacs/persp-switch-to-7 :exit t)
  ("8" spacemacs/persp-switch-to-8 :exit t)
  ("9" spacemacs/persp-switch-to-9 :exit t)
  ("0" spacemacs/persp-switch-to-0 :exit t)
  ("<tab>" polaris/jump-to-last-layout)
  ("<return>" nil :exit t)
  ("C-h" persp-prev)
  ("C-l" persp-next)
  ("a" persp-add-buffer :exit t)
  ("A" persp-import-buffers :exit t)
  ("b" polaris/persp-helm-mini :exit t)
  ("d" polaris/layouts-close)
  ("D" polaris/layouts-close-other :exit t)
  ("h" polaris/layout-goto-default :exit t)
  ("l" polaris/helm-perspectives :exit t)
  ("L" persp-load-state-from-file :exit t)
  ("n" persp-next)
  ("N" persp-prev)
  ("p" persp-prev)
  ("r" persp-remove-buffer :exit t)
  ("R" polaris/layouts-rename :exit t)
  ("s" persp-save-state-to-file :exit t)
  ("S" persp-save-to-file-by-names :exit t)
  ("t" persp-temporarily-display-buffer :exit t)
  ("w" hydra-workspaces/body :exit t)
  ("x" polaris/layouts-kill)
  ("X" polaris/layouts-kill-other :exit t))

;; Paste
(evil-define-command polaris/transient-state-0 ()
  :keep-visual t
  :repeat nil
  (interactive)
  (if current-prefix-arg
      (progn
        (setq this-command #'digit-argument)
        (call-interactively #'digit-argument))
    (setq this-command #'evil-beginning-of-line
          hydra-deactivate t)
    (call-interactively #'evil-beginning-of-line)))

(defhydra hydra-paste (:hint nil)
  "
                        [%(length kill-ring-yank-pointer)/%(length kill-ring)]

[_C-j_/_C-k_] cycles through yanked text, [_p_/_P_] pastes the same text above or below. Anything else exits.

"
  ("C-j" evil-paste-pop)
  ("C-k" evil-paste-pop-next)
  ("p" evil-paste-after)
  ("P" evil-paste-before)
  ("0" polaris/transient-state-0))

(define-key evil-normal-state-map
  "p" 'hydra-paste/evil-paste-after)
(define-key evil-normal-state-map
  "P" 'hydra-paste/evil-paste-before)

;;
;; Emacs-Lisp
;;

(defhydra hydra-macrostep (:foreign-keys run :hint nil)
  "
[_e_] expand [_c_] collapse [_n_/_N_] next/previous [_q_] quit

"
  ("e" macrostep-expand)
  ("c" macrostep-collapse)
  ("n" macrostep-next-macro)
  ("N" macrostep-prev-macro)
  ("q" macrostep-collapse-all :exit t))

(provide 'core-hydra)
;;; core-hydra.el ends here

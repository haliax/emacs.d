;; -*- lexical-binding: t -*-
;; core-evil.el --- the root of all evil

(use-package evil
  :init
  ;; highlight matching delimiters where it's important
  (defun show-paren-mode-off () (show-paren-mode -1))
  (add-hook 'evil-insert-state-entry-hook    'show-paren-mode)
  (add-hook 'evil-insert-state-exit-hook     'show-paren-mode-off)
  (add-hook 'evil-visual-state-entry-hook    'show-paren-mode)
  (add-hook 'evil-visual-state-exit-hook     'show-paren-mode-off)
  (add-hook 'evil-operator-state-entry-hook  'show-paren-mode)
  (add-hook 'evil-operator-state-exit-hook   'show-paren-mode-off)
  (add-hook 'evil-normal-state-entry-hook    'show-paren-mode-off)

  ;; Disable highlights on insert-mode
  (add-hook! evil-insert-state-entry 'evil-ex-nohighlight)
  (setq evil-move-beyond-eol t
        evil-auto-indent t
        evil-magic 'very-magic
        evil-search-module 'evil-search
        evil-want-C-u-scroll nil             ; enable C-u for scrolling
        evil-ex-visual-char-range t          ; column range for ex commands
        evil-want-Y-yank-to-eol t
        evil-want-visual-char-semi-exclusive t
        evil-ex-search-vim-style-regexp t
        evil-ex-interactive-search-highlight 'selected-window
        evil-want-C-w-in-emacs-state t
        evil-echo-state nil
        evil-ex-substitute-global t
        evil-insert-skip-empty-lines t
        evil-search-module 'evil-search

        ;; NOTE: a bug in emacs 25 breaks undoing in evil. See
        ;; https://bitbucket.org/lyro/evil/issues/594/undo-doesnt-behave-like-vim
        evil-want-fine-undo (if (> emacs-major-version 24) 'fine 'no)

        evil-normal-state-tag "N"
        evil-insert-state-tag "I"
        evil-visual-state-tag "V"
        evil-emacs-state-tag "E"
        evil-operator-state-tag "O"
        evil-motion-state-tag "M"
        evil-replace-state-tag "R"

        evil-cross-lines t
        evil-move-cursor-back nil
        evil-symbol-word-search t
        evil-ex-complete-emacs-commands t
        evil-shift-round nil)

  :config

  (evil-mode 1)
  (evil-select-search-module 'evil-search-module 'evil-search)

  (defun cursor-colors ()
    (interactive)
    (setq-default evil-emacs-state-cursor    '("#5E8491" box)
                  evil-insert-state-cursor   '("#D4754E" hbar)
                  evil-normal-state-cursor   '("#34646F" box)
                  evil-visual-state-cursor   '("#7F355E" box)
                  evil-motion-state-cursor   '("#AAB151" bar)
                  evil-replace-state-cursor  '("#6F0D0B" box)
                  evil-operator-state-cursor '("#953331" hollow)))

  (add-hook 'after-init-hook #'cursor-colors)


  ;; esc quits almost everywhere, Gotten from
  ;; http://stackoverflow.com/questions/8483182/emacs-evil-mode-best-practice,;;
  ;; trying to emulate the Vim behaviour
  (define-key evil-normal-state-map [escape] #'keyboard-quit)
  (define-key evil-emacs-state-map [escape] #'evil-normal-state)
  (define-key evil-visual-state-map [escape] #'keyboard-quit)
  (define-key evil-motion-state-map [escape] #'evil-normal-state)
  (define-key evil-operator-state-map [escape] #'evil-normal-state)
  (define-key minibuffer-local-map [escape] #'minibuffer-keyboard-quit)
  (define-key minibuffer-local-ns-map [escape] #'minibuffer-keyboard-quit)
  (define-key minibuffer-local-completion-map [escape] #'minibuffer-keyboard-quit)
  (define-key minibuffer-local-must-match-map [escape] #'minibuffer-keyboard-quit)
  (define-key minibuffer-local-isearch-map [escape] #'minibuffer-keyboard-quit)

  (evil-define-key 'normal evil-command-window-mode-map [escape] 'kill-buffer-and-window)

  (global-set-key (kbd "C-<backspace>") #'evil-delete-backward-word)

  (evil-define-command evil-delete-backward-word-smart ()
    "Delete previous word."
    (require 'subword)
    (if (and (bolp) (not (bobp)))
        (progn
          (unless evil-backspace-join-lines (user-error "Beginning of line"))
          (delete-char -1))
      (evil-delete (max
                    (let ((word-point (save-excursion
                                        (subword-backward)
                                        (point))))
                      (if (= word-point (save-excursion
                                          (backward-char)
                                          (point)))
                          (save-excursion
                            (backward-char)
                            (subword-backward)
                            (point))
                        word-point))
                    (line-beginning-position))
                   (point)
                   'exclusive
                   nil)))

  (global-set-key (kbd "<C-backspace>") #'evil-delete-backward-word-smart)
  (define-key evil-insert-state-map (kbd "C-t") #'transpose-chars)
  (define-key evil-insert-state-map (kbd "C-d") #'evil-delete)
  (global-set-key (kbd "<remap> <kill-whole-line>") #'evil-delete-whole-line)

  (define-key evil-insert-state-map (kbd "<insert>") #'evil-replace-state)
  (define-key evil-replace-state-map (kbd "<insert>") #'evil-insert-state)

  (evil-define-command evil-cycle-spacing (&optional count)
    (cycle-spacing (or count 1)))

  (global-set-key (kbd "<remap> <just-one-space>") #'evil-cycle-spacing)
  (global-set-key (kbd "<remap> <delete-horizontal-space>") #'evil-cycle-spacing)

  (global-set-key (kbd "C-0") #'delete-window)
  (global-set-key (kbd "C-1") #'delete-other-windows)
  (global-set-key (kbd "C-2") #'split-window-below)
  (global-set-key (kbd "C-3") #'split-window-right)
  (global-set-key (kbd "C-4") #'find-file-other-window)
  (global-set-key (kbd "C-5") #'make-frame-command)
  (global-set-key (kbd "M-j") #'evil-join)
  (global-set-key (kbd "C-.") #'er/expand-region)

  (define-key key-translation-map (kbd "<mouse-21>") (kbd "<C-mouse-5>"))
  (define-key key-translation-map (kbd "<mouse-20>") (kbd "<C-mouse-4>"))
  (global-set-key (kbd "<C-mouse-5>") #'evil-scroll-page-down)
  (global-set-key (kbd "<C-mouse-4>") #'evil-scroll-page-up)

  ;; modes to map to different default states
  (dolist (mode-map '((compilation-mode . normal)
                      (backups-mode . insert)
                      (message-mode . normal)
                      (debugger-mode . normal)
                      (diff-mode . motion)
                      (erc-mode . insert)
                      (tabulated-list-mode . emacs)
                      (profile-report-mode . emacs)
                      (log-view-mode . emacs)
                      (Info-mode . emacs)
                      (eshell-mode . emacs)
                      (shell-mode . emacs)
                      (term-mode . emacs)
                      (deft-mode . emacs)
                      (occur-mode . emacs)
                      (view-mode . emacs)
                      (comint-mode . emacs)
                      (cider-repl-mode . emacs)
                      (cider-docview-mode . emacs)
                      (cider-inspector-mode . emacs)
                      (cider-popup-buffer-mode . emacs)
                      (cider-stacktrace-mode . emacs)
                      (cider-test-report-mode . emacs)
                      (cljr--change-signature-mode . emacs)
                      (Man-mode . motion)
                      (grep-mode . emacs)
                      (image-mode . normal)
                      (undo-tree-visualizer-mode . motion)
                      ))
    (evil-set-initial-state `,(car mode-map) `,(cdr mode-map)))

  ;; Shortcuts for the evil expression register
  (defmacro $= (str &rest args)
    `(calc-eval (format ,str ,@args)))
  (defmacro $r (char)
    `(evil-get-register ,char))
  (defmacro $expand (path)
    `(evil-ex-replace-special-filenames ,path))

  ;; ah -- whole buffer.
  (evil-define-text-object evil-whole-buffer (count &rest other-args)
    "Select the whole buffer."
    :type 'inclusive
    (list (point-min) (point-max)))
  (bind-key "h" #'evil-whole-buffer evil-outer-text-objects-map)

  ;; ih -- from the first to the last non-whitespace character.
  (evil-define-text-object evil-whole-buffer-without-whitespace (count &rest other-args)
    "Select from the first to the last non-whitespace character in the buffer.
Whitespace characters are \n and characters marked as whitespace in current syntax table.
If the buffer consists only of spaces, select the whole buffer."
    :type 'exclusive
    (let ((non-whitespace-regex "[^\n[:space:]]"))
      (save-excursion
        (let ((after-first-non-space
               (progn (goto-char (point-min))
                      (re-search-forward non-whitespace-regex nil t)))
              (after-last-non-space
               (progn (goto-char (point-max))
                      (re-search-backward non-whitespace-regex nil t))))
          (if after-first-non-space ; If the buffer contains non-whitespace characters...
              (list (- after-first-non-space 1) after-last-non-space)
            (list (point-min) (point-max)))))))
  (bind-key "h" #'evil-whole-buffer-without-whitespace evil-inner-text-objects-map)

  (evil-define-operator evil-replace-symbol (beg end)
    "Build an Evil command for replacing a symbol in the supplied part of the buffer.
When called without region and there's symbol at point, use it."
    :type line ; Takes whole lines. Evil's ":s" command works on whole lines anyway.
    :move-point nil ; Don't go to `beg' before executing (this would mess with `thing-at-point').
    (cl-flet ((ex-before-after (before after) ; Prompt for an Ex command, with point between BEFORE and AFTER.
                               (evil-ex (cons (concat before after) (1+ (length before)))))) ; When the argument is (STRING . POSITION), point is placed at one-indexed POSITION (documented in `read-from-minibuffer').
      (let ((symbol (thing-at-point 'symbol t)))
        (if (and symbol (not (use-region-p)))
            ;; When we were called as an operator with a symbol at point, insert the symbol as the text to replace.
            (let ((replaced (concat (rx symbol-start) symbol (rx symbol-end)))
                  (line-range (concat (number-to-string (line-number-at-pos beg)) ","
                                      (number-to-string (line-number-at-pos end)))))
              (ex-before-after (concat line-range "s/" replaced "/")
                               "/gI")) ; I -- turn off case insensitivity.
          ;; When we were called in visual state or there's no symbol at point, insert an empty place for a symbol.
          (let ((range (if (use-region-p) "'<,'>" "")))
            (ex-before-after (concat range "s/" (rx symbol-start))
                             (concat (rx symbol-end) "//g")))))))

  (bind-key "Q" #'evil-replace-symbol evil-normal-state-map)
  (bind-key "Q" #'evil-replace-symbol evil-visual-state-map)

  ;; ' -- go to a mark (instead of the mark's line).
  (bind-key "'" #'evil-goto-mark evil-motion-state-map)

  ;; M -- store the point position in an Emacs register
  ;; ` -- jump to a register.
  (bind-key "M" #'point-to-register evil-motion-state-map)
  (bind-key "`" #'jump-to-register evil-motion-state-map)

  ;; Insert a character verbatim
  (bind-key "C-v" #'quoted-insert evil-insert-state-map)
  (bind-key "C-v" #'quoted-insert evil-replace-state-map)

  ;; Insert newline after/before the current line.
  (defun my-insert-line-below (count)
    (interactive "p")
    (cl-dotimes (unused-var count)
      (evil-insert-newline-below)))
  (bind-key "g o" #'my-insert-line-below evil-normal-state-map)

  (defun my-insert-line-above (count)
    (interactive "p")
    (cl-dotimes (unused-var count)
      (evil-insert-newline-above)))
  (bind-key "g O" #'my-insert-line-above evil-normal-state-map)

  ;; buffer-local ex commands, thanks to:
  ;; http://emacs.stackexchange.com/questions/13186
  (defun evil-ex-define-cmd-local (cmd function)
    "Locally binds the function FUNCTION to the command CMD."
    (unless (local-variable-p 'evil-ex-commands)
      (setq-local evil-ex-commands (copy-alist evil-ex-commands)))
    (evil-ex-define-cmd cmd function))

  ;; define text objects
  (def-textobj! "$" "dollar" "$" "$")
  (def-textobj! "*" "star" "*" "*")
  (def-textobj! "8" "block-star" "/*" "*/")
  (def-textobj! "|" "bar" "|" "|")
  (def-textobj! "%" "percent" "%" "%")
  (def-textobj! "/" "slash" "/" "/")
  (def-textobj! "_" "underscore" "_" "_")
  (def-textobj! "-" "hyphen" "-" "-")
  (def-textobj! "~" "tilde" "~" "~")
  (def-textobj! "=" "equal" "=" "=")
  (evil-define-text-object evil-pasted (count &rest args)
    (list (save-excursion (evil-goto-mark ?\[) (point))
          (save-excursion (evil-goto-mark ?\]) (point))))
  (define-key evil-inner-text-objects-map "P" 'evil-pasted)
  ;; define text-object for entire buffer
  (evil-define-text-object evil-inner-buffer (count &optional beg end type)
    (list (point-min) (point-max)))
  (define-key evil-inner-text-objects-map "g" 'evil-inner-buffer)

  (progn ; evil hacks
    (advice-add 'evil-force-normal-state :after 'polaris*evil-esc-quit)
    (defun polaris*evil-esc-quit ()
      "Close popups, disable search highlights and quit the minibuffer if open."
      (when (minibuffer-window-active-p (minibuffer-window))
        (abort-recursive-edit))
      (ignore-errors
        (evil-ex-nohighlight))
      ;; Close non-repl popups and clean up `polaris-popup-windows'
      (unless (memq (get-buffer-window) polaris-popup-windows)
        (mapc (lambda (w)
                (if (window-live-p w)
                    (with-selected-window w
                      (unless (derived-mode-p 'comint-mode)
                        (polaris/popup-close w)))
                  (polaris/popup-remove w)))
              polaris-popup-windows)))

    ;; Fix harmless (yet disruptive) error reporting w/ hidden buffers caused by
    ;; workgroups killing windows
    ;; TODO Delete timer on dead windows
    (defadvice evil-ex-hl-do-update-highlight (around evil-ex-hidden-buffer-ignore-errors activate)
      (ignore-errors ad-do-it))

    ;; Hide keystroke display while isearch is active
    (add-hook! isearch-mode     (setq echo-keystrokes 0))
    (add-hook! isearch-mode-end (setq echo-keystrokes 0.02))

    ;; Repeat motions with SPC/S-SPC
    (defmacro def-repeat! (command next-func prev-func)
      `(defadvice ,command
           (before ,(intern (format "polaris-space--%s" (symbol-name command))) activate)
         (define-key evil-motion-state-map (kbd "TAB") ',next-func)
         (define-key evil-motion-state-map (kbd "S-TAB") ',prev-func)))

    (after! evil-snipe
      (def-repeat! evil-snipe-f evil-snipe-repeat evil-snipe-repeat-reverse)
      (def-repeat! evil-snipe-F evil-snipe-repeat evil-snipe-repeat-reverse)
      (def-repeat! evil-snipe-t evil-snipe-repeat evil-snipe-repeat-reverse)
      (def-repeat! evil-snipe-T evil-snipe-repeat evil-snipe-repeat-reverse)
      (def-repeat! evil-snipe-s evil-snipe-repeat evil-snipe-repeat-reverse)
      (def-repeat! evil-snipe-S evil-snipe-repeat evil-snipe-repeat-reverse)
      (def-repeat! evil-snipe-x evil-snipe-repeat evil-snipe-repeat-reverse)
      (def-repeat! evil-snipe-X evil-snipe-repeat evil-snipe-repeat-reverse))

    (after! evil-visualstar
      (def-repeat! evil-visualstar/begin-search-forward evil-ex-search-next evil-ex-search-previous)
      (def-repeat! evil-visualstar/begin-search-backward evil-ex-search-previous evil-ex-search-next))

    (def-repeat! evil-ex-search-next evil-ex-search-next evil-ex-search-previous)
    (def-repeat! evil-ex-search-previous evil-ex-search-next evil-ex-search-previous)
    (def-repeat! evil-ex-search-forward evil-ex-search-next evil-ex-search-previous)
    (def-repeat! evil-ex-search-backward evil-ex-search-next evil-ex-search-previous)

    ;; A monkey patch to add all of vim's file ex substitution flags to evil-mode.
    (defun evil-ex-replace-special-filenames (file-name)
      "Replace special symbols in FILE-NAME."
      (let ((case-fold-search nil)
            (regexp (concat "\\(?:^\\|[^\\\\]\\)"
                            "\\([#%@]\\)"
                            "\\(\\(?::\\(?:[phtreS~.]\\|g?s[^: $]+\\)\\)*\\)")))
        (dolist (match (s-match-strings-all regexp file-name))
          (let ((flags (split-string (caddr match) ":" t))
                (path (file-relative-name
                       (pcase (cadr match)
                         ("@" (polaris/project-root))
                         ("%" (buffer-file-name))
                         ("#" (and (other-buffer) (buffer-file-name (other-buffer)))))
                       default-directory))
                flag global)
            (when path
              (while flags
                (setq flag (pop flags))
                (when (string-suffix-p "\\" flag)
                  (setq flag (concat flag (pop flags))))
                (when (string-prefix-p "gs" flag)
                  (setq global t flag (string-remove-prefix "g" flag)))
                (setq path
                      (or (pcase (substring flag 0 1)
                            ("p" (expand-file-name path))
                            ("~" (file-relative-name path "~"))
                            ("." (file-relative-name path default-directory))
                            ("h" (directory-file-name path))
                            ("t" (file-name-nondirectory (directory-file-name path)))
                            ("r" (file-name-sans-extension path))
                            ("e" (file-name-extension path))
                            ("s" (let* ((args (evil-delimited-arguments (substring flag 1) 2))
                                        (pattern (evil-transform-vim-style-regexp (car args)))
                                        (replace (cadr args)))
                                   (replace-regexp-in-string
                                    (if global pattern (concat "\\(" pattern "\\).*\\'"))
                                    (evil-transform-vim-style-regexp replace) path t t
                                    (unless global 1))))
                            ("S" (shell-quote-argument path))
                            (t path))
                          "")))
              (setq file-name
                    (replace-regexp-in-string (format "\\(?:^\\|[^\\\\]\\)\\(%s\\)"
                                                      (string-trim-left (car match)))
                                              path file-name t t 1)))))
        ;; Clean up
        (setq file-name (replace-regexp-in-string regexp "\\1" file-name t)))))

  ;; Make :g[lobal] highlight matches
  ;; TODO Redo this mess
  (defvar polaris-buffer-match-global evil-ex-substitute-global "")
  (defun polaris--ex-buffer-match (flag &optional arg)
    (let ((hl-name 'evil-ex-buffer-match))
      (with-selected-window (minibuffer-selected-window)
        (polaris/-ex-match-init hl-name)
        (polaris/-ex-buffer-match arg hl-name (list (if polaris-buffer-match-global ?g))))))
  (defun polaris--ex-global-match (flag &optional arg)
    (let ((hl-name 'evil-ex-global-match))
      (with-selected-window (minibuffer-selected-window)
        (polaris/-ex-match-init hl-name)
        (let ((result (car-safe (evil-ex-parse-global arg))))
          (polaris/-ex-buffer-match result hl-name nil (point-min) (point-max))))))

  (evil-ex-define-argument-type buffer-match :runner polaris--ex-buffer-match)
  (evil-ex-define-argument-type global-match :runner polaris--ex-global-match)

  (evil-define-interactive-code "<//>"
    :ex-arg buffer-match
    (list (when (evil-ex-p) evil-ex-argument)))
  (evil-define-interactive-code "<g//>"
    :ex-arg global-match
    (when (evil-ex-p) (evil-ex-parse-global evil-ex-argument)))

  (evil-define-operator polaris:align (&optional beg end bang pattern)
    "Ex interface to `align-regexp'. Accepts vim-style regexps."
    (interactive "<r><!><//>")
    (align-regexp
     beg end
     (concat "\\(\\s-*\\)"
             (if bang
                 (regexp-quote pattern)
               (evil-transform-vim-style-regexp pattern)))
     1 1))

  (evil-define-operator polaris:evil-ex-global (beg end pattern command &optional invert)
    :motion mark-whole-buffer
    :move-point nil
    (interactive "<r><g//><!>")
    (evil-ex-global beg end pattern command invert))
  (evil-ex-define-cmd "g[lobal]" 'polaris:evil-ex-global))

(use-package evil-evilified-state)

;; evil plugins
(use-package evil-anzu
  :init (global-anzu-mode)
  :config
  (setq anzu-cons-mode-line-p nil
        anzu-replace-to-string-separator " → "
        anzu-minimum-input-length 1
        anzu-search-threshold 250))

(use-package evil-args
  :commands (evil-inner-arg evil-outer-arg evil-forward-arg evil-backward-arg evil-jump-out-args)
  :init
  (define-key evil-normal-state-map "K" #'evil-jump-out-args)
  (define-key evil-inner-text-objects-map "a" #'evil-inner-arg)
  (define-key evil-outer-text-objects-map "a" #'evil-outer-arg))

;; (use-package evil-commentary
;;   :diminish evil-commentary-mode
;;   :commands (evil-commentary
;;              evil-commentary-yank
;;              evil-commentary-line)
;;   :config (evil-commentary-mode 1))

(use-package evil-ediff
  :config (evil-ediff-init))

(use-package evil-exchange
  :commands evil-exchange
  :config
  (advice-add 'evil-force-normal-state :after 'polaris*evil-exchange-off))

(use-package evil-multiedit
  :commands (evil-multiedit-match-all
             evil-multiedit-match-and-next
             evil-multiedit-match-and-prev
             evil-multiedit-toggle-or-restrict-region
             evil-multiedit-next
             evil-multiedit-prev
             evil-multiedit-abort
             evil-multiedit-ex-match))

(use-package evil-indent-plus
  :init (evil-indent-plus-default-bindings))

(use-package evil-matchit
  :commands (evilmi-jump-items evilmi-text-object global-evil-matchit-mode)
  :config (global-evil-matchit-mode 1))

(use-package evil-numbers
  :commands (evil-numbers/inc-at-pt evil-numbers/dec-at-pt))

(use-package evil-textobj-anyblock
  :commands (evil-textobj-anyblock-inner-block evil-textobj-anyblock-a-block)
  :init
  (define-key evil-inner-text-objects-map "B" 'evil-textobj-anyblock-inner-block)
  (define-key evil-outer-text-objects-map "B" 'evil-textobj-anyblock-a-block))


(use-package evil-search-highlight-persist
  :disabled t
  :config
  (global-evil-search-highlight-persist t)
  (advice-add 'evil-force-normal-state :after 'evil-search-highlight-persist-remove-all))

(use-package evil-easymotion
  :defer 1
  :init (defvar polaris--evil-snipe-repeat-fn)
  :config
  (evilem-default-keybindings "g SPC")
  (evilem-define (kbd "g SPC n") 'evil-ex-search-next)
  (evilem-define (kbd "g SPC N") 'evil-ex-search-previous)
  (define-key evil-normal-state-map (kbd "g SPC l") #'evil-avy-goto-line)
  (define-key evil-motion-state-map (kbd "g SPC l") #'evil-avy-goto-line)
  (define-key evil-normal-state-map (kbd "g SPC c") #'avy-goto-char-timer)
  (define-key evil-motion-state-map (kbd "g SPC c") #'avy-goto-char-timer)
  (evilem-define "gs" 'evil-snipe-repeat
                 :pre-hook (save-excursion
                             (ignore-errors
                               (call-interactively #'evil-snipe-s)))
                 :bind ((evil-snipe-scope 'buffer)
                        (evil-snipe-enable-highlight)
                        (evil-snipe-enable-incremental-highlight)))
  (evilem-define "gS" 'evil-snipe-repeat-reverse
                 :pre-hook (save-excursion
                             (ignore-errors
                               (call-interactively #'evil-snipe-s)))
                 :bind ((evil-snipe-scope 'buffer)
                        (evil-snipe-enable-highlight)
                        (evil-snipe-enable-incremental-highlight)))
  (setq polaris--evil-snipe-repeat-fn
        (evilem-create 'evil-snipe-repeat
                       :bind ((evil-snipe-scope 'whole-buffer)
                              (evil-snipe-enable-highlight)
                              (evil-snipe-enable-incremental-highlight)))))


(use-package evil-escape
  :config
  (setq-default evil-escape-key-sequence "jk"
                evil-escape-delay 0.25)
  (evil-escape-mode +1)

  ;; evil-escape causes noticable lag in linewise motions in visual mode, so disable it in
  ;; visual mode
  (defun polaris|evil-escape-disable() (evil-escape-mode -1))
  (add-hook 'evil-visual-state-entry-hook 'polaris|evil-escape-disable)
  (add-hook 'evil-visual-state-exit-hook 'evil-escape-mode)

  (push 'neotree-mode evil-escape-excluded-major-modes))

(use-package evil-nerd-commenter)

(use-package evil-quickscope
  :disabled t
  :init
  (setq evil-quickscope-word-separator " -./")
  (defun nadvice/evil-quickscope-update-overlays-bidirectional ()
    "Update overlays in both directions from point."
    (evil-quickscope-remove-overlays)
    (when (memq evil-state '(normal motion))
      (evil-quickscope-apply-overlays-forward)
      (evil-quickscope-apply-overlays-backward)))

  (advice-add 'evil-quickscope-update-overlays-bidirectional
              :override
              #'nadvice/evil-quickscope-update-overlays-bidirectional)
  :config
  (global-evil-quickscope-always-mode 1))

(use-package evil-snipe
  :diminish evil-snipe-local-mode
  :init
  (setq-default
   evil-snipe-smart-case t
   evil-snipe-repeat-keys nil ; using space to repeat
   evil-snipe-show-prompt nil
   evil-snipe-scope 'whole-buffer
   evil-snipe-repeat-scope 'visible
   evil-snipe-override-evil-repeat-keys nil ; causes problems with remapped ;
   evil-snipe-aliases '((?\[ "[[{(]")
                        (?\] "[]})]")
                        (?\; "[;:]")))
  (define-key evil-motion-state-map "f" #'evil-snipe-f)
  (define-key evil-motion-state-map "F" #'evil-snipe-F)
  (define-key evil-motion-state-map "t" #'evil-snipe-t)
  (define-key evil-motion-state-map "T" #'evil-snipe-T)
  (define-key evil-motion-state-map "s" #'evil-snipe-s)
  (define-key evil-motion-state-map "S" #'evil-snipe-S)

  (define-key evil-normal-state-map "f" #'evil-snipe-f)
  (define-key evil-normal-state-map "F" #'evil-snipe-F)
  (define-key evil-normal-state-map "t" #'evil-snipe-t)
  (define-key evil-normal-state-map "T" #'evil-snipe-T)
  (define-key evil-normal-state-map "s" #'evil-snipe-s)
  (define-key evil-normal-state-map "S" #'evil-snipe-S)

  (define-key evil-visual-state-map "f" #'evil-snipe-f)
  (define-key evil-visual-state-map "F" #'evil-snipe-F)
  (define-key evil-visual-state-map "t" #'evil-snipe-t)
  (define-key evil-visual-state-map "T" #'evil-snipe-T)
  (define-key evil-visual-state-map "s" #'evil-snipe-s)
  (define-key evil-visual-state-map "S" #'evil-snipe-S)
  :config
  (evil-snipe-mode 1)
  (evil-snipe-override-mode 1)
  (define-key evil-snipe-parent-transient-map (kbd "C-;") 'polaris/evil-snipe-easymotion))

(use-package evil-surround
  :commands (global-evil-surround-mode
             evil-surround-edit
             evil-Surround-edit
             evil-surround-region)
  :init (global-evil-surround-mode +1)
  :config

  ;; Escaped surround characters
  (setq-default evil-surround-pairs-alist
                (cons '(?\\ . polaris/evil-surround-escaped)
                      evil-surround-pairs-alist))

  (add-hook! emacs-lisp-mode
    (push '(?\` . ("`" . "'")) evil-surround-pairs-alist))
  (add-hook! python-mode
    (push '((?d . ("\"\"\"" . "\"\"\""))) evil-surround-pairs-alist)))

(use-package evil-embrace
  :after evil-surround
  :config
  (evil-embrace-enable-evil-surround-integration)
  (embrace-add-pair-regexp ?\\ "\\[[{(]" "\\[]})]" 'polaris/evil-surround-escaped))

(use-package evil-visualstar
  :config
  (global-evil-visualstar-mode 1))

(use-package link-hint
  :commands (link-hint-open-link
             link-hint-copy-link))

;; Text-Objects

;;; === Evil motion section ===

(eval-and-compile
  (defun my/smart-evil-visual-line ()
    (unless (fboundp #'evil-visual-line-hydra/body)
      (require 'hydra)
      (defhydra evil-visual-line-hydra
        (:pre (setq hydra-is-helpful nil)
              :post (setq hydra-is-helpful t))
        ("j" evil-next-visual-line)
        ("k" evil-previous-visual-line)))
    (evil-visual-line-hydra/body))

  (defun my/smart-evil-scroll-page (&rest _args)
    (require 'hydra)
    (unless (fboundp 'my/smart-evil-scroll-page-hydra/body)
      (defhydra my/smart-evil-scroll-page-hydra
        (:pre (setq hydra-is-helpful nil)
              :post (setq hydra-is-helpful t))
        "Scroll by page"
        ("f" evil-scroll-page-down)
        ("b" evil-scroll-page-up)))
    (my/smart-evil-scroll-page-hydra/body))

  (evil-define-motion evil-smart-next-visual-line (count)
    (evil-next-visual-line (or count 1))
    (my/smart-evil-visual-line))

  (evil-define-motion evil-smart-previous-visual-line (count)
    (evil-previous-visual-line (or count 1))
    (my/smart-evil-visual-line))

  (evil-define-motion evil-smart-scroll-page-down (count)
    (evil-scroll-page-down (or count 1))
    (my/smart-evil-scroll-page))

  (evil-define-motion evil-smart-scroll-page-up (count)
    (evil-scroll-page-up (or count 1))
    (my/smart-evil-scroll-page)))

(define-key evil-motion-state-map "gj" #'evil-smart-next-visual-line)
(define-key evil-motion-state-map "gk" #'evil-smart-previous-visual-line)

(define-key evil-motion-state-map (kbd "C-f") #'evil-smart-scroll-page-down)
(define-key evil-motion-state-map (kbd "C-b") #'evil-smart-scroll-page-up)

;;; === Evil text object section ===
;; evil block indentation textobject for Python
(defun my/evil-indent--current-indentation ()
  "Return the indentation of the current line. Moves point."
  (buffer-substring-no-properties (point-at-bol)
                                  (progn (back-to-indentation)
                                         (point))))

(defun my/evil-indent--block-range (&optional count point)
  "Return the point at the begin and end of the text block "
  ;; there are faster ways to mark the entire file
  ;; so assume the user wants a block and skip to there
  (while (and (string-empty-p
               (my/evil-indent--current-indentation))
              (not (eobp)))
    (forward-line))
  (cl-flet* ((empty-line-p ()
                           (string-match-p "^[[:space:]]*$"
                                           (buffer-substring-no-properties
                                            (line-beginning-position)
                                            (line-end-position))))
             (line-indent-ok (indent)
                             (or (<= (length indent)
                                     (length
                                      (my/evil-indent--current-indentation)))
                                 (empty-line-p))))
    (let ((indent (my/evil-indent--current-indentation)) start begin end)
      ;; now skip ahead to the Nth block with this indentation
      (dotimes (_ (or count 0))
        (while (and (line-indent-ok) (not (eobp))) (forward-line))
        (while (or (line-indent-ok indent) (eobp)) (forward-line)))
      (save-excursion
        (setq start (goto-char (or point (point))))
        (while (and (line-indent-ok indent) (not (bobp)))
          (setq begin (point))
          (forward-line -1))
        (goto-char start)
        (while (and (line-indent-ok indent) (not (eobp)))
          (setq end (point))
          (forward-line))
        (goto-char end)
        (while (empty-line-p)
          (forward-line -1)
          (setq end (point)))
        (list begin end)))))

(evil-define-text-object evil-indent-i-block (&optional count _beg _end _type)
  "Text object describing the block with the same indentation as the current line."
  (cl-destructuring-bind (begin end)
      (my/evil-indent--block-range count)
    (evil-range begin end 'line)))

(evil-define-text-object evil-indent-a-block (&optional count _beg _end _type)
  "Text object describing the block with the same indentation as the current line and the line above."
  :type line
  (cl-destructuring-bind (begin end)
      (my/evil-indent--block-range count)
    (evil-range (save-excursion
                  (goto-char begin)
                  (forward-line -1)
                  (point-at-bol))
                end
                'line)))

(evil-define-text-object evil-indent-a-block-end (count &optional _beg _end _type)
  "Text object describing the block with the same indentation as the current line and the lines above and below."
  :type line
  (cl-destructuring-bind (begin end)
      (my/evil-indent--block-range count)
    (evil-range (save-excursion
                  (goto-char begin)
                  (forward-line -1)
                  (point-at-bol))
                (save-excursion
                  (goto-char end)
                  (forward-line 1)
                  (point-at-eol))
                'line)))

(define-key evil-inner-text-objects-map "i" #'evil-indent-i-block)
(define-key evil-outer-text-objects-map "i" #'evil-indent-a-block)
(define-key evil-inner-text-objects-map "I" #'evil-indent-a-block-end)
(define-key evil-outer-text-objects-map "I" #'evil-indent-a-block-end)

(defun evil-avy-jump-line-and-revert ()
  (interactive)
  (let ((top (save-excursion
               (evil-window-top)
               (line-number-at-pos))))
    (prog1
        (progn
          (deactivate-mark)
          (avy-goto-line)
          (point))
      (scroll-up-line (- top (save-excursion
                               (evil-window-top)
                               (line-number-at-pos)))))))

(evil-define-text-object evil-i-line-range (count &optional _beg _end _type)
  "Text object describing the block with the same indentation as the current line."
  :type line
  (save-excursion
    (let ((beg (evil-avy-jump-line-and-revert))
          (end (evil-avy-jump-line-and-revert)))
      (if (> beg end)
          (evil-range beg end #'line)
        (evil-range end beg #'line)))))

(define-key evil-inner-text-objects-map "r" #'evil-i-line-range)

(define-key evil-inner-text-objects-map "." #'evil-inner-sentence)
(define-key evil-outer-text-objects-map "." #'evil-a-sentence)

(evil-define-text-object evil-i-entire-buffer (count &optional _beg _end _type)
  "Text object describing the entire buffer excluding empty lines at the end"
  :type line
  (evil-range (point-min) (save-excursion
                            (goto-char (point-max))
                            (skip-chars-backward " \n\t")
                            (point)) 'line))

(evil-define-text-object evil-an-entire-buffer (count &optional _beg _end _type)
  "Text object describing the entire buffer"
  :type line
  (evil-range (point-min) (point-max) 'line))

(define-key evil-inner-text-objects-map "e" #'evil-i-entire-buffer)
(define-key evil-outer-text-objects-map "e" #'evil-an-entire-buffer)

(evil-define-text-object evil-inner-last-paste (count &optional _beg _end _type)
  :type char
  (evil-range (evil-get-marker ?\[) (evil-get-marker ?\]) 'char))

(define-key evil-inner-text-objects-map "P" #'evil-inner-last-paste)

;; === evil operators ;;
(evil-define-operator evil-macro-on-all-lines (beg end &optional _arg)
  (evil-with-state 'normal
    (goto-char end)
    (evil-visual-state)
    (goto-char beg)
    (evil-ex-normal (region-beginning) (region-end)
                    (concat "@"
                            (single-key-description
                             (read-char "What macro?"))))))

(define-key evil-operator-state-map "g@" #'evil-macro-on-all-lines)
(define-key evil-normal-state-map "g@" #'evil-macro-on-all-lines)

(evil-define-operator evil-eval-region (beg end _type)
  (save-excursion
    (evil-with-state 'normal
      (goto-char beg)
      (evil-visual-state)
      (goto-char end)
      (eval-region beg end))))

(define-key evil-operator-state-map "gV" #'evil-eval-region)
(define-key evil-normal-state-map "gV" #'evil-eval-region)

(evil-define-operator evil-align-regexp (beg end _type)
  (save-excursion
    (evil-with-state 'normal
      (goto-char beg)
      (evil-visual-state)
      (goto-char end)
      (call-interactively #'align-regexp))))

(define-key evil-operator-state-map "g|" #'evil-align-regexp)
(define-key evil-normal-state-map "g|" #'evil-align-regexp)

;; ----------- ;;
;; Ex-Commands ;;
;; ----------- ;;

(evil-ex-define-cmd "echo"          'polaris:echo)
(evil-ex-define-cmd "minor"         'helm-describe-modes) ; list minor modes
;; Quick mapping keys to commands, allows :nmap \m !make
(evil-ex-define-cmd "nmap"          'polaris:nmap)
(evil-ex-define-cmd "imap"          'polaris:imap)
(evil-ex-define-cmd "vmap"          'polaris:vmap)
(evil-ex-define-cmd "mmap"          'polaris:mmap)
(evil-ex-define-cmd "omap"          'polaris:omap)

;; Editing
(evil-ex-define-cmd "@"             'polaris/evil-macro-on-all-lines) ; run macro on each line
(evil-ex-define-cmd "al[ign]"       'polaris:align)                   ; align by regexp
(evil-ex-define-cmd "na[rrow]"      'polaris:narrow)                  ; narrow buffer to selection
(evil-ex-define-cmd "ref[actor]"    'emr-show-refactor-menu)          ; open emr menu
(evil-ex-define-cmd "retab"         'polaris:whitespace-retab)
(evil-ex-define-cmd "settr[im]"     'polaris:toggle-delete-trailing-whitespace)
(evil-ex-define-cmd "snip[pets]"    'polaris:yas-snippets) ; visit a snippet
(evil-ex-define-cmd "tsnip[pets]"   'polaris:yas-file-templates) ; visit a file template
(evil-ex-define-cmd "wal[ign]"      'polaris:whitespace-align) ; align spaces
(evil-ex-define-cmd "rec[ent]"      'polaris:helm-recentf)    ; show recent files in helm
(evil-ex-define-cmd "reo[rient]"    'polaris/window-reorient) ; scroll all windows to left
(evil-ex-define-cmd "ie[dit]"       'evil-multiedit-ex-match)

;; External resources
(evil-ex-define-cmd "dash"          'helm-dash-at-point) ; look up in browser
(evil-ex-define-cmd "http"          'httpd-start)        ; start http server
(evil-ex-define-cmd "re[gex]"       'polaris:regex)      ; open re-builder
(evil-ex-define-cmd "repl"          'polaris:repl)       ; invoke or send to repl
(evil-ex-define-cmd "t[mux]"        'polaris:tmux)       ; send to tmux
(evil-ex-define-cmd "tcd"           'polaris:tmux-cd)    ; cd to default-directory in tmux
(evil-ex-define-cmd "x"             'polaris:send-to-scratch-or-org)

;; GIT
(evil-ex-define-cmd "gbr[owse]"     'polaris:git-remote-browse) ; show file in github/gitlab

;; Dealing with buffers
(evil-ex-define-cmd "k[ill]"        'polaris/kill-real-buffer) ; Kill current buffer
(evil-ex-define-cmd "k[ill]all"     'polaris:kill-all-buffers) ; Kill all buffers (bang = in project)
(evil-ex-define-cmd "k[ill]buried"  'polaris:kill-buried-buffers) ; Kill all buried buffers (bang = in project)
(evil-ex-define-cmd "k[ill]o"       'polaris:kill-other-buffers)  ; kill all other buffers
(evil-ex-define-cmd "k[ill]unreal"  'polaris/kill-unreal-buffers) ; kill unreal buffers
(evil-ex-define-cmd "l[ast]"        'polaris/popup-last-buffer)   ; pop up last popup
(evil-ex-define-cmd "m[sg]"         'polaris/popup-messages)      ; open *messages* in popup

;; Project navigation
(evil-ex-define-cmd "a"             'helm-projectile-find-other-file) ; open alternate file
(evil-ex-define-cmd "ag"            'polaris:helm-ag-search)          ; project text search
(evil-ex-define-cmd "ag[cw]d"       'polaris:helm-ag-search-cwd)      ; current directory search
(evil-ex-define-cmd "cd"            'polaris:cd)
(evil-ex-define-cmd "se[arch]"        'polaris:helm-swoop) ; in-file search
;; Project tools
(evil-ex-define-cmd "ma[ke]"        'polaris:build)
(evil-ex-define-cmd "build"        'polaris:build)
;; File operations
(evil-ex-define-cmd "mv"            'polaris:file-move)
(evil-ex-define-cmd "rm"            'polaris:file-delete) ; rm[!]

;; Presentation/demo
(evil-ex-define-cmd "big"           'big-mode)
(evil-ex-define-cmd "full[scr]"     'polaris:toggle-fullscreen)
(evil-ex-define-cmd "w[riting]"     'write-mode-toggle)

;; Org-mode
(evil-ex-define-cmd "link"          'polaris:org-link)
(evil-ex-define-cmd "cols"          'polaris:set-columns)
(evil-ex-define-cmd "org"           'polaris/helm-org)
(evil-ex-define-cmd "cap[ture]"     'helm-org-capture-templates)
(evil-ex-define-cmd "cont[act]"     'polaris:org-crm-contact)
(evil-ex-define-cmd "proj[ect]"     'polaris:org-crm-project)
(evil-ex-define-cmd "invo[ice]"     'polaris:org-crm-invoice)
(evil-ex-define-cmd "att[ach]"      'polaris:org-attach)      ; attach file to org file
(evil-ex-define-cmd "org"           'polaris:org-helm-search) ; search org notes

;; Sessions/tabs
(evil-ex-define-cmd "sl[oad]"       'polaris:load-session)
(evil-ex-define-cmd "ss[ave]"       'polaris:save-session)
(evil-ex-define-cmd "tabs"          'polaris/tab-display)
(evil-ex-define-cmd "tabn[ew]"      'polaris:tab-create)
(evil-ex-define-cmd "tabr[ename]"   'polaris:tab-rename)
(evil-ex-define-cmd "tabc[lose]"    'polaris:kill-tab)
(evil-ex-define-cmd "tabc[lose]o"   'polaris:kill-other-tabs)
(evil-ex-define-cmd "tabn[ext]"     'polaris:switch-to-tab-right)
(evil-ex-define-cmd "tabp[rev]"     'polaris:switch-to-tab-left)
(evil-ex-define-cmd "tabl[ast]"     'polaris:switch-to-tab-last)

;; Plugins
(after! flycheck
  (evil-ex-define-cmd "er[rors]"    (λ! (flycheck-buffer) (flycheck-list-errors))))

;; Debuggers
(evil-ex-define-cmd "debug"    'polaris:debug)

(provide 'core-evil)
;;; core-evil.el ends here

;; -*- lexical-binding: t -*-
;;; core-editor.el

;;;; Editor behavior ;;;;;;;;;;;;;;;;
(setq-default
 default-input-method 'TeX
 ;; spaces instead of tabs
 indent-tabs-mode nil
 tab-always-indent t
 tab-width 4

 require-final-newline t
 delete-trailing-lines nil
 fill-column 80
 line-spacing 0
 word-wrap t
 truncate-lines t
 truncate-partial-width-windows 50

 visual-fill-column-center-text nil
 confirm-nonexistent-file-or-buffer nil

 ;; Sane scroll settings
 scroll-margin 0
 scroll-conservatively 1001
 scroll-preserve-screen-position t
 hscroll-step 1
 hscroll-margin 1

 shift-select-mode t
 tabify-regexp "^\t* [ \t]+"
 whitespace-line-column fill-column
 whitespace-style '(face tabs tab-mark
                         trailing indentation lines-tail)
 whitespace-display-mappings
 '((tab-mark ?\t [?› ?\t])
   (newline-mark 10 [36 10])))

;; auto-chmod +x
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; Save point across sessions
(use-package saveplace
  :demand t
  :config
  (setq-default save-place-file (concat polaris-temp-dir "saveplace")
                save-place t)
  (when (>= emacs-major-version 25)
    (save-place-mode +1)))

;; Save history across sessions
(use-package savehist
  :config
  (setq savehist-file (concat polaris-temp-dir "savehist")
        savehist-save-minibuffer-history t
        savehist-autosave-interval 60
        history-length 1000
        savehist-additional-variables
        '(kill-ring search-ring regexp-search-ring))
  (savehist-mode 1)

  ;; text properties severely bloat the history so delete them (courtesy of PythonNut)
  (defun unpropertize-savehist ()
    (mapc (lambda (list)
            (with-demoted-errors
                (when (boundp list)
                  (set list (mapcar #'substring-no-properties (eval list))))))
          '(kill-ring minibuffer-history helm-grep-history helm-ff-history file-name-history
                      read-expression-history extended-command-history evil-ex-history)))
  (add-hook 'kill-emacs-hook    #'unpropertize-savehist)
  (add-hook 'savehist-save-hook #'unpropertize-savehist))

;; Keep track of recently opened files
(use-package recentf
  :config
  (setq recentf-save-file (concat polaris-temp-dir "recentf")
        recentf-exclude '("/tmp/" "/ssh:" "\\.?ido\\.last$" "\\.revive$" "/TAGS$"
                          "emacs\\.d/private/cache/.+" "emacs\\.d/workgroups/.+$" "wg-default"
                          "/company-statistics-cache.el$")
        recentf-max-menu-items 500
        recentf-max-saved-items 1000
        recentf-auto-cleanup 300)
  (add-to-list 'recentf-exclude "COMMIT_EDITMSG\\'")
  (recentf-mode 1)
  (run-with-idle-timer 600 t #'recentf-save-list))

;; gc
(defun my-minibuffer-setup-hook () (setq gc-cons-threshold most-positive-fixnum))
(defun my-minibuffer-exit-hook () (setq gc-cons-threshold (* 64 1024 1024)))
(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)

;; pcomplete
(use-package pcomplete
  :config (setq pcomplete-ignore-case t))

;; comint
(use-package comint
  :config
  (defun my-toggle-comint-scroll-to-bottom-on-output ()
    (interactive)
    (if comint-scroll-to-bottom-on-output
        (setq comint-scroll-to-bottom-on-output nil)
      (setq comint-scroll-to-bottom-on-output t))))

;; compile
(setq compilation-always-kill t)
(setq compilation-ask-about-save nil)
(add-hook 'compilation-filter-hook
          (lambda ()
            (when (eq major-mode 'compilation-mode)
              (require 'ansi-color)
              (let ((inhibit-read-only t))
                (ansi-color-apply-on-region (point-min) (point-max))))))

;; Let editorconfig handle global whitespace settings
(use-package editorconfig
  :config
  (editorconfig-mode +1)
  ;; So whitespace in tabs indentation mode
  (add-hook! 'editorconfig-custom-hooks (if indent-tabs-mode (whitespace-mode +1)))
  (associate! editorconfig-conf-mode :match "/\\.?editorconfig$"))

;;
;; Automatic minor modes
;;

(defvar polaris-auto-minor-mode-alist '()
  "Alist of filename patterns vs corresponding minor mode functions, see
`auto-mode-alist'. All elements of this alist are checked, meaning you can
enable multiple minor modes for the same regexp.")

(defun polaris|enable-minor-mode-maybe ()
  "Check file name against `polaris-auto-minor-mode-alist'."
  (when buffer-file-name
    (let ((name buffer-file-name)
          (remote-id (file-remote-p buffer-file-name))
          (alist polaris-auto-minor-mode-alist))
      ;; Remove backup-suffixes from file name.
      (setq name (file-name-sans-versions name))
      ;; Remove remote file name identification.
      (when (and (stringp remote-id)
                 (string-match-p (regexp-quote remote-id) name))
        (setq name (substring name (match-end 0))))
      (while (and alist (caar alist) (cdar alist))
        (if (string-match (caar alist) name)
            (funcall (cdar alist) 1))
        (setq alist (cdr alist))))))

(add-hook 'find-file-hook #'polaris|enable-minor-mode-maybe)

;;
;; Modes, hooks 'n hacks
;;

(associate! makefile-gmake-mode :match "/Makefile$")
(add-hook! eldoc-mode   (diminish 'eldoc-mode  " ⓓ"))
(add-hook! special-mode (setq truncate-lines nil))

(defadvice delete-trailing-whitespace
    (around delete-trailing-whitespace-ignore-line activate)
  "Don't delete trailing whitespace on current line, if in insert mode."
  (let ((spaces (1- (current-column)))
        (linestr (buffer-substring-no-properties
                  (line-beginning-position)
                  (line-end-position))))
    ad-do-it
    (when (and (evil-insert-state-p)
               (string-match-p "^[\s\t]*$" linestr))
      (insert linestr))))

;; If file is oversized...
(add-hook! find-file
  (when (> (buffer-size) 1048576)
    (setq buffer-read-only t)
    (buffer-disable-undo)
    (fundamental-mode)
    (visual-line-mode)))

;; Smarter electric-indent (see `def-electric!')
(electric-indent-mode -1) ; on by default
(defvar polaris-electric-indent-words '())
(make-variable-buffer-local 'polaris-electric-indent-words)
(setq electric-indent-chars '(?\n ?\^?))
(push (lambda (c)
        (when (eolp)
          (save-excursion
            (backward-word)
            (looking-at-p (concat "\\<" (regexp-opt polaris-electric-indent-words))))))
      electric-indent-functions)

;;
;; (global-whitespace-mode -1)  ; Show whitespace
(global-visual-line-mode 1) ; wrap buffers
(global-auto-revert-mode 1)    ; revert buffers for changed files
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)
;; Enable syntax highlighting for older emacs
(unless (bound-and-true-p global-font-lock-mode)
  (global-font-lock-mode t))
(xterm-mouse-mode t)
(which-function-mode t)
(transient-mark-mode t)
(delete-selection-mode t)
(random t) ;; seed


;; window config undo/redo
(setq winner-dont-bind-my-keys t)
(winner-mode 1)
(add-hook! after-init
  (setq winner-boring-buffers polaris-ignore-buffers))

;; always ensure UTF-8
(defun cleanup-buffer-safe ()
  (interactive)
  (set-buffer-file-coding-system 'utf-8))

(defun cleanup-buffer-unsafe ()
  (interactive)
  (untabify (point-min) (point-max))
  (delete-trailing-whitespace)
  (set-buffer-file-coding-system 'utf-8))

(add-hook 'before-save-hook #'cleanup-buffer-safe)

;; add intelligent buffer renaming
(defun rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-polarisfied-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

(global-set-key (kbd "C-x C-r") #'rename-current-buffer-file)

;;
;; Plugins
;;

(use-package adaptive-wrap
  :config
  (setq-default adaptive-wrap-extra-indent 2)
  (add-hook 'visual-line-mode-hook #'adaptive-wrap-prefix-mode)
  (add-hook 'visual-line-mode-hook
            (lambda ()
              (diminish 'visual-line-mode))))

(use-package avy
  :init
  (setq avy-all-windows nil
        avy-background t
        avy-timeout-seconds 0.3
        avy-keys (eval-when-compile (string-to-list "jfkdlsaurieowncpqmxzb"))
        avy-style 'de-bruijn
        avy-styles-alist '((avy-goto-char-2 . post)
                           (ivy-avy . pre)
                           (avy-goto-line . pre))))

(use-package ace-window
  :commands ace-window
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
        aw-ignore-current t
        aw-swap-invert t)

  ;; bind command to switch to minibuffer
  (defun switch-window-dwim (arg)
    "switch to minibuffer window (if active)"
    (interactive "P")
    (let ((num-windows (length (mapcar #'window-buffer (window-list)))))
      (cond ((= num-windows 1)
             (call-interactively #'split-window-right))
            ((minibufferp)
             (other-window (or arg 1)))
            ((active-minibuffer-window)
             (select-window (active-minibuffer-window)))
            ((or (> (length (visible-frame-list)) 1)
                 (> num-windows 3)
                 (numberp arg))
             (ace-window arg))
            (t
             (other-window (or arg 1))))))

  (global-set-key (kbd "C-'") #'switch-window-dwim)
  (global-set-key (kbd "C-c '") #'switch-window-dwim))

(use-package ace-link
  :init
  (progn
    (with-eval-after-load 'info
      (define-key Info-mode-map "o" 'ace-link-info))
    (with-eval-after-load 'help-mode
      (define-key help-mode-map "o" 'ace-link-help))
    (with-eval-after-load 'eww
      (define-key eww-link-keymap "o" 'ace-link-eww)
      (define-key eww-mode-map "o" 'ace-link-eww))))

(use-package ivy
  :diminish ivy-mode
  :config
  (setq ivy-display-style 'fancy
        ivy-height 10
        ivy-re-builders-alist '((t . ivy--regex-fuzzy))
        ivy-flx-limit 2000
        ivy-initial-inputs-alist nil ;; don't insert ^
        ivy-extra-directories nil
        ivy-count-format " %d/%d "
        ivy-use-virtual-buffers t
        ivy-virtual-abbreviate 'full)

  (require 'ivy-hydra)

  (defun nadvice/completing-read-ivy (&rest _args)
    (ivy-mode +1)
    (advice-remove #'completing-read #'nadvice/completing-read-ivy))

  (advice-add 'completing-read :before #'nadvice/completing-read-ivy)

  (ivy-mode))

(use-package swiper
  :config
  (after! swiper
    (defadvice swiper (before dotemacs activate)
      (setq gc-cons-threshold most-positive-fixnum))
    (defadvice swiper-all (before dotemacs activate)
      (setq gc-cons-threshold most-positive-fixnum))))

(use-package counsel
  :init
  (setq counsel-find-file-at-point t
        counsel-find-file-ignore-regexp
        (concat
         ;; file names beginning with # or .
         "\\(?:\\`[#.]\\)"
         ;; file names ending with # or ~
         "\\|\\(?:\\`.+?[#~]\\'\\)"))

  ;; Redefine `counsel-ag-base-command' with my required options, especially
  ;; the `--follow' option to allow search through symbolic links.
  (setq counsel-ag-base-command (concat "ag "
                                        "--noheading "
                                        "--nogroup "
                                        "--nocolor "
                                        "--skip-vcs-ignores "
                                        "--smart-case "
                                        "--follow " ; follow symlinks
                                        "%S"))

  (ivy-set-actions
   'counsel-find-file
   `(("x"
      (lambda (x) (delete-file (expand-file-name x ivy--directory)))
      ,(propertize "delete" 'face 'font-lock-warning-face)))))

(use-package clean-buffers
  :config
  (clean-buffers-turn-on-auto-clean-buffers))

(use-package dumb-jump
  :commands (dumb-jump-go dumb-jump-quick-look dumb-jump-back)
  :config
  (setq dumb-jump-default-project polaris-emacs-dir)
  (dumb-jump-mode +1))

(use-package emr
  :commands (emr-initialize emr-show-refactor-menu emr-declare-command)
  :config (define-key popup-menu-keymap [escape] 'keyboard-quit))

(use-package expand-region
  :commands (er/expand-region er/contract-region er/mark-symbol er/mark-word))

(use-package fancy-narrow
  :commands (fancy-narrow-to-region fancy-widen))

(use-package focus-autosave-mode        ; Save buffers when focus is lost
  :init (focus-autosave-mode)
  :diminish focus-autosave-mode)

(use-package goto-last-change :commands goto-last-change)

(use-package help-fns+ ; Improved help commands
  :commands (describe-buffer describe-command describe-file
                             describe-keymap describe-option describe-option-of-type))

(use-package hideshow
  :commands (hs-minor-mode hs-toggle-hiding hs-already-hidden-p)
  :diminish hs-minor-mode
  :config (setq hs-isearch-open t)
  :init
  (add-hook! prog-mode 'hs-minor-mode)
  (after! evil
    (defun polaris-load-hs-minor-mode ()
      (hs-minor-mode 1)
      (advice-remove 'evil-toggle-fold 'polaris-load-hs-minor-mode))
    (advice-add 'evil-toggle-fold :before 'polaris-load-hs-minor-mode))

  ;; Prettify code folding in emacs ;;;;;;
  (define-fringe-bitmap 'hs-marker [16 48 112 240 112 48 16] nil nil 'center)
  (defface hs-face '((t (:background "#ff8")))
    "Face to hightlight the ... area of hidden regions"
    :group 'hideshow)
  (defface hs-fringe-face '((t (:foreground "#888")))
    "Face used to highlight the fringe on folded regions"
    :group 'hideshow)

  (setq hs-set-up-overlay
        (lambda (ov)
          (when (eq 'code (overlay-get ov 'hs))
            (let* ((marker-string "*fringe-dummy*")
                   (marker-length (length marker-string))
                   (display-string (format " ... " (count-lines (overlay-start ov)
                                                                (overlay-end ov)))))
              (put-text-property 0 marker-length 'display
                                 (list 'right-fringe 'hs-marker 'hs-fringe-face) marker-string)
              (put-text-property 0 (length display-string) 'face 'hs-face display-string)
              (overlay-put ov 'before-string marker-string)
              (overlay-put ov 'display display-string))))))

(use-package imenu-list
  :commands imenu-list-minor-mode
  :config
  (setq imenu-list-mode-line-format nil
        imenu-list-position 'right
        imenu-list-size 32)

  (evilified-state-evilify-map imenu-list-major-mode-map
    :mode imenu-list-major-mode
    :bindings
    [escape] #'polaris/imenu-list-quit
    "RET" #'imenu-list-goto-entry
    "SPC" #'imenu-list-display-entry
    "d" #'imenu-list-display-entry
    "q" #'imenu-list-minor-mode))

(use-package miniedit
  :commands minibuffer-edit
  :init
  (map! :map (minibuffer-local-map
              minibuffer-local-ns-map
              minibuffer-local-completion-map
              minibuffer-local-must-match-map)
        "\M-\C-e" 'miniedit))

(use-package re-builder
  :commands (re-builder reb-mode-buffer-p)
  :init
  (add-hook 'reb-mode-hook 'polaris|reb-cleanup)
  (evil-set-initial-state 'reb-mode 'insert)
  :config
  (setq reb-re-syntax 'string))

(use-package rotate-text
  :commands (rotate-text rotate-text-backward)
  :config (push '("true" "false") rotate-text-words))

(use-package smart-forward :commands (smart-up smart-down smart-left smart-right))

(use-package smartparens
  :diminish (smartparens-mode . " ⓟ")
  :config
  (setq sp-autowrap-region nil          ; let evil-surround handle this
        sp-highlight-pair-overlay nil
        sp-cancel-autoskip-on-backward-movement nil
        sp-show-pair-from-inside t
        sp-show-pair-delay 0)

  (smartparens-global-mode 1)
  (use-package smartparens-config)

  (dolist (hook '(inferior-emacs-lisp-mode-hook
                  scheme-mode-hook
                  clojure-mode-hook
                  lisp-mode-hook
                  emacs-lisp-mode-hook))
    (add-hook hook #'smartparens-strict-mode))

  ;; Smartparens interferes with Replace mode
  (add-hook 'evil-replace-state-entry-hook 'turn-off-smartparens-mode)
  (add-hook 'evil-replace-state-exit-hook  'turn-on-smartparens-mode)

  ;; Auto-close more conservatively
  (sp-pair "'" nil :unless '(sp-point-after-word-p))
  (sp-pair "\"" nil :unless '(sp-point-before-word-p sp-point-before-same-p))
  (sp-pair "{" nil :post-handlers '(("||\n[i]" "RET") ("| " " "))
           :unless '(sp-point-before-word-p sp-point-before-same-p))
  (sp-pair "(" nil :post-handlers '(("||\n[i]" "RET") ("| " " "))
           :unless '(sp-point-before-word-p sp-point-before-same-p))

  (sp-pair "[" nil :post-handlers '(("| " " "))
           :unless '(sp-point-before-word-p sp-point-before-same-p))

  (sp-local-pair 'css-mode "/*" "*/" :post-handlers '(("[d-3]||\n[i]" "RET") ("| " "SPC")))
  (sp-local-pair '(sh-mode markdown-mode) "`" "`" :unless '(sp-point-before-word-p sp-point-before-same-p))
  (sp-with-modes '(xml-mode nxml-mode php-mode)
    (sp-local-pair "<!--" "-->"   :post-handlers '(("| " "SPC"))))

  ;; disable "'" pairing in text mode, as it's often an apostrophe
  (sp-local-pair 'text-mode "'" nil :actions nil))

(use-package paren
  :config
  (defun nadvice/show-paren-mode (old-fun &rest args)
    ;; http://emacs.stackexchange.com/questions/12532/buffer-local-idle-timer
    (cl-letf* ((old-run-with-idle-timer (symbol-function #'run-with-idle-timer))
               ((symbol-function #'run-with-idle-timer)
                (lambda (&rest args)
                  (cl-destructuring-bind (_secs _repeat function &rest rest)
                      args
                    (let* ( ;; Chicken and egg problem.
                           (fns (make-symbol "local-idle-timer"))
                           (timer (apply old-run-with-idle-timer args))
                           (fn `(lambda (&rest args)
                                  (if (active-minibuffer-window)
                                      (with-current-buffer ,(current-buffer)
                                        (apply (function ,function) args))
                                    (cancel-timer ,timer)))))
                      (fset fns fn)
                      timer)))))
      (apply old-fun args)))

  (advice-add 'show-paren-mode :around #'nadvice/show-paren-mode)
  (add-hook 'minibuffer-setup-hook (lambda ()
                                     (show-smartparens-mode -1)
                                     (show-paren-mode +1))))

(use-package smex
  :commands (smex smex-major-mode-commands smex-initialize smex-update)
  :init (setq smex-save-file (concat polaris-temp-dir "smex-items"))
  :config (smex-initialize)
  ;; Hook up smex to auto-update, rather than update on every run
  (defun smex-update-after-load (unused)
    (when (boundp 'smex-cache) (smex-update)))
  (add-hook 'after-load-functions 'smex-update-after-load))

(use-package which-key                  ; Show help popups for prefix keys
  :init (which-key-mode)
  :config
  ;; (which-key-setup-minibuffer)
  (setq which-key-idle-delay 0.5
        which-key-special-keys nil
        which-key-use-C-h-for-paging t
        which-key-prevent-C-h-from-cycling t
        which-key-echo-keystrokes 0.02
        which-key-max-description-length 32
        which-key-sort-order 'which-key-key-order-alpha
        which-key-allow-evil-operators t
        which-key-key-replacement-alist
        '(("<\\([[:alnum:]-]+\\)>" . "\\1")
          ("left"                . "◀")
          ("right"               . "▶")
          ("up"                  . "▲")
          ("down"                . "▼")
          ("DEL"                   . "⌫")
          ("deletechar"            . "⌦")
          ("RET"                   . "⏎"))
        which-key-description-replacement-alist
        '(("Prefix Command" . "prefix")
          ;; Lambdas
          ("\\`\\?\\?\\'"   . "λ")
          ("/body\\'" . "") ; Remove display the "/body" portion of hydra fn names
          ;; Drop my personal prefix
          ("polaris/\\(.+\\)"  . "")
          ("polaris|\\(.+\\)"  . "")
          ("polaris-\\(.+\\)"  . "")))
  (which-key-declare-prefixes
    ;; Prefixes for global prefixes and minor modes
    "SPC @" "outline"
    "SPC !" "flycheck"
    "SPC 8" "typo"
    "SPC 8 -" "typo/dashes"
    "SPC 8 <" "typo/left-brackets"
    "SPC 8 >" "typo/right-brackets"
    ;; Prefixes for my personal bindings
    "SPC a" "applications"
    "SPC b" "buffers"
    "SPC c" "compile-and-comments"
    "SPC e" "errors"
    "SPC f" "files"
    "SPC f v" "variables"
    "SPC g" "git"
    "SPC g g" "github"
    "SPC h" "helm/help"
    "SPC i" "insert"
    "SPC i l" "licenses"
    "SPC j" "jump"
    "SPC l" "language/spelling"
    "SPC m" "major mode"
    "SPC o" "cursors"
    "SPC p" "projects"
    "SPC s" "search"
    "SPC t" "toggle"
    "SPC w" "windows/frames"
    "SPC x" "text")

  (which-key-declare-prefixes-for-mode 'markdown-mode
    "SPC TAB" "markdown/images"
    "SPC C-a" "markdown/links"
    "SPC C-c" "markdown/process"
    "SPC C-s" "markdown/style"
    "SPC C-t" "markdown/header"
    "SPC C-x" "markdown/structure"
    "SPC m" "markdown/personal")

  (which-key-declare-prefixes-for-mode 'emacs-lisp-mode
    "SPC m" "elisp/personal"
    "SPC m e" "eval")

  (which-key-declare-prefixes-for-mode 'js2-mode
    "SPC m" "js/personal"
    "SPC m r" "refactor")

  (which-key-declare-prefixes-for-mode 'scala-mode
    "SPC C-b" "ensime/build"
    "SPC C-d" "ensime/debug"
    "SPC C-r" "ensime/refactor"
    "SPC C-v" "ensime/misc"
    "SPC m" "scala/personal"
    "SPC m b" "scala/build")
  :diminish (which-key-mode . " Ⓚ"))

;; Attempt to Do The Right Thing when indenting code
(use-package dtrt-indent
  :config (add-hook 'find-file-hook #'dtrt-indent-mode))

;; Turn page breaks into lines
(use-package page-break-lines
  :diminish page-break-lines-mode
  :init (global-page-break-lines-mode))

(use-package ws-butler
  :commands (ws-butler-mode)
  :diminish ws-butler-mode
  :config
  ;; autoload ws-butler on file open
  (defun my/ws-butler-onetime-setup ()
    (ws-butler-global-mode +1)
    (remove-hook 'find-file-hook #'my/ws-butler-onetime-setup))

  (add-hook 'find-file-hook #'my/ws-butler-onetime-setup))

;; (use-package keychain-environment
;;   :init (keychain-refresh-environment))

;; (use-package super-save
;;   :init (super-save-mode))

(use-package subword
  :diminish subword-mode
  :config (global-subword-mode 1))

(use-package prog-mode
  :config
  (progn
    ;; Temporarily unprettify the symbol if the cursor is on the symbol or on
    ;; its right edge.
    (when (>= emacs-major-version 25)
      (setq prettify-symbols-unprettify-at-point 'right-edge))
    (global-prettify-symbols-mode)))

(use-package framemove
  :init
  (windmove-default-keybindings)
  (setq framemove-hook-into-windmove t))

(use-package vlf-setup
  :init
  (setq vlf-application 'dont-ask))

;; http://polarisparentheses.com/emacs-narrow-or-widen-dwim.html
(defun narrow-or-widen-dwim (p)
  "Widen if buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree, or defun,
whichever applies first. Narrowing to org-src-block actually
calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer is
already narrowed."
  (interactive "P")
  (declare (interactive-only))
  (cond ((and (buffer-narrowed-p) (not p)) (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning) (region-end)))
        ((derived-mode-p 'org-mode)
         ;; `org-edit-src-code' is not a real narrowing
         ;; command. Remove this first conditional if you
         ;; don't want it.
         (cond ((ignore-errors (org-edit-src-code))
                (delete-other-windows))
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        ((derived-mode-p 'latex-mode)
         (LaTeX-narrow-to-environment))
        (t (narrow-to-defun))))

(add-hook 'LaTeX-mode-hook
          (lambda () (define-key LaTeX-mode-map "\C-xn" nil)))

(eval-after-load 'org-src
  '(define-key org-src-mode-map
     "\C-x\C-s" #'org-edit-src-exit))

(use-package outline
  ;; It is NECESSARY that the `outline-minor-mode-prefix' variable is set to
  ;; "\M-#" BEFORE `outline' library is loaded. After loading the library,
  ;; changing this prefix key requires manipulating keymaps.
  :preface
  (setq outline-minor-mode-prefix "\M-#")
  :commands (outline-mode outline-minor-mode))

(use-package outshine
  :disabled t
  :config
  (progn
    (setq outshine-use-speed-commands t)
    (setq outshine-org-style-global-cycling-at-bob-p t)

    ;; http://emacs.stackexchange.com/a/2803/115
    (defun polaris/outline-toc ()
      "Create a table of contents for outshine headers.
For `emacs-lisp-mode':
 - The Contents header has to be “;; Contents:”
 - Level 1 headers will be of the form “;;; L1 Header”
 - Level 2 headers will be of the form “;;;; L2 Header”
 - ..
For other major modes:
 - The Contents header has to be “<comment-start> Contents:”
 - Level 1 headers will be of the form “<comment-start> * L1 Header”
 - Level 2 headers will be of the form “<comment-start> ** L2 Header”
 - ..
Don't add “Revision Control” heading to TOC."
      (interactive)
      (save-excursion
        (goto-char (point-min))
        (let ((outline-comment-start
               (concat "\\(\\s<"
                       (when comment-start
                         (concat
                          "\\|"
                          ;; trim white space from comment-start
                          (replace-regexp-in-string " " "" comment-start)))
                       "\\)"))
              (el-mode (derived-mode-p 'emacs-lisp-mode))
              parsed-outline-comment-start
              headings-list stars-list
              heading star)
          ;; (message "%s" outline-comment-start)
          (while (re-search-forward
                  (concat "^\\(?1:" ; beginning of line
                          outline-comment-start
                          (if el-mode
                              (concat "\\{2\\}\\)" ; 2 consecutive ; in `emacs-lisp-mode'
                                      ";\\(?2:;*\\)") ; followed by one or more ; chars
                            (concat "\\s-\\{1\\}\\)" ; SINGLE white space
                                    "\\*\\(?2:\\**\\)")) ; followed by one or more * chars
                          " " ; followed by a space
                          "\\(?3:.+\\)") ; followed by heading
                  nil :noerror)
            (setq parsed-outline-comment-start (match-string-no-properties 1))
            ;; Note that the below `star' var stores one less * than the actual;
            ;; that's intentional. Also note that for `emacs-lisp-mode' the 3rd
            ;; consecutive ; onwards is counted as a “star”.
            (setq star    (match-string-no-properties 2))
            (setq heading (match-string-no-properties 3))
            ;; (message "%s %s %s" parsed-outline-comment-start star heading)
            (when (not (string= heading "Revision Control"))
              (setq stars-list    (cons star stars-list))
              (setq headings-list (cons heading headings-list))))
          (setq stars-list    (nreverse stars-list))
          (setq headings-list (nreverse headings-list))

          (goto-char (point-min))
          (while (re-search-forward
                  (concat "^"
                          outline-comment-start
                          (when el-mode
                            "\\{2\\}") ; 2 consecutive ; in `emacs-lisp-mode'
                          " Contents:")
                  nil :noerror)
            (forward-line 1)
            ;; First delete old contents
            ;; Keep on going on to the next line till it reaches a blank line
            (while (progn
                     (when (looking-at (concat "^" outline-comment-start))
                       ;; Delete current line without saving to kill-ring
                       (let (p1 p2)
                         (save-excursion
                           (setq p1 (line-beginning-position))
                           (next-line 1)
                           (setq p2 (line-beginning-position))
                           (delete-region p1 p2))))
                     (not (looking-at "^\n"))))
            ;; Then print table of contents
            (let ((content-comment-prefix
                   (if el-mode
                       ";; " ; 2 consecutive ; in `emacs-lisp-mode'
                     parsed-outline-comment-start)))
              (insert (format "%s\n" content-comment-prefix))
              (let ((n 1))
                (dolist (h headings-list)
                  ;; (insert (format "// %2d. %s\n" n heading))
                  (insert (format "%s %s%s\n"
                                  content-comment-prefix
                                  (replace-regexp-in-string
                                   (if el-mode ";" "\\*") "  " (pop stars-list))
                                  h))
                  (setq n (1+ n)))))))))

    (defvar polaris/outline-minor-mode-hooks '(verilog-mode-hook
                                               emacs-lisp-mode-hook
                                               conf-space-mode-hook) ; for .tmux.conf
      "List of hooks of major modes in which `outline-minor-mode' should be enabled.")

    (defun polaris/turn-on-outline-minor-mode ()
      "Turn on `outline-minor-mode' only for specific modes."
      (interactive)
      (dolist (hook polaris/outline-minor-mode-hooks)
        (add-hook hook #'outline-minor-mode)))

    (defun polaris/turn-off-outline-minor-mode ()
      "Turn off `outline-minor-mode' only for specific modes."
      (interactive)
      (dolist (hook polaris/outline-minor-mode-hooks)
        (remove-hook hook #'outline-minor-mode)))

    (defun polaris/outshine-update-toc ()
      "Auto-generate/update TOC on file saves."
      (add-hook 'before-save-hook #'polaris/outline-toc nil :local))
    (advice-add 'outshine-hook-function :after #'polaris/outshine-update-toc)

    ;; Always enable Outshine in `outline-minor-mode'
    (add-hook 'outline-minor-mode-hook #'outshine-hook-function)

    (polaris/turn-on-outline-minor-mode)

    (with-eval-after-load 'outline
      (use-package foldout
        :config
        (progn
          (bind-keys
           :map outline-minor-mode-map
           ("C-c C-z" . foldout-zoom-subtree)
           ("C-c C-x" . foldout-exit-fold)))))

    ;; Mirror the default org-mode behavior in `outline-minor-mode-map'
    (bind-keys
     :map outline-minor-mode-map
     ("<backtab>" . outshine-cycle-buffer) ; global cycle using S-TAB
     ("M-p"       . outline-previous-visible-heading)
     ("M-n"       . outline-next-visible-heading)
     ("<M-up>"    . outline-move-subtree-up)
     ("<M-down>"  . outline-move-subtree-down)
     ("<M-left>"  . outline-promote)
     ("<M-right>" . outline-demote))))

(use-package helm-gtags
  :diminish helm-gtags-mode
  :init
  (progn
    (setq helm-gtags-ignore-case t
          helm-gtags-auto-update t
          helm-gtags-use-input-at-cursor t
          helm-gtags-pulse-at-cursor t))
  :config
  (progn
    ;; if anyone uses helm-gtags, they would want to use these key bindings
    (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
    (define-key helm-gtags-mode-map (kbd "C-x 4 .") 'helm-gtags-find-tag-other-window)
    (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
    (define-key helm-gtags-mode-map (kbd "M-*") 'helm-gtags-pop-stack)))

(use-package clean-aindent-mode
  :init (add-hook 'prog-mode-hook 'clean-aindent-mode))

(use-package electric-operator
  :config
  (dolist (hook '(python-mode-hook r-mode-hook js2-mode-hook php-mode-hook coffee-mode-hook))
    (add-hook hook #'electric-operator-mode)))

(provide 'core-editor)
;;; core-editor.el ends here

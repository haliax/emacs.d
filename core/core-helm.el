;; -*- lexical-binding: t -*-
;;; core-helm.el

(use-package helm
  :defer 1
  :commands (helm helm-other-buffer helm-mode)
  :diminish (helm-mode . " 🅗")
  :config
  (defvar helm-global-prompt ":: ")
  (setq-default helm-quick-update t
                helm-prevent-escaping-from-minibuffer t

                helm-ff-auto-update-initial-value t
                helm-find-files-doc-header nil
                ;; Use recentf to manage file name history
                helm-ff-file-name-history-use-recentf t
                ;; Find libraries from `require', etc.
                helm-ff-search-library-in-sexp t
                ;; Don't override evil-ex's completion
                helm-mode-handle-completion-in-region nil

                helm-candidate-number-limit 40

                helm-display-header-line nil
                helm-move-to-line-cycle-in-source t

                ;; Speedier without fuzzy matching
                helm-mode-fuzzy-match nil
                helm-completion-in-region-fuzzy-match t
                helm-case-fold-search 'smart
                helm-candidate-separator (make-string 20 ?─)
                helm-inherit-input-method nil
                )

  (after! yasnippet (push 'helm-alive-p yas-dont-activate))

  ;; (add-hook 'helm-update-hook (lambda () (setq cursor-in-non-selected-windows nil)))
  (defun dwim-helm-find-files-up-one-level-maybe ()
    (interactive)
    (if (looking-back "/" 1)
        (call-interactively 'helm-find-files-up-one-level)
      (delete-backward-char 1)))

  (defun dwim-helm-find-files-navigate-forward (orig-fun &rest args)
    "Adjust how helm-execute-persistent actions behaves, depending on context"
    (if (file-directory-p (helm-get-selection))
        (apply orig-fun args)
      (helm-maybe-exit-minibuffer)))
  (advice-add 'helm-execute-persistent-action :around #'dwim-helm-find-files-navigate-forward)

  ;; hide minibuffer in Helm session, since we use the header line already
  (defun helm-hide-minibuffer-maybe ()
    (when (with-helm-buffer helm-echo-input-in-header-line)
      (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
        (overlay-put ov 'window (selected-window))
        (overlay-put ov 'face
                     (let ((bg-color (face-background 'default nil)))
                       `(:background ,bg-color :foreground ,bg-color)))
        (setq-local cursor-type nil))))
  (add-hook 'helm-minibuffer-set-up-hook 'helm-hide-minibuffer-maybe)

  ;; fuzzy matching setting
  (with-eval-after-load 'helm-source
    (defun polaris/helm-make-source (f &rest args)
      (let ((source-type (cadr args))
            (props (cddr args)))
        (unless (eq source-type 'helm-source-async)
          (plist-put props :fuzzy-match t)))
      (apply f args))
    (advice-add 'helm-make-source :around #'polaris/helm-make-source)))

;; (use-package helm-mode
;;   :after helm
;;   :config (helm-mode 1))

(use-package helm-tags
  :commands (helm-tags-get-tag-file helm-etags-select))

(use-package helm-bookmark
  :commands (helm-bookmarks helm-filtered-bookmarks)
  :config (setq-default helm-bookmark-show-location t))

(use-package helm-projectile
  :commands (helm-projectile-find-other-file
             helm-projectile-switch-project
             helm-projectile-find-file
             helm-projectile-find-dir))

(use-package helm-files
  :commands (helm-browse-project helm-find helm-find-files helm-for-files helm-multi-files helm-recentf)
  :config
  (setq helm-ff-transformer-show-only-basename nil
        helm-ff-newfile-prompt-p nil
        helm-recentf-fuzzy-match t
        helm-ff-auto-update-initial-value t
        helm-ff--auto-update-state t

        helm-source-recentf (helm-make-source "Recentf"
                                'helm-recentf-source
                              :fuzzy-match helm-recentf-fuzzy-match)

        helm-boring-file-regexp-list (append helm-boring-file-regexp-list
                                             '("/\\.$"
                                               "/\\.\\.$"
                                               "\\.undo\\.xz$"
                                               "\\.elc$"
                                               "\\#$"
                                               "\\~$"
                                               "\\.zwc\\.old$"
                                               "\\.zwc$"))))

(use-package helm-ag
  :commands (helm-ag
             helm-ag-mode
             helm-do-ag
             helm-do-ag-this-file
             helm-do-ag-project-root
             helm-do-ag-buffers
             helm-ag-project-root
             helm-ag-pop-stack
             helm-ag-buffers))

(use-package helm-buffers
  :config (setq helm-buffers-fuzzy-matching t
                helm-boring-buffer-regexp-list '("\\ "
                                                 "\\*helm"
                                                 "\\*Compile"
                                                 "\\*Quail")))

(use-package helm-imenu
  :config
  (setq helm-imenu-fuzzy-match t
        helm-imenu-execute-action-at-once-if-one nil
        helm-source-imenu (helm-make-source "Imenu"
                              'helm-imenu-source
                            :fuzzy-match helm-imenu-fuzzy-match)))

(use-package helm-locate
  :config
  (setq helm-locate-fuzzy-match nil
        helm-source-locate
        (helm-make-source "Locate" 'helm-locate-source
          :pattern-transformer 'helm-locate-pattern-transformer
          :candidate-number-limit 100)
        helm-locate-command "locate %s -r %s -be -l 100"))

(use-package helm-css-scss ; https://github.com/ShingoFukuyama/helm-css-scss
  :commands (helm-css-scss
             helm-css-scss-multi
             helm-css-scss-insert-close-comment)
  :config
  (setq helm-css-scss-split-direction 'split-window-vertically
        helm-css-scss-split-with-multiple-windows t))

(use-package helm-swoop    ; https://github.com/ShingoFukuyama/helm-swoop
  :config
  ;; From helm-swoop to helm-multi-swoop-all
  (setq helm-swoop-use-line-number-face t
        helm-swoop-candidate-number-limit 200
        helm-swoop-speed-or-color t
        helm-swoop-split-direction 'split-window-vertically
        helm-swoop-split-window-function 'helm-default-display-buffer
        helm-swoop-pre-input-function (lambda () "")
        helm-multi-swoop-edit-save t))

(use-package helm-describe-modes :commands helm-describe-modes)
(use-package helm-ring :commands helm-show-kill-ring)
(use-package helm-semantic
  :commands helm-semantic-or-imenu
  :config
  (setq helm-semantic-fuzzy-match t
        helm-source-semantic (helm-make-source "Semantic Tags"
                                 'helm-semantic-source
                               :fuzzy-match helm-semantic-fuzzy-match)))
(use-package helm-elisp :commands helm-apropos)
(use-package helm-command
  :commands helm-M-x
  :config
  (setq helm-M-x-fuzzy-match t))

(use-package helm-grep
  :init (setq helm-grep-default-command "ag --vimgrep -z %p %f"
              helm-grep-default-recurse-command "ag --vimgrep -z %p %f"))

(use-package helm-fuzzier
  :disabled t
  :init (helm-fuzzier-mode 1))

(use-package helm-dash
  :config
  (setq helm-dash-enable-debugging nil)
  (defun dash/activate-package-docsets (path)
    "Add dash docsets from specified PATH."
    (setq helm-dash-docsets-path path
          helm-dash-common-docsets (helm-dash-installed-docsets))
    (message (format "activated %d docsets from: %s"
                     (length helm-dash-common-docsets) path)))
  (dash/activate-package-docsets helm-dash-docsets-path))

(provide 'core-helm)
;;; core-helm.el ends here

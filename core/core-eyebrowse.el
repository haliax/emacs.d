;; -*- lexical-binding: t -*-
;;; core-eyebrowse.el
;; see lib/defuns-eyebrowse.el

(use-package eyebrowse
  :diminish eyebrowse-mode
  :disabled t
  :init
  (progn
    (setq eyebrowse-new-workspace #'polaris/home
          eyebrowse-switch-back-and-forth t
          eyebrowse-wrap-around t)
    (eyebrowse-mode)))

(provide 'core-eyebrowse)
;;; core-eyebrowse.el ends here

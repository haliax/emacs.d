;; -*- lexical-binding: t -*-
;;; core-project.el --- all your (basic) project navigational needs

;;; When opening a file in a non-existing directory, offer to create it.
;; Source: <http://iqbalansari.github.io/blog/2014/12/07/automatically-create-parent-directories-on-visiting-a-new-file-in-emacs/>.

(defun my-create-non-existent-dir ()
  (let ((parent-dir (file-name-directory buffer-file-name)))
    (when (and (not (file-exists-p parent-dir))
               (y-or-n-p (format "Directory `%s' does not exist! Create?" parent-dir)))
      (make-directory parent-dir t))))

(add-to-list 'find-file-not-found-functions #'my-create-non-existent-dir)

;;; Move/delete file.

(defun move-this-buffer-and-file ()
  "Moves (renames) the current buffer and the file it is visiting (after saving it)."
  (interactive)
  (save-buffer)
  (let ((filename (buffer-file-name)))
    (unless filename
      (user-error "Not visiting a file"))
    (unless (file-exists-p filename)
      (user-error "File doesn't exist: %s" filename))
    (let ((new-name
           (read-file-name "New name: "
                           (file-name-directory filename)
                           nil nil
                           (file-name-nondirectory filename))))
      (when (or (file-exists-p new-name)
                (file-symlink-p new-name))
        (user-error "File already exists: %s" new-name))
      (rename-file filename new-name)
      (rename-buffer new-name t) ; t -- if the name is taken, pick an unique one.
      (set-visited-file-name new-name)))
  (set-buffer-modified-p nil))
(bind-key "C-c m" #'move-this-buffer-and-file)

(defun delete-this-buffer-and-file (no-confirmation)
  "Removes the file visited by the current buffer and kills the buffer."
  (interactive "P")
  (let ((filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (user-error "Not visiting a file")
      (when (or no-confirmation
                (yes-or-no-p (format "Delete %s?" filename)))
        (delete-file filename t) ; t -- move to thrash (when on Windows).
        (unless (file-exists-p filename)
          (kill-buffer))))))
(bind-key "C-c d" #'delete-this-buffer-and-file)

(use-package ido
  :functions (ido-to-end)
  :init
  (setq ido-ignore-buffers
        '("\\` " "^\\*ESS\\*" "^\\*Messages\\*" "^\\*Help\\*" "^\\*Buffer"
          "^\\*.*Completions\\*$" "^\\*Ediff" "^\\*tramp" "^\\*cvs-"
          "_region_" " output\\*$" "^TAGS$" "^\*Ido")
        ido-use-faces nil
        ido-confirm-unique-completion t
        ido-case-fold t
        ido-create-new-buffer 'always
        ido-enable-flex-matching t
        ido-enable-last-directory-history t
        ido-enable-tramp-completion nil
        ido-enable-tramp-completion t
        ido-cr+-max-items 10000
        ido-save-directory-list-file (concat polaris-temp-dir "ido.last"))
  (add-hook 'ido-setup-hook 'polaris|ido-setup-home-keybind)
  :config
  (add-hook! ido-setup
    (advice-add 'ido-sort-mtime :override 'polaris*ido-sort-mtime)

    (require 'ido-vertical-mode)
    (ido-vertical-mode 1)
    (require 'flx-ido)
    (flx-ido-mode 1))

  (add-hook! (ido-make-file-list ido-make-dir-list) 'polaris*ido-sort-mtime))

(use-package neotree
  :commands (neotree-show
             neotree-hide
             neotree-toggle
             neotree-dir
             neotree-find
             neo-global--window-exists-p)
  :init
  (setq neo-create-file-auto-open t
        neo-auto-indent-point t
        neo-mode-line-type 'none
        neo-persist-show nil
        neo-window-width 26
        neo-show-updir-line nil
        neo-auto-indent-point t
        neo-banner-message nil)

  (defun polaris/neotree-maybe-attach-window ()
    (when (get-buffer-window (neo-global--get-buffer))
      (neo-global--attach)))

  (defun polaris/neotree-key-bindings ()
    "Set the key bindings for a neotree buffer."
    (evilified-state-evilify-map neotree-mode-map
      :mode neotree-mode
      :bindings
      (kbd "TAB")  'neotree-stretch-toggle
      (kbd "RET") 'neotree-enter
      (kbd "|") 'neotree-enter-vertical-split
      (kbd "-") 'neotree-enter-horizontal-split
      (kbd "?") 'evil-search-backward
      (kbd "c") 'neotree-create-node
      (kbd "d") 'neotree-delete-node
      (kbd "gr") 'neotree-refresh
      (kbd "h") 'polaris/neotree-collapse-or-up
      (kbd "H") 'neotree-select-previous-sibling-node
      (kbd "J") 'neotree-select-down-node
      (kbd "K") 'neotree-select-up-node
      (kbd "l") 'polaris/neotree-expand-or-open
      (kbd "L") 'neotree-select-next-sibling-node
      (kbd "q") 'neotree-hide
      (kbd "r") 'neotree-rename-node
      (kbd "R") 'neotree-change-root
      (kbd "s") 'neotree-hidden-file-toggle))
  :config
  (progn
    (polaris/neotree-key-bindings)
    (add-hook 'persp-activated-hook #'polaris/neotree-maybe-attach-window)
    (add-hook 'eyebrowse-post-window-switch-hook #'polaris/neotree-maybe-attach-window)

    ;; Shorter pwd in neotree
    (defun polaris*neotree-shorten-pwd (node)
      (list (abbreviate-file-name (car node))))
    (advice-add 'neo-buffer--insert-root-entry :filter-args 'polaris*neotree-shorten-pwd)

    ;; Don't ask for confirmation when creating files
    (defun polaris*neotree-create-node (orig-fun &rest args)
      (cl-letf (((symbol-function 'yes-or-no-p) (lambda (&rest _) t)))
        (apply orig-fun args)))
    (advice-add 'neotree-create-node :around 'polaris*neotree-create-node)

    (defun polaris*save-neotree (orig-fun &rest args)
      (polaris/neotree-save (apply orig-fun args)))
    ;; Prevents messing up the neotree buffer on window changes
    (advice-add 'polaris--evil-window-move  :around 'polaris*save-neotree)
    (advice-add 'polaris--evil-swap-windows :around 'polaris*save-neotree)

    ;; A custom and simple theme for neotree
    (advice-add 'neo-buffer--insert-fold-symbol :override 'polaris*neo-buffer-fold-symbol)))

(use-package dired
  :init
  (setq
   ;; Always copy/delete recursively
   dired-recursive-copies (quote always)
   dired-recursive-deletes (quote top)
   dired-isearch-filenames t
   dired-listing-switches "-Ahl --group-directories-first"
   dired-marker-char 8594
   ;; Auto refresh dired, but be quiet about it
   global-auto-revert-non-file-buffers t
   auto-revert-verbose nil
   dired-omit-verbose nil
   dired-omit-files "^\\.$\\|^\\.\\.$"
   dired-dwim-target t)

  (add-hook 'dired-mode-hook 'dired-omit-mode)

  ;; List directories first
  (defun polaris|dired-sort ()
    "Dired sort hook to list directories first."
    (save-excursion
      (let (buffer-read-only)
        (forward-line 2) ;; beyond dir. header
        (sort-regexp-fields t "^.*$" "[ ]*." (point) (point-max))))
    (and (featurep 'xemacs)
         (fboundp 'dired-insert-set-properties)
         (dired-insert-set-properties (point-min) (point-max)))
    (set-buffer-modified-p nil))
  (add-hook 'dired-after-readin-hook 'polaris|dired-sort)

  ;; Automatically create missing directories when creating new files
  (defun polaris|create-non-existent-directory ()
    (let ((parent-directory (file-name-directory buffer-file-name)))
      (when (and (not (file-exists-p parent-directory))
                 (y-or-n-p (format "Directory `%s' does not exist! Create it?" parent-directory)))
        (make-directory parent-directory t))))
  (push 'polaris|create-non-existent-directory find-file-not-found-functions)

  (require 'dired-x)
  (require 'dired-aux)

  (setq dired-guess-shell-alist-user
        '(("\\.pdf\\'" "ec")
          ("\\.\\(?:djvu\\|eps\\)\\'" "ec")
          ("\\.\\(?:jpg\\|jpeg\\|png\\|gif\\|xpm\\)\\'" "pqiv")
          ("\\.\\(?:xcf\\)\\'" "gimp")
          ("\\.\\(?:csv\\|odt\\|ods\\)\\'" "libreoffice")
          ("\\.\\(?:mp4\\|mp3\\|mkv\\|avi\\|flv\\|ogv\\)\\(?:\\.part\\)?\\'"
           "mpv")
          ("\\.\\(?:mp3\\|?:m4a\\|flac\\)\\'" "rhythmbox")
          ("\\.html?\\'" "firefox")))

  (defvar dired-filelist-cmd
    '(("mpv")))

  (defun dired-start-process (cmd &optional file-list)
    (interactive
     (let ((files (dired-get-marked-files
                   t current-prefix-arg)))
       (list
        (dired-read-shell-command "& on %s: "
                                  current-prefix-arg files)
        files)))
    (let (list-switch)
      (start-process
       cmd nil shell-file-name
       shell-command-switch
       (format
        "nohup 1>/dev/null 2>/dev/null %s \"%s\""
        (if (and (> (length file-list) 1)
                 (setq list-switch
                       (cadr (assoc cmd dired-filelist-cmd))))
            (format "%s %s" cmd list-switch)
          cmd)
        (mapconcat #'expand-file-name file-list "\" \""))))))

(use-package ranger
  :init
  (setq ranger-cleanup-on-disable t
        ranger-show-dotfiles nil
        ranger-show-literal nil
        ranger-dont-show-binary t
        ranger-hide-cursor t
        ranger-preview-file nil
        ranger-override-dired t)
  (add-hook 'dired-mode-hook #'ranger-override-dired-fn))

(use-package bs
  :commands
  (bs-cycle-next
   bs-cycle-previous
   bs-show)
  :config
  (defun bs-visits-erc-channel (buffer)
    (with-current-buffer buffer
      (not (eq 'erc-mode major-mode))))
  (setq bs-default-configuration "files")
  (add-hook 'bs-mode-hook #'hl-line-mode)
  (add-hook 'bs-mode-hook (lambda () (setq scroll-margin 0)))
  (add-to-list 'bs-configurations '("erc-channels" nil nil nil bs-visits-erc-channel nil)))

(use-package ibuffer
  :commands ibuffer
  :config
  (setq ibuffer-show-empty-filter-groups nil)
  (defun ibuffer-projectile-setup ()
    (ibuffer-projectile-set-filter-groups)
    (unless (eq ibuffer-sorting-mode 'alphabetic)
      (ibuffer-do-sort-by-alphabetic)))

  (setq ibuffer-expert t)
  (setq ibuffer-show-empty-filter-groups nil)
  (add-hook 'ibuffer-mode-hook #'ibuffer-auto-mode)

  (add-hook 'ibuffer-hook #'ibuffer-projectile-setup)
  (add-hook 'ibuffer-mode-hook #'hl-line-mode)

  (setq ibuffer-formats
        '((mark modified read-only vc-status-mini " "
                (name 18 18 :left :elide)
                " "
                (size 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " "
                (vc-status 16 16 :left)
                " "
                filename-and-process)))
  (evilified-state-evilify-map ibuffer-mode-map
    :mode ibuffer-mode)
  (setq ibuffer-marked-char 8594))

(use-package projectile
  :diminish projectile-mode
  :config
  (setq projectile-completion-system 'ivy
        projectile-mode-line
        '(:eval (format (if (display-graphic-p) " ↠" " /")))
        projectile-sort-order 'recentf
        projectile-require-project-root nil
        projectile-enable-caching t
        projectile-cache-file (concat polaris-temp-dir "projectile.cache")
        projectile-known-projects-file (concat polaris-temp-dir "projectile.projects")
        projectile-indexing-method 'alien
        projectile-project-root-files polaris-project-root-files
        projectile-file-exists-remote-cache-expire nil)

  ;; Don't cache ignored files!
  (defun polaris*projectile-cache-current-file (orig-fun &rest args)
    (unless (--any (f-descendant-of? buffer-file-name it) (projectile-ignored-directories))
      (apply orig-fun args)))
  (advice-add 'projectile-cache-current-file :around 'polaris*projectile-cache-current-file)

  (push "ido.last" projectile-globally-ignored-files)
  (push "assets"   projectile-globally-ignored-directories)
  (push ".cask"    projectile-globally-ignored-directories)

  (projectile-global-mode +1)

  (defhydra hydra/projectile-tools (:color blue :hint nil :idle 0.3)
    "
Find^^             Operate on project^^      Other window
_f_ file           _c_ compile project       _O f_ file
_d_ dir            _R_ regenerate tags       _O d_ dir
_g_ file dwim      _S_ save project          _O g_ file dwim
_a_ other file     ^!^ command in /          _O a_ other file
_l_ file in dir    ^&^ async command in /    _O b_ switch buffer
_T_ test file      _z_ cache current file    _O t_ implementation←→test
_j_ tag            _i_ invalidate cache      _O C-o_ display buffer
Special^^       Buffers^^                    Search and replace
_D_ dired       _b_ switch buffer            _o_ multi-occur
_e_ recentf     _k_ kill buffers             _r_ replace
_I_ ibuffer     _p_ switch project           _s g_ grep
_v_ vc          _ESC_ project other buffer   _s s_ ag
_m_ commander   _F_ file any project         _s a_ ack
Tests   _P_ test-project    _t_ toggle implementation←→test"
    ("O a" projectile-find-other-file-other-window)
    ("O b" projectile-switch-to-buffer-other-window)
    ("O C-o" projectile-display-buffer)
    ("O d" projectile-find-dir-other-window)
    ("O f" projectile-find-file-other-window)
    ("O g" projectile-find-file-dwim-other-window)
    ("O t" projectile-find-implementation-or-test-other-window)
    ("!" projectile-run-shell-command-in-root)
    ("&" projectile-run-async-shell-command-in-root)
    ("a" projectile-find-other-file)
    ("b" projectile-switch-to-buffer)
    ("c" projectile-compile-project)
    ("d" projectile-find-dir)
    ("D" projectile-dired)
    ("e" projectile-recentf)
    ("f" projectile-find-file)
    ("g" projectile-find-file-dwim)
    ("F" projectile-find-file-in-known-projects)
    ("i" projectile-invalidate-cache)
    ("I" projectile-ibuffer)
    ("j" projectile-find-tag)
    ("k" projectile-kill-buffers)
    ("l" projectile-find-file-in-directory)
    ("m" projectile-commander)
    ("o" projectile-multi-occur)
    ("p" projectile-switch-project)
    ("P" projectile-test-project)
    ("r" projectile-replace)
    ("R" projectile-regenerate-tags)
    ("s a" helm-projectile-ack)
    ("s g" projectile-grep)
    ("s s" helm-projectile-ag)
    ("S" projectile-save-project-buffers)
    ("t" projectile-toggle-between-implementation-and-test)
    ("T" projectile-find-test-file)
    ("v" projectile-vc)
    ("z" projectile-cache-current-file)
    ("ESC" projectile-project-buffers-other-buffer)))

(provide 'core-project)
;;; core-project.el ends here

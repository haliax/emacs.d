;; -*- lexical-binding: t -*-
;;; core-key-chord.el

(use-package key-chord
  :init
  (setq key-chord-two-keys-delay 0.2
        key-chord-one-key-delay 0.3)
  (key-chord-mode 1))

(provide 'core-key-chord)
;;; core-key-chord.el ends here

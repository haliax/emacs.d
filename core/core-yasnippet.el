;; -*- lexical-binding: t -*-
;;; core-yasnippet.el --- For the lazy typist

(use-package yasnippet
  :mode ("emacs\\.d/private/\\(snippets\\|templates\\)/.+$" . snippet-mode)
  :diminish (yas-minor-mode . " Ⓨ")
  :commands (yas-minor-mode
             yas-minor-mode-on
             yas-expand
             yas-insert-snippet
             yas-new-snippet
             yas-visit-snippet-file)
  :init
  (defun polaris/load-yasnippet ()
    (unless yas-global-mode (yas-global-mode 1))
    (yas-minor-mode 1))

  (setq yas-verbosity 0
        yas-indent-line 'auto
        yas-also-auto-indent-first-line t
        yas-triggers-in-field t
        yas-wrap-around-region t
        ;; ;; Only load personal snippets
        yas-snippet-dirs polaris-snippet-dirs
        yas-prompt-functions '(yas-completing-prompt))

  (add-hook! (text-mode prog-mode snippet-mode markdown-mode org-mode)
    'yas-minor-mode-on)

  (defvar yas-minor-mode-map
    (let ((map (make-sparse-keymap)))
      (evil-define-key 'insert map [(tab)] 'yas-expand)
      (evil-define-key 'visual map (kbd "<backtab>") 'polaris/yas-insert-snippet)
      map))
  :config
  (yas-reload-all)

  (associate! snippet-mode :match "emacs\\.d/private/\\(snippets\\|templates\\)/.+$")

  ;; Prevents evil's visual-line from gobbling up the newline on the right due to an
  ;; off-by-one issue.
  (defadvice yas-expand-snippet (around yas-expand-snippet-visual-line activate)
    (when (polaris/evil-visual-line-state-p)
      (ad-set-arg 2 (1- (ad-get-arg 2)))) ad-do-it)

  ;; Once you're in normal mode, you're out
  (add-hook 'jevil-normal-state-entry-hook #'yas-abort-snippet)
  ;; Strip out the shitespace before a line selection
  (add-hook 'yas-before-expand-snippet-hook #'polaris|yas-before-expand)
  ;; Previous hook causes yas-selected-text to persist between expansions.
  ;; This little hack fixes that.
  (add-hook 'yas-after-exit-snippet-hook #'polaris|yas-after-expand)

  ;; Exit snippets on ESC in normal mode
  (advice-add 'evil-force-normal-state :before 'yas-exit-all-snippets)

  ;; Fix an issue with smartparens' keybindings interfering with yasnippet keybindings.
  (advice-add 'yas-expand :before 'sp-remove-active-pair-overlay))

(use-package auto-yasnippet
  :commands (aya-create aya-expand aya-open-line aya-persist-snippet)
  :config
  (setq aya-persist-snippets-dir (concat polaris-private-dir "/auto-snippets/")))

(provide 'core-yasnippet)
;;; core-yasnippet.el ends here

;; -*- lexical-binding: t -*-
;;; core-icicles.el

(use-package icicles
  :config
  (use-package icicles-mac)

  (setq icicle-top-level-key-bindings nil

        icicle-max-candidates 1000
        icicle-sorting-max-candidates 1000

        icicle-sort-comparer #'icicle-flx-score-greater-p
        icicle-Completions-text-scale-decrease 0
        icicle-expand-input-to-common-match 1
        icicle-highlight-lighter-flag nil
        icicle-show-Completions-help-flag nil
        icicle-yank-function #'cua-paste)

  (defun icicle-flx-score-greater-p (s1 s2)
    "Return non-nil if S1 scores higher than S2 using `flx-score`."
    ;; (message "Testing testing!")
    (let* ((input   (if (icicle-file-name-input-p)
                        (file-name-nondirectory icicle-current-input)
                      icicle-current-input))
           (score1  (flx-score s1 input))
           (score2  (flx-score s2 input)))
      (and score1  score2  (> (car score1) (car score2)))))

  (eval-and-compile
    (icicle-define-sort-command "by flx score"
                                ;; icicle-dirs-last-p
                                icicle-flx-score-greater-p
                                "Sort completions by flx score.")))

(provide 'core-icicles)
;;; core-icicles.el ends here

;; -*- lexical-binding: t -*-
;;; core-flycheck.el --- check yourself before you shrek yourself

(use-package flycheck
  :commands (flycheck-mode flycheck-list-errors flycheck-buffer)
  :diminish (flycheck-mode . " 🅕")
  :init
  (setq-default
   flycheck-indication-mode 'right-fringe ;; right-fringe, left-fringe or nil
   flycheck-display-errors-function #'my/display-error-messages-condensed
   ;; Removed checks on idle/change for snappiness
   flycheck-check-syntax-automatically '(save mode-enabled)
   flycheck-disabled-checkers '(emacs-lisp emacs-lisp-checkdoc make html-tidy))

  (defun my/display-error-messages-condensed (errors)
    (require 'dash)
    (-when-let (messages (-keep #'flycheck-error-message errors))
      (when (flycheck-may-use-echo-area-p)
        (display-message-or-buffer (mapconcat #'identity messages "\n")
                                   flycheck-error-message-buffer))))

  (global-flycheck-mode +1)

  ;; And on ESC in normal mode.
  (advice-add 'evil-force-normal-state :after 'polaris*flycheck-buffer)

  (define-fringe-bitmap 'flycheck-fringe-bitmap-double-arrow
    [0 0 0 8 24 56 120 248 120 56 24 8 0 0 0]
    ;; (fringe-helper-convert
    ;;  "........"
    ;;  "........"
    ;;  "........"
    ;;  "....X..."
    ;;  "...XX..."
    ;;  "..XXX..."
    ;;  ".XXXX..."
    ;;  "XXXXX..."
    ;;  ".XXXX..."
    ;;  "..XXX..."
    ;;  "...XX..."
    ;;  "....X..."
    ;;  "........"
    ;;  "........"
    ;;  "........")
    )

  (flycheck-define-error-level 'error
    :overlay-category 'flycheck-error-overlay
    :fringe-bitmap 'flycheck-fringe-bitmap-double-arrow
    :fringe-face 'flycheck-fringe-error)

  (flycheck-define-error-level 'warning
    :overlay-category 'flycheck-warning-overlay
    :fringe-bitmap 'flycheck-fringe-bitmap-double-arrow
    :fringe-face 'flycheck-fringe-warning)

  (flycheck-define-error-level 'info
    :overlay-category 'flycheck-info-overlay
    :fringe-bitmap 'flycheck-fringe-bitmap-double-arrow
    :fringe-face 'flycheck-fringe-info)

  (defun nadvice/flycheck-mode-line-status-text (&optional status)
    (let ((text (pcase (or status flycheck-last-status-change)
                  (`not-checked "")
                  (`no-checker "-")
                  (`running "*")
                  (`errored "!")
                  (`finished
                   (if flycheck-current-errors
                       (let ((error-counts (flycheck-count-errors
                                            flycheck-current-errors)))
                         (format "%s/%s"
                                 (or (cdr (assq 'error error-counts)) "")
                                 (or (cdr (assq 'warning error-counts)) "")))
                     ""))
                  (`interrupted "-")
                  (`suspicious "?"))))
      (concat (if (display-graphic-p) " ✓" " Γ") text)))

  (advice-add 'flycheck-mode-line-status-text :override
              #'nadvice/flycheck-mode-line-status-text)
  :config
  ;; fixes Unknown defun property `interactive-only' error by compiling flycheck
  (let ((path (locate-library "flycheck")))
    (unless (f-ext? path "elc")
      (byte-compile-file path)))

  (evilified-state-evilify-map flycheck-error-list-mode-map
    :mode flycheck-error-list-mode
    :bindings
    "RET" 'flycheck-error-list-goto-error
    "j" 'flycheck-error-list-next-error
    "k" 'flycheck-error-list-previous-error)

  ;; Check buffer when normal mode is entered
  (add-hook! evil-normal-state-entry 'polaris*flycheck-buffer)
  ;; And on ESC in normal mode.
  (advice-add 'evil-force-normal-state :after 'polaris*flycheck-buffer))

(use-package flycheck-package
  :after flycheck
  :config (flycheck-package-setup))

(use-package flycheck-pos-tip
  :after flycheck
  :config
  (setq flycheck-pos-tip-timeout 10
        flycheck-display-errors-delay 0.5)
  (flycheck-pos-tip-mode +1))

(use-package ispell
  :init
  (cond
   ((executable-find "aspell")
    (setq ispell-program-name "aspell"))
   ((executable-find "hunspell")
    (setq ispell-program-name "hunspell")
    ;; just reset dictionary to the safe one "en_US" for hunspell.
    ;; if we need use different dictionary, we specify it in command line arguments
    (setq ispell-local-dictionary "en_US")
    (setq ispell-local-dictionary-alist
          '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil nil nil utf-8))))
   (t (setq ispell-program-name nil)))

  (setq ispell-extra-args (polaris/flyspell-detect-ispell-args t))
  ;; (setq ispell-cmd-args (flyspell-detect-ispell-args))
  (defadvice ispell-word (around my-ispell-word activate)
    (let ((old-ispell-extra-args ispell-extra-args))
      (ispell-kill-ispell t)
      (setq ispell-extra-args (polaris/flyspell-detect-ispell-args))
      ad-do-it
      (setq ispell-extra-args old-ispell-extra-args)
      (ispell-kill-ispell t)))

  (defadvice flyspell-auto-correct-word (around my-flyspell-auto-correct-word activate)
    (let ((old-ispell-extra-args ispell-extra-args))
      (ispell-kill-ispell t)
      ;; use emacs original arguments
      (setq ispell-extra-args (polaris/flyspell-detect-ispell-args))
      ad-do-it
      ;; restore our own ispell arguments
      (setq ispell-extra-args old-ispell-extra-args)
      (ispell-kill-ispell t)))

  ;; Save a new word to personal dictionary without asking
  (setq ispell-silently-savep t))

(use-package flyspell
  :commands flyspell-mode
  :diminish (flyspell-mode . " 🅢")
  :init
  (setq flyspell-issue-message-flag nil
        flyspell-issue-welcome-flag nil)
  (add-hook 'text-mode-hook #'flyspell-mode)

  :config
  (defun nadvice/ispell-init-process (old-fun &rest args)
    (cl-letf (((symbol-function 'message) #'format))
      (apply old-fun args)))

  (advice-add 'ispell-init-process :around #'nadvice/ispell-init-process))

(use-package auto-dictionary
  :init
  (progn
    (add-hook 'flyspell-mode-hook 'auto-dictionary-mode)
    ;; Select the buffer local dictionary if it was set, otherwise
    ;; auto-dictionary will replace it with a guessed one at each activation.
    ;; https://github.com/nschum/auto-dictionary-mode/issues/5
    (defun polaris/adict-set-local-dictionary ()
      "Set the local dictionary if not nil."
      (when (and (fboundp 'adict-change-dictionary)
                 ispell-local-dictionary)
        (adict-change-dictionary ispell-local-dictionary)))
    (add-hook 'auto-dictionary-mode-hook
              'polaris/adict-set-local-dictionary 'append)))

(provide 'core-flycheck)
;;; core-flycheck.el ends here

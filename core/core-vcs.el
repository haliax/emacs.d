;; -*- lexical-binding: t -*-
;;; core-vcs.el --- version control awareness

;; Follow symlinks automatically
(setq vc-follow-symlinks t              ; Follow symlinks automatically
      vc-make-backup-files t            ; Make backup of files, even (if ) they're in version control
      )

(use-package gitconfig-mode
  :mode ("/\\.gitconfig\\'"
         "/\\.git/config\\'"
         "/git/config\\'"
         "/\\.gitmodules\\'")
  :init (add-hook 'gitconfig-mode-hook #'flyspell-mode))

(use-package gitignore-mode
  :mode ("/\\.gitignore\\'"
         "/\\.git/info/exclude\\'"
         "/git/ignore\\'"))

(use-package gitattributes-mode
  :mode ("/\\.gitattributes\\'"
         "/\\.git/info/attributes\\'"
         "/git/attributes\\'"))


(use-package git-gutter
  :diminish git-gutter-mode
  :commands (git-gutter-mode polaris/vcs-next-hunk polaris/vcs-prev-hunk
                             polaris/vcs-show-hunk polaris/vcs-stage-hunk polaris/vcs-revert-hunk)
  :init
  (add-hook! (text-mode prog-mode conf-mode) 'git-gutter-mode)
  :config
  (require 'git-gutter-fringe)

  (define-fringe-bitmap 'git-gutter-fr:added
    [240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240]
    nil nil 'center)
  (define-fringe-bitmap 'git-gutter-fr:modified
    [240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240]
    nil nil 'center)
  (define-fringe-bitmap 'git-gutter-fr:deleted
    [0 0 0 0 0 0 128 192 224 240 248]
    nil nil 'center)

  ;; Refresh git-gutter on ESC in normal mode
  (advice-add 'evil-force-normal-state :after 'git-gutter)

  (add-hook 'focus-in-hook 'git-gutter:update-all-windows)

  (defalias 'polaris/vcs-next-hunk    'git-gutter:next-hunk)
  (defalias 'polaris/vcs-prev-hunk    'git-gutter:previous-hunk)
  (defalias 'polaris/vcs-show-hunk    'git-gutter:popup-hunk)
  (defalias 'polaris/vcs-stage-hunk   'git-gutter:stage-hunk)
  (defalias 'polaris/vcs-revert-hunk  'git-gutter:revert-hunk))

(use-package ediff
  :config
  (defvar ediff-saved-window-configuration)

  (add-hook 'ediff-before-setup-hook
            (lambda ()
              (setq ediff-saved-window-configuration (current-window-configuration))))

  (let ((restore-window-configuration
         (lambda ()
           (set-window-configuration ediff-saved-window-configuration))))
    (add-hook 'ediff-quit-hook restore-window-configuration 'append)
    (add-hook 'ediff-suspend-hook restore-window-configuration 'append))

  (defun nadvice/ediff-setup-keymap (&rest _args)
    (define-key ediff-mode-map "j" #'ediff-next-difference)
    (define-key ediff-mode-map "k" #'ediff-previous-difference))

  (advice-add 'ediff-setup-keymap :after #'nadvice/ediff-setup-keymap)
  (defun my/command-line-ediff (_switch)
    (let ((file1 (pop command-line-args-left))
          (file2 (pop command-line-args-left)))
      (ediff file1 file2)))

  (add-to-list 'command-switch-alist '("diff" . my/command-line-ediff)))

;; Emacs interface to git
(use-package magit
  :init
  (setq magit-push-always-verify nil
        magit-completing-read-function 'ivy-completing-read
        magit-log-format-graph-function 'magit-log-format-unicode-graph
        magit-item-highlight-face 'bold
        magit-repo-dirs-depth 1
        git-commit-finish-query-functions nil
        magit-save-some-buffers nil     ;don't ask to save buffers
        magit-set-upstream-on-push t    ;ask to set upstream
        magit-diff-refine-hunk t        ;show word-based diff for current hunk
        magit-default-tracking-name-function 'magit-default-tracking-name-branch-only) ;don't track with origin-*
  :config
  (add-to-list 'magit-no-confirm 'stage-all-changes)

  (define-key magit-log-mode-map (kbd "j") #'next-line)
  (define-key magit-refs-mode-map (kbd "j") #'next-line)
  (define-key magit-status-mode-map (kbd "j") #'next-line)

  (magit-define-popup-action 'magit-rebase-popup
    ?R "Rockstar" 'magit-rockstar)
  (magit-define-popup-action 'magit-commit-popup
    ?n "Reshelve" 'magit-reshelve)

  (add-hook! with-editor-mode 'evil-emacs-state)

  (define-key (current-global-map)
    [remap async-shell-command] 'with-editor-async-shell-command)
  (define-key (current-global-map)
    [remap shell-command] 'with-editor-shell-command))

(use-package evil-magit)

(after! vc-annotate
  (evil-set-initial-state 'vc-annotate-mode 'normal)
  (evil-set-initial-state 'vc-git-log-view-mode 'normal))

(evil-define-command polaris:git-remote-browse (&optional bang file)
  "Open the website for the current (or specified) version controlled FILE. If BANG,
then use hub to do it."
  (interactive "<!><f>")
  (let ((url (shell-command-to-string "hub browse -u -- ")))
    (when url
      (setq url (concat (s-trim url) "/" (f-relative (buffer-file-name) (polaris/project-root)))))
    (if bang
        (message "Url copied to clipboard: %s" (kill-new url))
      (browse-url url))))

(use-package diff
  :config
  (setq diff-switches "-u"))

(provide 'core-vcs)
;;; core-vcs.el ends here

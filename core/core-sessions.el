;; -*- lexical-binding: t -*-
;;; core-sessions.el

;; I use workgroups to accomplish two things:
;;   1. Vim-like tab emulation (type :tabs to see a list of tabs -- maybe I'll add some
;;      code to make a permanent frame header to display these some day)
;;   2. Session persistence (with :ss and :sl)

(defvar polaris-wg-frames '()
  "A list of all the frames opened as separate workgroups. See
defuns/defuns-workgroups.el.")

(defvar polaris-wg-names '()
  "A list of fixed names for workgroups. If a name is set, workgroup names aren't
automatically renamed to the project name.")

(use-package workgroups2
  ;; :when (display-graphic-p)
  :init
  (setq-default
   wg-session-file (expand-file-name "workgroups/last" polaris-temp-dir)
   wg-workgroup-directory (expand-file-name "workgroups/" polaris-temp-dir)
   wg-first-wg-name "*untitled*"
   wg-session-load-on-start nil
   wg-mode-line-display-on nil
   wg-mess-with-buffer-list nil
   wg-emacs-exit-save-behavior 'save ; Options: 'save 'ask nil
   wg-workgroups-mode-exit-save-behavior 'save
   wg-log-level 0

   ;; NOTE: Some of these make workgroup-restoration unstable
   wg-restore-mark t
   wg-restore-frame-position t
   wg-restore-remote-buffers nil
   wg-restore-scroll-bars nil
   wg-restore-fringes nil
   wg-restore-margins nil
   wg-restore-point-max t ; Throws silent errors if non-nil

   wg-list-display-decor-divider         " "
   wg-list-display-decor-left-brace      ""
   wg-list-display-decor-right-brace     "| "
   wg-list-display-decor-current-left    ""
   wg-list-display-decor-current-right   ""
   wg-list-display-decor-previous-left   ""
   wg-list-display-decor-previous-right  "")

  (add-hook 'emacs-startup-hook #'workgroups-mode)
  :config
  (unless (file-exists-p wg-workgroup-directory)
    (mkdir wg-workgroup-directory))

  ;; Remember the set names in between sessions
  (push 'polaris-wg-names savehist-additional-variables)

  ;; `wg-mode-line-display-on' wasn't enough
  (advice-add 'wg-change-modeline :override 'ignore)

  ;; Don't remember popup and neotree windows
  (add-hook 'kill-emacs-hook 'polaris|wg-cleanup)

  (after! projectile
    ;; Create a new workgroup on switch-project
    (setq projectile-switch-project-action 'polaris/wg-projectile-switch-project))

  ;; This helps abstract some of the underlying functions away, just in case I want to
  ;; switch to a different package in the future, like persp-mode, eyebrowse or wconf.
  (defalias 'polaris/tab-display 'polaris/workgroup-display)
  (defalias 'polaris/helm-tabs 'polaris:helm-wg)
  (defalias 'polaris/close-window-or-tab 'polaris/close-window-or-workgroup)
  (defalias 'polaris:tab-create 'polaris:workgroup-new)
  (defalias 'polaris:tab-rename 'polaris:workgroup-rename)
  (defalias 'polaris:kill-tab 'polaris:workgroup-delete)
  (defalias 'polaris:kill-other-tabs  'polaris:kill-other-workgroups)
  (defalias 'polaris:switch-to-tab 'polaris:switch-to-workgroup-at-index)
  (defalias 'polaris:switch-to-tab-left 'wg-switch-to-workgroup-left)
  (defalias 'polaris:switch-to-tab-right 'wg-switch-to-workgroup-right)
  (defalias 'polaris:switch-to-tab-last 'wg-switch-to-previous-workgroup))

(use-package eyebrowse
  :init
  (progn
    (setq eyebrowse-wrap-around t)
    (eyebrowse-mode)
    ;; hooks
    (add-hook 'persp-before-switch-functions
              #'polaris/update-eyebrowse-for-perspective)
    (add-hook 'eyebrowse-post-window-switch-hook
              #'polaris/save-eyebrowse-for-perspective)
    (add-hook 'persp-activated-functions
              #'polaris/load-eyebrowse-for-perspective)
    ;; vim-style tab switching
    (define-key evil-motion-state-map "gt" 'eyebrowse-next-window-config)
    (define-key evil-motion-state-map "gT" 'eyebrowse-prev-window-config)))

(use-package persp-mode
  :diminish persp-mode
  :init
  (progn
    (setq persp-auto-resume-time -1
          persp-nil-name "Home"
          persp-reset-windows-on-nil-window-conf nil
          persp-set-last-persp-for-new-frames nil
          persp-save-dir (expand-file-name "perspectives/" polaris-temp-dir))

    (defvar layouts-autosave-delay 900
      "Delay in seconds between each layouts auto-save.")

    (defvar polaris--last-selected-layout "Home"
      "Previously selected layout.")

    (defvar polaris--layouts-autosave-timer nil
      "Timer for layouts auto-save.")

    ;; always activate persp-mode
    (persp-mode))
  :config
  (progn
    (defadvice persp-activate (before polaris/save-toggle-layout activate)
      (setq polaris--last-selected-layout persp-last-persp-name))

    ))

(provide 'core-sessions)
;;; core-sessions.el ends here

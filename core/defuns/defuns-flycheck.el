;; -*- lexical-binding: t -*-
;;; defuns-flycheck.el
;; for ../core-flycheck.el

;;;###autoload
(defun polaris*flycheck-buffer ()
  (when (bound-and-true-p flycheck-mode)
    (flycheck-buffer)))

;;;###autoload
(defun polaris/flycheck-next-error ()
  (interactive)
  (call-interactively
   (if (bound-and-true-p flycheck-mode)
       'flycheck-next-error
     'next-error)))

;;;###autoload
(defun polaris/flycheck-previous-error ()
  (interactive)
  (call-interactively
   (if (bound-and-true-p flycheck-mode)
       'flycheck-previous-error
     'previous-error)))

;;;###autoload
(defun polaris/flyspell-detect-ispell-args (&optional RUN-TOGETHER)
  "if RUN-TOGETHER is true, spell check the CamelCase words"
  (let (args)
    (cond
     ((string-match  "aspell$" ispell-program-name)
      ;; force the English dictionary, support Camel Case spelling check (tested with aspell 0.6)
      (setq args (list "--sug-mode=ultra" "--lang=en_US"))
      (if RUN-TOGETHER
          (setq args (append args '("--run-together" "--run-together-limit=5" "--run-together-min=2")))))
     ((string-match "hunspell$" ispell-program-name)
      (setq args nil)))
    args))

;;;###autoload
(defun polaris/display-error-messages-condensed (errors)
  (require 'dash)
  (-when-let (messages (-keep #'flycheck-error-message errors))
    (when (flycheck-may-use-echo-area-p)
      (require 's)
      (display-message-or-buffer (s-join "\n" messages)
                                 flycheck-error-message-buffer))))

;;;###autoload
(defun polaris/flycheck-errors ()
  (interactive)
  (when (bound-and-true-p flycheck-mode)
    (flycheck-buffer)
    (flycheck-list-errors)))

(provide 'defuns-flycheck)
;;; defuns-flycheck.el ends here

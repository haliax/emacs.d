;; -*- lexical-binding: t -*-
;;; defuns-company.el

;;;###autoload
(defun polaris/company-evil-complete-next (&optional arg)
  (call-interactively 'company-dabbrev)
  (if (eq company-candidates-length 1)
      (company-complete)))

;;;###autoload
(defun polaris/company-evil-complete-previous (&optional arg)
  (let ((company-selection-wrap-around t))
    (call-interactively 'company-dabbrev)
    (if (eq company-candidates-length 1)
        (company-complete)
      (call-interactively 'company-select-previous))))

;;;###autoload
(defun polaris/company-complete-common-or-complete-full ()
  (interactive)
  (when (company-manual-begin)
    (if (eq last-command #'company-complete-common-or-cycle)
        (let ((company-selection-wrap-around t))
          (call-interactively #'company-complete-selection))
      (let ((buffer-mod-tick (buffer-chars-modified-tick)))
        (call-interactively #'company-complete-common)
        (when (= buffer-mod-tick (buffer-chars-modified-tick))
          (call-interactively #'company-complete-selection)
          (call-interactively #'company-complete))))))

;;;###autoload
(defun company-select-above (&optional arg)
  (interactive "p")
  (if (let ((ov company-pseudo-tooltip-overlay))
        (and ov (< (overlay-get ov 'company-height) 0)))
      (company-select-next-or-abort arg)
    (company-select-previous-or-abort arg)))

;;;###autoload
(defun company-select-below (&optional arg)
  (interactive "p")
  (if (let ((ov company-pseudo-tooltip-overlay))
        (and ov (< (overlay-get ov 'company-height) 0)))
      (company-select-previous-or-abort arg)
    (company-select-next-or-abort arg)))

(defun polaris--company-whole-lines ()
  (split-string
   (replace-regexp-in-string
    "^[\t\s]+" ""
    (concat (buffer-substring-no-properties (point-min) (line-beginning-position))
            (buffer-substring-no-properties (line-end-position) (point-max))))
   "\\(\r\n\\|[\n\r]\\)" t))

;;;###autoload
(defun polaris/company-whole-lines (command &optional arg &rest ignored)
  (interactive (list 'interactive))
  (let ((lines (polaris--company-whole-lines)))
    (cl-case command
      (interactive (company-begin-backend 'polaris/company-whole-lines))
      (prefix (company-grab-line "^[\t\s]*\\(.+\\)" 1))
      (candidates (all-completions arg lines)))))

;;;###autoload
(defun polaris/company-dict-or-keywords ()
  (interactive)
  (let ((company-backends '((company-keywords company-dict))))
    (call-interactively 'company-complete)))

;;;###autoload
(defun polaris/company-complete ()
  "Bring up the completion popup. If there is only one result, auto-complete it."
  (interactive)
  (when (and (company-manual-begin)
             (= company-candidates-length 1))
    (company-complete-common)))

(provide 'defuns-company)
;;; defuns-company.el ends here

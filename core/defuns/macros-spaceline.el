;; -*- lexical-binding: t -*-
;;; macros-spaceline.el

;;;###autoload
(defmacro def-env-command! (mode command)
  "Define a COMMAND for MODE that will set `polaris--env-command' when that mode is
activated, which should return the version number of the current environment. It is used
by `polaris|spaceline-env-update' to display a version number in the modeline. For instance:

  (def-env-command! ruby-mode \"ruby --version | cut -d' ' -f2\")

This will display the ruby version in the modeline in ruby-mode buffers. It is cached the
first time."
  (add-hook! (focus-in find-file) 'polaris|spaceline-env-update)
  `(add-hook ',(intern (format "%s-hook" (symbol-name mode)))
             (lambda () (setq polaris--env-command ,command))))

(provide 'macros-spaceline)
;;; macros-spaceline.el ends here

;; -*- lexical-binding: t -*-
;;; defuns-tramp.el

;;;###autoload
(defun polaris/root-file-name-p (file-name)
  (and (featurep 'tramp)
       (tramp-tramp-file-p file-name)
       (with-parsed-tramp-file-name file-name parsed
         (string= "root" (substring-no-properties parsed-user)))))

;;;###autoload
(defun polaris/make-root-file-name (file-name)
  (require 'tramp)
  (let ((sudo (let ((default-directory
                      (file-name-directory file-name)))
                (= (process-file "sudo" nil nil nil "-n" "true") 0))))
    (if (tramp-tramp-file-p file-name)
        (with-parsed-tramp-file-name file-name parsed
          (tramp-make-tramp-file-name
           (if sudo "sudo" "su")
           "root"
           parsed-host
           parsed-localname
           (let ((tramp-postfix-host-format "|")
                 (tramp-prefix-format))
             (tramp-make-tramp-file-name
              (if (string= "scp" parsed-method)
                  "ssh"
                parsed-method)
              parsed-user
              parsed-host
              ""
              parsed-hop))))
      (concat (if sudo "/sudo::" "/su::")
              file-name))))

;;;###autoload
(defun polaris/edit-file-as-root ()
  "Find file as root"
  (interactive)
  (find-alternate-file (polaris/make-root-file-name buffer-file-name)))

;;;###autoload
(defun polaris/edit-file-as-root-maybe ()
  "Find file as root if necessary."
  (when (and buffer-file-name
             (not (file-writable-p buffer-file-name))
             (not (string= user-login-name
                           (nth 3 (file-attributes buffer-file-name 'string))))
             (not (polaris/root-file-name-p buffer-file-name))
             (y-or-n-p "File is not writable. Open with root? "))
    (polaris/edit-file-as-root)))

(provide 'defuns-tramp)
;;; defuns-tramp.el ends here

;;; defuns-hydra.el

;;;###autoload
(defun polaris/hydra-key-doc-function (key key-width doc doc-width)
  "Custom hint documentation format for keys."
  (format (format "[%%%ds] %%%ds" key-width (- -1 doc-width))
          key doc))

(provide 'defuns-hydra)
;;; defuns-hydra.el ends here

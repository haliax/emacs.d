;; -*- lexical-binding: t -*-
;;; macros-evil.el

;;;###autoload
(defmacro def-textobj! (key name start end)
  (let ((inner-name (make-symbol (concat "evil-inner-" name)))
        (outer-name (make-symbol (concat "evil-outer-" name)))
        (start-regex (regexp-opt (list start)))
        (end-regex (regexp-opt (list end))))
    `(progn
       (evil-define-text-object ,inner-name (count &optional beg end type)
         (evil-select-paren ,start-regex ,end-regex beg end type count nil))
       (evil-define-text-object ,outer-name (count &optional beg end type)
         (evil-select-paren ,start-regex ,end-regex beg end type count t))
       (define-key evil-inner-text-objects-map ,key (quote ,inner-name))
       (define-key evil-outer-text-objects-map ,key (quote ,outer-name))
       (with-eval-after-load 'evil-surround
         (push (cons (string-to-char ,key)
                     (if ,end
                         (cons ,start ,end)
                       ,start))
               evil-surround-pairs-alist)))))

;;;###autoload
(defmacro def-tmp-excmd! (cmd-on cmd-off &rest commands)
  "Creates on-off defuns for a set of ex commands, named CMD-ON and CMD-OFF."
  (declare (indent 2))
  `(progn
     (defun ,cmd-on (&rest _)
       (mapc (lambda (cmd) (evil-ex-define-cmd (car cmd) (cdr cmd)))
             ',commands))
     (defun ,cmd-off (&rest _)
       (mapc (lambda (cmd) (polaris/evil-ex-undefine-cmd (car cmd)))
             ',commands))))

;; Shortcuts for the evil expression register
;;;###autoload
(defmacro $= (str &rest args)
  `(calc-eval (format ,str ,@args)))

;;;###autoload
(defmacro $r (char)
  `(evil-get-register ,char))

;;;###autoload
(defmacro $expand (path)
  `(evil-ex-replace-special-filenames ,path))

(provide 'macros-evil)
;;; macros-evil.el ends here

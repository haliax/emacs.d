;; -*- lexical-binding: t -*-
;;; defuns-repl.el

;;;###autoload  (autoload 'polaris:repl "defuns-repl" nil t)
(evil-define-command polaris:repl (&optional bang command)
  :repeat nil
  (interactive "<!><a>")
  (if (and polaris--repl-buffer (buffer-live-p polaris--repl-buffer))
      (polaris/popup-buffer polaris--repl-buffer)
    (rtog/toggle-repl (if (use-region-p) 4))
    (setq polaris--repl-buffer (current-buffer))
    (when command
      (with-current-buffer polaris--repl-buffer
        (insert command)
        (unless bang (comint-send-input))))))

;;;###autoload  (autoload 'polaris:repl-eval "defuns-repl" nil t)
(evil-define-operator polaris:repl-eval (&optional beg end bang)
  :type inclusive
  :repeat nil
  (interactive "<r><!>")
  (let ((region-p (use-region-p))
        (selection (s-trim (buffer-substring-no-properties beg end))))
    (polaris:repl bang)
    (when (and region-p beg end)
      (let* ((buf polaris--repl-buffer)
             (win (get-buffer-window buf)))
        (unless (eq buf (polaris/popup-p (get-buffer-window buf)))
          (polaris/popup-buffer buf))
        (when (and polaris--repl-buffer (buffer-live-p polaris--repl-buffer))
          (with-current-buffer polaris--repl-buffer
            (goto-char (point-max))
            (insert selection)))))))


(provide 'defuns-repl)
;;; defuns-repl.el ends here

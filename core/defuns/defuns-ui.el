;; -*- lexical-binding: t -*-
;;; defuns-ui.el
;; for ../core-ui.el


;;;###autoload (autoload 'polaris:toggle-fullscreen "defuns-ui" nil t)
;;;###autoload (autoload 'polaris:set-columns "defuns-ui" nil t)
(after! evil
  (evil-define-command polaris:set-columns (&optional bang columns)
    "Adjusts visual-fill-column-width on the fly."
    (interactive "<!><a>")
    (if (or (= (length columns) 0) bang)
        (progn
          (setq visual-fill-column-width 80)
          (when visual-fill-column-mode
            (visual-fill-column-mode -1)))
      (setq columns (string-to-number columns))
      (when (> columns 30)
        (setq visual-fill-column-width columns)))
    (if visual-fill-column-mode
        (visual-fill-column--adjust-window)
      (visual-fill-column-mode 1)))

  (evil-define-command polaris:toggle-fullscreen ()
    (interactive)
    (set-frame-parameter nil 'fullscreen (if (not (frame-parameter nil 'fullscreen)) 'fullboth))))

;;;###autoload
(defun polaris/reset-theme ()
  (interactive)
  (polaris/load-theme (or polaris-current-theme polaris-default-theme)))

;;;###autoload
(defun polaris/load-font (font)
  (interactive)
  (set-frame-font font t)
  (setq polaris-current-font font))

;;;###autoload
(defun polaris/load-theme (theme &optional suppress-font)
  (interactive)
  (when polaris-current-theme
    (disable-theme polaris-current-theme))
  (load-theme theme t)
  (unless suppress-font
    (polaris/load-font polaris-current-font))
  (setq polaris-current-theme theme))

;;;###autoload
(defun polaris/show-as (how &optional pred)
  (let* ((beg (match-beginning 1))
         (end (match-end 1))
         (ok (or (not pred) (funcall pred beg end))))
    (when ok
      (compose-region beg end how 'decompose-region))
    nil))

;;;###autoload
(defun color-mix (&rest colors-weights-list)
  "Mix colors in the given proportions.
Example: (color-mix \"#ffffff\" 0.6 \"#ff0000\" 0.4) => \"#ff9999\".
If the sum of weights (proportions) is larger than 1.0, the result will be brighter.
If one of the colors is unspecified (cannot be displayed), return 'unspecified."
  (let* ((color-weight-pairs (-partition 2 colors-weights-list)) ; '((color weight) ...)
         (all-colors-defined-p (-all? (lambda (color-weight-pair)
                                        (color-defined-p (car color-weight-pair)))
                                      color-weight-pairs)))
    (if all-colors-defined-p
        (->> color-weight-pairs
             ;; Change pairs of (color-name weight) to triples of (red green blue).
             (mapcar (lambda (color-weight-pair)
                       (cl-destructuring-bind (color-name weight) color-weight-pair
                         (let ((color-rgb (color-name-to-rgb color-name)))
                           (mapcar (lambda (rgb-component) (* rgb-component weight))
                                   color-rgb)))))
             ;; Sum the list of (red green blue) triples.
             (-reduce (lambda (rgb-1 rgb-2)
                        (cl-mapcar '+ rgb-1 rgb-2)))
             ;; Convert to hex notation.
             (apply 'color-rgb-to-hex))
      'unspecified)))

;;;###autoload
(defun polaris/add-whitespace (&optional start end)
  "Maintain indentation whitespace in buffer. Used so that highlight-indentation will
display consistent guides. Whitespace is stripped out on save, so this doesn't affect the
end file."
  (interactive (progn (barf-if-buffer-read-only)
                      (if (use-region-p)
                          (list (region-beginning) (region-end))
                        (list nil nil))))
  (unless indent-tabs-mode
    (save-match-data
      (save-excursion
        (let ((end-marker (copy-marker (or end (point-max))))
              (start (or start (point-min))))
          (goto-char start)
          (while (and (re-search-forward "^$" end-marker t) (not (>= (point) end-marker)))
            (let (line-start line-end next-start next-end)
              (save-excursion
                ;; Check previous line indent
                (forward-line -1)
                (setq line-start (point)
                      line-end (save-excursion (back-to-indentation) (point)))
                ;; Check next line indent
                (forward-line 2)
                (setq next-start (point)
                      next-end (save-excursion (back-to-indentation) (point)))
                ;; Back to origin
                (forward-line -1)
                ;; Adjust indent
                (let* ((line-indent (- line-end line-start))
                       (next-indent (- next-end next-start))
                       (indent (min line-indent next-indent)))
                  (insert (make-string (if (zerop indent) 0 (1+ indent)) ? )))))
            (forward-line 1)))))
    (set-buffer-modified-p nil))
  nil)

;;;###autoload
(defun polaris/imenu-list-quit ()
  (interactive)
  (quit-window)
  (mapc (lambda (b) (with-current-buffer b
                 (when imenu-list-minor-mode
                   (imenu-list-minor-mode -1))))
        (polaris/get-visible-buffers (polaris/get-real-buffers))))

;;;###autoload
(defun polaris|hide-mode-line (&rest _)
  (setq mode-line-format nil))

(provide 'defuns-ui)
;;; defuns-ui.el ends here

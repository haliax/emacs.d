;; -*- lexical-binding: t -*-
;;; defuns-nlinum.el

;;;###autoload
(defun polaris/nlinum-toggle ()
  (interactive)
  (if (bound-and-true-p nlinum-mode)
      (polaris|nlinum-disable)
    (polaris|nlinum-enable)))

;;;###autoload
(defun polaris|nlinum-disable ()
  (nlinum-mode -1)
  (remove-hook 'post-command-hook 'polaris|nlinum-hl-line t)
  (polaris|nlinum-unhl-line))

;;;###autoload
(defun polaris|nlinum-unhl-line ()
  "Unhighlight line number"
  (when polaris--hl-nlinum-overlay
    (let* ((disp (get-text-property
                  0 'display (overlay-get polaris--hl-nlinum-overlay 'before-string)))
           (str (nth 1 disp)))
      (put-text-property 0 (length str) 'face 'linum str)
      (setq polaris--hl-nlinum-overlay nil
            polaris--hl-nlinum-line nil)
      disp)))

;;;###autoload
(defun polaris|nlinum-hl-line (&optional line)
  "Highlight line number"
  (let ((line-no (or line (string-to-number (format-mode-line "%l")))))
    (if (and nlinum-mode (not (eq line-no polaris--hl-nlinum-line)))
        (let* ((pbol (if line
                         (save-excursion (goto-char 1)
                                         (forward-line line-no)
                                         (line-beginning-position))
                       (line-beginning-position)))
               (peol (1+ pbol))
               (max (point-max)))
          ;; Handle EOF case
          (when (>= peol max)
            (setq peol max))
          (jit-lock-fontify-now pbol peol)
          (let ((ov (-first (lambda (item) (overlay-get item 'nlinum)) (overlays-in pbol peol))))
            (when ov
              (polaris|nlinum-unhl-line)
              (let ((str (nth 1 (get-text-property 0 'display (overlay-get ov 'before-string)))))
                (put-text-property 0 (length str) 'face 'linum-highlight-face str)
                (setq polaris--hl-nlinum-overlay ov
                      polaris--hl-nlinum-line line-no))))))))

(provide 'defuns-nlinum)
;;; defuns-nlinum.el ends here

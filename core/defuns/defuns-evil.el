;; -*- lexical-binding: t -*-
;;; defuns-evil.el
;; for ../core-evil.el

;;;###autoload (autoload 'polaris/evil-open-folds "defuns-evil" nil t)
(evil-define-command polaris/evil-open-folds (count)
  "Instead of `evil-open-folds'. Accepts COUNT for dictating fold level."
  (interactive "P")
  (unless (bound-and-true-p hs-minor-mode)
    (hs-minor-mode 1))
  (if count (hs-hide-level count) (evil-open-folds)))

;;;###autoload (autoload 'polaris/evil-close-folds "defuns-evil" nil t)
(evil-define-command polaris/evil-close-folds (count)
  "Instead of `evil-close-folds'. Accepts COUNT for dictating fold level."
  (interactive "P")
  (unless (bound-and-true-p hs-minor-mode)
    (hs-minor-mode 1))
  (if count (hs-hide-level count) (evil-close-folds)))

;;;###autoload (autoload 'polaris/multi-next-line "defuns-evil" nil t)
(evil-define-motion polaris/multi-next-line (count)
  "Move down 6 lines"
  :type line
  (let ((line-move-visual visual-line-mode))
    (evil-line-move (* 6 (or count 1)))))

;;;###autoload (autoload 'polaris/multi-previous-line "defuns-evil" nil t)
(evil-define-motion polaris/multi-previous-line (count)
  "Move up 6 lines"
  :type line
  (let ((line-move-visual visual-line-mode))
    (evil-line-move (- (* 6 (or count 1))))))

;;;###autoload
(defun polaris/evil-visual-line-state-p ()
  "Returns non-nil if in visual-line mode, nil otherwise."
  (and (evil-visual-state-p)
       (eq (evil-visual-type) 'line)))

;;;###autoload
(defun polaris*evil-exchange-off ()
  (when evil-exchange--overlays
    (evil-exchange-cancel)))

;;;###autoload
(defun polaris/evil-surround-escaped ()
  "Escaped surround characters."
  (let* ((char (string (read-char "\\")))
         (pair (acond ((cdr-safe (assoc (string-to-char char) evil-surround-pairs-alist))
                       `(,(car it) . ,(cdr it)))
                      (t `(,char . ,char))))
         (text (if (sp-point-in-string) "\\\\%s" "\\%s")))
    (cons (format text (car pair))
          (format text (cdr pair)))))

;;;###autoload
(defun polaris/evil-surround-latex ()
  "LaTeX commands"
  (cons (format "\\%s{" (read-string "\\")) "}"))

;;;###autoload (autoload 'polaris/evil-macro-on-all-lines "defuns-evil" nil t)
(evil-define-operator polaris/evil-macro-on-all-lines (beg end &optional macro)
  "Apply macro to each line."
  :motion nil
  :move-point nil
  (interactive "<r><a>")
  (unless (and beg end)
    (setq beg (region-beginning)
          end (region-end)))
  (evil-ex-normal beg end
                  (concat "@"
                          (single-key-description
                           (or macro (read-char "@-"))))))

;;; Custom argument handlers
;;;###autoload
(defun polaris/-ex-match-init (name &optional face update-hook)
  (with-current-buffer evil-ex-current-buffer
    (cond
     ((eq flag 'start)
      (evil-ex-make-hl name
                       :face (or face 'evil-ex-substitute-matches)
                       :update-hook (or update-hook #'evil-ex-pattern-update-ex-info))
      (setq flag 'update))

     ((eq flag 'stop)
      (evil-ex-delete-hl name)))))

;;;###autoload
(defun polaris/-ex-buffer-match (arg &optional hl-name flags beg end)
  (when (and (eq flag 'update)
             evil-ex-substitute-highlight-all
             (not (zerop (length arg))))
    (condition-case lossage
        (let ((pattern (evil-ex-make-substitute-pattern
                        (if evil-ex-bang (regexp-quote arg) arg)
                        (or flags (list))))
              (range (or (evil-copy-range evil-ex-range)
                         (evil-range (or beg (line-beginning-position))
                                     (or end (line-end-position))
                                     'line
                                     :expanded t))))
          (evil-expand-range range)
          (evil-ex-hl-set-region hl-name
                                 (max (evil-range-beginning range) (window-start))
                                 (min (evil-range-end range) (window-end)))
          (evil-ex-hl-change hl-name pattern))
      (end-of-file
       (evil-ex-pattern-update-ex-info nil "incomplete replacement"))
      (user-error
       (evil-ex-pattern-update-ex-info nil (format "?%s" lossage))))))

;;;###autoload
(defun polaris/evil-ex-undefine-cmd (cmd)
  (if (string-match "^[^][]*\\(\\[\\(.*\\)\\]\\)[^][]*$" cmd)
      (let ((abbrev (replace-match "" nil t cmd 1))
            (full (replace-match "\\2" nil nil cmd 1)))
        (setq evil-ex-commands (delq (assoc full evil-ex-commands) evil-ex-commands))
        (setq evil-ex-commands (delq (assoc abbrev evil-ex-commands) evil-ex-commands)))
    (setq evil-ex-commands (delq (assoc cmd evil-ex-commands) evil-ex-commands))))

(defvar polaris:map-maps '())

;;;###autoload (autoload 'polaris:map "defuns-evil" nil t)
(evil-define-command polaris:map (bang input &optional mode)
  "Map ex commands to keybindings. INPUT should be in the format [KEY] [EX COMMAND]."
  (interactive "<!><a>")
  (let* ((parts (s-split-up-to " " input 2 t))
         (mode (or mode 'normal))
         (key (kbd (car parts)))
         (command (s-join " " (cdr parts)))
         (map (cl-case mode
                ('normal evil-normal-state-local-map)
                ('insert evil-insert-state-local-map)
                ('visual evil-visual-state-local-map)
                ('motion evil-motion-state-local-map)
                ('operator evil-operator-state-local-map)))
         (fn `(lambda () (interactive) (evil-ex-eval ,command))))
    (if bang
        (evil-define-key mode nil key fn)
      (define-key map key fn))))

;;;###autoload (autoload 'polaris:nmap "defuns-evil" nil t)
(evil-define-command polaris:nmap (bang input &optional mode)
  (interactive "<!><a>") (polaris:map bang input 'normal))

;;;###autoload (autoload 'polaris:imap "defuns-evil" nil t)
(evil-define-command polaris:imap (bang input &optional mode)
  (interactive "<!><a>") (polaris:map bang input 'insert))

;;;###autoload (autoload 'polaris:vmap "defuns-evil" nil t)
(evil-define-command polaris:vmap (bang input &optional mode)
  (interactive "<!><a>") (polaris:map bang input 'visual))

;;;###autoload (autoload 'polaris:mmap "defuns-evil" nil t)
(evil-define-command polaris:mmap (bang input &optional mode)
  (interactive "<!><a>") (polaris:map bang input 'motion))

;;;###autoload (autoload 'polaris:omap "defuns-evil" nil t)
(evil-define-command polaris:omap (bang input &optional mode)
  (interactive "<!><a>") (polaris:map bang input 'operator))

;;;###autoload
(defun polaris/evil-snipe-easymotion ()
  (interactive)
  (require 'evil-easymotion)
  (call-interactively polaris--evil-snipe-repeat-fn))

(provide 'defuns-evil)
;;; defuns-evil.el ends here

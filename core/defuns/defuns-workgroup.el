;;; defuns-workgroup.el

;;;###autoload
(defun polaris|wg-cleanup ()
  (polaris/popup-close-all)
  (when (and (featurep 'neotree) (neo-global--window-exists-p))
    (neotree-hide)))

;;;###autoload
(defun polaris/wg-projectile-switch-project ()
  (let ((project-root (polaris/project-root)))
    (polaris:workgroup-new nil (file-name-nondirectory (directory-file-name project-root)) t)
    (polaris|update-scratch-buffer-cwd project-root)
    (when (featurep 'neotree)
      (neotree-projectile-action))))

;;;###autoload (autoload 'polaris:save-session "defuns-workgroup" nil t)
(evil-define-command polaris:save-session (&optional bang session-name)
  (interactive "<!><a>")
  (unless (wg-workgroup-list)
    (wg-create-workgroup wg-first-wg-name))
  (polaris|wg-cleanup)
  (wg-save-session-as (if session-name
                          (concat wg-workgroup-directory session-name)
                        (if bang
                            (concat wg-workgroup-directory (f-filename (polaris/project-root)))
                          wg-session-file))))

;;;###autoload (autoload 'polaris:load-session "defuns-workgroup" nil t)
(evil-define-command polaris:load-session (&optional bang session-name)
  (interactive "<!><a>")
  (let ((session-file (if session-name
                          (concat wg-workgroup-directory session-name)
                        (let ((sess (concat wg-workgroup-directory (f-filename (polaris/project-root)))))
                          (if bang
                              (when (file-exists-p sess)
                                sess)
                            wg-session-file)))))
    (unless session-file
      (user-error "No session found"))
    (wg-open-session session-file))
  (polaris/workgroup-display t))

;;;###autoload
(defun polaris/clear-sessions ()
  "Delete all session files."
  (interactive)
  (mapc 'delete-file (f-glob (expand-file-name "*" wg-workgroup-directory))))

;;;###autoload (autoload 'polaris:workgroup-new "defuns-workgroup" nil t)
(evil-define-command polaris:workgroup-new (bang name &optional silent)
  "Create a new workgroup. If BANG, overwrite any workgroup named NAME."
  (interactive "<!><a>")
  (unless name
    (setq name (format "#%s" (1+ (length (wg-workgroup-list))))))
  (let ((new-wg (wg-get-workgroup name t)))
    (when (and new-wg bang)
      (wg-delete-workgroup new-wg)
      (setq new-wg nil))
    (setq new-wg (or new-wg (wg-make-and-add-workgroup name t)))
    (add-to-list 'polaris-wg-names (wg-workgroup-uid new-wg))
    (wg-switch-to-workgroup new-wg))
  (unless silent
    (polaris--workgroup-display (wg-previous-workgroup t)
                             (format "Created %s" name)
                             'success)))

;;;###autoload (autoload 'polaris:workgroup-rename "defuns-workgroup" nil t)
(evil-define-command polaris:workgroup-rename (bang &optional new-name)
  (interactive "<!><a>")
  (let* ((wg (wg-current-workgroup))
         (wg-uid (wg-workgroup-uid wg))
         (old-name (wg-workgroup-name wg)))
    (if bang
        (setq polaris-wg-names (delete wg-uid polaris-wg-names))
      (unless new-name
        (user-error "You didn't enter in a name"))
      (wg-rename-workgroup new-name wg)
      (add-to-list 'polaris-wg-names wg-uid)
      (polaris--workgroup-display wg (format "Renamed '%s'->'%s'" old-name new-name) 'success))))

;;;###autoload (autoload 'polaris:workgroup-delete "defuns-workgroup" nil t)
(evil-define-command polaris:workgroup-delete (&optional bang name)
  (interactive "<!><a>")
  (let* ((current-wg (wg-current-workgroup))
         (wg-name (or name (wg-workgroup-name current-wg))))
    (when bang
      (setq wg-name (wg-read-workgroup-name)))
    (let ((wg (wg-get-workgroup name)))
      (setq polaris-wg-names (delete (wg-workgroup-uid wg) polaris-wg-names))
      (if (eq wg current-wg)
          (wg-kill-workgroup)
        (wg-delete-workgroup wg))
      (polaris--workgroup-display nil (format "Deleted %s" wg-name) 'success))))

;;;###autoload
(defun polaris:kill-other-workgroups ()
  "Kill all other workgroups."
  (interactive)
  (let (workgroup (wg-current-workgroup))
    (dolist (w (wg-workgroup-list))
      (unless (wg-current-workgroup-p w)
        (wg-kill-workgroup w)))))

(defun polaris--num-to-unicode (num)
  "Return a nice unicode representation of a single-digit number STR."
  (cl-case num
   (1 "➊")
   (2 "➋")
   (3 "➌")
   (4 "➍")
   (5 "➎")
   (6 "❻")
   (7 "➐")
   (8 "➑")
   (9 "➒")
   (0 "➓")))

(defun polaris--workgroup-display (&optional suppress-update message message-face)
  (message "%s%s" (polaris/workgroup-display suppress-update t)
           (propertize message 'face message-face)))

;;;###autoload
(defun polaris/workgroup-display (&optional suppress-update return-p message)
  (interactive)
  (when (wg-current-session t)
    (unless (eq suppress-update t)
      (polaris/workgroup-update-names (if (wg-workgroup-p suppress-update) suppress-update)))
    (let ((output (wg-display-internal
                   (lambda (workgroup index)
                     (if (not workgroup) wg-nowg-string
                       (wg-element-display
                        workgroup
                        (format " %s %s " (polaris--num-to-unicode (1+ index)) (wg-workgroup-name workgroup))
                        'wg-current-workgroup-p)))
                   (wg-workgroup-list))))
      (if return-p
          output
        (message "%s%s" output (or message ""))))))

;;;###autoload
(defun polaris/workgroup-update-names (&optional wg)
  (let ((wg (or wg (wg-current-workgroup))))
    (unless (member (wg-workgroup-uid wg) polaris-wg-names)
      (ignore-errors
        (let ((old-name (wg-workgroup-name wg))
              (new-name (f-filename (polaris/project-root))))
          (unless (string= new-name old-name)
            (wg-rename-workgroup new-name wg)))))))

(defun polaris--switch-to-workgroup (direction &optional count)
  (interactive "<c>")
  (assert (memq direction '(left right)))
  (condition-case err
      (progn
        (if count
            (wg-switch-to-workgroup-at-index (1- count))
          (funcall (intern (format "wg-switch-to-workgroup-%s" direction))))
        (polaris/workgroup-display t))
      (error (polaris/workgroup-display t nil (format "Nope! %s" (cadr err))))))

;;;###autoload (autoload 'polaris:switch-to-workgroup-left "defuns-workgroup" nil t)
(evil-define-command polaris:switch-to-workgroup-left (count)
  (interactive "<c>")
  (polaris--switch-to-workgroup 'left))

;;;###autoload (autoload 'polaris:switch-to-workgroup-right "defuns-workgroup" nil t)
(evil-define-command polaris:switch-to-workgroup-right (count)
  (interactive "<c>")
  (polaris--switch-to-workgroup 'right))

;;;###autoload
(defun polaris:switch-to-workgroup-at-index (index)
  (interactive)
  (polaris/workgroup-update-names)
  (let ((wg (nth index (wg-workgroup-list-or-error)))
        msg)
    (if wg
        (unless (eq wg (wg-current-workgroup t))
          (wg-switch-to-workgroup-at-index index))
      (setq msg (format "No tab #%s" (1+ index))))
    (polaris/workgroup-display t nil msg)))

;;;###autoload
(defun polaris/undo-window-change ()
  (interactive)
  (call-interactively (if (wg-current-workgroup t) 'wg-undo-wconfig-change 'winner-undo)))

;;;###autoload
(defun polaris/redo-window-change ()
  (interactive)
  (call-interactively (if (wg-current-workgroup t) 'wg-redo-wconfig-change 'winner-redo)))

;;;###autoload
(defun polaris/close-window-or-workgroup ()
  (interactive)
  (if (memq (get-buffer-window) polaris-popup-windows)
      (polaris/popup-close)
    (polaris/kill-real-buffer)
    (if (and (one-window-p t)
             (> (length (wg-workgroup-list)) 1))
        (if (string= (wg-workgroup-name (wg-current-workgroup)) wg-first-wg-name)
            (evil-window-delete)
          (polaris:workgroup-delete))
      (evil-window-delete))))

(provide 'defuns-workgroup)
;;; defuns-workgroup.el ends here

;;;###autoload
(defun polaris|spaceline-env-update ()
  (when polaris--env-command
    (let ((default-directory (polaris/project-root)))
      (let ((s (shell-command-to-string polaris--env-command)))
        (setq polaris--env-version (if (string-match "[ \t\n\r]+\\'" s)
                                       (replace-match "" t t s)
                                     s))))))

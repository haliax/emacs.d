;; -*- lexical-binding: t -*-
;;; defuns-popup.el

;;;###autoload
(defun polaris/popup-remove (window)
  (setq polaris-popup-windows (delete window polaris-popup-windows)))

 ;;;###autoload
(defun polaris/popup-p (&optional window)
  "Whether WINDOW is a shackle popup window or not."
  (and polaris-popup-windows
       (-any? (lambda (w)
                (if (window-live-p w) t (polaris/popup-remove w) nil))
              polaris-popup-windows)
       (if window
           (-any? (lambda (w) (eq window w)) polaris-popup-windows)
         t)))

;;;###autoload
(defmacro polaris/popup-save (&rest body)
  `(let ((popup-p (polaris/popup-p)))
     (when popup-p (polaris/popup-close-all t))
     ,@body
     (when popup-p
       (save-selected-window
         (polaris/popup-last-buffer)))))

 ;;;###autoload
(defun polaris/popup-buffer (buffer &optional plist)
  "Display BUFFER in a shackle popup."
  (let ((buffer-name (if (stringp buffer) buffer (buffer-name buffer))))
    (shackle-display-buffer (get-buffer-create buffer-name)
                            nil (or plist (shackle-match buffer-name)))))

 ;;;###autoload
(defun polaris/popup-close (&optional window dont-kill dont-close-all)
  "Find and close the currently active popup (if available)."
  (interactive)
  (when (not window)
    (if (polaris/popup-p (selected-window))
        (setq window (selected-window))
      (unless dont-close-all
        (polaris/popup-close-all dont-kill))))
  (when (and window (window-live-p window))
    ;; REPL buffer
    (cond ((and (derived-mode-p 'comint-mode)
                (featurep 'repl-toggle)
                repl-toggle-mode)
           (setq rtog/--last-buffer nil))
          ((eq major-mode 'messages-buffer-mode)
           (bury-buffer)
           (setq dont-kill t)))
    (polaris/popup-remove window)
    (unless dont-kill
      (let ((kill-buffer-query-functions (delq 'process-kill-buffer-query-function kill-buffer-query-functions)))
        (kill-buffer (window-buffer window))))
    (delete-window window)))

 ;;;###autoload
(defun polaris/popup-close-all (&optional dont-kill-buffers)
  "Closes all popup windows (and kills the buffers if DONT-KILL-BUFFERS is non-nil)"
  (interactive)
  (mapc (lambda (w) (polaris/popup-close w dont-kill-buffers))
        polaris-popup-windows)
  (setq polaris-popup-windows nil))

 ;;;###autoload
(defun polaris/popup-toggle ()
  "Toggles the popup window, reopening the last popup (if available)."
  (interactive)
  (if (polaris/popup-p)
      (polaris/popup-close t)
    (polaris/popup-last-buffer)))

;;;###autoload
(defun polaris/popup-last-buffer ()
  "Pop up the last popup buffer."
  (interactive)
  (unless shackle-last-buffer
    (error "No popup to restore."))
  (polaris/popup-buffer shackle-last-buffer))

;;;###autoload
(defun polaris/popup-messages ()
  "Pop up the *Messages* buffer."
  (interactive)
  (polaris/popup-buffer "*Messages*")
  (with-current-buffer "*Messages*" (polaris|hide-mode-line)
                       (goto-char (point-max))))

;;;###autoload
(defun polaris|popup-init ()
  (add-to-list 'polaris-popup-windows (get-buffer-window))
  (local-set-key [escape escape] 'polaris/popup-close)
  (when (or (bound-and-true-p repl-toggle-mode)
            (derived-mode-p 'tabulated-list-mode)
            (memq major-mode '(messages-buffer-mode flycheck-error-list-mode-hook esup-mode)))
    (let ((map evil-normal-state-local-map))
      (define-key map [escape] 'polaris/popup-close)
      (define-key map (kbd "ESC") 'polaris/popup-close))))

(defvar shackle-popup-hook '() "Hook run whenever a popup is opened.")

;;;###autoload
(defun polaris|run-popup-hooks (&rest _)
  (with-current-buffer shackle-last-buffer
    (run-hooks 'shackle-popup-hook)))

(provide 'defuns-popup)
;;; defuns-popup.el ends here

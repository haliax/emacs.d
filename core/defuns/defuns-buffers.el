;; -*- lexical-binding: t -*-
;;; defuns-buffers.el

;;;###autoload (autoload 'polaris:narrow "defuns-buffers" nil t)
(evil-define-operator polaris:narrow (&optional beg end bang)
  "Restrict editing in this buffer to the current region, indirectly. With BANG,
clone the buffer and hard-narrow the selection. Otherwise use fancy-narrow. If
mark isn't active, then widen the buffer (if narrowed).

Inspired from http://demonastery.org/2013/04/emacs-evil-narrow-region/"
  (interactive "<r><!>")
  (if (region-active-p)
      (progn
        (deactivate-mark)
        (let ((buf (clone-indirect-buffer nil nil)))
          (with-current-buffer buf
            (narrow-to-region beg end))
          (switch-to-buffer buf)))
    (widen)))

;;;###autoload
(defun polaris/set-read-only-region (begin end)
  "See http://stackoverflow.com/questions/7410125"
  (let ((modified (buffer-modified-p)))
    (add-text-properties begin end '(read-only t))
    (set-buffer-modified-p modified)))

;;;###autoload
(defun polaris/set-region-writeable (begin end)
  "See http://stackoverflow.com/questions/7410125"
  (let ((modified (buffer-modified-p))
        (inhibit-read-only t))
    (remove-text-properties begin end '(read-only t))
    (set-buffer-modified-p modified)))


;; Buffer Life and Death ;;;;;;;;;;;;;;;

(unless (display-graphic-p)
  (defalias 'wg-workgroup-associated-buffers 'ignore)
  (defalias 'wg-current-workgroup 'ignore)
  (defalias 'wg-save-session 'ignore))

;;;###autoload
(defun polaris/get-buffers (&optional project-p)
  "Get all buffers in the current workgroup.

    If PROJECT-P is non-nil, get all buffers in current workgroup
    If both are non-nil, get all project buffers across all workgroups"
  (let* ((assocbuf (wg-workgroup-associated-buffers nil))
         (buffers (if (wg-current-workgroup t)
                      (--filter (memq it assocbuf) (buffer-list))
                    (buffer-list)))
         project-root)
    (aif (and project-p (polaris/project-root t))
        (funcall (if (eq project-p 'not) '-remove '-filter)
                 (lambda (b) (projectile-project-buffer-p b it))
                 buffers)
      buffers)))

;;;###autoload
(defun polaris/get-buffer-names (&optional project-p)
  (mapcar (lambda (b) (buffer-name b))
          (polaris/get-buffers project-p)))

;;;###autoload
(defun polaris/get-visible-windows (&optional buffer-list)
  (-map #'get-buffer-window
        (polaris/get-visible-buffers (or buffer-list (polaris/get-buffers)))))

;;;###autoload
(defun polaris/get-visible-buffers (&optional buffer-list)
  "Get a list of buffers that are not buried (i.e. visible)"
  (-filter #'get-buffer-window (or buffer-list (polaris/get-buffers))))

;;;###autoload
(defun polaris/get-buried-buffers (&optional buffer-list)
  "Get a list of buffers that are buried (i.e. not visible)"
  (let* ((buffers (or buffer-list (polaris/get-buffers)))
         (old-len (length buffers)))
    (-remove 'get-buffer-window buffers)))

;;;###autoload
(defun polaris/get-matching-buffers (pattern &optional buffer-list)
  "Get a list of buffers that match the pattern"
  (--filter (string-match-p pattern (buffer-name it))
            (or buffer-list (polaris/get-buffers))))

;;;###autoload
(defun polaris/get-buffers-in-modes (modes &optional buffer-list)
  "Get a list of buffers whose major-mode is one of MODES"
  (--filter (with-current-buffer it (memq major-mode modes))
            (or buffer-list (polaris/get-buffers))))

;;;###autoload
(defun polaris/get-buffers-in-modes (modes &optional buffer-list)
   "Get a list of buffers whose major-mode is one of MODES"
   (--filter (with-current-buffer it (memq major-mode modes))
             (or buffer-list (polaris/get-buffers))))

;;;###autoload
(defun polaris/get-real-buffers (&optional buffer-list)
  (-filter #'polaris/real-buffer-p (or buffer-list (polaris/get-buffers))))

;;;###autoload
(defun polaris/kill-real-buffer ()
  "Kill buffer (but only bury scratch buffer), then switch to a real buffer. Only buries
the buffer if it is being displayed in another window."
  (interactive)
  (let (new-dir)
    (if (string-match-p "^\\*scratch\\*" (or (buffer-name) ""))
        (message "Already in the scratch buffer")
      (setq new-dir (polaris/project-root))
      (if (> (length (get-buffer-window-list (current-buffer) nil t)) 1)
          (bury-buffer)
        (kill-this-buffer)))
    (if (polaris/popup-p (selected-window))
        (polaris/popup-close)
      (unless (polaris/real-buffer-p (current-buffer))
        (polaris/previous-real-buffer)
        (polaris|update-scratch-buffer-cwd)))))

;;;###autoload
(defun polaris/kill-unreal-buffers ()
  "Kill all buried, unreal buffers in current frame. See `polaris-unreal-buffers'"
  (interactive)
  (let* ((all-buffers (polaris/get-buffers))
         (real-buffers (polaris/get-real-buffers all-buffers))
         (kill-list (--filter (not (memq it real-buffers))
                              (polaris/get-buried-buffers all-buffers))))
    (mapc 'kill-buffer kill-list)
    (polaris/kill-process-buffers)
    (message "Cleaned up %s buffers" (length kill-list))))

;;;###autoload
(defun polaris/kill-process-buffers ()
  "Kill all buffers that represent running processes and aren't visible."
  (interactive)
  (let ((buffer-list (polaris/get-buffers))
        (killed-procesess 0))
    (dolist (p (process-list))
      (let* ((process-name (process-name p))
             (assoc (assoc process-name polaris-cleanup-processes-alist)))
        (when (and assoc
                   (not (string= process-name "server"))
                   (process-live-p p)
                   (not (--any? (let ((mode (buffer-local-value 'major-mode it)))
                                  (eq mode (cdr assoc)))
                                buffer-list)))
          (delete-process p)
          (incf killed-processes))))
    (message "Cleaned up %s processes" killed-processes)))

;;;###autoload
(defun polaris/kill-matching-buffers (regexp &optional buffer-list)
  (interactive)
  (let ((i 0))
    (mapc (lambda (b)
            (when (string-match-p regexp (buffer-name b))
              (kill-buffer b)
              (setq i (1  i))))
          (if buffer-list buffer-list (polaris/get-buffers)))
    (message "Killed %s matches" i)))

;;;###autoload
(defun polaris/cycle-real-buffers (&optional n)
  "Switch to the previous buffer and avoid special buffers. If there's nothing
left, create a scratch buffer."
  (let* ((start-buffer (current-buffer))
         (move-func (if (< n 0) 'switch-to-next-buffer 'switch-to-prev-buffer))
         (real-buffers (polaris/get-real-buffers))
         (realc (length real-buffers))
         (max 25)
         (i 0)
         (continue t))
    (if (or (= realc 0)
            (and (= realc 1) (eq (car real-buffers) (current-buffer))))
        (progn
          (polaris|update-scratch-buffer-cwd)
          (switch-to-buffer "*scratch*")
          (message "Nowhere to go"))
      (funcall move-func)
      (while (and continue)
        (let ((current-buffer (current-buffer)))
          (cond ((or (eq current-buffer start-buffer)
                     (>= i max))
                 (polaris|update-scratch-buffer-cwd)
                 (switch-to-buffer "*scratch*")
                 (setq continue nil))
                ((not (memq current-buffer real-buffers))
                 (funcall move-func))
                (t
                 (setq continue nil))))
        (cl-incf i)))))

;;;###autoload
(defun polaris/real-buffer-p (&optional buffer-or-name)
  (let ((buffer (if buffer-or-name (get-buffer buffer-or-name) (current-buffer))))
    (when (buffer-live-p buffer)
      (not (--any? (if (stringp it)
                       (string-match-p it (buffer-name buffer))
                     (eq (buffer-local-value 'major-mode buffer) it))
                   polaris-unreal-buffers)))))

;; Inspired by spacemacs <https://github.com/syl20bnr/spacemacs/blob/master/spacemacs/funcs.el>
;;;###autoload
(defun polaris/next-real-buffer ()
  "Switch to the next buffer and avoid special buffers."
  (interactive)
  (polaris/cycle-real-buffers  1))

;;;###autoload
(defun polaris/previous-real-buffer ()
  "Switch to the previous buffer and avoid special buffers."
  (interactive)
  (polaris/cycle-real-buffers -1))


(defun polaris--kill-buffers (buffers &optional filter-func)
  (let ((buffers (if filter-func (-filter filter-func buffers) buffers))
        (affected 0))
    (mapc (lambda (b) (when (kill-buffer b) (incf affected))) buffers)
    (unless (polaris/real-buffer-p)
      (polaris/previous-real-buffer))
    (message "Killed %s buffers" affected)))

;;;###autoload (autoload 'polaris:kill-all-buffers "defuns-buffers" nil t)
(evil-define-command polaris:kill-all-buffers (&optional bang)
  "Kill all project buffers. If BANG, kill *all* buffers (in workgroup)."
  (interactive "<!>")
  (polaris--kill-buffers (polaris/get-buffers (not bang)))
  (mapc (lambda (w) (when (eq (window-buffer w) (get-buffer "*scratch*"))
                 (delete-window w)))
        (polaris/get-visible-windows)))

;;;###autoload (autoload 'polaris:kill-other-buffers "defuns-buffers" nil t)
(evil-define-command polaris:kill-other-buffers (&optional bang)
  "Kill all other project buffers. If BANG, kill *all* other buffers (in workgroup)."
  (interactive "<!>")
  (polaris--kill-buffers (polaris/get-buffers (not bang))
                         (lambda (b) (not (eq b (current-buffer)))))
  (when bang
    (delete-other-windows)))

;;;###autoload (autoload 'polaris:kill-buried-buffers "defuns-buffers" nil t)
(evil-define-command polaris:kill-buried-buffers (&optional bang)
  "Kill buried project buffers (in workgroup) and report how many it found. BANG = get all
buffers regardless of project."
  (interactive "<!>")
  (polaris--kill-buffers (polaris/get-buried-buffers (polaris/get-buffers (not bang)))))

;;;###autoload (autoload 'polaris:kill-buried-buffers "defuns-buffers" nil t)
(evil-define-command polaris:kill-matching-buffers (&optional bang pattern)
  "Kill project buffers matching regex pattern PATTERN. If BANG, then extend search to
buffers regardless of project."
  :repeat nil
  (interactive "<!><a>")
  (polaris-kill-buffers (polaris/get-matching-buffers pattern (polaris/get-buffers (not bang)))))

;;;###autoload (autoload 'polaris:send-to-scratch-or-org "defuns-buffers" nil t)
(evil-define-operator polaris:send-to-scratch-or-org (&optional beg end bang)
  "Send a selection to the scratch buffer. If BANG, then send it to org-capture instead."
  :move-point nil
  :type inclusive
  (interactive "<r><!>")
  (let ((mode major-mode)
        (text (when (and (evil-visual-state-p) beg end)
                (buffer-substring beg end))))
    (if bang
        (org-capture-string text)
      ;; or scratch buffer by default
      (let* ((project-dir (polaris/project-root t))
             (buffer-name "*scratch*"))
        (polaris/popup-buffer buffer-name)
        (with-current-buffer buffer-name
         (when project-dir
            (cd project-dir))
          (if text (insert text))
          (funcall mode))
        ))))

;;;###autoload (autoload 'polaris:cd "defuns-buffers" nil t)
(evil-define-command polaris:cd (dir)
  "Ex-command alias for `cd'"
  :repeat nil
  (interactive "<f>")
  (cd (if (zerop (length dir)) "~" dir)))

;;;###autoload
(defun polaris/kill-all-buffers-do-not-remember ()
  "Kill all buffers so that workgroups2 will wipe its current session."
  (interactive)
  (let ((confirm-kill-emacs nil))
    (mapc 'kill-buffer (polaris/get-buffers))
    (kill-this-buffer)
    (delete-other-windows)
    (wg-save-session t)
    (save-buffers-kill-terminal)))

;;;###autoload
(defun polaris/kill-other-buffers ()
  "Kill all buffers but the current one.
Doesn't mess with special buffers."
  (interactive)
  (when (y-or-n-p "Are you sure you want to kill all buffers but the current one? ")
    (seq-each
     #'kill-buffer
     (delete (current-buffer) (seq-filter #'buffer-file-name (buffer-list))))))

(provide 'defuns-buffers)
;;; defuns-buffers.el ends here

;; -*- lexical-binding: t -*-
;; defuns-neotree.el
;; for ../core-project.el

;;;###autoload
(defun polaris/neotree ()
  "Toggle the neotree window"
  (interactive)
  (let ((in-neotree (and (neo-global--window-exists-p)
                         (window-live-p neo-global--buffer)
                         (eq (current-buffer) neo-global--buffer)))
        (path buffer-file-name))
    (if in-neotree
        (neotree-hide)
      (let ((project-root (polaris/project-root)))
        (unless (and (neo-global--window-exists-p)
                     (f-same? (neo-global--with-window neo-buffer--start-node) project-root))
          (neotree-dir project-root))
        (neotree-find path project-root)))))

;;;###autoload
(defmacro polaris/neotree-save (&rest body)
  `(let ((neo-p (neo-global--window-exists-p)))
     (when neo-p (neotree-hide))
     ,@body
     (when neo-p
       (save-selected-window
         (neotree-show)))))

;;;###autoload
(defun polaris|neotree-close-on-window-change (&rest _)
  "Close neotree to prevent ensuing mindow buggery."
  (unless (and (neo-global--window-exists-p)
               (eq (current-buffer) (neo-global--get-buffer)))
    (neotree-hide)))

;;;###autoload
(defun polaris*neo-buffer-fold-symbol (name)
  "Custom hybrid ascii theme with leading whitespace."
  (let ((n-insert-symbol (lambda (n)
                           (neo-buffer--insert-with-face
                            n 'neo-expand-btn-face))))
    (or (and (eq name 'open)  (funcall n-insert-symbol "- "))
        (and (eq name 'close) (funcall n-insert-symbol "+ "))
        (and (eq name 'leaf)  (funcall n-insert-symbol "  ")))))

(provide 'defuns-neotree)
;;; defuns-neotree.el ends here

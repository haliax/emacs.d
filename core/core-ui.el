;; -*- lexical-binding: t -*-
;;; core-ui.el --- interface settings

(setq-default
 blink-matching-paren nil               ; don't blink matching delimiters--too distracting
 show-paren-delay 0.075

 ;; Multiple cursors across buffers cause a strange redraw delay for
 ;; some things, like auto-complete or evil-mode's cursor color
 ;; switching.
 cursor-in-non-selected-windows nil ; no cursors except in active buffer
 highlight-nonselected-windows nil
 hl-line-sticky-flag nil ; only highlight in one window

 uniquify-buffer-name-style nil ; my mode-line does this for me
 visible-bell nil
 visible-cursor nil
 x-stretch-cursor t
 x-select-enable-clipboard t
 select-enable-clipboard t
 x-underline-at-descent-line t
 frame-resize-pixelwise t      ; allow frame size to inc/dec by a pixel
 use-dialog-box nil            ; always avoid GUI
 redisplay-dont-pause t        ; don't pause display on input
 indicate-buffer-boundaries nil  ; show indicators where buffer starts/ends
 indicate-empty-lines nil        ; show indicators on empty lines
 fringes-outside-margins t     ; switches order of fringe and margin
 split-width-threshold nil     ; favor horizontal splits
 show-help-function nil        ; hide :help-echo text

 custom-safe-themes t       ; Treat themes as safe
 line-spacing 0.2       ; Increase line-spacing (default 0)
 history-length 1000    ; Store more history

 ;; Disable bidirectional text support for slight performance bonus
 bidi-display-reordering nil

 jit-lock-defer-time nil
 jit-lock-stealth-time 0.2
 jit-lock-stealth-nice 0.1
 jit-lock-stealth-verbose nil

 ;; Minibuffer resizing
 resize-mini-windows 'grow-only
 max-mini-window-height 0.3

 ;; Remove arrow on the right fringe when wrapped
 fringe-indicator-alist (delq (assoc 'continuation fringe-indicator-alist)
                              fringe-indicator-alist))

(setq frame-resize-pixelwise t          ; Resize by pixels
      frame-title-format
      '(:eval (if (buffer-file-name)
                  (abbreviate-file-name (buffer-file-name)) "%b"))
      ;; Size new windows proportionally wrt other windows
      window-combination-resize t)

(fset 'yes-or-no-p 'y-or-n-p)           ; y/n instead of yes/no

;; Ask for confirmation on exit only if there are real buffers left
(when window-system
  (setq confirm-kill-emacs
        (lambda (_)
          (if (polaris/get-real-buffers)
              (y-or-n-p ">> Gee, I dunno Brain... Are you sure?")
            t))))

(put 'set-goal-column 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'erase-buffer 'disabled nil)

;; Enable font-lock or syntax highlighting globally
(global-font-lock-mode 1)

(setq font-lock-maximum-decoration t)

(blink-cursor-mode -1)    ; don't blink cursor
(tooltip-mode      -1)    ; show tooltips in echo area

;; Set up minibuffer and fringe
(if (not window-system)
    (menu-bar-mode -1)
  (scroll-bar-mode -1)  ; no scrollbar
  (tool-bar-mode   -1)  ; no toolbar

  ;; full filename in frame title
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))

  ;; Set fonts
  ;; (polaris/load-font polaris-default-font)
  ;; (set-face-attribute 'default t :font polaris-default-font)

  ;; Standarize fringe width
  (fringe-mode polaris-fringe-size)

  ;; Show tilde in margin on empty lines
  (define-fringe-bitmap 'tilde [64 168 16] nil nil 'center)
  (setcdr (assq 'empty-line fringe-indicator-alist) 'tilde)
  (set-fringe-bitmap-face 'tilde 'font-lock-comment-face)

  ;; Brighter minibuffer when active + no fringe in minibuffer
  (defface polaris-minibuffer-active '((t (:inherit mode-line)))
    "Face for active minibuffer")
  (add-hook! minibuffer-setup
    (set-window-fringes (selected-window) 0 0 nil)
    (make-local-variable 'face-remapping-alist)
    (add-to-list 'face-remapping-alist '(default polaris-minibuffer-active)))
  (add-hook! 'after-init-hook (set-window-fringes (minibuffer-window) 0 0 nil)))

(set-face-attribute 'default nil
                    :family "Source Code Pro" :height 130)
(set-face-attribute 'variable-pitch nil
                    :family "Source Sans Pro" :height 200 :weight 'bold)

;; Make the italics show as actual italics. For some unknown reason, the below
;; is needed to render the italics in org-mode. The issue could be related to
;; the fonts in use. But having this doesn't hurt regardless.
(set-face-attribute 'italic nil :inherit nil :slant 'italic)

(defun polaris-configure-fonts (frame)
  "Set up fonts for FRAME.

Set the default font, and configure various overrides for
symbols, emojis, greek letters, as well as fall backs for."
  ;; Additional fonts for special characters and fallbacks
  ;; Test range: 🐷 ❤ ⊄ ∫ 𝛼 α 🜚 Ⓚ

  (dolist (script '(symbol mathematical))
    (set-fontset-font t script (font-spec :size 20 :family "XITS Math")
                      frame 'prepend))

  ;; Define a font set stack for symbols, greek and math characters
  (dolist (script '(symbol greek mathematical))
    (set-fontset-font t script (font-spec :size 20 :family "DejaVu Sans Mono")
                      frame 'prepend)
    (set-fontset-font t script (font-spec :size 20 :family "Hack")
                      frame 'prepend)
    (set-fontset-font t script (font-spec :size 20 :family "Monoid" :weight 'light)
                      frame 'prepend))

  ;; (set-fontset-font t 'symbol (font-spec :family "Emoji One Color" :size 20)
  ;;                   frame 'prepend)

  (set-fontset-font t 'symbol (font-spec :family "FontAwesome" :size 20)
                    frame 'prepend)

  (set-fontset-font t nil (font-spec :family "Symbola" :size 20)
                    frame 'append))

(-when-let (frame (selected-frame))
  (polaris-configure-fonts frame))
(add-hook 'after-make-frame-functions #'polaris-configure-fonts)

;; Frames
(use-package frame
  :bind ("C-c w f" . toggle-frame-fullscreen)
  :init
  (progn
    ;; Kill `suspend-frame'
    (unbind-key "C-z")
    (unbind-key "C-x C-z")))

(if (fboundp 'menu-bar-mode) (menu-bar-mode -1)) ; do not show the menu bar with File|Edit|Options|...
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1)) ; disable the scroll bars

;;; Show Paren
;; Highlight closing parentheses; show the name of the body being closed with
;; the closing parentheses in the minibuffer.
(show-paren-mode 1)

;; Try to display unicode characters without upsetting line-hieght (as much as possible)
(mapc (lambda (set)
        (let ((font (car set))
              (chars  (cadr set))
              (size  (caddr set)))
          (mapc (lambda (x) (set-fontset-font
                        "fontset-default" `(,x . ,x)
                        (font-spec :name font :size size) nil 'prepend))
                chars)))
      '(("DejaVu Sans" (?☑ ?☐ ?✍ ?⚠ ?★ ?λ
                           ?➊ ?➋ ?➌ ?➍ ?➎ ?❻ ?➐ ?➑ ?➒ ?➓))
        ;; File attachment symbols (for org-mode)
        ("FontAwesome" (? ? ? ? ? ? ? ? ?) 13)
        ;; Certain math symbols
        ("Hack"        (?× ?∙ ?÷ ?⌉ ?⌈ ?⌊ ?⌋
                           ?∩ ?∪ ?⊆ ?⊂ ?⊄ ?⊇ ?⊃ ?⊅
                           ?⇒ ?⇐ ?⇔ ?↔ ?→ ?≡ ?∴ ?∵ ?⊕ ?∀ ?∃ ?∄ ?∈ ?∉
                           ?∨ ?∧ ?¬))))

;; on by default in Emacs 25; I prefer to enable on a mode-by-mode basis, so disable it
(when (and (> emacs-major-version 24) (featurep 'eldoc))
  (global-eldoc-mode -1))

;; Highlight TODO/FIXME/NOTE tags
(defface polaris-todo-face  '((t (:inherit font-lock-warning-face))) "Face for TODOs")
(defface polaris-fixme-face '((t (:inherit font-lock-warning-face))) "Face for FIXMEs")
(defface polaris-note-face  '((t (:inherit font-lock-warning-face))) "Face for NOTEs")
(add-hook! (prog-mode emacs-lisp-mode)
  (font-lock-add-keywords
   nil '(("\\<\\(TODO\\((.+)\\)?:?\\)"  1 'polaris-todo-face prepend)
         ("\\<\\(FIXME\\((.+)\\)?:?\\)" 1 'polaris-fixme-face prepend)
         ("\\<\\(NOTE\\((.+)\\)?:?\\)"  1 'polaris-note-face prepend))))

;; Fade out when unfocused ;;;;;;;;;;;;;
(add-hook! focus-in  (set-frame-parameter nil 'alpha 100))
(add-hook! focus-out (set-frame-parameter nil 'alpha 85))

;; Hide mode-line in help/compile window
(add-hook 'help-mode-hook 'polaris|hide-mode-line)
(add-hook 'compilation-mode-hook 'polaris|hide-mode-line)

;;
;; Plugins
;;

(use-package hl-line
  :init
  (add-hook! (prog-mode markdown-mode) 'hl-line-mode) ; line highlighting
  :config
  (defvar-local polaris--hl-line-mode nil)

  (defun polaris|hl-line-on ()  (if polaris--hl-line-mode (hl-line-mode +1)))
  (defun polaris|hl-line-off () (if polaris--hl-line-mode (hl-line-mode -1)))

  (add-hook! hl-line-mode (if hl-line-mode (setq polaris--hl-line-mode t)))
  ;; Disable line highlight in visual mode
  (add-hook 'evil-visual-state-entry-hook 'polaris|hl-line-off)
  (add-hook 'evil-visual-state-exit-hook  'polaris|hl-line-on))

(use-package visual-fill-column :defer t
  :config
  (setq-default visual-fill-column-center-text nil
                visual-fill-column-width fill-column))

(use-package highlight-numbers :commands (highlight-numbers-mode))

(use-package rainbow-delimiters
  :commands rainbow-delimiters-mode
  :init
  (dolist (hook '(text-mode-hook prog-mode-hook))
    (add-hook hook #'rainbow-delimiters-mode)))

(use-package rainbow-mode
  :commands (rainbow-mode)
  :init
  (dolist (hook '(html-mode-hook web-mode-hook css-mode-hook
                                 sass-mode-hook less-css-mode-hook scss-mode-hook))
    (add-hook hook #'rainbow-mode)))

(use-package winner
  :init
  ;; also allow undo/redo on window configs
  (add-hook 'window-configuration-change-hook #'winner-mode))

;;
;; Mode-line
;;

(use-package spaceline-config
  :init
  (defvar polaris--env-version nil)
  (defvar polaris--env-command nil)
  (make-variable-buffer-local 'polaris--env-version)
  (make-variable-buffer-local 'polaris--env-command)
  (setq-default
   powerline-default-separator nil
   powerline-height 25
   spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)

  :config
  (defface mode-line-is-modified nil "Face for mode-line modified symbol")
  (defface mode-line-buffer-file nil "Face for mode-line buffer file path")

  ;; Custom modeline segments
  (spaceline-define-segment *buffer-size ""
                            "%I"
                            :when buffer-file-name)

  (spaceline-define-segment *buffer-path
    (if buffer-file-name
        (let* ((project-path (polaris/project-root))
               (buffer-path (f-relative buffer-file-name project-path))
               (max-length 40))
          (concat (projectile-project-name) "/"
                  (if (> (length buffer-path) max-length)
                      (let ((path (reverse (split-string buffer-path "/")))
                            (output ""))
                        (when (and path (equal "" (car path)))
                          (setq path (cdr path)))
                        (while (and path (< (length output) (- max-length 4)))
                          (setq output (concat (car path) "/" output))
                          (setq path (cdr path)))
                        (when path
                          (setq output (concat "../" output)))
                        output)
                    buffer-path)))
      "%b")
    :face (if active 'mode-line-buffer-file 'mode-line-inactive)
    :skip-alternate t)

  (spaceline-define-segment *remote-host
    "Hostname for remote buffers."
    (concat "@" (file-remote-p default-directory 'host))
    :when (file-remote-p default-directory 'host))

  (spaceline-define-segment *buffer-modified
    (concat
     (when buffer-file-name
       (concat
        (when (buffer-modified-p) "[+]")
        (unless (file-exists-p buffer-file-name) "[!]")))
     (if buffer-read-only "[RO]"))
    :face mode-line-is-modified
    :when (not (string-prefix-p "*" (buffer-name)))
    :skip-alternate t)

  (spaceline-define-segment *buffer-position
    "A more vim-like buffer position."
    (let ((start (window-start))
          (end (window-end))
          (pend (point-max)))
      (if (and (eq start 1)
               (eq end pend))
          ":All"
        (let ((perc (/ end 0.01 pend)))
          (cond ((eq start 1) ":Top")
                ((>= perc 100) ":Bot")
                (t (format ":%d%%%%" perc)))))))

  (spaceline-define-segment *vc
    "Version control info"
    (concat (replace-regexp-in-string
             (format "^ %s" (vc-backend buffer-file-name))
             "" vc-mode))
    :when (and active vc-mode)
    :face other-face)

  (spaceline-define-segment *env-version
    "Shows the environment version of a mode (e.g. pyenv for python or rbenv for ruby).
See `def-env-command!' to define one for a mode."
    polaris--env-version
    :when polaris--env-version
    :face other-face)

  ;; search indicators
  (defface mode-line-count-face nil "")
  (make-variable-buffer-local 'anzu--state)
  (spaceline-define-segment *anzu
    "Show the current match number and the total number of matches. Requires
anzu to be enabled."
    (let ((here anzu--current-position)
          (total anzu--total-matched))
      (format " %s/%d%s "
              here total
              (if anzu--overflow-p "+" "")))
    :face (if active 'mode-line-count-face 'mode-line-inactive)
    :when (evil-ex-hl-active-p 'evil-ex-search))

  (spaceline-define-segment *iedit
    "Show the number of matches and what match you're on (or after). Requires iedit."
    (let ((this-oc (iedit-find-current-occurrence-overlay))
          (length  (or (ignore-errors (length iedit-occurrences-overlays)) 0)))
      (format " %s/%s "
              (save-excursion
                (unless this-oc
                  (iedit-prev-occurrence)
                  (setq this-oc (iedit-find-current-occurrence-overlay)))
                (if this-oc
                    ;; NOTE: Not terribly reliable
                    (- length (-elem-index this-oc iedit-occurrences-overlays))
                  "-"))
              length))
    :when (bound-and-true-p iedit-mode)
    :face (if active 'mode-line-count-face 'mode-line-inactive))

  (defface mode-line-substitute-face nil "")
  (spaceline-define-segment *evil-substitute
    "Show number of :s matches in real time."
    (let ((range (if evil-ex-range
                     (cons (car evil-ex-range) (cadr evil-ex-range))
                   (cons (line-beginning-position) (line-end-position))))
          (pattern (car-safe (evil-delimited-arguments evil-ex-argument 2))))
      (if pattern
          (format " %s matches "
                  (count-matches pattern (car range) (cdr range))
                  evil-ex-argument)
        " ... "))
    :when (and (evil-ex-p) (evil-ex-hl-active-p 'evil-ex-substitute))
    :face (if active 'mode-line-count-face 'mode-line-inactive))

  (spaceline-define-segment *macro-recording
    "Show when recording macro"
    (format " %s ▶ " (char-to-string evil-this-macro))
    :when (and active defining-kbd-macro)
    :face highlight-face)

  (spaceline-define-segment *buffer-encoding-abbrev
    "The line ending convention used in the buffer."
    (format "%s" buffer-file-coding-system)
    :when (not (string-match-p "\\(utf-8\\|undecided\\)"
                               (symbol-name buffer-file-coding-system))))

  (spaceline-define-segment *major-mode
    (concat
     (and (featurep 'face-remap) (/= text-scale-mode-amount 0) (format "(%+d) " text-scale-mode-amount))
     (if (stringp mode-name) mode-name (car mode-name))
     (if (stringp mode-line-process) mode-line-process)))

  (defun polaris--col-at-pos (pos)
    (save-excursion (goto-char pos) (current-column)))
  (spaceline-define-segment *selection-info
    "Information about the size of the current selection, when applicable.
Supports both Emacs and Evil cursor conventions."
    (let ((reg-beg (region-beginning))
          (reg-end (region-end)))
      (let* ((lines (count-lines reg-beg (min (1+ reg-end) (point-max))))
             (chars (- (1+ reg-end) reg-beg))
             (cols (1+ (abs (- (polaris--col-at-pos reg-end)
                               (polaris--col-at-pos reg-beg)))))
             (evil (eq 'visual evil-state))
             (rect (or (bound-and-true-p rectangle-mark-mode)
                       (and evil (eq 'block evil-visual-selection))))
             (multi-line (or (> lines 1) (eq 'line evil-visual-selection))))
        (cond
         (rect (format "%dx%dB" lines (if evil cols (1- cols))))
         (multi-line
          (if (and (eq evil-state 'visual) (eq evil-this-type 'line))
              (format "%dL" lines)
            (format "%dC %dL" chars lines)))
         (t (format "%dC" (if evil chars (1- chars)))))))
    :when (eq 'visual evil-state)
    :face highlight-face)

  ;; flycheck
  (defun polaris--flycheck-count (state)
    "Return flycheck information for the given error type STATE."
    (when (flycheck-has-current-errors-p state)
      (if (eq 'running flycheck-last-status-change)
          "?"
        (cdr-safe (assq state (flycheck-count-errors flycheck-current-errors))))))

  (defface spaceline-flycheck-error
    '((t (:foreground "#FC5C94" :distant-foreground "#A20C41")))
    "Face for flycheck error feedback in the modeline.")
  (defface spaceline-flycheck-warning
    '((t (:foreground "#F3EA98" :distant-foreground "#968B26")))
    "Face for flycheck warning feedback in the modeline.")
  (defface spaceline-flycheck-info
    '((t (:foreground "#8DE6F7" :distant-foreground "#21889B")))
    "Face for flycheck info feedback in the modeline.")

  (defvar-local polaris--flycheck-err-cache nil "")
  (defvar-local polaris--flycheck-cache nil "")
  (spaceline-define-segment *flycheck
    "Persistent and cached flycheck indicators in the mode-line."
    (or (and (or (eq polaris--flycheck-err-cache polaris--flycheck-cache)
                 (memq flycheck-last-status-change '(running not-checked)))
             polaris--flycheck-cache)
        (and (setq polaris--flycheck-err-cache flycheck-current-errors)
             (setq polaris--flycheck-cache
                   (let ((fe (polaris--flycheck-count 'error))
                         (fw (polaris--flycheck-count 'warning))
                         (fi (polaris--flycheck-count 'info)))
                     (concat
                      (when fe (powerline-raw (format " ⚠%s " fe) 'spaceline-flycheck-error))
                      (when fw (powerline-raw (format " ⚠%s " fw) 'spaceline-flycheck-warning))
                      (when fi (powerline-raw (format " ⚠%s " fi) 'spaceline-flycheck-info)))))))
    :when (and (bound-and-true-p flycheck-mode)
               (or flycheck-current-errors
                   (eq 'running flycheck-last-status-change))))

  (defvar polaris--mode-line-padding (pl/percent-xpm powerline-height 100 0 100 0 1 nil nil))
  (spaceline-define-segment *pad
    "A HUD that shows which part of the buffer is currently visible."
    polaris--mode-line-padding)

  (defun polaris-spaceline-init ()
    (spaceline-install
     ;; Left side
     '(((persp-name workspace-number)
        :fallback evil-state
        :face highlight-face)
       ((*macro-recording *anzu *iedit *evil-substitute *flycheck)
        :skip-alternate t
        :fallback *buffer-size)
       (*buffer-path *remote-host)
       *buffer-modified
       *vc
       (erc-track :when active)
       (org-pomodoro :when active)
       (org-clock :when active)
       )
     ;; Right side
     '((*selection-info :when active)
       *buffer-encoding-abbrev
       *major-mode
       *env-version
       (global :when active)
       ("%l/%c" *buffer-position)
       *pad
       )))

  ;; Initialize modeline
  (polaris-spaceline-init)

  (spaceline-helm-mode t)
  (spaceline-info-mode t)
  )

(use-package beacon
  :disabled t
  :diminish beacon-mode
  :init
  (setq beacon-push-mark 35
        beacon-blink-when-focused t
        beacon-size 20)
  (beacon-mode 1))

(use-package paradox
  :commands
  (paradox-list-packages
   paradox-upgrade-packages)
  :config
  (setq paradox-execute-asynchronously nil ; async update, please
        paradox-spinner-type 'moon         ; Fancy spinner
        paradox-github-token t
        ;; Show all possible counts
        paradox-display-download-count t
        paradox-display-star-count t
        ;; Hide download button, and wiki packages
        paradox-use-homepage-buttons nil ; Can type v instead
        paradox-hide-wiki-packages t)
  (evilified-state-evilify paradox-menu-mode paradox-menu-mode-map
    "H" 'paradox-menu-quick-help
    "J" 'paradox-next-describe
    "K" 'paradox-previous-describe
    "L" 'paradox-menu-view-commit-list
    "o" 'paradox-menu-visit-homepage))

;;; ====================================
;;; iflib - switch buffers alt-tab style
;;; ====================================
(use-package iflipb
  :config
  (defhydra iflipb-hydra
    (:pre (setq hydra-is-helpful nil)
          :post (setq hydra-is-helpful t))
    ("<C-tab>"
     (call-interactively #'iflipb-next-buffer))
    ("TAB"
     (call-interactively #'iflipb-next-buffer))
    ("<C-S-iso-lefttab>"
     (call-interactively #'iflipb-previous-buffer))
    ("<backtab>"
     (call-interactively #'iflipb-previous-buffer)))

  (setq iflipb-ignore-buffers '("^ " "^*helm" "^*Compile" "^*Quail")
        iflipb-wrap-around 't)

  (defun nadvice/iflipb-first-iflipb-buffer-switch-command ())

  (advice-add #'iflipb-first-iflipb-buffer-switch-command
              :override
              #'nadvice/iflipb-first-iflipb-buffer-switch-command)

  (defun iflipb-next-buffer-smart ()
    "A `hydra' enabled next-buffer"
    (interactive)
    (require 'iflipb)
    (cl-letf (((symbol-function #'iflipb-first-iflipb-buffer-switch-command)
               (lambda () t)))
      (call-interactively #'iflipb-next-buffer))
    (iflipb-hydra/body))

  (defun iflipb-previous-buffer-smart ()
    "A `hydra' enabled previous-buffer"
    (interactive)
    (require 'iflipb)
    (cl-letf (((symbol-function #'iflipb-first-iflipb-buffer-switch-command)
               (lambda () t)))
      (call-interactively #'iflipb-previous-buffer))
    (iflipb-hydra/body))

  (global-set-key (kbd "<C-tab>") #'iflipb-next-buffer-smart)
  (global-set-key (kbd "C-S-<iso-lefttab>") #'iflipb-previous-buffer-smart)

  (global-set-key (kbd "C-c TAB") #'iflipb-next-buffer-smart)
  (global-set-key (kbd "C-c <backtab>") #'iflipb-previous-buffer-smart))

(use-package linum-relative
  :config
  (progn
    (setq linum-relative-current-symbol ""
          linum-relative-format "%3s "
          linum-delay t)

    (set-face-background 'linum nil)
    (set-face-attribute 'linum-relative-current-face nil
                        :weight 'extra-bold
                        :foreground nil
                        :background nil
                        :inherit '(hl-line default))

    ;; truncate current line to three digits
    (defun nadvice/linum-relative (line-number)
      (let* ((diff1 (abs (- line-number linum-relative-last-pos)))
             (diff (if (minusp diff1)
                       diff1
                     (+ diff1 linum-relative-plusp-offset)))
             (current-p (= diff linum-relative-plusp-offset))
             (current-symbol (if (and linum-relative-current-symbol current-p)
                                 (if (string= "" linum-relative-current-symbol)
                                     (number-to-string (% line-number 1000))
                                   linum-relative-current-symbol)
                               (number-to-string diff)))
             (face (if current-p 'linum-relative-current-face 'linum)))
        (propertize (format linum-relative-format current-symbol) 'face face)))

    (advice-add 'linum-relative :override #'nadvice/linum-relative)

    (defun linum-cycle ()
      (interactive)
      (if (bound-and-true-p linum-mode)
          (if (eq linum-format 'dynamic)
              (linum-mode -1)
            (setq linum-format 'dynamic))
        (progn
          (linum-mode +1)
          (setq linum-format 'linum-relative))))

    (global-set-key (kbd "C-c L") #'linum-cycle)
    (global-set-key (kbd "C-c C-l") #'linum-cycle)))

(use-package windsize
  :init
  (setq windsize-cols 16
        windsize-rows 8)
  (windsize-default-keybindings))

(use-package help-fns+ ; Improved help commands
  :commands (describe-buffer describe-command describe-file
                             describe-keymap describe-option describe-option-of-type))

(use-package info+
  :defer t
  :init
  (progn
    (with-eval-after-load 'info
      (require 'info+))
    (setq Info-fontify-angle-bracketed-flag nil)))

(use-package hl-anything
  :init
  (progn
    (hl-highlight-mode)
    (setq-default hl-highlight-save-file
                  (concat polaris-temp-dir ".hl-save"))))

(use-package auto-highlight-symbol
  :init
  (progn
    (setq ahs-case-fold-search nil
          ahs-default-range 'ahs-range-whole-buffer
          ;; by default disable auto-highlight of symbol
          ;; current symbol can always be highlighted with `SPC s h'
          ahs-idle-timer 0
          ahs-idle-interval 0.25
          ahs-inhibit-face-list nil)

    ;; since we are creating our own maps,
    ;; prevent the default keymap from getting created
    (setq auto-highlight-symbol-mode-map (make-sparse-keymap))

    (dolist (hook '(prog-mode-hook markdown-mode-hook))
      (add-hook hook #'auto-highlight-symbol-mode))))

(provide 'core-ui)
;;; core-ui.el ends here

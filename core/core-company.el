;; -*- lexical-binding: t -*-
;;; core-company.el --- auto completion backend (Company-mode)

(use-package company
  :diminish (company-mode . " 🅒")
  :init
  (after! abbrev (diminish 'abbrev-mode " 🅐"))
  (setq company-idle-delay 0.1
        company-echo-delay 0
        company-minimum-prefix-length 2
        company-tooltip-limit 10
        company-dabbrev-downcase nil
        company-dabbrev-ignore-case nil
        company-dabbrev-code-everywhere t
        company-tooltip-flip-when-above t
        company-tooltip-align-annotations t
        company-require-match 'never
        company-global-modes '(not eshell-mode comint-mode erc-mode message-mode help-mode)
        company-frontends '(company-pseudo-tooltip-frontend company-echo-metadata-frontend)
        company-auto-complete 'company-explicit-action-p
        company-backends '((company-capf
                            company-yasnippet
                            company-dabbrev-code
                            company-keywords
                            company-files)
                           company-dabbrev))

  :config
  ;; Rewrites evil-complete to use company-dabbrev
  (setq evil-complete-next-func      'polaris/company-evil-complete-next
        evil-complete-previous-func  'polaris/company-evil-complete-previous)
  (push 'company-sort-by-occurrence company-transformers)

  (define-key company-active-map "\C-w" nil)
  (define-key company-active-map [return] 'nil)
  (define-key company-active-map (kbd "RET") 'nil)

  (global-company-mode +1))

(use-package company-dabbrev
  :commands company-dabbrev
  :init
  (setq company-dabbrev-minimum-length 2))

(use-package company-dabbrev-code
  :commands company-dabbrev-code
  :init
  (setq company-dabbrev-code-modes t
        company-dabbrev-code-everywhere t))

(use-package company-template
  :after company
  :config
  (defun setup-company-template-faces ()
    (set-face-attribute 'company-template-field nil
                        :foreground nil
                        :background nil
                        :inherit 'region))
  (add-hook! load-theme 'setup-company-template-faces)
  (setup-company-template-faces))



(use-package company-quickhelp
  :after company
  :init (setq company-quickhelp-delay nil)
  :config (company-quickhelp-mode +1))

(use-package company-statistics
  :after company
  :init (setq company-statistics-file (concat polaris-temp-dir "company-stats-cache.el"))
  :config (company-statistics-mode +1))

(use-package company-dict
  :commands company-dict
  :config (setq company-dict-dir (concat polaris-private-dir "/dict")))

;;; ==================================================
;;; Hippie expand - secondary autocompletion framework
;;; ==================================================

(use-package hippie-exp
  :config
  (defun my/he-try-expand-flx-regexp (str)
    "Generate regexp for flexible matching of str."
    (concat (rx word-boundary)
            (mapconcat (lambda (x)
                         (concat (rx (zero-or-more word) (zero-or-more "-"))
                                 (list x)))
                       str
                       "")
            (rx (zero-or-more word) word-boundary)))

  (defun my/he-try-expand-flx-collect (str)
    "Find and collect all words that flex-match str, and sort by flx score"
    (let ((coll)
          (regexp (my/he-try-expand-flx-regexp str)))
      (save-excursion
        (goto-char (point-min))
        (while (search-forward-regexp regexp nil t)
          (push (thing-at-point 'symbol) coll)))
      (sort coll #'(lambda (a b)
                     (> (car (flx-score a str))
                        (car (flx-score b str)))))))

  (defun my/he-try-expand-flx (old)
    "Try to complete word using flx matching."
    (unless old
      (he-init-string (he-lisp-symbol-beg) (point))
      (unless (he-string-member he-search-string he-tried-table)
        (push he-search-string he-tried-table))
      (setq he-expand-list
            (unless (equal he-search-string "")
              (my/he-try-expand-flx-collect he-search-string))))
    (while (and he-expand-list
                (he-string-member (car he-expand-list) he-tried-table))
      (pop he-expand-list))
    (prog1
        (null he-expand-list)
      (if (null he-expand-list)
          (when old (he-reset-string))
        (he-substitute-string (pop he-expand-list)))))

  (setq hippie-expand-try-functions-list
        '(
          yas-hippie-try-expand
          ;; Try to expand word "dynamically", searching the current buffer.
          try-expand-dabbrev
          ;; Try to expand word "dynamically", searching the kill ring.
          try-expand-dabbrev-from-kill
          my/he-try-expand-flx
          ;; Try to expand word "dynamically", searching all other buffers.
          try-expand-dabbrev-all-buffers
          ;; Try to complete text as a file name, as many characters as unique.
          try-complete-file-name-partially
          ;; Try to complete text as a file name.
          try-complete-file-name
          ;; Try to expand word before point according to all abbrev tables.
          try-expand-all-abbrevs
          ;; Try to complete the current line to an entire line in the buffer.
          try-expand-list
          ;; Try to complete the current line to an entire line in the buffer.
          try-expand-line
          ;; Try to complete as an Emacs Lisp symbol, as many characters as
          ;; unique.
          try-complete-lisp-symbol-partially
          ;; Try to complete word as an Emacs Lisp symbol.
          try-complete-lisp-symbol))

  (global-set-key (kbd "M-/") 'hippie-expand))

(use-package ycmd
  :disabled t
  :init
  (set-variable 'ycmd-server-command '("python2" "-u")
                )

  (set-variable 'ycmd-global-config (expand-file-name ".ycm_extra_conf.py" polaris-home-dir))
  (add-to-list 'ycmd-server-command (expand-file-name "local/ycmd/ycmd" polaris-home-dir) t)
  (ycmd-setup)
  (add-hook! after-init 'global-ycmd-mode)
  :config
  (use-package company-ycmd
    :init
    (company-ycmd-setup))
  (use-package flycheck-ycmd
    :init
    (flycheck-ycmd-setup))
  )

(provide 'core-company)
;;; core-company.el ends here

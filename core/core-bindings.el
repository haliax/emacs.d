;;; core-bindings.el

(use-package general
  :config
  (setq general-default-keymaps 'global)
  (general-evil-setup t)

  (general-create-definer gmap :keymaps 'global)
  (defalias 'mmap 'general-mmap)

  (gmap
   "M-x"     'counsel-M-x
   "M-:"     'eval-expression
   "M-w"     'evil-window-delete
   "M-;"     'evilnc-comment-or-uncomment-lines
   "M-b"     'polaris:build
   "M-t"     'polaris:tab-create
   "M-T"     'polaris/tab-display
   "C-'"     'polaris/popup-messages
   "C-~"     'polaris:repl
   "M-'"     'polaris/popup-toggle
   "M-p"     'ace-window
   "M-i"     'helm-swoop

   "H-;"     'eval-expression
   "H-/"     'evilnc-comment-or-uncomment-lines

   "M-0"     (λ! (text-scale-set 0))
   "C-+"     'text-scale-increase
   "C--"     'text-scale-decrease

   "M-w"     'polaris/close-window-or-tab
   "M-W"     'delete-frame
   "M-n"     'polaris/new-buffer
   "M-N"     'polaris/new-frame

   "C-x b"   'ivy-switch-buffer
   "C-h f"   'counsel-describe-function
   "C-h v"   'counsel-describe-variable
   "C-h M-k" 'describe-keymap
   "C-h C-m" 'discover-my-major
   "C-x C-b" 'helm-buffers-list
   "C-x C-f" 'helm-find-files
   "C-x f"   'counsel-find-file

   "C-s"     'polaris/swiper

   "C-u"     'kill-whole-line
   "M-u"     'universal-argument

   "M-a"     'mark-whole-buffer
   "M-c"     'evil-yank
   "M-o"     'helm-find-files
   "M-q"     'evil-quit-all
   "M-v"     'clipboard-yank
   "M-z"     'undo-tree-undo
   "M-Z"     'undo-tree-redo
   "C-M-f"   'polaris:toggle-fullscreen
   "C-b"     'backward-word

   "<left-margin> <down-mouse-1>" 'polaris/mouse-drag-line
   "<left-margin> <mouse-1>"      'polaris/mouse-select-line
   "<left-margin> <drag-mouse-1>" 'polaris/mouse-select-line

   ;; Global hydras
   "<f1>"    'hydra-help/body
   "C-x w"   'hydra-window-tools/body
   "M-s"     'hydra-isearch/body
   "C-x 4"   'hydra-frame-tools/body

   )

  (imap
   "RET" 'newline-and-indent

   [(tab)] nil

   ;; Textmate-esque indent shift left/right
   "M-]" 'polaris/smart-indent
   "M-[" 'polaris/dumb-indent

   ;; Highjacks space/backspace to:
   ;;   a) delete spaces on either side of the cursor, if present ( | ) -> (|)
   ;;   b) allow backspace to delete space-indented blocks intelligently
   ;;   c) and not do any of this magic when inside a string
   "SPC" 'polaris/inflate-space-maybe
   [remap backward-delete-char-untabify] 'polaris/deflate-space-maybe
   [remap newline] 'polaris/newline-and-indent

   ;; Restore bash-esque keymaps in insert mode; C-w and C-a already exist
   "C-e" 'polaris/move-to-eol
   "C-u" 'polaris/backward-kill-to-bol-and-indent

   ;; Fixes delete
   "<kp-delete>" 'delete-char

   "<M-left>"   'polaris/move-to-bol
   "<M-right>"  'polaris/move-to-eol
   "<M-up>"     'beginning-of-buffer
   "<M-down>"   'end-of-buffer
   "<C-up>"     'smart-up
   "<C-down>"   'smart-down

   ;; Textmate-esque insert-line before/after
   "<M-return>"    'evil-open-below
   "<S-M-return>"  'evil-open-above

   )

  (vmap
   "<backtab>" nil
   "=" 'evil-indent
   "gR" 'polaris:eval-region-and-replace
   "@"  'polaris/evil-macro-on-all-lines
   "."  'evil-repeat

   ;; vnoremap < <gv
   "<"   (λ! (evil-shift-left (region-beginning) (region-end))
             (evil-normal-state)
             (evil-visual-restore))
   ;; vnoremap > >gv
   ">"   (λ! (evil-shift-right (region-beginning) (region-end))
             (evil-normal-state)
             (evil-visual-restore))
   "S"   'evil-surround-region
   "v"   'er/expand-region
   "V"   'er/contract-region

   )

  (mmap

   ;; Workgroups2
   "M-1"  (λ! (polaris:switch-to-tab 0))
   "M-2"  (λ! (polaris:switch-to-tab 1))
   "M-3"  (λ! (polaris:switch-to-tab 2))
   "M-4"  (λ! (polaris:switch-to-tab 3))
   "M-5"  (λ! (polaris:switch-to-tab 4))
   "M-6"  (λ! (polaris:switch-to-tab 5))
   "M-7"  (λ! (polaris:switch-to-tab 6))
   "M-8"  (λ! (polaris:switch-to-tab 7))
   "M-9"  (λ! (polaris:switch-to-tab 8))

   "M-j"  'polaris/multi-next-line
   "M-k"  'polaris/multi-previous-line

   "C-u"  'evil-scroll-up
   "gD"   'polaris/find-def

   ";"    'evil-ex

   "]d" 'polaris/vcs-next-hunk
   "[d" 'polaris/vcs-prev-hunk
   "]e" 'polaris/flycheck-next-error
   "[e" 'polaris/flycheck-previous-error

   "gt" 'polaris:switch-to-tab-right
   "gT" 'polaris:switch-to-tab-left

   "gl" 'avy-goto-line
   "gw" 'avy-goto-char-2
   "g]" 'smart-right
   "g[" 'smart-left

   )


  (nmap
   "M-r"  'polaris:eval-buffer
   "="  (λ! (save-excursion (call-interactively 'evil-indent)))
   "zr" 'polaris/evil-open-folds
   "zm" 'polaris/evil-close-folds
   "zx" 'polaris/kill-real-buffer
   "ZX" 'bury-buffer

   ;; These are intentionally reversed
   "]b" 'polaris/next-real-buffer
   "[b" 'polaris/previous-real-buffer

   ;; Switch tabs
   "]w" 'polaris:switch-to-tab-right
   "[w" 'polaris:switch-to-tab-left

   ;; Increment/decrement number under cursor
   "g=" 'evil-numbers/inc-at-pt
   "g-" 'evil-numbers/dec-at-pt

   ;; NOTE: Helm is too bulky for ffap (which I use for quick file navigation)
   "gf" (λ! (helm-mode -1)
            (call-interactively 'find-file-at-point)
            (helm-mode +1))
   "gp" 'polaris/reselect-paste
   "gr" 'polaris:eval-region
   "gR" 'polaris:eval-buffer
   "g@" 'polaris/evil-macro-on-all-lines

   ;; insert lines in-place)
   "<M-return>"    (λ! (save-excursion (evil-insert-newline-below)))
   "<S-M-return>"  (λ! (save-excursion (evil-insert-newline-above)))


   )

  (nvmap
   "gx" 'evil-exchange
   "gX" 'evil-exchange-cancel
   ;; undo/redo for regions (NOTE: Buggy!)
   "u"    'undo-tree-undo
   "C-r"  'undo-tree-redo

   )

  (general-define-key :states '(insert replace visual)
                      "C-g" 'evil-normal-state)


  (setq polaris-leader "SPC")

  ;; Leader
  (nmap
   :prefix polaris-leader
   "SPC" 'counsel-M-x

   ;; Buffers
   "b k" 'polaris/kill-real-buffer
   "b K" 'polaris/kill-other-buffers
   "b l" 'ibuffer

   ;; Files
   "f f" 'helm-find-files
   "f t" 'neotree-toggle
   "f r" 'ranger
   "f d" 'dired-jump
   "f i" 'polaris/helm-interfile-omni

   ;; Avy
   "j w" 'avy-goto-word-1
   "j l" 'avy-goto-line
   "j b" 'avy-pop-mark
   "j j" 'avy-goto-char-2

   ;; Toggle
   "t"   'hydra-toggle/body

   ;; Window
   "w =" 'balance-windows
   "w k" 'delete-window
   "w /" 'split-window-right
   "w -" 'split-window-below
   "w m" 'delete-other-windows
   "w c" 'polaris/popup-close
   "w o" 'ace-window
   "w F" 'toggle-frame-fullscreen

   ;; Layouts (eyebrowse + persp)
   "l" 'hydra-persp/body

   ;; Git
   "g"   'hydra-git/body

   ;; Comment
   "c i" 'evilnc-comment-or-uncomment-lines
   "c l" 'evilnc-quick-comment-or-uncomment-to-the-line
   "c c" 'evilnc-copy-and-comment-lines
   "c p" 'evilnc-comment-or-uncomment-paragraphs
   "c r" 'comment-or-uncomment-region
   "c v" 'evilnc-toggle-invert-comment-line-by-line

   ;; Errors
   "e" 'hydra-flycheck-errors/body

   ;; Applications
   "a" 'hydra-apps/body

   ;; Text
   "x a" 'whitespace-cleanup
   "x i" 'indent-region
   "x t" 'tildify-region

   ;; Search
   "s s" 'polaris/swiper
   "s a" 'polaris:helm-ag-search
   )

  ;; Narrow region
  (gmap :prefix "C-x"
        "n" 'narrow-or-widen-dwim)

  ;; Packages bindings

  ;; Avy

  ;; Company
  (imap
   ;; Vim omni-complete emulation
   "C-SPC" 'polaris/company-complete)
  (imap :prefix "C-x"
        "C-l"   'polaris/company-whole-lines
        "C-k"   'polaris/company-dict-or-keywords
        "C-f"   'company-files
        "s"     'company-ispell
        "C-s"   'company-yasnippet
        "C-o"   'company-capf
        "C-n"   'company-dabbrev-code
        "C-p"   (λ! (let ((company-selection-wrap-around t))
                      (call-interactively 'company-dabbrev-code)
                      (company-select-previous-or-abort))))
  (iemap :keymaps 'company-active-map
         "C-o"        'company-search-kill-others
         "C-n"        'company-select-next
         "C-p"        'company-select-previous
         "C-h"        'company-quickhelp-manual-begin
         "C-S-s"      'company-search-candidates
         "C-s"        'company-filter-candidates
         "C-SPC"      'company-complete-common-or-cycle
         "<tab>"      'polaris/company-complete-common-or-complete-full
         "TAB"        'polaris/company-complete-common-or-complete-full
         "C-d"        'company-show-doc-buffer
         "C-j"        'company-select-next
         "C-k"        'company-select-previous
         "C-l"        'company-complete-selection
         "<up>"       'company-select-previous
         "<down>"     'company-select-next
         "<backtab>"  'company-select-previous
         [escape]     (λ! (company-abort) (evil-normal-state 1))
         "C-:"        'helm-company)

  (iemap :keymaps 'company-mode-map
         "C-:"        'helm-company)

  (iemap :keymaps 'company-search-map
         "C-n"        'company-search-repeat-forward
         "C-p"        'company-search-repeat-backward
         "ESC"        'company-search-abort)

  ;; Debug
  (gmap :keymaps 'debugger-mode-map
        "RET" 'debug-help-follow
        "n"   'debugger-step-through
        "c"   'debugger-continue)

  ;; Dired
  (gmap :keymaps 'dired-mode-map
        "r" 'dired-start-process)

  ;; Evil-Multiedit
  (vmap "R" 'evil-multiedit-match-all)
  (nmap "M-C-D" 'evil-multiedit-restore)
  (nvmap "M-d" 'evil-multiedit-match-and-next
         "M-D" 'evil-multiedit-match-and-prev)
  (vmap "RET" 'evil-multiedit-toggle-or-restrict-region)
  (gmap :keymaps 'evil-multiedit-state-map
        "RET" 'evil-multiedit-toggle-or-restrict-region
        "C-n" 'evil-multiedit-next
        "C-p" 'evil-multiedit-prev)
  (gmap :keymaps 'evil-multiedit-insert-state-map
        "C-n" 'evil-multiedit-next
        "C-p" 'evil-multiedit-prev)

  ;; Evil NerdCommenter
  (gmap "M-;" 'evilnc-comment-or-uncomment-lines)
  (nmap "gc" 'evilnc-comment-operator)
  (omap "gc" 'evilnc-comment-operator)

  ;; Evil Text-objects
  (otomap
   "n" 'evil-a-next-thing
   "N" 'evil-a-previous-thing)
  (itomap
   "n" 'evil-i-next-thing
   "N" 'evil-i-previous-thing)

  ;; ERC
  (imap :keymaps 'erc-mode-map
        "C-c '" 'erc-popup-input-buffer
        "C-n"   'erc-next-command
        "C-p"   'erc-previous-command)
  (iemap :keymaps 'erc-list-menu-mode-map
         "/"        'evil-search-forward
         "<return>" 'erc-list-join
         "?"        'evil-search-backward
         "C-c C-u"  'erc-list-revert
         "C-d"      'evil-scroll-down
         "C-u"      'evil-scroll-up
         "G"        'end-of-buffer
         "N"        'evil-search-previous
         "g"        nil
         "gg"       'beginning-of-buffer
         "j"        'next-line
         "k"        'previous-line
         "n"        'evil-search-next)

  ;; ERT
  (gmap :keymaps 'ert-results-mode-map
        [escape]   'quit-window
        "<escape>" 'quit-window)

  ;; Eyebrowse
  ;; (nmap "gt" 'eyebrowse-next-window-config
  ;;       "gT" 'eyebrowse-prev-window-config)

  ;; FlyCheck
  (nmap :keymaps 'flycheck-error-list-mode-map
        "C-n"    'flycheck-error-list-next-error
        "C-p"    'flycheck-error-list-previous-error
        "j"      'flycheck-error-list-next-error
        "k"      'flycheck-error-list-previous-error
        "RET"    'flycheck-error-list-goto-error)

  ;; FlySpell
  (gmap :keymaps 'flyspell-mode-map
        "C-,"   nil
        "C-M-i" nil
        "C-;"   nil)

  ;; Helm
  (imap "C-p"         'polaris/helm-interfile-omni)
  (mmap "C-p"         'polaris/helm-interfile-omni)
  (gmap "C-c C-o"     'polaris/helm-interfile-omni
        "M-p"         'polaris/helm-intrafile-omni)
  (gmap :keymaps '(helm-read-file-map helm-find-files-map)
        "<backspace>" 'dwim-helm-find-files-up-one-level-maybe)
  (gmap :keymaps 'helm-map
        "M-v"         'clipboard-yank
        "C-w"         'backward-kill-word
        "C-r"         'evil-ex-paste-from-register ; Evil registers in helm! Glorious!
        "TAB"         'helm-execute-persistent-action
        "C-i"         'helm-execute-persistent-action
        "C-z"         'helm-select-action
        "C-u"         'helm-delete-minibuffer-contents
        "C-s"         'isearch-backward-regexp
        "C-'"         'ace-jump-helm-line-execute-action
        "C-b"         'backward-word
        "<left>"      'backward-char
        "<right>"     'forward-char
        "<escape>"    'helm-keyboard-quit
        "C-u"         'helm-delete-minibuffer-contents)
  (gmap :keymaps 'helm-ag-map
        "<backtab>"   'helm-ag-edit)
  (gmap :keymaps 'helm-ag-edit-map
        "<escape>"    'helm-ag--edit-abort
        "zx"          'helm-ag--edit-abort)
  (gmap :keymaps 'helm-swoop-map
        "M-i"    'helm-multi-swoop-all-from-helm-swoop)

  ;; Help
  (gmap :keymaps 'help-map
        "e" 'polaris/popup-messages
        ;; Remove slow/annoying help subsections
        "h" nil
        "g" nil)
  (nmap :keymaps 'help-map
        "]]" 'help-go-forward
        "[[" 'help-go-back
        "<escape>" 'polaris/popup-close)

  ;; Ido
  (gmap :keymaps '(ido-common-completion-map
                   ido-completion-map
                   ido-file-completion-map)
        "C-n" 'ido-next-match
        "C-p" 'ido-prev-match
        "C-w" 'ido-delete-backward-word-updir
        "C-u" 'ido-up-directory)

  ;; Info hydra
  (emap
   :keymaps 'Info-mode-map
   "j" 'evil-next-line
   "k" 'evil-previous-line
   "?" 'hydra-info/body)

  ;; Org-mode

  (emap :keymaps 'org-agenda-mode-map
        "<escape>" 'polaris/org-agenda-quit
        "ESC" 'polaris/org-agenda-quit
        [escape] 'polaris/org-agenda-quit)
  (gmap :keymaps 'org-agenda-mode-map
        "q" 'polaris/org-agenda-quit
        "Q" 'polaris/org-agenda-quit)

  ;; Projectile
  (gmap "C-c p" 'hydra/projectile-tools/body)

  ;; Realgud
  (nmap :keymaps 'realgud:shortkey-mode-map
        "j" 'evil-next-line
        "k" 'evil-previous-line
        "h" 'evil-backward-char
        "l" 'evil-forward-char
        ;; FIXME Greedy command buffer always grabs focus
        "n" 'realgud:cmd-next
        "b" 'realgud:cmd-break
        "B" 'realgud:cmd-clear
        "c" 'realgud:cmd-continue)

  ;; Ranger
  (mmap :keymaps 'ranger-mode-map
        "r" 'dired-start-process
        "-" 'ranger-up-directory)
  (nmap :prefix polaris-leader
        "dr" 'ranger
        "dd" 'deer)

  ;; Repl-toggle
  (iemap :keymaps 'repl-toggle-mode-map
         "C-n"    'comint-next-input
         "C-p"    'comint-previous-input
         "<down>" 'comint-next-input
         "<up>"   'comint-previous-input)
  (gmap :keymaps 'repl-toggle-mode-map
        "ESC ESC" 'polaris/popup-close)

  ;; Re-Builder
  (nmap :keymaps 'rxt-help-mode-map
        [escape] 'kill-buffer-and-window)
  (nmap :keymaps 'reb-mode-map
        "C-g"        'reb-quit
        [escape]     'reb-quit
        [backtab]    'reb-change-syntax)

  ;; YaSnippet
  (iemap :keymaps 'yas-keymap
         "C-e"           'polaris/yas-goto-end-of-field
         "C-a"           'polaris/yas-goto-start-of-field
         "<M-right>"     'polaris/yas-goto-end-of-field
         "<M-left>"      'polaris/yas-goto-start-of-field
         "<S-tab>"       'yas-prev-field
         "<M-backspace>" 'polaris/yas-clear-to-sof

         "<escape>"      'evil-normal-state
         [backspace]     'polaris/yas-backspace
         "<delete>"      'polaris/yas-delete)

  ;; VC Annotate
  (nmap :keymaps 'vc-annotate-mode-map
        "q" 'kill-this-buffer
        "d" 'vc-annotate-show-diff-revision-at-line
        "D" 'vc-annotate-show-changeset-diff-revision-at-line
        "SPC" 'vc-annotate-show-log-revision-at-line
        "]]" 'vc-annotate-next-revision
        "[[" 'vc-annotate-prev-revision
        [tab] 'vc-annotate-toggle-annotation-visibility
        "RET" 'vc-annotate-find-revision-at-line)

  ;; Modes

  ;; Processing
  (mmap :keymaps 'processing-mode-map
        "M-r" 'processing-sketch-run
        "gd" 'processing-find-in-reference
        "gF" 'processing-find-sketch
        )
  (mmap :keymaps 'processing-mode-map
        :prefix polaris-leader
        "me" 'processing-export-application
        "mh" 'processing-open-reference
        "me" 'processing-open-examples
        "mo" 'processing-open-sketchbook)

  ;; PHP
  (nmap :keymaps 'php-mode-map
        "gd" 'ac-php-find-symbol-at-point
        "gD" 'ac-php-location-stack-back)

  ;; Web-Mode
  (nmap :keymaps 'web-mode-map
        "gb" 'web-mode-element-beginning
        "ge" 'web-mode-element-end
        "gc" 'web-mode-element-child
        "gp" 'web-mode-element-parent
        "gs" 'web-mode-element-sibling-next
        "z" 'web-mode-fold-or-unfold
        "cz" 'web-mode-element-children-fold-or-unfold)

  ;; Java
  (mmap :keymaps 'java-mode-map
        "gd" 'eclim-java-find-declaration)

  )

(provide 'core-bindings)
;;; core-bindings.el ends here

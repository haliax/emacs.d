;; -*- lexical-binding: t -*-
;;; core.el --- The heart of the beast
;;
;;; Naming conventions:
;;
;;   polaris-...     A public variable/constant or function
;;   polaris--...    An internal variable or function (non-interactive)
;;   polaris/...     An autoloaded interactive function
;;   polaris:...     An ex command
;;   polaris|...     A hook
;;   polaris*...     An advising function
;;   polaris....     Custom prefix commands
;;   ...!         Macro
;;
;;  You will find all autoloaded function in {core,modules}/defuns/defuns-*.el
;;
;;;

(setq-default
 ad-redefinition-action             'accept ; silence the advised function warnings
 compilation-always-kill            t ; kill compilation process before spawning another
 compilation-ask-about-save         nil ; save all buffers before compiling
 compilation-scroll-output          t   ; scroll with output while compiling
 delete-by-moving-to-trash          t
 echo-keystrokes                    0.01 ; show me what I type
 ediff-diff-options                 "-w"
 ediff-split-window-function       'split-window-horizontally ; side-by-side diffs
 ediff-window-setup-function       'ediff-setup-windows-plain ; no extra frames
 reb-re-syntax                     'string ; fix backslash madness
 enable-recursive-minibuffers       nil    ; no minibufferception
 idle-update-delay                  2           ; update a little less often
 inhibit-startup-echo-area-message  t
 inhibit-startup-message            t
 inhibit-startup-screen             t   ; don't show emacs start screen
 initial-major-mode                'emacs-lisp-mode ; initial scratch buffer mode
 inhibit-default-init               t
 initial-scratch-message            nil
 custom-safe-themes                 t
 tooltip-use-echo-area              t
 imenu-auto-rescan                  t
 create-lockfiles                   nil
 mark-ring-max 64
 global-mark-ring-max 128

 ;; Better scroll
 scroll-conservatively 9999
 scroll-preserve-screen-position t
 scroll-margin 3

 ring-bell-function                'ignore ; silence of the bells!
 save-interprogram-paste-before-kill t
 sentence-end-double-space          nil

 epg-gpg-program "gpg2"

 ;; remove annoying ellipsis when printing sexp in message buffer
 eval-expression-print-length       nil
 eval-expression-print-level        nil

 ;; http://ergoemacs.org/emacs/emacs_stop_cursor_enter_prompt.html
 minibuffer-prompt-properties
 '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)

 find-file-visit-truename t

 ;; Bookmark
 bookmark-save-flag                 t
 bookmark-default-file              (expand-file-name "bookmarks" polaris-temp-dir)

 ;; cedet
 srecode-map-save-file              (expand-file-name "srecode-map.el" polaris-temp-dir)

 ;; url
 url-configuration-directory        (concat polaris-temp-dir "url/")

 ;; Disable all backups (that's what git/dropbox are for)
 history-length                     1000
 vc-make-backup-files               nil
 auto-save-default                  t
 auto-save-list-file-name           (concat polaris-temp-dir "autosave")

 make-backup-files                  t
 create-lockfiles                   nil
 backup-directory-alist            `(list (cons "." polaris-backup-dir))
 backup-by-copying                 t
 delete-old-versions               t
 kept-new-versions                 6
 kept-old-versions                 2
 make-backup-files                 nil

 ;; Remember undo history
 ;; undo-tree-auto-save-history t
 ;; undo-tree-history-directory-alist `(("." . ,(concat polaris-temp-dir "undo/")))
 undo-tree-visualizer-timestamps t
 undo-tree-visualizer-diff t)

;; UTF-8 please
(setq locale-coding-system    'utf-8)   ; pretty
(set-terminal-coding-system   'utf-8)   ; pretty
(set-keyboard-coding-system   'utf-8)   ; pretty
(set-selection-coding-system  'utf-8)   ; please
(prefer-coding-system         'utf-8)   ; with sugar on top
(set-charset-priority 'unicode)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

(setq-default buffer-file-coding-system 'utf-8
              indent-tabs-mode nil
              indicate-buffer-boundaries '((bottom . left)))

;; ;; fix certificate issue: "gnutls.c: [0] (Emacs) fatal error: The TLS connection was non-properly terminated."
;; ;; https://github.com/nicferrier/elmarmalade/issues/55#issuecomment-166271364
;; (if (fboundp 'gnutls-available-p)
;;     (fmakunbound 'gnutls-available-p))
;; (setq tls-program '("gnutls-cli --tofu -p %p %h")
;;       imap-ssl-program '("gnutls-cli --tofu -p %p %s")
;;       smtpmail-stream-type 'starttls
;;       starttls-extra-arguments '("--tofu")
;;       )

;;
;; Variables
;;

(defvar polaris-leader-prefix "," "Prefix key for <leader> maps")
(defvar polaris-localleader-prefix "\\" "Prefix key for <localleader> maps")

;; Buffers/Files
(defvar polaris-unreal-buffers '("^ ?\\*.+\\*"
                              image-mode
                              dired-mode
                              reb-mode
                              messages-buffer-mode)
  "A list of regexps or modes whose buffers are considered unreal, and will be
ignored when using `polaris:next-real-buffer' and `polaris:previous-real-buffer', and
killed by `polaris/kill-unreal-buffers'.

`polaris:kill-this-buffer' will also gloss over these buffers when finding a new
buffer to display.")

(defvar polaris-ignore-buffers '("*Completions*" "*Compile-Log*" "*inferior-lisp*"
                              "*Fuzzy Completions*" "*Apropos*" "*Help*" "*cvs*"
                              "*Buffer List*" "*Ibuffer*" "*esh command on file*"
                              "*WoMan-Log*" "*compilation*" "*use-package*"
                             "*quickrun*" "*eclim: problems*" "*Flycheck errors*"
                              "*popwin-dummy*" " *NeoTree*"
                              ;; Helm
                              "*helm*" "*helm recentf*" "*helm projectile*" "*helm imenu*"
                              "*helm company*" "*helm buffers*" "*Helm Css SCSS*"
                              "*helm-ag*" "*helm-ag-edit*" "*Helm Swoop*"
                              "*helm M-x*" "*helm mini*" "*Helm Completions*"
                              "*Helm Find Files*" "*helm mu*" "*helm mu contacts*"
                              "*helm-mode-describe-variable*" "*helm-mode-describe-function*"
                              ;; Org
                              "*Org todo*" "*Org Links*" "*Agenda Commands*")
  "List of buffer names to ignore when using `winner-undo', or `winner-redo'")

(defvar polaris-cleanup-processes-alist '(("pry" . ruby-mode)
                                       ("irb" . ruby-mode)
                                       ("ipython" . python-mode))
  "An alist of (process-name . major-mode), that `polaris:cleanup-processes' checks
before killing processes. If there are no buffers with matching major-modes, it
gets killed.")

(defvar polaris-project-root-files
  '(".git" ".hg" ".svn" ".project" "local.properties" "project.properties"
    "rebar.config" "project.clj" "SConstruct" "pom.xml" "build.sbt"
    "build.gradle" "Gemfile" "requirements.txt" "tox.ini" "package.json"
    "gulpfile.js" "Gruntfile.js" "bower.json" "composer.json" "Cargo.toml"
    "mix.exs")
  "A list of files that count as 'project files', which determine whether a
    folder is the root of a project or not.")

;; Fringe/margins
(defvar polaris-fringe-size 10 "Default width to use for the fringes.")

;;
;; Bootstrap
;;

(autoload 'awhen "anaphora" "" nil 'macro)
(autoload 'aif "anaphora" "" nil 'macro)
(require 'use-package)
(setq use-package-verbose init-file-debug
      use-package-inject-hooks t)

(unless (require 'autoloads nil t)
  (load (concat polaris-emacs-dir "/scripts/generate-autoloads.el"))
  (require 'autoloads))
(require 'core-defuns)
(require 'diminish)

(eval-when-compile
  (setq use-package-verbose nil)

  ;; Make any folders needed
  (mapc (lambda (dir)
          (let ((path (concat polaris-temp-dir dir)))
            (unless (file-exists-p path)
              (make-directory path t))))
        '("" "/undo" "/backup")))


(use-package persistent-soft
  :commands (persistent-soft-store
             persistent-soft-fetch
             persistent-soft-exists-p
             persistent-soft-flush
             persistent-soft-location-readable
             persistent-soft-location-destroy)
  :init (defvar pcache-directory (concat polaris-temp-dir "/pcache/")))

(use-package async
  :commands (async-start
             async-start-process
             async-get
             async-wait
             async-inject-variables))

(use-package s
  :commands s-join
  :init
  (defun join-nonempty (separator &rest parts)
    "Return a string with nonempty (not nil or \"\") elements of PARTS joined with SEPARATOR."
    (declare (indent defun))
    (s-join separator (delq nil (delete "" parts)))))

(use-package delsel
  :config (delete-selection-mode 1))

(use-package hl-line
  :disabled t
  :config (global-hl-line-mode 1))

(use-package epa
  :config
  (setq epa-file-name-regexp (eval-when-compile
                               (concat (regexp-opt (list ".gpg"
                                                         ".asc"))
                                       "$")))
  (epa-file-name-regexp-update)
  (setenv "GPG_AGENT_INFO" nil))

;; clean up old buffers periodically
(use-package midnight
  :config (midnight-delay-set 'midnight-delay 0))

;; better buffer names for duplicates
(use-package uniquify
  :config
  (setq uniquify-buffer-name-style 'forward
        uniquify-separator "/"
        uniquify-ignore-buffers-re "^\\*" ; leave special buffers alone
        uniquify-after-kill-buffer-p t))

;;
;; We add this to `after-init-hook' to allow errors to stop this advice
(add-hook! after-init
  (defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
    "Prevent annoying \"Active processes exist\" query when you quit Emacs."
    (cl-flet ((process-list ())) ad-do-it)))

(when (display-graphic-p)
  (require 'server)
  (unless (server-running-p)
    (server-start)))

(defun display-startup-echo-area-message ()
  (message ":: Loaded in %s" (emacs-init-time)))

(provide 'core)
;;; core.el ends here

EMACS=emacs
CACHE_DIR="private/cache/`hostname`/`emacs --version | grep -o '2[0-9]\.[0-9]'`"
CACHE_DIR="private/cache/`hostname`/`emacs --version | grep -o '2[0-9]\.[0-9]'`"
REPO_URL="https://gitlab.com/haliax"
BASEDIR := $(shell pwd)

all: install autoloads bootstrap.elc

# If you keep emacs open while running this, run polaris/reload afterwards
install: autoloads update bootstrap.elc

autoloads:
	@$(EMACS) --batch \
		-l bootstrap.el \
		-l core/core-defuns.el \
		--eval '(polaris-reload-autoloads)' 2>&1

compile: bootstrap.elc
	@$(EMACS) --batch -f batch-byte-compile 2>&1 \
		{core,modules,modules/contrib,private}/*.el \
		{core,modules}/defuns/*.el

clean:
	@rm -rf auto-save-list recentf places ido.last async-bytecomp.log elpa tramp projectile-bookmarks.eld projectile.cache company-statistics-cache.el var semanticdb anaconda-mode
	@rm -f *.elc {core,modules,private,contrib}/*.elc {core,modules}/defuns/*.elc

reset:
	@find $(CACHE_DIR) -type f -maxdepth 1 -delete
	@rm -f $(CACHE_DIR)/{workgroups,pcache,ltxpng,backup}/*


%.elc: %.el
	@$(EMACS) --batch -f batch-byte-compile 2>&1 $<

update:
	cd $(BASEDIR)
	@$(EMACS) --batch -l packages.el 2>&1

.PHONY: all

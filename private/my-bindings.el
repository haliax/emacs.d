;;; my-bindings.el

;; Minimalistic key mapping! Why go so far for this?
;; ...
;; Uh. Good question.

(eval-when-compile (require 'core-defuns))

(map!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Global keymaps                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 "M-x"   'counsel-M-x
 ;; "M-:"   'helm-eval-expression
 "M-:"   'eval-expression
 "M-w"   'evil-window-delete
 "M-/"   'evilnc-comment-or-uncomment-lines
 "M-b"   'polaris:build
 "M-t"   'polaris:tab-create
 "M-T"   'polaris/tab-display
 "C-`"   'polaris/popup-messages
 "C-~"   'polaris:repl
 "M-`"   'polaris/popup-toggle
 "M-p"   'ace-window
 "M-i"   'helm-swoop

 "H-;"   'eval-expression
 "H-/"   'evilnc-comment-or-uncomment-lines

 "M-0" (λ! (text-scale-set 0))
 "C-+" 'text-scale-increase
 "C--" 'text-scale-decrease

 "M-w"   'polaris/close-window-or-tab
 "M-W"   'delete-frame
 "M-n"   'polaris/new-buffer
 "M-N"   'polaris/new-frame

 "C-x b"   'ivy-switch-buffer
 "C-h f"   'counsel-describe-function
 "C-h v"   'counsel-describe-variable
 "C-x C-b" 'helm-buffers-list
 "C-x C-f" 'helm-find-files
 "C-x f"   'counsel-find-file

 "C-s" 'swiper

 "C-u" 'kill-whole-line
 "M-u" 'universal-argument

 (:map universal-argument-map
   "C-u" nil
   "M-u" 'universal-argument-more)

 :i "RET" 'newline-and-indent

 "C-<escape>" 'evil-emacs-state
 :e "C-<escape>" 'evil-normal-state

 :m "C-u"  'evil-scroll-up

 :n "U"    'redo

 :m "M-1"  (λ! (polaris:switch-to-tab 0))
 :m "M-2"  (λ! (polaris:switch-to-tab 1))
 :m "M-3"  (λ! (polaris:switch-to-tab 2))
 :m "M-4"  (λ! (polaris:switch-to-tab 3))
 :m "M-5"  (λ! (polaris:switch-to-tab 4))
 :m "M-6"  (λ! (polaris:switch-to-tab 5))
 :m "M-7"  (λ! (polaris:switch-to-tab 6))
 :m "M-8"  (λ! (polaris:switch-to-tab 7))
 :m "M-9"  (λ! (polaris:switch-to-tab 8))

 (:when IS-LINUX

   "<H-left>"       'backward-word
   "<H-right>"      'forward-word
   "<M-backspace>"  'polaris/backward-kill-to-bol-and-indent
   "H-SPC"          'just-one-space
   "M-a"            'mark-whole-buffer
   "M-c"            'evil-yank
   "M-o"            'helm-find-files
   "M-q"            'evil-quit-all
   "M-s"            'evil-write
   "M-v"            'clipboard-yank
   "M-z"            'undo-tree-undo
   "M-Z"            'undo-tree-redo
   "C-M-f"          'polaris:toggle-fullscreen

   :m "M-j"  'polaris/multi-next-line
   :m "M-k"  'polaris/multi-previous-line

   :n "M-r"  'polaris:eval-buffer
   :v "M-r"  'polaris:eval-region

   :ni  "<M-f1>" 'helm-dash-at-point
   ;; Textmate-esque indent shift left/right
   :i "M-]" 'polaris/smart-indent
   :i "M-[" 'polaris/dumb-indent)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Local keymaps                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 :mv ";" 'evil-ex

 ;; <leader>
 (:leader
   :m  "SPC" 'counsel-M-x
   :nv ","  'polaris/helm-buffers-dwim
   :nv "<"  'helm-buffers-list
   :nv "."  'helm-find-files
   :nv ">"  'helm-projectile-find-file-in-known-projects
   :nv "/"  'helm-projectile-find-file
   :nv ";"  'helm-semantic-or-imenu
   :nv ":"  'helm-imenu-in-all-buffers
   :nv "]"  'helm-etags-select
   :nv "a"  'helm-projectile-find-other-file
   :nv "m"  'helm-recentf
   :nv "M"  'helm-projectile-recentf    ; recent PROJECT files
   :nv "P"  'helm-projectile-switch-project
   :v  "="  'align-regexp

   :n  "h"  'help-command
   :n  "R"  'helm-themes
   :n  "e"  'polaris/flycheck-errors
   :n  "s"  'yas-visit-snippet-file
   :n  "S"  'polaris/yas-find-file
   :n  "D"  'vc-annotate
   (:prefix "d"
     :n "." 'polaris/vcs-show-hunk
     :n "/" 'vc-diff
     :n "s" 'polaris/vcs-stage-hunk
     :n "r" 'polaris/vcs-revert-hunk)

   :n  "b"  'helm-bookmarks

   :nv "qq" 'evil-save-and-quit
   :nv "QQ" 'polaris/kill-all-buffers-do-not-remember

   ;; Quick access to my dotfiles and emacs.d
   :nv "E"  'polaris/helm-find-in-emacsd
   :nv "\\" 'polaris/helm-find-in-dotfiles

   ;; toggle
   (:prefix "t"
     :nv "l"  'polaris/nlinum-toggle
     :n  "n"  'polaris/neotree-toggle
     )

   ;; Org notes
   :n "X" 'polaris/org-start
   (:prefix "x"
     :n "." 'polaris/org-find-file
     :n "/" 'polaris/org-find-file-in-notes
     :n "e" 'polaris/org-find-exported-file
     ))
 (:localleader
   :n "\\" 'polaris/neotree
   :n "k" 'polaris/helm-descbinds-localleader
   :n "b" 'polaris:build
   :n "R" 'polaris:repl
   :v "R" 'polaris:repl-eval
   :v "r" 'polaris:eval-region
   (:prefix "r"
     :n  "e" 'emr-show-refactor-menu
     :n  "r" 'polaris:eval-buffer))

 :nv "K" 'smart-up

 ;; Don't move cursor on indent
 :n  "="  (λ! (save-excursion (call-interactively 'evil-indent)))
 :v  "="  'evil-indent

 :n  "zr" 'polaris/evil-open-folds
 :n  "zm" 'polaris/evil-close-folds
 :n  "zx" 'polaris/kill-real-buffer
 :n  "ZX" 'bury-buffer

 ;; These are intentionally reversed
 :n  "]b" 'polaris/next-real-buffer
 :n  "[b" 'polaris/previous-real-buffer
 :m  "]d" 'polaris/vcs-next-hunk
 :m  "[d" 'polaris/vcs-prev-hunk
 :m  "]e" 'polaris/flycheck-next-error
 :m  "[e" 'polaris/flycheck-previous-error

 ;; Switch tabs
 :n  "]w" 'polaris:switch-to-tab-right
 :n  "[w" 'polaris:switch-to-tab-left
 :m  "gt" 'polaris:switch-to-tab-right
 :m  "gT" 'polaris:switch-to-tab-left

 ;; Increment/decrement number under cursor
 :n "g=" 'evil-numbers/inc-at-pt
 :n "g-" 'evil-numbers/dec-at-pt

 ;; NOTE: Helm is too bulky for ffap (which I use for quick file navigation)
 :n  "gf" (λ! (helm-mode -1)
              (call-interactively 'find-file-at-point)
              (helm-mode +1))

 :n "gp" 'polaris/reselect-paste
 :n "gx" 'evil-exchange
 :v "gx" 'evil-exchange
 :n "gX" 'evil-exchange-cancel
 :v "gX" 'evil-exchange-cancel
 :n "gr" 'polaris:eval-region
 :n "gR" 'polaris:eval-buffer
 :v "gR" 'polaris:eval-region-and-replace
 :m "gl" 'avy-goto-line
 :m "gw" 'avy-goto-char-2
 :m "g]" 'smart-right
 :m "g[" 'smart-left
 :v "@"  'polaris/evil-macro-on-all-lines
 :n "g@" 'polaris/evil-macro-on-all-lines

 :v "."  'evil-repeat

 ;; vnoremap < <gv
 :v "<"   (λ! (evil-shift-left (region-beginning) (region-end))
              (evil-normal-state)
              (evil-visual-restore))
 ;; vnoremap > >gv
 :v ">"   (λ! (evil-shift-right (region-beginning) (region-end))
              (evil-normal-state)
              (evil-visual-restore))

 ;; undo/redo for regions (NOTE: Buggy!)
 :nv "u"    'undo-tree-undo
 :nv "C-r"  'undo-tree-redo

 :v "*"   'evil-visualstar/begin-search-forward
 :v "#"   'evil-visualstar/begin-search-backward

 :n "Y"   "y$"

 ;; paste from recent yank register; which isn't overwritten by deletes or
 ;; other operations.
 :v "C-p"   "\"0p"

 :v "S"   'evil-surround-region
 :v "v"   'er/expand-region
 :v "V"   'er/contract-region

 ;; iedit and my own version of multiple-cursors
 :v "R"    'evil-iedit-state/iedit-mode  ; edit all instances of marked region
 :nv "M-d" 'polaris/mark-and-next
 :nv "M-D" 'polaris/mark-and-prev

 ;; aliases for %
 :m "%"   'evilmi-jump-items
 :m [tab] (λ! (if (ignore-errors (hs-already-hidden-p))
                  (hs-toggle-hiding)
                (call-interactively 'evilmi-jump-items)))

 ;; Textmate-esque newlines
 :i "<backspace>"   'backward-delete-char-untabify
 :i "<M-backspace>" 'polaris/backward-kill-to-bol-and-indent
 :i "<C-return>"    'evil-ret-and-indent

 ;; escape from insert mode (more responsive than using key-chord-define)
 :ir  "j"    'polaris:exit-mode-maybe
 :ir  "J"    'polaris:exit-mode-maybe
 :irv "C-g"  'evil-normal-state

 :o "s"      'evil-surround-edit
 :o "S"      'evil-Surround-edit

 :n "!"      'rotate-text

 (:map evil-window-map                  ; prefix "C-w"
   "u"       'polaris/undo-window-change

   ;; Jump to new splits
   "s"       'polaris/evil-window-split
   "v"       'polaris/evil-window-vsplit

   ;; Move window in one step
   "H"       'polaris/evil-window-move-left
   "J"       'polaris/evil-window-move-down
   "K"       'polaris/evil-window-move-up
   "L"       'polaris/evil-window-move-right

   "C-u"     'polaris/undo-window-change
   "C-r"     'polaris/redo-window-change
   "C-h"     'evil-window-left
   "C-j"     'evil-window-down
   "C-k"     'evil-window-up

   "C-l"     'evil-window-right

   "C-w"     'ace-window
   "C-S-w"   (λ! (ace-window 4))        ; swap windows
   "C-C"     (λ! (ace-window 16)))      ; delete windows

 ;; Vim omni-complete emulation
 :i "C-SPC" 'polaris/company-complete
 (:prefix "C-x"
   :i "C-l"   'polaris/company-whole-lines
   :i "C-k"   'polaris/company-dict-or-keywords
   :i "C-f"   'company-files
   :i "C-]"   'company-tags
   :i "s"     'company-ispell
   :i "C-s"   'company-yasnippet
   :i "C-o"   'company-semantic
   :i "C-n"   'company-dabbrev-code
   :i "C-p"   (λ! (let ((company-selection-wrap-around t))
                    (call-interactively 'company-dabbrev-code)
                    (company-select-previous-or-abort))))

 (:after company
   (:map company-active-map
     "C-o"        'company-search-kill-others
     "C-n"        'company-select-next
     "C-p"        'company-select-previous
     "C-h"        'company-quickhelp-manual-begin
     "C-S-h"      'company-show-doc-buffer
     "C-S-s"      'company-search-candidates
     "C-s"        'company-filter-candidates
     "C-SPC"      'company-complete-common-or-cycle
     "<tab>"      'polaris/company-complete-common-or-complete-full
     "TAB"        'polaris/company-complete-common-or-complete-full
     "RET"        nil
     "<return>"   nil
     "<up>"       'company-select-above
     "<down>"     'company-select-below
     "<backtab>"  'company-select-previous
     [escape]     (λ! (company-abort) (evil-normal-state 1))
     "<C-return>" 'helm-company)
   (:map company-search-map
     "C-n"        'company-search-repeat-forward
     "C-p"        'company-search-repeat-backward
     [escape]     'company-search-abort))

 (:after help-mode
   (:map help-map
     "e" 'polaris/popup-messages
     ;; Remove slow/annoying help subsections
     "h" nil
     "g" nil)
   (:map help-mode-map
     :n "]]" 'help-go-forward
     :n "[[" 'help-go-back
     :n "<escape>" 'polaris/popup-close)))

;; Line-wise mouse selection on margin
(global-set-key (kbd "<left-margin> <down-mouse-1>") 'polaris/mouse-drag-line)
(global-set-key (kbd "<left-margin> <mouse-1>")      'polaris/mouse-select-line)
(global-set-key (kbd "<left-margin> <drag-mouse-1>") 'polaris/mouse-select-line)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Keymap fixes                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This section is dedicated to bindings that "fix" certain keys so that they behave more
;; like vim (or how I like it).

(map!
 "C-b" 'backward-word
 ;; Highjacks space/backspace to:
 ;;   a) delete spaces on either side of the cursor, if present ( | ) -> (|)
 ;;   b) allow backspace to delete space-indented blocks intelligently
 ;;   c) and not do any of this magic when inside a string
 :i "SPC"                                  'polaris/inflate-space-maybe
 :i [remap backward-delete-char-untabify]  'polaris/deflate-space-maybe
 :i [remap newline]                        'polaris/newline-and-indent

 ;; Smarter move-to-beginning-of-line
 :i [remap move-beginning-of-line]         'polaris/move-to-bol

 ;; Restore bash-esque keymaps in insert mode; C-w and C-a already exist
 :i "C-e" 'polaris/move-to-eol
 :i "C-u" 'polaris/backward-kill-to-bol-and-indent

 ;; Fixes delete
 :i "<kp-delete>" 'delete-char

 ;; Fix osx keymappings and then some
 :i "<M-left>"   'polaris/move-to-bol
 :i "<M-right>"  'polaris/move-to-eol
 :i "<M-up>"     'beginning-of-buffer
 :i "<M-down>"   'end-of-buffer
 :i "<C-up>"     'smart-up
 :i "<C-down>"   'smart-down

 ;; Fix emacs motion keys
 :i "A-b"      'evil-backward-word-begin
 :i "A-w"      'evil-forward-word-begin
 :i "A-e"      'evil-forward-word-end

 ;; Textmate-esque insert-line before/after
 :i "<M-return>"    'evil-open-below
 :i "<S-M-return>"  'evil-open-above
 ;; insert lines in-place)
 :n "<M-return>"    (λ! (save-excursion (evil-insert-newline-below)))
 :n "<S-M-return>"  (λ! (save-excursion (evil-insert-newline-above)))

 ;; Make ESC quit all the things
 :e [escape] 'evil-normal-state
 (:map (minibuffer-local-map
        minibuffer-local-ns-map
        minibuffer-local-completion-map
        minibuffer-local-must-match-map
        minibuffer-local-isearch-map)
   [escape] 'polaris-minibuffer-quit
   "C-r" 'evil-paste-from-register)

 (:map read-expression-map "C-w" 'backward-kill-word)

 (:map view-mode-map "<escape>" 'View-quit-all)

 (:map evil-ex-completion-map "C-a" 'move-beginning-of-line))

(provide 'my-bindings)
;;; my-bindings.el ends here

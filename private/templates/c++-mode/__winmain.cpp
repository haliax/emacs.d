# -*- mode: snippet -*-
# group: file templates
# contributor: Axel Parra
# --
#include <Windows.h>

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    $0

    return 0;
}

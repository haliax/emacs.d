# -*- mode: snippet -*-
# group: file templates
# contributor: Axel Parra
# --
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    $0

    return 0;
}

;;; polaris-theme.el --- A dark, soothing theme.

;; Copyright (C) 2016 , Axel Parra

;; Author: Axel Parra
;; URL: <https://github.com/polaristerslayer/polaris-theme>
;;
;; Version: 0.0.1

;; Keywords: color, theme
;; Package-Requires: ((emacs "24"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of Emacs.

;;; Commentary:

;; To use it, put the following in your Emacs configuration file:
;;
;;   (load-theme 'material t)
;;
;; Requirements: Emacs 24.

;;; Code:

;; Utilities

;; Color helper functions
;; Shamelessly *borrowed* from solarized
(defun polaris-color-name-to-rgb (color &optional frame)
  "Convert COLOR string to a list of normalized RGB components.
COLOR should be a color name (e.g. \"white\") or an RGB triplet
string (e.g. \"#ff12ec\").
Normally the return value is a list of three floating-point
numbers, (RED GREEN BLUE), each between 0.0 and 1.0 inclusive.
Optional argument FRAME specifies the frame where the color is to be
displayed.  If FRAME is omitted or nil, use the selected frame.
If FRAME cannot display COLOR, return nil."
  ;; `colors-values' maximum value is either 65535 or 65280 depending on the
  ;; display system.  So we use a white conversion to get the max value.
  (let ((valmax (float (car (color-values "#FFFFFF")))))
    (mapcar (lambda (x) (/ x valmax)) (color-values color frame))))

(defun polaris-color-rgb-to-hex  (red green blue)
  "Return hexadecimal notation for the color RED GREEN BLUE.
RED, GREEN, and BLUE should be numbers between 0.0 and 1.0, inclusive."
  (format "#%02x%02x%02x"
          (* red 255) (* green 255) (* blue 255)))

(defun polaris-color-blend (color1 color2 alpha)
  "Blends COLOR1 onto COLOR2 with ALPHA.
COLOR1 and COLOR2 should be color names (e.g. \"white\") or RGB
triplet strings (e.g. \"#ff12ec\").
Alpha should be a float between 0 and 1."
  (apply 'polaris-color-rgb-to-hex
         (-zip-with '(lambda (it other)
                       (+ (* alpha it) (* other (- 1 alpha))))
                    (polaris-color-name-to-rgb color1)
                    (polaris-color-name-to-rgb color2))))

(deftheme polaris "The Polaris color theme")
(display-color-cells (selected-frame))

(let* ((class '((class color) (min-colors 89)))
       ;; Polaris palette
       (polaris-fg-4      "#937248")
       (polaris-fg-3      "#A07F55")
       (polaris-fg-2      "#AD8C62")
       (polaris-fg-1      "#B9986E")
       (polaris-fg        "#C6A57B")
       (polaris-fg+1      "#D3B288")
       (polaris-fg+2      "#E0BF95")
       (polaris-fg+3      "#ECCBA1")
       (polaris-fg+4      "#F9D8AE")

       (polaris-bg-1      "#101010")
       (polaris-bg-05     "#121212")
       (polaris-bg        "#151515")
       (polaris-bg+1      "#2F2F2F")
       (polaris-bg+2      "#484848")
       (polaris-bg+3      "#626262")
       (polaris-bg+4      "#7B7B7B")

       (polaris-red-4     "#620000")
       (polaris-red-3     "#6F0D0B")
       (polaris-red-2     "#7C1A18")
       (polaris-red-1     "#882624")
       (polaris-red       "#953331")
       (polaris-red+1     "#A2403E")
       (polaris-red+2     "#AF4D4B")
       (polaris-red+3     "#BB5957")
       (polaris-red+4     "#C86664")

       (polaris-orange-4  "#872801")
       (polaris-orange-3  "#94350E")
       (polaris-orange-2  "#A1421B")
       (polaris-orange-1  "#AD4E27")
       (polaris-orange    "#BA5B34")
       (polaris-orange+1  "#C76841")
       (polaris-orange+2  "#D4754E")
       (polaris-orange+3  "#E0815A")
       (polaris-orange+4  "#ED8E67")

       (polaris-yellow-4  "#5D6404")
       (polaris-yellow-3  "#6A7111")
       (polaris-yellow-2  "#777E1E")
       (polaris-yellow-1  "#838A2A")
       (polaris-yellow    "#909737")
       (polaris-yellow+1  "#9DA444")
       (polaris-yellow+2  "#AAB151")
       (polaris-yellow+3  "#B6BD5D")
       (polaris-yellow+4  "#C3CA6A")

       (polaris-green-4   "#213700")
       (polaris-green-3   "#2E4403")
       (polaris-green-2   "#3B5110")
       (polaris-green-1   "#475D1C")
       (polaris-green     "#546A29")
       (polaris-green+1   "#617736")
       (polaris-green+2   "#6E8443")
       (polaris-green+3   "#7A904F")
       (polaris-green+4   "#879D5C")

       (polaris-cyan-4    "#01343C")
       (polaris-cyan-3    "#0E4149")
       (polaris-cyan-2    "#1B4E56")
       (polaris-cyan-1    "#275A62")
       (polaris-cyan      "#34676F")
       (polaris-cyan+1    "#41747C")
       (polaris-cyan+2    "#4E8189")
       (polaris-cyan+3    "#5A8D95")
       (polaris-cyan+4    "#679AA2")

       (polaris-blue-4    "#052B38")
       (polaris-blue-3    "#123845")
       (polaris-blue-2    "#1F4552")
       (polaris-blue-1    "#2B515E")
       (polaris-blue      "#385e6b")
       (polaris-blue+1    "#456B78")
       (polaris-blue+2    "#527885")
       (polaris-blue+3    "#5E8491")
       (polaris-blue+4    "#6B919E")

       (polaris-magenta-4 "#4C022B")
       (polaris-magenta-3 "#590F38")
       (polaris-magenta-2 "#661C45")
       (polaris-magenta-1 "#722851")
       (polaris-magenta   "#7F355E")
       (polaris-magenta+1 "#8C426B")
       (polaris-magenta+2 "#994F78")
       (polaris-magenta+3 "#A55B84")
       (polaris-magenta+4 "#B26891"))

  (apply 'custom-theme-set-faces 'polaris
         (mapcar
          (lambda (x) `(,(car x) ((t ,(cdr x)))))
          `(
            ;; basic coloring
            (default :foreground ,polaris-fg :background ,polaris-bg)
            (default-italic :italic t)
            (cursor :foreground ,polaris-fg :background ,polaris-fg)
            (button :inherit link :underline t)
            (error :foreground ,polaris-red :weight bold)
            (escape-glyph-face :foreground ,polaris-red)
            (fringe :foreground ,polaris-fg+1 :background ,polaris-bg)
            (header-line :foreground ,polaris-yellow
                         :background ,polaris-bg-1
                         :box (:line-width -1 :color ,polaris-bg :style released-button))
            (highlight :background ,polaris-bg+1)
            (link :foreground ,polaris-yellow :underline t :weight bold)
            (link-visited :foreground ,polaris-yellow-2 :underline t :weight normal)
            (match :background ,polaris-bg-1 :foreground ,polaris-orange :weight bold)
            (menu :foreground ,polaris-fg :background ,polaris-bg)
            (minibuffer-prompt :foreground ,polaris-blue)
            (region :background ,polaris-fg :foreground ,polaris-blue)
            (secondary-selection :background ,polaris-bg+2)
            (shadow :foreground ,polaris-bg+3)
            (success :foreground ,polaris-green :weight bold)
            (trailing-whitespace :background ,polaris-red)
            (vertical-border :foreground ,polaris-fg)
            (warning :foreground ,polaris-yellow)

            ;; apropos
            (apropos-keybinding :foreground ,polaris-red-2 :weight bold)
            (apropos-symbol :foreground ,polaris-green+2 :wight bold)

            ;; calendar
            (calendar-today :box (:line-width -1 :color ,polaris-red :style nil))
            (holiday :background ,polaris-bg+2)

            ;; compilation
            (compilation-column-face :foreground ,polaris-yellow)
            (compilation-column-number :inherit font-lock-doc-face :foreground ,polaris-cyan
                                       :underline nil)
            (compilation-enter-directory-face :foreground ,polaris-green)
            (compilation-error :inherit error :underline nil)
            (compilation-error-face :foreground ,polaris-red-1 :weight bold :underline t)
            (compilation-face :foreground ,polaris-fg)
            (compilation-info-face :foreground ,polaris-blue)
            (compilation-info :foreground ,polaris-green+4 :underline t)
            (compilation-leave-directory-face :foreground ,polaris-green)
            (compilation-line-face :foreground ,polaris-yellow)
            (compilation-line-number :foreground ,polaris-yellow)
            (compilation-message-face :foreground ,polaris-blue)
            (compilation-warning :inherit warning :underline nil)
            (compilation-warning-face :foreground ,polaris-yellow-1 :weight bold :underline t)

            (compilation-mode-line-exit :foreground unspecified :weight bold)
            (compilation-mode-line-fail :inherit compilation-error :foreground ,polaris-red :weight bold)
            (compilation-mode-line-run :foreground ,polaris-yellow :weight bold)

            ;; diary
            (diary :foreground ,polaris-green+2 :weight bold)
            (diary-anniversary :foreground ,polaris-red)
            (diary-time :foreground ,polaris-blue-4)

            ;; dired
            (dired-directory :foreground ,polaris-blue :weight normal)
            (dired-flagged :foreground ,polaris-red)
            (dired-header :foreground ,polaris-bg :background ,polaris-blue)
            (dired-ignored :inherit shadow)
            (dired-mark :foreground ,polaris-yellow :weight bold)
            (dired-marked :foreground ,polaris-magenta :weight bold)
            (dired-perm-write :foreground ,polaris-bg-1 :underline t)
            (dired-symlink :foreground ,polaris-cyan :weight normal :slant italic)
            (dired-warning :foreground ,polaris-yellow :underline t)

            ;; dired-async
            (dired-async-message :background ,polaris-yellow)
            (dired-async-mode-message :background ,polaris-red)

            ;; dired-efap
            (dired-efap-face (:box nil
                                   :background ,polaris-blue-1
                                   :foreground ,polaris-bg+2
                                   :underline t
                                   :weight bold))

            ;; ecb
            (ecb-default-highlight-face :background ,polaris-blue :foreground ,polaris-bg+3)
            (ecb-history-bucket-node-dir-soure-path-face
             :inherit ecb-history-bucket-node-face :foreground ,polaris-yellow)
            (ecb-source-in-directories-buffer-face :inherit ecb-directories-general-face
                                                   :foreground ,polaris-blue-4)
            (ecb-history-dead-buffer-face :inherit ecb-history-general-face
                                          :foreground ,polaris-blue-3)
            (ecb-directory-not-accessible-face :inherit ecb-directories-general-face
                                               :foreground ,polaris-blue-3)
            (ecb-bucket-node-face :inherit ecb-default-general-face :weight normal
                                  :foreground ,polaris-blue)
            (ecb-tag-header-face :background ,polaris-bg+2)
            (ecb-analyse-bucket-element-face :inherit ecb-analyse-general-face
                                             :foreground ,polaris-green)
            (ecb-directories-general-face :inherit ecb-default-general-face :height 1.0)
            (ecb-method-non-semantic-face :inherit ecb-methods-general-face
                                          :foreground ,polaris-cyan)
            (ecb-mode-line-prefix-face :foreground ,polaris-green)
            (ecb-tree-guide-line-face :inherit ecb-default-general-face
                                      :foreground ,polaris-bg+3 :height 1.0)

            ;; font lock
            (font-lock-builtin-face :foreground ,polaris-blue)
            (font-lock-comment-face :foreground ,polaris-bg+2 :slant italic)
            (font-lock-comment-delimiter-face :foreground ,polaris-bg+2)
            (font-lock-constant-face :foreground ,polaris-magenta)
            (font-lock-doc-face :foreground ,polaris-green+1)
            (font-lock-doc-string-face :foreground ,polaris-blue+1)
            (font-lock-function-name-face :foreground ,polaris-blue)
            (font-lock-keyword-face :foreground ,polaris-yellow)
            (font-lock-negation-char-face :foreground ,polaris-fg :bold t)
            (font-lock-preprocessor-char-face :foreground ,polaris-blue :bold t)
            (font-lock-regexp-grouping-backslash :foreground ,polaris-red-1 :bold t)
            (font-lock-regexp-grouping-construct :foreground ,polaris-yellow+1 :bold t)
            (font-lock-string-face :foreground ,polaris-red)
            (font-lock-type-face :foreground ,polaris-yellow)
            (font-lock-variable-name-face :foreground ,polaris-orange)
            (font-lock-warning-face :foreground ,polaris-yellow-1 :weight bold :underline t)
            (c-annotation-face :inherit font-lock-constant-face)

            ;; grep
            (grep-context-face :foreground ,polaris-fg)
            (grep-error-face :foreground ,polaris-red-1 :weight bold :underline t)
            (grep-hit-face :foreground ,polaris-blue)
            (grep-match-face :foreground ,polaris-orange :weight bold)

            ;; isearch
            (isearch :foreground ,polaris-yellow :background ,polaris-bg-1)
            (isearch-fail :foreground ,polaris-fg :background ,polaris-red-4)
            (lazy-highlight :foreground ,polaris-yellow :background ,polaris-bg+2)

            ;; mode-line
            (mode-line :foreground ,polaris-fg
                       :background ,polaris-bg-1
                       :box (:line-width 5 :color ,polaris-bg-1))
            (mode-line-buffer-id :foreground ,polaris-yellow :weight bold)
            (mode-line-emphasis :foreground ,polaris-orange)
            (mode-line-highlight :foreground ,polaris-orange)
            (mode-line-inactive :foreground ,polaris-bg+3
                                :background ,polaris-bg-1
                                :box (:line-width 5 :color ,polaris-bg-1))
            (mode-line-is-modified-face :foreground ,polaris-red)
            (mode-line-count-face :foreground ,polaris-bg :background ,polaris-magenta)

            ;; Third party

            ;; auctex
            (font-latex-bold-face :inherit bold :foreground ,polaris-fg+1)
            (font-latex-doctex-documentation-face :background unspecified)
            (font-latex-doctex-preprocessor-face :inherit (font-latex-doctex-documentation-face
                                                           font-lock-builtin-face
                                                           font-lock-preprocessor-face))
            (font-latex-italic-face :inherit italic :foreground ,polaris-fg+1)
            (font-latex-math-face :foreground ,polaris-magenta)
            (font-latex-sectioning-0-face :inherit font-latex-sectioning-1-face)
            (font-latex-sectioning-1-face :inherit font-latex-sectioning-2-face)
            (font-latex-sectioning-2-face :inherit font-latex-sectioning-3-face)
            (font-latex-sectioning-3-face :inherit font-latex-sectioning-4-face)
            (font-latex-sectioning-4-face :inherit font-latex-sectioning-5-face)
            (font-latex-sectioning-5-face :inherit variable-pitch :foreground ,polaris-yellow
                                          :weight bold)
            (font-latex-sedate-face :foreground ,polaris-yellow)
            (font-latex-slide-title-face :inherit (variable-pitch font-lock-type-face)
                                         :weight bold)
            (font-latex-string-face :foreground ,polaris-cyan)
            (font-latex-verbatim-face :inherit fixed-pitch :foreground ,polaris-blue-1
                                      :slant italic)
            (font-latex-warning-face :inherit bold :foreground ,polaris-orange)

            ;; auto highlight symbol
            (ahs-definition-face :foreground ,polaris-magenta :background unspecified
                                 :slant normal)
            (ahs-edit-mode-face :foreground ,polaris-bg :background ,polaris-magenta)
            (ahs-face :foreground ,polaris-magenta :background unspecified)
            (ahs-plugin-bod-face :foreground ,polaris-magenta :background unspecified )
            (ahs-plugin-defalt-face :foreground ,polaris-magenta :background unspecified)
            (ahs-plugin-whole-buffer-face :foreground ,polaris-magenta  :background unspecified)
            (ahs-warning-face :foreground ,polaris-red :weight bold)

            ;; android mode
            (android-mode-debug-face :foreground ,polaris-green)
            (android-mode-error-face :foreground ,polaris-orange :weight bold)
            (android-mode-info-face :foreground ,polaris-blue)
            (android-mode-verbose-face :foreground ,polaris-blue-1)
            (android-mode-warning-face :foreground ,polaris-yellow)

            ;; anzu-mode
            (anzu-mode-line :background ,polaris-blue+1 :foreground ,polaris-bg-1 :weight bold)

            ;; avy-mode
            (avy-lead-face :inherit isearch)
            (avy-lead-face-0 :inherit isearch :background ,polaris-magenta)
            (avy-lead-face-2 :inherit isearch :background ,polaris-cyan)

            ;; bm
            (bm-face :overline ,polaris-fg)
            (bm-fringe-face :overline ,polaris-fg)
            (bm-fringe-persistent-face :overline ,polaris-fg)
            (bm-persistent-face :overline ,polaris-fg)

            ;; clojure-test-mode
            (clojure-test-failure-face :foreground ,polaris-orange :weight bold :underline t)
            (clojure-test-error-face :foreground ,polaris-red :weight bold :underline t)
            (clojure-test-success-face :foreground ,polaris-green :weight bold :underline t)

            ;; cider-repl-mode
            (cider-repl-err-output-face :inherit ,font-lock-warning-face :underline nil)

            ;; cider-test-mode
            (cider-test-failure-face :foreground ,polaris-orange :weight bold :underline t)
            (cider-test-error-face :foreground ,polaris-red :weight bold :underline t)
            (cider-test-success-face :foreground ,polaris-green :weight bold :underline t)

            ;; company-mode
            (tooltip :background ,polaris-bg+2 :foreground ,polaris-orange-1)
            (company-tooltip :background ,polaris-bg-1 :foreground ,polaris-fg :weight bold :underline nil)
            (company-tooltip-annotation :background ,polaris-bg-1 :foreground ,polaris-green-1)
            (company-tooltip-annotation-selection :background ,polaris-bg-1 :foreground ,polaris-green-1)
            (company-tooltip-selection :background ,polaris-bg-1 :foreground ,polaris-blue+1 :weight bold :underline nil)
            (company-tooltip-common :inherit company-tooltip)
            (company-tooltip-common-selection :inherit company-tooltip-selection)
            (company-tooltip-search :foreground ,polaris-yellow)
            (company-echo-common :inherit company-tooltip :foreground ,polaris-yellow)
            (company-scrollbar-bg :background ,polaris-bg+1)
            (company-scrollbar-fg :background ,polaris-bg+3)
            (company-tooltip-annotation :inherit company-tooltip :foreground ,polaris-fg-1)
            (company-preview :background ,polaris-bg-1 :foreground ,polaris-fg)
            (company-preview-common :foreground ,polaris-bg+3)
            (company-preview-search :background ,polaris-bg+3)

            ;; coffee
            (coffee-mode-class-name :foreground ,polaris-yellow :weight bold)
            (coffee-mode-function-param :foreground ,polaris-magenta :slant italic)

            ;; custom
            (custom-state :foreground ,polaris-green)
            (custom-button :background ,polaris-bg+1 :foreground ,polaris-fg
                           :box (:line-width 2 :style released-button))
            (custom-button-mouse :background ,polaris-fg :foreground ,polaris-bg+1
                                 :box (:line-width 2 :style released-button))
            (custom-button-pressed :background ,polaris-bg+1 :foreground ,polaris-fg-1
                                   :box (:line-width 2 :style pressed-button))
            (custom-button-unraised :inherit underline)
            (custom-button-pressed-unraised :inherit custom-button-unraised :foreground ,polaris-magenta)

            ;; diff
            (diff-added :foreground ,polaris-green)
            (diff-changed :foreground ,polaris-yellow)
            (diff-removed :foreground ,polaris-red)
            (diff-header :background ,polaris-bg+1)
            (diff-file-header :background ,polaris-bg+2 :foreground ,polaris-fg :bold t)
            (diff-context :foreground ,polaris-fg-1)
            (diff-indicator-added :inherit diff-added)
            (diff-indicator-changed :inherit diff-changed)
            (diff-indicator-removed :inherit diff-removed)
            (diff-refine-added :inherit diff-added
                               :underline t)
            (diff-refine-changed :inherit diff-changed
                                 :underline t)
            (diff-refine-removed :inherit diff-removed
                                 :underline t)
            (diff-header :foreground ,polaris-blue-2 :weight bold)
            (diff-hunk-header :inherit diff-header
                              :foreground ,polaris-green-1)
            (diff-file-header :inherit diff-header
                              :foreground ,polaris-cyan-1)
            (diff-function :inherit diff-header
                           :foreground ,polaris-blue)
            (diff-index :inherit diff-header
                        :foreground ,polaris-red-1)
            (diff-nonexistent :inherit diff-header
                              :foreground ,polaris-bg+3)

            ;; diff-hl
            (diff-hl-change :background ,polaris-bg+1 :foreground ,polaris-blue)
            (diff-hl-delete :background ,polaris-bg+1 :foreground ,polaris-red)
            (diff-hl-insert :background ,polaris-bg+1 :foreground ,polaris-green)
            (diff-hl-unknown :background ,polaris-bg+1 :foreground ,polaris-orange)

            ;; ediff
            (ediff-even-diff-A :background ,polaris-bg)
            (ediff-odd-diff-A  :background ,polaris-bg-1)
            (ediff-even-diff-B :inherit ediff-even-diff-A)
            (ediff-odd-diff-B  :inherit ediff-odd-diff-A)
            (ediff-even-diff-C :inherit ediff-even-diff-A)
            (ediff-odd-diff-C  :inherit ediff-odd-diff-A)

            ;; enh-ruby-mode
            (enh-ruby-string-delimiter-face :foreground ,polaris-yellow)
            (enh-ruby-heredoc-delimiter-face :inherit enh-ruby-string-delimiter-face)
            (enh-ruby-regexp-delimiter-face :inherit enh-ruby-string-delimiter-face)
            (enh-ruby-op-face :foreground ,polaris-bg+3)
            (erm-syn-errline :inherit flymake-errline)
            (erm-syn-warnline :inherit flymake-warnline)

            ;; eldoc
            (eldoc-highlight-function-argument :foreground ,polaris-green
                                               :weight bold)

            ;; elfeed
            (elfeed-search-date-face :foreground ,polaris-blue)
            (elfeed-search-feed-face :foreground ,polaris-bg+2)
            (elfeed-search-tag-face :foreground ,polaris-blue)
            (elfeed-search-title-face :foreground ,polaris-blue)

            ;; erc
            (erc-action-face :inherit erc-default-face)
            (erc-bold-face :weight bold)
            (erc-current-nick-face :foreground ,polaris-blue :weight bold)
            (erc-dangerous-host-face :inherit font-lock-warning)
            (erc-default-face :foreground ,polaris-fg)
            (erc-direct-msg-face :inherit erc-default)
            (erc-error-face :inherit font-lock-warning)
            (erc-fool-face :inherit erc-default)
            (erc-action-face :inherit erc-bold-face)
            (erc-header-line :inherit header-line)
            (erc-highlight-face :inherit hover-highlight)
            (erc-input-face :foreground ,polaris-yellow)
            (erc-keyword-face :foreground ,polaris-blue :weight bold)
            (erc-nick-default-face :foreground ,polaris-yellow :weight bold)
            (erc-my-nick-face :foreground ,polaris-red :weigth bold)
            (erc-nick-msg-face :inherit erc-default)
            (erc-notice-face :foreground ,polaris-green)
            (erc-pal-face :foreground ,polaris-orange :weight bold)
            (erc-prompt-face :foreground ,polaris-orange :background ,polaris-bg :weight bold)
            (erc-timestamp-face :foreground ,polaris-green+1)
            (erc-underline-face :underline t)

            ;; epa
            (epa-mark :foreground ,polaris-blue+1)
            (epa-string :foreground ,polaris-cyan+2)
            (epa-validity-disabled :foreground ,polaris-fg-2)
            (epa-validity-high :foreground ,polaris-green-1)
            (epa-validity-medium :foreground ,polaris-yellow-1)
            (epa-validity-low :foreground ,polaris-red-1)

            ;; eshell
            (eshell-prompt :foreground ,polaris-yellow)
            (eshell-ls-archive :foreground ,polaris-red-1 :weight bold)
            (eshell-ls-backup :inherit font-lock-comment)
            (eshell-ls-clutter :inherit font-lock-comment)
            (eshell-ls-directory :foreground ,polaris-blue+1 :weight bold)
            (eshell-ls-executable :foreground ,polaris-red+1 :weight bold)
            (eshell-ls-unreadable :foreground ,polaris-fg)
            (eshell-ls-missing :inherit font-lock-warning-face)
            (eshell-ls-product :inherit font-lock-doc-face)
            (eshell-ls-readonly :foreground ,polaris-fg-2)
            (eshell-ls-special :foreground ,polaris-yellow)
            (eshell-ls-symlink :foreground ,polaris-cyan :weight bold)

            ;; evil-snipe
            (evil-snipe-first-match-face :foreground ,polaris-yellow :background ,polaris-bg-05)
            (evil-snipe-matches-face :foreground ,polaris-orange :underline t)

            ;; flymake
            (flymake-errline :underline (:style wave :color ,polaris-red)
                             :foreground ,polaris-red :weight bold)
            (flymake-warnline :underline (:style wave :color ,polaris-orange)
                              :foreground ,polaris-orange :weight bold)
            (flymake-infoline :underline (:style wave :color ,polaris-blue)
                              :foreground ,polaris-blue :weight bold)

            ;; flycheck
            (flycheck-error :underline (:style wave :color ,polaris-red)
                            :foreground ,polaris-red :weight bold :underline t)
            (flycheck-warning :underline (:style wave :color ,polaris-orange)
                              :foreground ,polaris-orange :weight bold :underline t)
            (flycheck-info :underline (:style wave :color ,polaris-green) :inherit unspecified)
            (flycheck-fringe-error :foreground ,polaris-red :background ,polaris-bg)
            (flycheck-fringe-warning :foreground ,polaris-orange :background ,polaris-bg)
            (flycheck-fringe-info :foreground ,polaris-blue
                                  :background ,polaris-bg :weight bold)

            ;; flyspell
            (flyspell-duplicate :underline (:style wave :color ,polaris-yellow-1) :inherit unspecified
                                :foreground ,polaris-yellow-1 :weight bold :underline t)
            (flyspell-incorrect :underline (:style wave :color ,polaris-red-1) :inherit unspecified
                                :foreground ,polaris-red-1 :weight bold :underline t)

            ;; geiser
            (geiser-font-lock-doc-title :inherit bold)
            (geiser-font-lock-doc-link :inherit link)
            (geiser-font-lock-doc-button :inherit button)
            (geiser-font-lock-xref-header :inherit bold)
            (geiser-font-lock-xref-link :inherit link)
            (geiser-font-lock-error-link :inherit (error link))
            (geiser-font-lock-autodoc-identifier :inherit font-lock-function-name-face)
            (geiser-font-lock-autodoc-current-arg :inherit font-lock-variable-name-face)

            ;; git-commit
            (git-commit-comment-action  :foreground ,polaris-fg+1  :weight bold)
            (git-commit-comment-branch  :foreground ,polaris-blue   :weight bold)
            (git-commit-comment-heading :foreground ,polaris-yellow :weight bold)

            ;; git-gutter
            (git-gutter:added :weight normal
                              :foreground ,polaris-green
                              :background ,polaris-bg)
            (git-gutter:deleted :weight normal
                                :foreground ,polaris-red
                                :background ,polaris-bg)
            (git-gutter:modified :weight normal
                                 :foreground ,polaris-blue
                                 :background ,polaris-bg)
            (git-gutter:unchanged :weight normal
                                  :foreground ,polaris-fg
                                  :background ,polaris-bg
                                  )
            ;; git-gutter-fr
            (git-gutter-fr:added :foreground ,polaris-green  :weight bold)
            (git-gutter-fr:deleted :foreground ,polaris-red :weight bold)
            (git-gutter-fr:modified :foreground ,polaris-blue :weight bold)

            ;; git-gutter+ and git-gutter+-fr
            (git-gutter+-added :background ,polaris-green :foreground ,polaris-bg+1
                               :weight bold)
            (git-gutter+-deleted :background ,polaris-red :foreground ,polaris-bg+1
                                 :weight bold)
            (git-gutter+-modified :background ,polaris-blue :foreground ,polaris-bg+1
                                  :weight bold)
            (git-gutter+-unchanged :background ,polaris-bg
                                   :foreground ,polaris-fg
                                   :weight bold)
            (git-gutter-fr+-added :foreground ,polaris-green :weight bold)
            (git-gutter-fr+-deleted :foreground ,polaris-red :weight bold)
            (git-gutter-fr+-modified :foreground ,polaris-blue :weight bold)

            ;; git-rebase
            (git-rebase-hash :foreground ,polaris-orange)

            ;; go-mode
            (go-coverage-0 :foreground ,polaris-orange)
            (go-coverage-1 :foreground ,(polaris-color-blend polaris-blue polaris-yellow (/ 2.0 6)))
            (go-coverage-2 :foreground ,(polaris-color-blend polaris-blue polaris-yellow (/ 3.0 6)))
            (go-coverage-3 :foreground ,(polaris-color-blend polaris-blue polaris-yellow (/ 4.0 6)))
            (go-coverage-4 :foreground ,(polaris-color-blend polaris-blue polaris-yellow (/ 5.0 6)))
            (go-coverage-5 :foreground ,polaris-blue)
            (go-coverage-6 :foreground ,(polaris-color-blend polaris-cyan polaris-blue (/ 2.0 6)))
            (go-coverage-7 :foreground ,(polaris-color-blend polaris-cyan polaris-blue (/ 3.0 6)))
            (go-coverage-8 :foreground ,(polaris-color-blend polaris-cyan polaris-blue (/ 4.0 6)))
            (go-coverage-9 :foreground ,(polaris-color-blend polaris-cyan polaris-blue (/ 5.0 6)))
            (go-coverage-10 :foreground ,polaris-cyan)
            (go-coverage-covered :foreground ,polaris-green)
            (go-coverage-untracked :foreground ,polaris-fg+1)

            ;; google-translate
            (google-translate-text-face :foreground ,polaris-blue+2)
            (google-translate-translation-face :foreground ,polaris-green+1)
            (google-translate-phonetic-face :foreground ,polaris-bg+3)
            (google-translate-suggestion-label-face :foreground ,polaris-red)
            (google-translate-suggestion-face :inherit button)
            (google-translate-listen-button-face :inherit custom-button)

            ;; helm
            (helm-bookmark-directory :inherit helm-ff-directory)
            (helm-bookmark-file :foreground ,polaris-blue-1)
            (helm-bookmark-gnus :foreground ,polaris-cyan)
            (helm-bookmark-info :foreground ,polaris-green)
            (helm-bookmark-man :foreground ,polaris-magenta)
            (helm-bookmark-w3m :foreground ,polaris-yellow)
            (helm-bookmarks-su :foreground ,polaris-orange)
            (helm-buffer-not-saved :foreground ,polaris-orange)
            (helm-buffer-saved-out :foreground ,polaris-red)
            (helm-buffer-size :foreground ,polaris-fg+1)
            (helm-buffer-directory :foreground ,polaris-fg-2)
            (helm-candidate-number :background unspecified :bold t)
            (helm-ff-file :foreground ,polaris-fg+3)
            (helm-ff-directory :inherit helm-buffer-directory)
            (helm-ff-executable :foreground ,polaris-green)
            (helm-ff-invalid-symlink :background ,polaris-red :foreground ,polaris-bg :slant italic)
            (helm-ff-prefix :foreground ,polaris-yellow+1)
            (helm-ff-symlink :foreground ,polaris-cyan)
            (helm-grep-file :foreground ,polaris-cyan)
            (helm-grep-finish :foreground ,polaris-green)
            (helm-grep-lineno :foreground ,polaris-orange)
            (helm-grep-match :inherit match)
            (helm-grep-running :inherit compilation-mode-line-run)
            (helm-header :inherit header-line)
            (helm-header-line-left-margin :inherit header-line)
            (helm-lisp-completion-info :foreground ,polaris-fg-1)
            (helm-lisp-show-completion :foreground ,polaris-yellow :bold t)
            (helm-M-x-key :foreground ,polaris-orange)
            (helm-moccur-buffer :foreground ,polaris-cyan)
            (helm-match :inherit match)
            (helm-selection :background ,polaris-bg-1)
            (helm-selection-line :inherit helm-selection)
            (helm-separator :foreground ,polaris-bg+1)
            (helm-source-header :family inherit :inherit header-line :foreground ,polaris-magenta)
            (helm-visible-mark :foreground ,polaris-bg :background ,polaris-yellow-2)

            ;; helm-swoop
            (helm-swoop-target-line-block-face :background ,polaris-bg+1)
            (helm-swoop-target-line-face :background ,polaris-bg+2)
            (helm-swoop-target-word-face :foreground ,polaris-yellow)
            (helm-swoop-line-number-face :foreground ,polaris-fg+1)

            ;; hl-line-mode
            (hl-line :background ,polaris-bg-1)
            (hl-line-face :background ,polaris-bg-1)

            ;; highlight-indentation
            (highlight-indentation-face :background ,polaris-bg+1)
            (highlight-indentation-current-column-face :background ,polaris-bg+1)
            (indent-guide-face :foreground ,polaris-bg-1)

            ;; highlight-numbers
            (highlight-numbers-number :foreground ,polaris-magenta+1)

            ;; highlight-quoted
            (highlight-quoted-symbol :foreground ,polaris-yellow+2)
            (highlight-quoted-quote :foreground ,polaris-cyan+2)

            ;; highlight-symbol
            (highlight-symbol-face :foreground ,polaris-magenta)

            ;; hideshow
            (hs-face :foreground ,polaris-bg+3 :background ,polaris-bg-1)
            (hs-fringe :foreground ,polaris-orange)

            ;; hydra
            (hydra-face-red :foreground ,polaris-red)
            (hydra-face-blue :foreground ,polaris-blue)
            (hydra-face-amaranth :foreground ,polaris-orange)
            (hydra-face-pink :foreground ,polaris-magenta)
            (hydra-face-teal :foreground ,polaris-cyan)

            ;; ido-mode
            (ido-first-match :foreground ,polaris-yellow :weight normal)
            (ido-only-match :foreground ,polaris-orange :weight normal)
            (ido-subdir :foreground ,polaris-blue)
            (ido-incomplete-regexp :foreground ,polaris-red :weight bold )
            (ido-indicator :background ,polaris-red :foreground ,polaris-bg+2 :width condensed)
            (ido-virtual :foreground ,polaris-cyan)

            ;; iedit-mode
            (iedit-occurrence :background ,polaris-bg-1 :foreground ,polaris-magenta :bold t)

            ;; info
            (info-title-1 :foreground ,polaris-fg-1 :weight bold)
            (info-title-2 :foreground ,polaris-fg-1 :weight bold)
            (info-title-3 :weight bold)
            (info-title-4 :weight bold)
            (info-node :foreground ,polaris-fg-1 :slant italic :weight bold)
            (info-header-node :inherit info-node)
            (info-header-xref :inherit info-xref)
            (info-index-match :inherit match)
            (info-menu-header :inherit variable-pitch :weight bold)
            (info-menu-star :foreground ,polaris-orange)
            (info-xref :inherit link)
            (info-xref-visited :inherit (link-visited info-xref))

            ;; info+
            (info-file :foreground ,polaris-yellow :background ,polaris-bg-1)
            (info-menu :foreground ,polaris-magenta+1 :background ,polaris-bg-1)
            (info-single-quote :foreground ,polaris-cyan :inherit font-lock-string-face)
            (info-quoted-name :foreground ,polaris-orange :inherit font-lock-string-face)
            (info-string :foreground ,polaris-blue :inherit font-lock-string-face)
            (info-command-ref-item :foreground ,polaris-green :background ,polaris-bg-1)
            (info-constant-ref-item :foreground ,polaris-red :background ,polaris-bg-1)
            (info-function-ref-item :foreground ,polaris-cyan :background ,polaris-bg-1)
            (info-macro-ref-item :foreground ,polaris-green :background ,polaris-bg-1)
            (info-reference-item :background ,polaris-bg-1)
            (info-special-form-ref-item :foreground ,polaris-magenta :background ,polaris-bg-1)
            (info-syntax-class-item :foreground ,polaris-magenta :background ,polaris-bg-1)
            (info-user-option-ref-item :foreground ,polaris-orange :background ,polaris-bg-1)

            ;; ivy
            (ivy-confirm-face :foreground ,polaris-green)
            (ivy-current-match :weight bold :background ,polaris-bg-1)
            (ivy-match-required-face :foreground ,polaris-red)
            (ivy-minibuffer-match-face-1 :foreground ,polaris-fg+2)
            (ivy-minibuffer-match-face-2 :inherit match)
            (ivy-minibuffer-match-face-3 :foreground ,polaris-cyan)
            (ivy-minibuffer-match-face-4 :foreground ,polaris-magenta+1)
            (ivy-remote :foreground ,polaris-cyan)

            ;; js2-mode
            (js2-warning :underline ,polaris-orange)
            (js2-error :foreground ,polaris-red :weight bold)
            (js2-instance-member :foreground ,polaris-magenta)
            (js2-jsdoc-html-tag-delimiter :foreground ,polaris-cyan)
            (js2-jsdoc-html-tag-name :foreground ,polaris-orange)
            (js2-jsdoc-tag :foreground ,polaris-cyan)
            (js2-jsdoc-type :foreground ,polaris-blue)
            (js2-jsdoc-value :foreground ,polaris-magenta)
            (js2-function-param :foreground, polaris-green+3)
            (js2-external-variable :foreground ,polaris-orange)

            ;; linum-mode
            (linum :foreground ,polaris-bg+2 :background ,polaris-bg :bold nil :height 0.8 :underline nil)
            (linum-highlight-face :foreground ,polaris-green+2 :background ,polaris-bg-1)

            ;; magit
            ;; headings and diffs
            (magit-section-highlight :background ,polaris-bg-1)
            (magit-section-heading :foreground ,polaris-yellow :weight bold)
            (magit-section-heading-selection :foreground ,polaris-orange :weight bold)
            (magit-diff-file-heading :weight bold)
            (magit-diff-file-heading-highlight :background ,polaris-bg-1 :weight bold)
            (magit-diff-file-heading-selection :background ,polaris-bg-1
                                               :foreground ,polaris-orange :weight bold)
            (magit-diff-hunk-heading :background ,(polaris-color-blend polaris-yellow polaris-bg 0.1))
            (magit-diff-hunk-heading-highlight :background ,(polaris-color-blend polaris-yellow polaris-bg-1 0.1))
            (magit-diff-hunk-heading-selection :background ,(polaris-color-blend polaris-yellow polaris-bg-1 0.1)
                                               :foreground ,polaris-orange
                                               :weight bold)
            (magit-diff-lines-heading :background ,polaris-orange
                                      :foreground ,polaris-bg+1)
            (magit-diff-context-highlight :background ,polaris-bg-1)
            (magit-diffstat-added :foreground ,polaris-green)
            (magit-diffstat-removed :foreground ,polaris-red)

            ;; popup
            (magit-popup-heading :foreground ,polaris-bg+2 :weight normal)
            (magit-popup-key :foreground ,polaris-bg+2 :weight bold)
            (magit-popup-argument :foreground ,polaris-bg+2 :weight bold)
            (magit-popup-disabled-argument :foreground ,polaris-fg-1 :weight normal)
            (magit-popup-option-value :foreground ,polaris-bg+2 :weight bold)

            ;; process
            (magit-process-ok :foreground ,polaris-green :weight bold)
            (magit-process-ng :foreground ,polaris-red :weight bold)

            ;; log
            (magit-log-author :foreground ,polaris-fg+1 :weight bold)
            (magit-log-date :foreground ,polaris-fg+1)
            (magit-log-graph :foreground ,polaris-fg+1)

            ;; sequence
            (magit-sequence-pick :foreground ,polaris-yellow-1)
            (magit-sequence-stop :foreground ,polaris-green)
            (magit-sequence-part :foreground ,polaris-yellow)
            (magit-sequence-head :foreground ,polaris-blue)
            (magit-sequence-drop :foreground ,polaris-red)
            (magit-sequence-done :foreground ,polaris-fg-1)
            (magit-sequence-onto :foreground ,polaris-fg-1)

            ;; bisect
            (magit-bisect-good :foreground ,polaris-green)
            (magit-bisect-skip :foreground ,polaris-yellow)
            (magit-bisect-bad  :foreground ,polaris-red)

            ;; blame
            (magit-blame-heading :background ,polaris-bg+2 :foreground ,polaris-green+1)
            (magit-blame-hash    :background ,polaris-bg+2 :foreground ,polaris-green+1)
            (magit-blame-name    :background ,polaris-bg+2 :foreground ,polaris-orange)
            (magit-blame-date    :background ,polaris-bg+2 :foreground ,polaris-orange)
            (magit-blame-summary :background ,polaris-bg+2 :foreground ,polaris-green+1 :weight bold)

            ;; references etc.
            (magit-dimmed         :foreground ,polaris-fg-1)
            (magit-hash           :foreground ,polaris-fg-1)
            (magit-tag            :foreground ,polaris-cyan :weight bold)
            (magit-branch-remote  :foreground ,polaris-green :weight bold)
            (magit-branch-local   :foreground ,polaris-blue :weight bold)
            (magit-branch-current :foreground ,polaris-blue :weight bold :box t)
            (magit-head           :foreground ,polaris-blue :weight bold)
            (magit-refname        :background ,polaris-bg-1 :foreground ,polaris-fg-1 :weight bold)
            (magit-refname-stash  :background ,polaris-bg-1 :foreground ,polaris-fg-1 :weight bold)
            (magit-refname-wip    :background ,polaris-bg-1 :foreground ,polaris-fg-1 :weight bold)
            (magit-signature-good      :foreground ,polaris-green)
            (magit-signature-bad       :foreground ,polaris-red)
            (magit-signature-untrusted :foreground ,polaris-yellow)
            (magit-cherry-unmatched    :foreground ,polaris-cyan)
            (magit-cherry-equivalent   :foreground ,polaris-magenta)
            (magit-reflog-commit       :foreground ,polaris-green)
            (magit-reflog-amend        :foreground ,polaris-magenta)
            (magit-reflog-merge        :foreground ,polaris-green)
            (magit-reflog-checkout     :foreground ,polaris-blue)
            (magit-reflog-reset        :foreground ,polaris-red)
            (magit-reflog-rebase       :foreground ,polaris-magenta)
            (magit-reflog-cherry-pick  :foreground ,polaris-green)
            (magit-reflog-remote       :foreground ,polaris-cyan)
            (magit-reflog-other        :foreground ,polaris-cyan)

            ;; markdown-mode
            (markdown-blockquote-face :inherit font-lock-doc-face)
            (markdown-bold-face :inherit bold)
            (markdown-comment-face :foreground ,polaris-bg+3 :strike-through t)
            (markdown-footnote-face :inherit default)
            (markdown-header-delimiter-face :foreground ,polaris-bg+3)
            (markdown-header-face :inherit variable-pitch)
            (markdown-header-rule-face :foreground ,polaris-bg+3)
            (markdown-inline-code-face :foreground ,polaris-bg+3)
            (markdown-italic-face :inherit italic)
            (markdown-language-keyword-face :inherit default)
            (markdown-line-break-face :inherit default :underline t)
            (markdown-link-face :inherit default :foreground ,polaris-yellow)
            (markdown-link-title-face :inherit font-lock-comment-face)
            (markdown-list-face :inherit font-lock-builtin-face)
            (markdown-math-face :inherit font-lock-string-face)
            (markdown-metadata-key-face :inherit font-lock-comment-face)
            (markdown-metadata-value-face :inherit default)
            (markdown-missing-link-face :inherit font-lock-warning-face)
            (markdown-pre-face :foreground ,polaris-bg+3)
            (markdown-reference-face :inherit default :foreground ,polaris-bg+3)
            (markdown-url-face :foreground ,polaris-bg+3)

            ;; multiple-cursors
            (mc/cursor-face :inherit cursor :inverse-video nil)

            ;; mic-paren
            (paren-face-match :foreground ,polaris-magenta+1 :background ,polaris-bg :weight bold)
            (paren-face-mismatch :foreground ,polaris-bg :background ,polaris-magenta :weight bold)
            (paren-face-no-match :foreground ,polaris-bg :background ,polaris-red :weight bold)

            ;; mingus
            (mingus-directory-face :foreground ,polaris-blue)
            (mingus-pausing-face :foreground ,polaris-magenta)
            (mingus-playing-face :foreground ,polaris-cyan)
            (mingus-playlist-face :foreground ,polaris-cyan )
            (mingus-song-file-face :foreground ,polaris-yellow)
            (mingus-stopped-face :foreground ,polaris-red)

            ;; mu4e
            (mu4e-cited-1-face :foreground ,polaris-green-2 :slant italic :weight normal)
            (mu4e-cited-2-face :foreground ,polaris-blue-2 :slant italic :weight normal)
            (mu4e-cited-3-face :foreground ,polaris-orange-2 :slant italic :weight normal)
            (mu4e-cited-4-face :foreground ,polaris-yellow-3 :slant italic :weight normal)
            (mu4e-cited-5-face :foreground ,polaris-cyan+4 :slant italic :weight normal)
            (mu4e-cited-6-face :foreground ,polaris-green+3 :slant italic :weight normal)
            (mu4e-cited-7-face :foreground ,polaris-blue+2 :slant italic :weight normal)
            (mu4e-flagged-face :foreground ,polaris-blue+4 :weight normal)
            (mu4e-unread-face :foreground ,polaris-green+4 :weight normal)
            (mu4e-view-url-number-face :foreground ,polaris-yellow-4 :weight normal)
            (mu4e-warning-face :foreground ,polaris-red-3 :slant normal :weight bold)
            (mu4e-header-highlight-face :inherit unspecified :foreground unspecified :background ,polaris-bg
                                        :underline unspecified  :weight unspecified)
            (mu4e-view-contact-face :foreground ,polaris-fg-3  :weight normal)
            (mu4e-view-header-key-face :inherit message-header-name :weight normal)
            (mu4e-view-header-value-face :foreground ,polaris-cyan-2 :weight normal :slant normal)
            (mu4e-view-link-face :inherit link)
            (mu4e-view-special-header-value-face :foreground ,polaris-blue+1 :weight normal :underline nil)

            ;; nav
            (nav-face-heading :foreground ,polaris-yellow-2)
            (nav-face-button-num :foreground ,polaris-cyan+1)
            (nav-face-dir :foreground ,polaris-green-3)
            (nav-face-hdir :foreground ,polaris-red-1)
            (nav-face-file :foreground ,polaris-fg-3)
            (nav-face-hfile :foreground ,polaris-red-3)

            ;; neotree
            (neo-banner-face :foreground ,polaris-orange-2)
            (neo-header-face :foreground ,polaris-blue+2)
            (neo-root-dir-face :foreground ,polaris-magenta+1 :weight bold)
            (neo-dir-link-face :foreground ,polaris-blue-1)
            (neo-file-link-face :foreground ,polaris-fg+2)
            (neo-expand-btn-face :foreground ,polaris-green-2)

            ;; org-mode
            (org-agenda-structure :foreground ,polaris-green :weight bold)
            (org-agenda-calendar-event :foreground ,polaris-orange)
            (org-agenda-calendar-sexp :foreground ,polaris-yellow-2 :slant italic)
            (org-agenda-date :foreground ,polaris-bg :background ,polaris-fg :weight normal
                             :box (:line-width 2 :color ,polaris-fg)
                             :slant italic)
            (org-agenda-date-weekend :inherit org-agenda-date :foreground ,polaris-fg+2 :weight bold)
            (org-agenda-date-today :inherit org-agenda-date :inverse-video t :weight bold
                                   :foreground ,polaris-blue-2)
            (org-agenda-done :foreground ,polaris-green+3 :slant italic)
            (org-agenda-dimmed-todo-face :foreground ,polaris-red+1)
            (org-agenda-restriction-lock :background ,polaris-yellow+1)
            (org-archived :foreground ,polaris-fg-1 :weight normal)
            (org-block :foreground ,polaris-fg-3 :background ,polaris-bg-1)
            (org-block-begin-line :background ,polaris-bg-1 :foreground ,polaris-bg+1 :slant italic)
            (org-block-background :background ,polaris-bg-1)
            (org-block-end-line :background ,polaris-bg-1 :foreground ,polaris-bg+1 :slant italic)
            (org-checkbox :background ,polaris-bg-1 :foreground ,polaris-fg-3
                          :box (:line-width 1 :style released-button))
            (org-clock-overlay :background ,polaris-yellow+1)
            (org-code :foreground ,polaris-cyan-2)
            (org-date :foreground ,polaris-blue+1 :underline t)
            (org-date-selected :background ,polaris-bg+3 :foreground ,polaris-fg-2)
            (org-document-info :foreground ,polaris-magenta-1)
            (org-document-title :foreground ,polaris-blue+2 :weight bold :underline t)
            (org-done :weight bold :foreground ,polaris-green-3)
            (org-drawer :foreground ,polaris-cyan+1)
            (org-ellipsis :foreground ,polaris-blue)
            (org-footnote :foreground ,polaris-magenta :slant italic)
            (org-formula :foreground ,polaris-yellow-2)
            (org-headline-done :foreground ,polaris-green+2)
            (org-hide :foreground ,polaris-fg-2)
            (org-level-1 :inherit variable-pitch :foreground ,polaris-orange)
            (org-level-2 :inherit variable-pitch :foreground ,polaris-green+1)
            (org-level-3 :inherit variable-pitch :foreground ,polaris-blue-1)
            (org-level-4 :inherit variable-pitch :foreground ,polaris-yellow-2)
            (org-level-5 :inherit variable-pitch
                         :foreground ,polaris-cyan)
            (org-level-6 :inherit variable-pitch
                         :foreground ,polaris-green-1)
            (org-level-7 :inherit variable-pitch
                         :foreground ,polaris-red-4)
            (org-level-8 :inherit variable-pitch
                         :foreground ,polaris-blue-4)
            (org-latex-and-export-specials :foreground ,polaris-orange)
            (org-link :foreground ,polaris-yellow-2 :underline t)
            (org-list-bullet :foreground ,polaris-orange :weight bold)
            (org-mode-line-clock-overrun :inherit mode-line :background ,polaris-red-1)
            (org-meta-line :foreground ,polaris-green+1)
            (org-priority :foreground ,polaris-orange+2 :bold t)
            (org-quote :inherit org-block :slant italic)
            (org-sexp-date :foreground ,polaris-magenta-3)
            (org-scheduled :foreground ,polaris-green+4)
            (org-scheduled-previously :foreground ,polaris-orange+2)
            (org-scheduled-today :foreground ,polaris-blue+1 :weight normal)
            (org-special-keyword :foreground ,polaris-yellow-1 :weight bold)
            (org-table :foreground ,polaris-green+2)
            (org-tag :weight bold)
            (org-time-grid :foreground ,polaris-orange)
            (org-todo :foreground ,polaris-cyan+3 :weight bold)
            (org-upcoming-deadline :foreground ,polaris-yellow-2  :weight normal :underline nil)
            (org-verbatim :foreground ,polaris-fg+2)
            (org-verse :inherit org-block :slant italic)
            (org-warning :foreground ,polaris-orange-1 :weight bold :underline nil)

            ;; outline
            (outline-1 :inherit org-level-1)
            (outline-2 :inherit org-level-2)
            (outline-3 :inherit org-level-3)
            (outline-4 :inherit org-level-4)
            (outline-5 :inherit org-level-5)
            (outline-6 :inherit org-level-6)
            (outline-7 :inherit org-level-7)
            (outline-8 :inherit org-level-8)

            ;; paren-face
            (paren-face  :foreground ,polaris-fg-2)

            ;; perspective
            (persp-selected-face :bold t :foreground ,polaris-blue)

            ;; popup
            (popup :inherit tooltip)
            (popup-tip-face :inherit tooltip)
            (popup-summary-face :background ,polaris-bg+3 :foreground ,polaris-fg+1)
            (popup-scroll-bar-foreground-face :background ,polaris-bg+3)
            (popup-scroll-bar-background-face :background ,polaris-fg+1)
            (popup-menu-mouse-face :background ,polaris-yellow+1 :foreground ,polaris-bg)
            (popup-tip-face :background ,polaris-bg+3 :foreground ,polaris-bg)

            ;; powerline
            (powerline-active1 :background ,polaris-bg)
            (powerline-active2 :background ,polaris-bg+1)
            (powerline-inactive1 :background ,polaris-bg+1)
            (powerline-inactive2 :background ,polaris-bg+1)

            ;; rainbow-delimiters
            (rainbow-delimiters-depth-1-face :foreground ,polaris-cyan)
            (rainbow-delimiters-depth-2-face :foreground ,polaris-yellow)
            (rainbow-delimiters-depth-3-face :foreground ,polaris-blue+1)
            (rainbow-delimiters-depth-4-face :foreground ,polaris-magenta+1)
            (rainbow-delimiters-depth-5-face :foreground ,polaris-orange)
            (rainbow-delimiters-depth-6-face :foreground ,polaris-blue-1)
            (rainbow-delimiters-depth-7-face :foreground ,polaris-green+4)
            (rainbow-delimiters-depth-8-face :foreground ,polaris-magenta-3)
            (rainbow-delimiters-depth-9-face :foreground ,polaris-yellow-2)
            (rainbow-delimiters-depth-10-face :foreground ,polaris-green+2)
            (rainbow-delimiters-depth-11-face :foreground ,polaris-blue+1)
            (rainbow-delimiters-depth-12-face :foreground ,polaris-magenta-4)
            (rainbow-delimiters-unmatched-face :foreground ,polaris-red-2 :inverse-video t)

            ;;re-builder
            (reb-match-0 :foreground ,polaris-orange+3 :inverse-video t)
            (reb-match-1 :foreground ,polaris-magenta+3 :inverse-video t)
            (reb-match-2 :foreground ,polaris-green+3 :inverse-video t)
            (reb-match-3 :foreground ,polaris-yellow+3 :inverse-video t)

            ;; rpm-mode
            (rpm-spec-dir-face :foreground ,polaris-green)
            (rpm-spec-doc-face :foreground ,polaris-green)
            (rpm-spec-ghost-face :foreground ,polaris-red)
            (rpm-spec-macro-face :foreground ,polaris-yellow)
            (rpm-spec-obsolete-tag-face :foreground ,polaris-red)
            (rpm-spec-package-face :foreground ,polaris-red)
            (rpm-spec-section-face :foreground ,polaris-yellow)
            (rpm-spec-tag-face :foreground ,polaris-blue)
            (rpm-spec-var-face :foreground ,polaris-red)

            ;; rst-mode
            (rst-level-1-face :foreground ,polaris-orange)
            (rst-level-2-face :foreground ,polaris-green+1)
            (rst-level-3-face :foreground ,polaris-blue-1)
            (rst-level-4-face :foreground ,polaris-yellow-2)
            (rst-level-5-face :foreground ,polaris-cyan)
            (rst-level-6-face :foreground ,polaris-green-1)

            ;; sh-mode
            (sh-quoted-exec :foreground ,polaris-magenta-1 :weight bold)
            (sh-escaped-newline :foreground ,polaris-yellow+2 :weight bold)
            (sh-heredoc :foreground ,polaris-yellow-2 :weight bold)

            ;; skewer
            (skewer-error-face :foreground ,polaris-orange :underline nil)
            (skewer-repl-log-face :foreground ,polaris-magenta+1)

            ;; smartparens
            (sp-pair-overlay-face :background ,polaris-bg-1)
            (sp-wrap-overlay-face :background ,polaris-bg-1)
            (sp-wrap-tag-overlay-face :background ,polaris-bg-1)
            (sp-show-pair-enclosing :inherit highlight)
            (sp-show-pair-match-face :foreground ,polaris-magenta+4
                                     :weight bold)
            (sp-show-pair-mismatch-face :foreground ,polaris-red-4
                                        :weight bold)

            ;; show-paren
            (show-paren-mismatch :foreground ,polaris-red-4 :background ,polaris-bg :weight bold)
            (show-paren-match :foreground ,polaris-magenta+4 :background ,polaris-bg :weight bold)

            ;; swiper
            (swiper-line-face :background ,polaris-bg-1)
            (swiper-match-face-1 :weight bold :foreground ,polaris-magenta)
            (swiper-match-face-2 :weight bold :foreground ,polaris-cyan)
            (swiper-match-face-3 :weight bold :foreground ,polaris-orange)
            (swiper-match-face-4 :weight bold :foreground ,polaris-yellow)

            ;; SLIME
            (slime-repl-inputed-output-face :foreground ,polaris-red)

            ;; spaceline
            (spaceline-python-venv :foreground ,polaris-cyan+3)
            (spaceline-flycheck-error :background ,polaris-red+1 :foreground ,polaris-bg)
            (spaceline-flycheck-info :background ,polaris-magenta-1 :foreground ,polaris-bg)
            (spaceline-flycheck-warning :background ,polaris-orange+1 :foreground ,polaris-bg)
            (spaceline-highlight-face :background ,polaris-cyan+2 :foreground ,polaris-bg-1 :weight bold)
            (spaceline-evil-normal :inherit mode-line :background ,polaris-green :foreground ,polaris-bg :weight bold)
            (spaceline-evil-insert :inherit mode-line :background ,polaris-magenta :foreground ,polaris-bg :weight bold)
            (spaceline-evil-emacs :inherit mode-line :background ,polaris-cyan :foreground ,polaris-bg :weight bold)
            (spaceline-evil-replace :inherit mode-line :background ,polaris-red :foreground ,polaris-bg :weight bold)
            (spaceline-evil-visual :inherit mode-line :background ,polaris-bg+2 :fareground ,polaris-bg :weight bold)
            (spaceline-evil-motion :inherit mode-line :background ,polaris-blue :foreground ,polaris-bg :weight bold)
            (spaceline-unmodified :inherit mode-line :background ,polaris-yellow+2 :foreground ,polaris-bg)
            (spaceline-modified :inherit mode-line :background ,polaris-orange+2 :foreground ,polaris-bg)
            (spaceline-read-only :inherit mode-line :background ,polaris-magenta+1 :foreground ,polaris-bg)

            ;; structured-haskell
            (shm-current-face :background ,polaris-bg)
            (shm-quarantine-face :background ,polaris-bg+1)

            ;; term
            (term :background ,polaris-bg :foreground ,polaris-fg)
            (term-color-black :foreground ,polaris-bg+1 :background ,polaris-bg-1)
            (term-color-red :foreground ,polaris-red+1 :background ,polaris-red-1)
            (term-color-green :foreground ,polaris-green+1 :background ,polaris-green-1)
            (term-color-yellow :foreground ,polaris-yellow+1 :background ,polaris-yellow-1)
            (term-color-blue :foreground ,polaris-blue :background ,polaris-blue-1)
            (term-color-magenta :foreground ,polaris-magenta+1 :background ,polaris-magenta-1)
            (term-color-cyan :foreground ,polaris-cyan+1 :background ,polaris-cyan-1)
            (term-color-white :foreground ,polaris-fg+3 :background ,polaris-fg-3)

            ;; undo-tree
            (undo-tree-visualizer-active-branch-face :foreground ,polaris-blue)
            (undo-tree-visualizer-current-face :foreground ,polaris-red :weight bold)
            (undo-tree-visualizer-default-face :foreground ,polaris-fg)
            (undo-tree-visualizer-register-face :foreground ,polaris-yellow)
            (undo-tree-visualizer-unmodified-face :foreground, polaris-fg)

            ;; volatile highlights
            (vhl/default-face :foreground ,polaris-green-3)

            ;; web-mode
            (web-mode-builtin-face :inherit font-lock-builtin-face)
            (web-mode-comment-face :inherit font-lock-comment-face)
            (web-mode-constant-face :foreground ,polaris-blue :weight bold)
            (web-mode-current-element-highlight-face :inherit highlight-symbol-face)
            (web-mode-css-at-rule-face :foreground ,polaris-magenta :slant italic)
            (web-mode-css-pseudo-class-face :foreground ,polaris-green :slant italic)
            (web-mode-doctype-face :inherit font-lock-comment-face
                                   :slant italic :weight bold)
            (web-mode-folded-face :underline t)
            (web-mode-function-name-face :foreground ,polaris-blue-1)
            (web-mode-html-attr-name-face :foreground ,polaris-blue :slant normal)
            (web-mode-html-attr-value-face :foreground ,polaris-cyan :slant italic)
            (web-mode-html-tag-face :foreground ,polaris-green+1)
            (web-mode-html-tag-bracket-face :foreground ,polaris-green+1)
            (web-mode-keyword-face :foreground ,polaris-yellow+3 :weight normal)
            (web-mode-preprocessor-face :foreground ,polaris-yellow-1 :slant normal :weight unspecified)
            (web-mode-string-face :foreground ,polaris-cyan-1)
            (web-mode-type-face :foreground ,polaris-yellow+1)
            (web-mode-variable-name-face :foreground ,polaris-blue+1)
            (web-mode-warning-face :inherit font-lock-warning-face)
            (web-mode-block-attr-name-face :inherit web-mode-html-attr-name-face)
            (web-mode-block-attr-value-face :inherit web-mode-html-attr-value-face)
            (web-mode-block-comment-face :inherit web-mode-comment-face)
            (web-mode-block-control-face :inherit font-lock-preprocessor-face)
            (web-mode-block-face :background unspecified)
            (web-mode-block-string-face :inherit web-mode-string-face)
            (web-mode-comment-keyword-face :box 1 :weight bold)
            (web-mode-css-color-face :inherit font-lock-builtin-face)
            (web-mode-css-function-face :inherit font-lock-builtin-face)
            (web-mode-css-priority-face :inherit font-lock-builtin-face)
            (web-mode-css-property-name-face :inherit font-lock-variable-name-face)
            (web-mode-css-selector-face :inherit font-lock-keyword-face)
            (web-mode-css-string-face :inherit web-mode-string-face)
            (web-mode-javascript-string-face inherit web-mode-string-face)
            (web-mode-json-context-face :foreground ,polaris-magenta+3)
            (web-mode-json-key-face :foreground ,polaris-magenta-2)
            (web-mode-json-string-face :inherit web-mode-string-face)
            (web-mode-param-name-face :foreground ,polaris-cyan)
            (web-mode-part-comment-face :inherit web-mode-comment-face)
            (web-mode-part-face :inherit web-mode-block-face)
            (web-mode-part-string-face :inherit web-mode-string-face)
            (web-mode-symbol-face :foreground ,polaris-yellow)
            (web-mode-whitespace-face :background ,polaris-red-1)
            (web-mode-html-tag-bracket-face :foreground ,polaris-orange)
            (web-mode-block-delimiter-face :inherit font-lock-preprocessor-face)
            (web-mode-css-comment-face :inherit web-mode-comment-face)
            (web-mode-css-variable-face :inherit web-mode-variable-name-face :slant italic)
            (web-mode-error-face :background ,polaris-red+2)
            (web-mode-function-call-face :inherit font-lock-function-name-face)
            (web-mode-html-attr-custom-face :inherit web-mode-html-attr-name-face)
            (web-mode-html-attr-engine-face :inherit web-mode-html-attr-custom-face)
            (web-mode-html-attr-equal-face :inherit web-mode-html-attr-name-face)
            (web-mode-html-tag-custom-face :inherit web-mode-html-tag-face)
            (web-mode-javascript-comment-face :inherit web-mode-comment-face)
            (web-mode-json-comment-face :inherit web-mode-comment-face)

            ;; which-key
            (which-key-command-description-face :foreground ,polaris-fg+3)
            (which-key-group-description-face :foreground ,polaris-cyan+2)
            (which-key-key-face :foreground ,polaris-yellow+2 :weight bold)
            (which-key-separator-face :foreground ,polaris-orange :background nil)
            (which-key-special-key-face :inherit which-key-key-face :inverse-video nil)

            ;; whitespace-mode
            (whitespace-space :background ,polaris-bg :foreground ,polaris-bg+1)
            (whitespace-hspace :background ,polaris-bg :foreground ,polaris-bg+1)
            (whitespace-tab :background ,polaris-bg :foreground ,polaris-red)
            (whitespace-newline :foreground ,polaris-bg+1)
            (whitespace-trailing :foreground ,polaris-red :background ,polaris-bg)
            (whitespace-line :background ,polaris-bg-05 :foreground ,polaris-magenta)
            (whitespace-space-before-tab :background ,polaris-orange :foreground ,polaris-orange)
            (whitespace-indentation :background ,polaris-yellow :foreground ,polaris-red)
            (whitespace-empty :background ,polaris-yellow :foreground ,polaris-red)
            (whitespace-space-after-tab :background ,polaris-yellow :foreground ,polaris-red)

            ;; which-func
            (which-func :foreground ,polaris-green+4)

            ;; workgroups2
            (wg-current-workgroup-face :background ,polaris-orange-1 :foreground ,polaris-bg-05)
            (wg-other-workgroup-face :background ,polaris-bg-1 :foreground ,polaris-bg+4)

            )))
  (custom-theme-set-variables
   'polaris
   `(evil-emacs-state-cursor '(,polaris-blue+3 box))
   `(evil-insert-state-cursor '(,polaris-orange+2 hbar))
   `(evil-normal-state-cursor '(,polaris-cyan box))
   `(evil-visual-state-cursor '(,polaris-magenta box))
   `(evil-motion-state-cursor '(,polaris-yellow+2 bar))
   `(evil-replace-state-cursor '(,polaris-red-3 box))
   `(evil-operator-state-cursor '(,polaris-red hollow))
   `(evil-iedit-state-cursor '(,polaris-red+2 box))

   `(pos-tip-foreground-color ,polaris-fg+3)
   `(pos-tip-background-color ,polaris-bg-1)

   `(highlight-symbol-foreground-color ,polaris-fg-2)
   `(highlight-symbol-colors '(,polaris-yellow
                               ,polaris-green
                               ,polaris-cyan
                               ,polaris-blue
                               ,polaris-magenta
                               ,polaris-orange))

   ;; vc
   `(vc-annotate-color-map
     '((20 . ,polaris-red)
       (40 . ,(polaris-color-blend polaris-yellow polaris-red (/ 2.0 4)))
       (60 . ,(polaris-color-blend polaris-yellow polaris-red (/ 3.0 4)))
       (80 . ,polaris-yellow)
       (100 . ,(polaris-color-blend polaris-green polaris-yellow (/ 2.0 6)))
       (120 . ,(polaris-color-blend polaris-green polaris-yellow (/ 3.0 6)))
       (140 . ,(polaris-color-blend polaris-green polaris-yellow (/ 4.0 6)))
       (160 . ,(polaris-color-blend polaris-green polaris-yellow (/ 5.0 6)))
       (180 . ,polaris-green)
       (200 . ,(polaris-color-blend polaris-cyan polaris-green (/ 2.0 6)))
       (220 . ,(polaris-color-blend polaris-cyan polaris-green (/ 3.0 6)))
       (240 . ,(polaris-color-blend polaris-cyan polaris-green (/ 4.0 6)))
       (260 . ,(polaris-color-blend polaris-cyan polaris-green (/ 5.0 6)))
       (280 . ,polaris-cyan)
       (300 . ,(polaris-color-blend polaris-blue polaris-cyan (/ 2.0 5)))
       (320 . ,(polaris-color-blend polaris-blue polaris-cyan (/ 3.0 5)))
       (340 . ,(polaris-color-blend polaris-blue polaris-cyan (/ 4.0 5)))
       (360 . ,polaris-blue)))

   `(beacon-color ,polaris-orange-3)

   `(tabbar-background-color ,polaris-bg-1)

   `(ansi-color-names-vector
     [,polaris-bg ,polaris-red-1 ,polaris-green ,polaris-yellow ,polaris-blue ,polaris-magenta ,polaris-cyan ,polaris-bg+4])

   ;; fill-column-indicator
   `(fci-rule-color ,polaris-bg-05)))

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory
                (file-name-directory load-file-name))))

(provide-theme 'polaris)

;; Local Variables:
;; no-byte-compile: t
;; indent-tabs-mode: nil
;; eval: (when (require 'rainbow-mode nil t) (rainbow-mode 1))
;; End:
;;; polaris-theme.el ends here

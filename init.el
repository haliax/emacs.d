;; -*- lexical-binding: t -*-
;;; Main file of this Emacs config.

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; (package-initialize)

(defconst polaris-default-font (font-spec :family "Hack"))
(defconst polaris-default-theme 'polaris)

;; (setq package-user-dir
;;       (expand-file-name "elpa" user-emacs-directory))

(load (expand-file-name "packages.el" user-emacs-directory))
(load (concat user-emacs-directory "bootstrap.el"))

(setq user-full-name "Axel Parra"
      user-mail-address (concat "apc" '(?@) "openmailbox" '(?.) "org"))

(polaris '(core                          ; core/core.el

           ;; The heart of POLARIS
           core-popup          ; taming sudden and inevitable windows
           core-ui             ; draw me like one of your French editors
           core-evil           ; come to the dark side, we have cookies
           core-editor         ; filling the editor-shaped hole in the emacs OS
           core-company        ; for the lazy typist
           core-yasnippet      ; for the lazier typist
           core-file-templates ; for the laziest typist
           core-flycheck       ; get tazed for every semicolon you forget
           core-project        ; whose project am I in?
           core-vcs            ; remember remember, that commit in November
           core-helm           ; a search engine for life and love
           core-sessions       ; cure Emacs alzheimers + tab emulation
           core-eval           ; run code, run; debugging too
           core-tramp          ; open files with sudo
           core-hydra          ; sticky bindings

           ;; Environment
           module-cc           ; C/C++/Obj-C madness
           module-crystal      ; ruby at the speed of c
           module-clojure
           module-data         ; config and data formats
           module-go           ; the hipster dialect
           module-haskell      ; a language that's lazier than I am
           module-java         ; the poster child for carpal tunnel syndome
           module-js           ; all(hope(abandon(ye(who(enter(here))))))
           module-julia        ; MATLAB, but fast
           module-lisp         ; drowning in parentheses
           module-latex
           module-lua          ; one-based indices? one-based indices.
           module-mail         ; mailing
           module-media        ; feed, irc...
           module-org           ; for organized fearless leader
           module-pass
           module-pdf          ; better pdf view
           module-php          ; making php less painful to work with
           module-processing   ; pretty prototypes
           module-python       ; beautiful is better than ugly
           module-ruby         ; 1.step do {|i| p "Ruby is #{i&1==0?'love':'life'}"}
           module-rust         ; Fe203.unwrap().unwrap().unwrap().unwrap()
           module-scheme
           module-sh           ; she sells Z-shells by the C XOR
           module-shell        ; different shells on emacs
           module-text         ; writing docs for people to ignore
           module-web          ; #big-bang::before {content "";}

           ;; Experimental
           ;; module-nim       ; look out Abraham
           ;; module-pony      ; Dear Princess Compiler

           ;; Extra libraries
           extra-demo          ; allow me to demonstrate...
           ;;extra-tags          ; if you liked it you should've generated a tag for it
           extra-tmux          ; closing the rift between GUI & terminal
           extra-write         ; for writing papers and fiction in Emacs

           core-bindings                ; personal bindings
           ))

(load custom-file :noerror :nomessage) ; Load the emacs `M-x customize` generated file

;;; I think so Brain...

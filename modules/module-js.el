;; -*- lexical-binding: t -*-
;;; module-js.el

(use-package js2-mode
  :mode (("\\.js$" . js2-mode)
         ("\\.jsx$" . js2-mode))
  :interpreter "node"
  :init
  (add-hook! js2-mode '(tern-mode emr-initialize))
  :config
  (def-repl! js2-mode nodejs-repl)
  (def-company-backend! js2-mode (tern))
  (def-electric! js2-mode :chars (?\} ?\) ?.) :words ("||" "&&"))
  (setq-default
   js2-highlight-level 3                ; Highlight many build
   js2-skip-preprocessor-directives t
   js2-show-parse-errors nil
   js2-global-externs '("module" "require" "buster" "sinon" "assert"
                        "refute" "setTimeout" "clearTimeout"
                        "setInterval" "clearInterval" "location"
                        "__dirname" "console" "JSON" "jQuery" "$"
                        ;; Launchbar API
                        "LaunchBar" "File" "Action" "HTTP" "include" "Lib"))

  ;; [pedantry intensifies]
  (add-hook! js2-mode (setq mode-name "JS2"))

  (map! :map js2-mode-map (:localleader :nv ";" 'polaris/append-semicolon)))

(use-package tern
  :after js2-mode
  :commands (tern-mode))

(use-package company-tern
  :after tern)

(use-package js2-refactor
  :after js2-mode
  :config
  (mapc (lambda (x)
          (let ((command-name (car x))
                (title (cadr x))
                (region-p (caddr x))
                predicate)
            (setq predicate (cond ((eq region-p 'both) nil)
                                  (t (if region-p
                                         (lambda () (use-region-p))
                                       (lambda () (not (use-region-p)))))))
            (emr-declare-command
                (intern (format "js2r-%s" (symbol-name command-name)))
              :title title :modes 'js2-mode :predicate predicate)))
        '((extract-function           "extract function"           t)
          (extract-method             "extract method"             t)
          (introduce-parameter        "introduce parameter"        t)
          (localize-parameter         "localize parameter"         nil)
          (expand-object              "expand object"              nil)
          (contract-object            "contract object"            nil)
          (expand-function            "expand function"            nil)
          (contract-function          "contract function"          nil)
          (expand-array               "expand array"               nil)
          (contract-array             "contract array"             nil)
          (wrap-buffer-in-iife        "wrap buffer in ii function" nil)
          (inject-global-in-iife      "inject global in ii function" t)
          (add-to-globals-annotation  "add to globals annotation"  nil)
          (extract-var                "extract variable"           t)
          (inline-var                 "inline variable"            t)
          (rename-var                 "rename variable"            nil)
          (var-to-this                "var to this"                nil)
          (arguments-to-object        "arguments to object"        nil)
          (ternary-to-if              "ternary to if"              nil)
          (split-var-declaration      "split var declaration"      nil)
          (split-string               "split string"               nil)
          (unwrap                     "unwrap"                     t)
          (log-this                   "log this"                   'both)
          (debug-this                 "debug this"                 'both)
          (forward-slurp              "forward slurp"              nil)
          (forward-barf               "forward barf"               nil))))

(use-package nodejs-repl :commands (nodejs-repl))

;;
(use-package jsx-mode :mode "\\.jsx$")

;; Skewer -- live web development minor mode.
;; Methods of launching:
;;   * M-x run-skewer
;;   * Use the Skewer bookmarklet to inject it into an existing page.
;; Keybindings resemble the Lisp ones:
;;   C-x C-e -- JS: eval form (with prefix: insert result); CSS: load declaration.
;;   C-M-x -- JS: eval top-level-form; CSS: load rule; HTML: load tag.
;;   C-c C-k -- JS, CSS: eval buffer.
;;   C-c C-z -- JS: switch to REPL (logging: "skewer.log()", like "console.log()").
;; Forms are sent to all attached clients simultaneously (use `list-skewer-clients' to show them).
;; If the browser disconnects, use "skewer()" in the browser console to reconnect.
(use-package skewer-mode
  :defer t
  :init
  (skewer-setup)) ; Integrate with js2-mode, html-mode and css-mode. (Don't worry about performance, this function is in a separate file.)

(use-package typescript-mode
  :mode "\\.ts$"
  :config
  (require 'tide)
  (defun polaris|typescript-setup ()
    (tide-setup)
    (flycheck-mode 1)
    (eldoc-mode 1))

  (add-hook! typescript-mode 'polaris|typescript-setup)
  (add-hook! web-mode (when (string-equal "tsx" (file-name-extension buffer-file-name)) (polaris|typescript-setup))))

(use-package coffee-mode
  :mode (("\\.coffee\\'" . coffee-mode)
         ("\\.iced\\'"   . coffee-mode)
         ("Cakefile\\'"  . coffee-mode)
         ("\\.cson\\'"   . coffee-mode))
  :interpreter "coffee"
  :config (setq-default coffee-indent-like-python-mode t))

;;
(def-project-type! nodejs "node"
  :modes (web-mode js-mode js2-mode json-mode coffee-mode scss-mode sass-mode less-css-mode)
  :files ("package.json"))

(def-project-type! angularjs "angular"
  :modes (web-mode js-mode js2-mode json-mode coffee-mode scss-mode sass-mode less-css-mode)
  :files ("public/libraries/angular/"))

(def-project-type! electron "electron"
  :modes (nodejs-project-mode)
  :files ("app/index.html" "app/main.js"))
;; TODO electron-compile support

;; TODO react

(provide 'module-js)
;;; module-js.el ends here

;; -*- lexical-binding: t -*-
;;; module-d.el

(use-package d-mode
  :defer t)

(use-package flycheck-dmd-dub
  :defer t
  :init
  (add-hook 'd-mode-hook #'flycheck-dmd-dub-set-include-path))

(provide 'module-d)
;;; module-d.el ends here

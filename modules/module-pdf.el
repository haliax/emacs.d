;; -*- lexical-binding: t -*-
;;; module-pdf.el

;; Take notes in org files while reading PDFs
(use-package interleave
  :commands (interleave interleave--open-notes-file-for-pdf))

(use-package pdf-view
  :defer t
  :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
  :config
  (add-hook 'pdf-view-mode-hook 'pdf-tools-enable-minor-modes)
  (require 'org-pdfview nil t))

(use-package pdf-tools
  :init
  (pdf-tools-install t nil t)
  (setq pdf-occur-prefer-string-search t
        pdf-view-display-size 'fit-page
        pdf-view-continuous t)
  (evil-define-key 'visual pdf-view-mode-map "y" 'pdf-view-kill-ring-save)

  ;; TODO: Make `/', `?' and `n' work like in Evil
  (evilified-state-evilify pdf-view-mode pdf-view-mode-map
    "." 'hydra-pdf-tools/body
    ;; Navigation
    "j"  'pdf-view-next-line-or-next-page
    "k"  'pdf-view-previous-line-or-previous-page
    "l"  'image-forward-hscroll
    "h"  'image-backward-hscroll
    "J"  'pdf-view-next-page
    "K"  'pdf-view-previous-page
    "gg"  'pdf-view-first-page
    "G"  'pdf-view-last-page
    "gt"  'pdf-view-goto-page
    "gl"  'pdf-view-goto-label
    "u" 'pdf-view-scroll-down-or-previous-page
    "d" 'pdf-view-scroll-up-or-next-page
    (kbd "C-u") 'pdf-view-scroll-down-or-previous-page
    (kbd "C-d") 'pdf-view-scroll-up-or-next-page
    (kbd "``")  'pdf-history-backward
    ;; Search
    "/" 'isearch-forward
    "?" 'isearch-backward
    ;; Actions
    "r"   'pdf-view-revert-buffer
    "o"   'pdf-links-action-perform
    "O"   'pdf-outline)
  (evilified-state-evilify pdf-outline-buffer-mode pdf-outline-buffer-mode-map
    "-"                'negative-argument
    "j"                'next-line
    "k"                'previous-line
    "gk"               'outline-backward-same-level
    "gj"               'outline-forward-same-level
    (kbd "<backtab>")  'show-all
    "gh"               'pdf-outline-up-heading
    "gg"               'beginning-of-buffer
    "G"                'pdf-outline-end-of-buffer
    "TAB"              'outline-toggle-children
    "RET"              'pdf-outline-follow-link
    (kbd "M-RET")      'pdf-outline-follow-link-and-quit
    "f"                'pdf-outline-display-link
    [mouse-1]          'pdf-outline-mouse-display-link
    "o"                'pdf-outline-select-pdf-window
    "``"               'pdf-outline-move-to-current-page
    "''"               'pdf-outline-move-to-current-page
    "Q"                'pdf-outline-quit-and-kill
    "q"                'quit-window
    "F"                'pdf-outline-follow-mode)
  (evilified-state-evilify pdf-annot-list-mode pdf-annot-list-mode-map
    "f"                'pdf-annot-list-display-annotation-from-id
    "d"                'tablist-flag-forward
    "x"                'tablist-do-flagged-delete
    "u"                'tablist-unmark-forward
    "q"                'tablist-quit)
  (evilified-state-evilify pdf-occur-buffer-mode pdf-occur-buffer-mode-map
    "q"              'tablist-quit
    "g"              'pdf-occur-revert-buffer-with-args
    "r"              'pdf-occur-revert-buffer-with-args
    "*"              'polaris/enter-ahs-forward
    "?"              'evil-search-backward))

(use-package doc-view
  :init
  (evilified-state-evilify doc-view-mode doc-view-mode-map
    "/"  'polaris/doc-view-search-new-query
    "?"  'polaris/doc-view-search-new-query-backward
    "gg" 'doc-view-first-page
    "G"  'polaris/doc-view-goto-page
    "gt" 'doc-view-goto-page
    "h"  'doc-view-previous-page
    "j"  'doc-view-next-line-or-next-page
    "k"  'doc-view-previous-line-or-previous-page
    "K"  'doc-view-kill-proc-and-buffer
    "l"  'doc-view-next-page
    "n"  'doc-view-search
    "N"  'doc-view-search-backward
    (kbd "C-d") 'doc-view-scroll-up-or-next-page
    (kbd "C-k") 'doc-view-kill-proc
    (kbd "C-u") 'doc-view-scroll-down-or-previous-page)
  :config
  (progn
    (defun polaris/doc-view-search-new-query ()
      "Initiate a new query."
      (interactive)
      (doc-view-search 'newquery))

    (defun polaris/doc-view-search-new-query-backward ()
      "Initiate a new query."
      (interactive)
      (doc-view-search 'newquery t))

    (defun polaris/doc-view-goto-page (&optional count)
      (interactive (list
                    (when current-prefix-arg
                      (prefix-numeric-value current-prefix-arg))))
      (if (null count)
          (doc-view-last-page)
        (doc-view-goto-page count)))

    ;; fixed a weird issue where toggling display does not
    ;; swtich to text mode
    (defadvice doc-view-toggle-display
        (around polaris/doc-view-toggle-display activate)
      (if (eq major-mode 'doc-view-mode)
          (progn
            ad-do-it
            (text-mode)
            (doc-view-minor-mode))
        ad-do-it))))

(provide 'module-pdf)
;;; module-pdf.el ends here

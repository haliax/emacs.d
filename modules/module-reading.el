;; -*- lexical-binding: t -*-
;;; module-reading.el

(use-package spray
  :commands spray-mode
  :init
  (evil-set-initial-state 'spray-mode 'emacs)
  (define-key spray-mode-map (kbd "h") 'spray-backward-word)
  (define-key spray-mode-map (kbd "l") 'spray-forward-word)
  (define-key spray-mode-map (kbd "q") 'spray-quit))

(provide 'module-reading)
;;; module-reading.el ends here

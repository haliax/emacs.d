;; -*- lexical-binding: t -*-
;;; module-shell.el

(use-package comint
  :init
  (setq comint-scroll-to-bottom-on-input t ;; always insert at the bottom
        ;; always add output at the bottom
        comint-scroll-to-bottom-on-output nil
        ;; scroll to show max possible output
        comint-scroll-show-maximum-output t
        ;; no duplicates in command history
        comint-input-ignoredups t
        ;; insert space/slash after file completion
        comint-completion-addsuffix t
        ;; if this is t, it breaks shell-command
        comint-prompt-read-only nil))

(defun polaris/shell-kill-buffer-sentinel (process event)
  (when (memq (process-status process) '(exit signal))
    (kill-buffer)))

(defun polaris/kill-process-buffer-on-exit ()
  (set-process-sentinel (get-buffer-process (current-buffer))
                        #'polaris/shell-kill-buffer-sentinel))

(dolist (hook '(ielm-mode-hook term-exec-hook comint-exec-hook))
  (add-hook hook 'polaris/kill-process-buffer-on-exit))

(defun set-scroll-conservatively ()
  "Add to shell-mode-hook to prevent jump-scrolling on newlines in shell buffers."
  (set (make-local-variable 'scroll-conservatively) 10))

(defadvice comint-previous-matching-input
    (around suppress-history-item-messages activate)
  "Suppress the annoying 'History item : NNN' messages from shell history isearch.
If this isn't enough, try the same thing with
comint-replace-by-expanded-history-before-point."
  (let ((old-message (symbol-function 'message)))
    (unwind-protect
        (progn (fset 'message 'ignore) ad-do-it)
      (fset 'message old-message))))

(add-hook 'shell-mode-hook #'set-scroll-conservatively)
;; truncate buffers continuously
(add-hook 'comint-output-filter-functions #'comint-truncate-buffer)
;; interpret and use ansi color codes in shell output windows
(add-hook 'shell-mode-hook #'ansi-color-for-comint-mode-on)

(setq explicit-shell-file-name "/usr/bin/zsh")

(use-package eshell
  :bind ("C-c e" . eshell)
  :init
  (setq eshell-directory-name (concat polaris-temp-dir "eshell")
        eshell-scroll-to-bottom-on-input 'all
        eshell-buffer-shorthand t

        eshell-cmpl-cycle-completions nil
        ;; auto truncate after 20k lines
        eshell-buffer-maximum-lines 20000
        ;; history size
        eshell-history-size 350
        ;; buffer shorthand -> echo foo > #'buffer
        eshell-buffer-shorthand t
        ;; my prompt is easy enough to see
        eshell-highlight-prompt nil
        ;; treat 'echo' like shell echo
        eshell-plain-echo-behavior t

        ;; em-glob
        eshell-glob-case-insensitive t
        eshell-error-if-no-glob t

        ;; em-alias
        eshell-aliases-file (concat polaris-temp-dir ".eshell-aliases"))

  (defun eshell/cds ()
    "Change directory to the project's root."
    (eshell/cd (locate-dominating-file default-directory ".git")))

  (defun eshell/l (&rest args) "Same as `ls -lh'"
         (apply #'eshell/ls "-lh" args))
  (defun eshell/ll (&rest args) "Same as `ls -lh'"
         (apply #'eshell/ls "-lh" args))
  (defun eshell/la (&rest args) "Same as `ls -alh'"
         (apply #'eshell/ls "-alh" args))

  (defun eshell/ec (pattern)
    (if (stringp pattern)
        (find-file pattern)
      (mapc #'find-file (mapcar #'expand-file-name pattern))))

  (defun eshell/clear ()
    "Clear the eshell buffer"
    (interactive)
    (let ((eshell-buffer-maximum-lines 0))
      (eshell-truncate-buffer)))

  (defun polaris/setup-eshell ()
    (interactive)
    ;; turn off semantic-mode in eshell buffers
    (semantic-mode -1)
    ;; turn off hl-line-mode
    (hl-line-mode -1))

  (add-hook 'eshell-mode-hook #'polaris/setup-eshell)

  ;; plan 9 smart shell
  (require 'em-smart)
  (add-hook! eshell-mode 'eshell-smart-initialize)
  (add-hook! eshell-mode (semantic-mode -1))
  (setq eshell-where-to-jump 'begin)
  (setq eshell-review-quick-commands nil)
  (setq eshell-smart-space-goes-to-end t)

  (setq eshell-modules-list
        '(eshell-alias eshell-basic eshell-cmpl eshell-dirs eshell-glob eshell-hist
                       eshell-ls eshell-pred eshell-prompt eshell-rebind
                       eshell-script eshell-smart eshell-term eshell-unix
                       eshell-xtra))
  (add-to-list 'eshell-output-filter-functions 'eshell-handle-ansi-color)

  (add-hook! eshell-mode 'polaris|eshell)

  (with-eval-after-load 'flyspell
    (bind-keys
     :map flyspell-mode-map
     ("C-;" . nil)))

  (defun polaris|eshell ()
    (bind-keys
     :map eshell-mode-map
     ("C-;" . delete-window)
     ("C-r" . helm-eshell-history)
     ([remap eshell-pcomplete] . helm-esh-pcomplete)))
  :config
  (require 'esh-opt)

  ;; quick commands
  (defalias 'e 'find-file-other-window)
  (defalias 'd 'ranger)
  (defalias 's 'magit-status)
  (setenv "PAGER" "cat")
  )

(use-package esh-help
  :defer t
  :init (add-hook 'eshell-mode-hook 'eldoc-mode)
  :config
  (setup-esh-help-eldoc))

(use-package eshell-prompt-extras
  :after esh-opt
  :config
  (defun my-eshell-theme ()
    "A eshell-prompt lambda theme."
    (concat
     "\n"
     (propertize
      user-login-name
      'font-lock-face '(:weight bold))
     (propertize
      (concat "@" (car (split-string system-name "\\.")))
      'font-lock-face '(:foreground "#546A29" :weight bold))
     (propertize
      (concat ":" (eshell-tildify (eshell/pwd)))
      'font-lock-face
      `(:foreground ,(face-foreground 'eshell-ls-directory-face) :weight bold))
     (when (and (eshell-search-path "git")
                (locate-dominating-file (eshell/pwd) ".git"))
       (propertize
        (concat " " (epe-git-branch))
        'font-lock-face '(:weight bold)))
     "\n"
     (propertize
      (format-time-string "%H:%M ")
      'font-lock-face '(:weight bold))
     (propertize
      (if (= (user-uid) 0) "# " "❯ ")
      'font-lock-face '(:weight bold))))
  (defun eshell-tildify (pwd)
    (interactive)
    (let* ((home (expand-file-name (getenv "HOME")))
           (home-len (length home)))
      (if (and
           (>= (length pwd) home-len)
           (equal home (substring pwd 0 home-len)))
          (concat "~" (substring pwd home-len))
        pwd)))
  (setq eshell-highlight-prompt nil)
  (setq eshell-prompt-function #'my-eshell-theme)
  (setq eshell-prompt-regexp "^.*[#❯] $"))

(use-package eshell-z
  :defer t
  :init
  (with-eval-after-load 'eshell
    (require 'eshell-z)))

(use-package shell-pop
  :defer t
  :init
  (progn
    (setq shell-pop-window-position 'bottom ; top, bottom, full
          shell-pop-window-height   30
          shell-pop-term-shell      'eshell ; eshell, shell, term, ansi-term
          shell-pop-full-span t)
    (defmacro make-shell-pop-command (type &optional shell)
      (let* ((name (symbol-name type)))
        `(defun ,(intern (concat "shell-pop-" name)) (index)
           (interactive "P")
           (require 'shell-pop)
           (shell-pop--set-shell-type
            'shell-pop-shell-type
            (backquote (,name
                        ,(concat "*" name "*")
                        (lambda nil (funcall ',type ,shell)))))
           (shell-pop index))))
    (make-shell-pop-command eshell)
    (make-shell-pop-command shell)
    (make-shell-pop-command term shell-pop-term-shell)
    (make-shell-pop-command ansi-term shell-pop-term-shell)

    (defun ansi-term-handle-close ()
      "Close current term buffer when `exit' from term buffer."
      (when (ignore-errors (get-buffer-process (current-buffer)))
        (set-process-sentinel (get-buffer-process (current-buffer))
                              (lambda (proc change)
                                (when (string-match "\\(finished\\|exited\\)" change)
                                  (kill-buffer (process-buffer proc))
                                  (delete-window))))))
    (add-hook 'term-mode-hook 'ansi-term-handle-close)
    (add-hook 'term-mode-hook (lambda () (linum-mode -1)))

    ))

(provide 'module-shell)
;;; module-shell.el ends here

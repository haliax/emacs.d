;; -*- lexical-binding: t -*-
;;; module-python.el
;; https://github.com/yyuu/pyenv

(use-package python
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :commands python-mode
  :init
  (add-hook! python-mode '(semantic-idle-summary-mode -1))

  (when (executable-find "ipython")
    (setq python-shell-interpreter "ipython"
          python-shell-interpreter-args "--deep-reload"
          python-shell-prompt-regexp "In \\[[0-9]+\\]: "
          python-shell-prompt-block-regexp "\\.\\.\\.\\.: "
          python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
          python-shell-completion-setup-code
          "from IPython.core.completerlib import module_completion"
          python-shell-completion-string-code
          "';'.join(get_ipython().Completer.all_completions('''%s'''))\n"))

  (setq python-environment-directory polaris-temp-dir
        python-shell-interpreter "ipython"
        python-shell-interpreter-args "--deep-reload"
        python-shell-prompt-regexp (rx "In ["
                                       (one-or-more digit)
                                       "]: ")
        python-shell-prompt-block-regexp (rx "....: ")
        python-shell-prompt-output-regexp (rx "Out["
                                              (one-or-more digit)
                                              "]: ")
        python-shell-completion-setup-code
        "from IPython.core.completerlib import module_completion"
        python-shell-completion-string-code
        "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")

  :config
  (def-company-backend! python-mode (anaconda))
  (def-env-command! python-mode "python --version 2>&1 | cut -d' ' -f2")
  (def-repl! python-mode polaris/inf-python)

  (define-key python-mode-map (kbd "DEL") nil)) ; interferes with smartparens

(use-package anaconda-mode
  :after python
  :diminish anaconda-mode
  :init
  (add-hook! python-mode '(anaconda-mode anaconda-eldoc-mode eldoc-mode))
  (setq anaconda-mode-installation-directory (concat polaris-temp-dir "anaconda/")
        anaconda-mode-eldoc-as-single-line t)

  :config
  (map! :map anaconda-mode-map     :m "gd"     'anaconda-mode-find-definitions)
  (map! :map anaconda-nav-mode-map :n [escape] 'anaconda-nav-quit)

  (advice-add 'anaconda-mode-doc-buffer :after 'polaris*anaconda-mode-doc-buffer)

  (evilified-state-evilify anaconda-mode-view-mode anaconda-mode-view-mode-map
    (kbd "q") 'quit-window))

(use-package company-anaconda
  :after anaconda-mode
  :init (add-hook 'anaconda-mode-hook 'emr-initialize)
  :config
  (mapc (lambda (x)
          (let ((command-name (car x))
                (title (cadr x))
                (region-p (caddr x))
                predicate)
            (setq predicate (lambda () (and (anaconda-mode-running-p)
                                       (not (use-region-p))
                                       (not (sp-point-in-string-or-comment)))))
            (emr-declare-command (intern (format "anaconda-mode-%s" (symbol-name command-name)))
              :title title :modes 'python-mode :predicate predicate)))
        '((show-doc          "view documentation" t)
          (find-assignments  "find assignments"  t)
          (find-definitions  "find definitions"  t)
          (find-file         "find assignments"  t)
          (find-references   "show usages"  nil))))

(use-package flycheck-pyflakes)
(use-package flycheck-mypy)

(use-package live-py-mode
  :commands live-py-mode)

(use-package helm-pydoc
  :commands helm-pydoc)

(use-package ein
  :commands ein:notebooklist-open
  :config
  (add-hook 'ein:connect-mode-hook #'ein:jedi-setup)
  (setq ein:complete-on-dot t
        ein:console-args nil))

(use-package elpy
  :disabled t
  :commands elpy-mode
  :diminish elpy-mode
  :init
  (add-hook! python-mode 'elpy-mode)
  :config
  (let ((disabled-modules '(elpy-module-flymake elpy-module-highlight-indentation)))
    (setq elpy-modules (-difference elpy-modules disabled-modules)))
  (elpy-use-ipython (executable-find "ipython"))
  (map! :map elpy-mode
        (:localleader
          :n "8" 'elpy-autopep8-fix-code
          :n "a" 'elpy-importmagic-add-import
          :n "f" 'elpy-importmagic-fixup
          :n "t" 'elpy-test-run))
  (setq elpy-rpc-python-command "python3"
        elpy-rpc-backend "jedi"))

(use-package pip-requirements
  :mode ("/requirements.txt$" . pip-requirements-mode)
  :config (def-company-backend! pip-requirements-mode (capf)))

(use-package nose
  :commands nose-mode
  :preface (defvar nose-mode-map (make-sparse-keymap))
  :init
  (associate! nose-mode :match "/test_.+\\.py$" :in (python-mode))
  :config
  (def-yas-mode! 'nose-mode)
  (map! :map nose-mode-map
        (:localleader
          :n "tr" 'nosetests-again
          :n "ta" 'nosetests-all
          :n "ts" 'nosetests-one
          :n "tv" 'nosetests-module
          :n "tA" 'nosetests-pdb-all
          :n "tO" 'nosetests-pdb-one
          :n "tV" 'nosetests-pdb-module)))

(provide 'module-python)
;;; module-python.el ends here

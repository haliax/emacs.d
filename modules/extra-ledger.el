;;; extra-ledger.el

(use-package ledger-mode
  :mode ("\\.\\(ledger\\|ldg\\)\\'" . ledger-mode)
  :defer t
  :init
  (progn
    (setq ledger-post-amount-alignment-column 62)
    (push 'company-capf company-backends-ledger-mode)
    ;; temporary hack to work-around an issue with evil-define-key
    ;; more info: https://bitbucket.org/lyro/evil/issues/301/evil-define-key-for-minor-mode-does-not
    ;; TODO remove this hack if the limitation is removed upstream
    (add-hook 'ledger-mode-hook 'evil-normalize-keymaps)
    (evilified-state-evilify ledger-report-mode ledger-report-mode-map)
    (require 'flycheck-ledger)))

(provide 'extra-ledger)
;;; extra-ledger.el ends here

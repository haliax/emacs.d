;; -*- lexical-binding: t -*-
;;; extra-write.el

;; This library offers the following:
;;   + TODO Write-mode: a mode that turns Emacs into an app for writing notes, papers, or
;;     fiction: it adds eye-candy to org-mode, switches to a light color theme and to a
;;     more readable font.
;;     Bibtex integration

;; Write-mode settings
(defconst write-mode nil)
(defconst write-mode-theme 'solarized-light)
(defconst write-mode-font (font-spec :family "Hack" :size 16))
(defconst write-mode-dir (expand-file-name "org/" polaris-user-dir))
(defconst write-mode-biblio-dir (concat polaris-home-dir "usr/docs/biblio/"))

(defconst write-mode--last-mode-line mode-line-format)
(defconst write-mode--last-line-spacing line-spacing)

;;
(defun write-mode|org-hook ()
  "A hook that runs everytime an org-mode buffer is visited/created while `write-mode' is
active.")

(defun write-mode-toggle ()
  "Enable write-mode, this is not a [global] minor mode because it mixes some frame-local
functionality with buffer-local ones, which can be buggy in a minor-mode."
  (interactive)
  (let* ((mode-p write-mode)
         (on-off (if mode-p -1 +1)))
    ;; (scroll-bar-mode on-off)
    (polaris/load-font (if mode-p polaris-default-font write-mode-font))
    (if write-mode
        (remove-hook 'org-mode-hook 'write-mode|org-hook)
      (add-hook 'org-mode-hook 'write-mode|org-hook))
    (mapc (lambda (b)
            (with-current-buffer b
              (setq line-spacing (if mode-p write-mode--last-line-spacing '2))
              (when (eq major-mode 'org-mode)
                (unless write-mode
                  ;; (font-lock-remove-keywords nil write-mode-org-font-lock-keywords)
                  (write-mode|org-hook))
                (org-bullets-mode on-off))))
          (polaris/get-buffers-in-modes '(org-mode markdown-mode)))
    (setq write-mode (not write-mode))))

(when (>= emacs-major-version 24)
  ;; From <https://github.com/joostkremers/visual-fill-column/pull/6>
  ;; Splitting windows while visual-fill-column makes Emacs go crazy. This prevents that
  ;; by simply disabled VFC before splitting.
  (after! visual-fill-column
    (advice-add 'split-window :around #'visual-fill-column--disable-on-split-window))
  (defun visual-fill-column--disable-on-split-window (fn window &rest args)
    "Undo the effects of `visual-fill-column-mode' for splitting window."
    (if (and (or (not window) (window-live-p window))
             (buffer-local-value 'visual-fill-column-mode
                                 (window-buffer (or window (selected-window)))))
        (let ((inhibit-redisplay t))
          (set-window-fringes (or window (selected-window)) nil)
          (set-window-margins (or window (selected-window)) 0 0)
          (unwind-protect (apply fn window args)
            (save-selected-window
              (when window (select-window window 'norecord))
              (visual-fill-column--adjust-window))))
      (apply fn window args))))

(use-package reftex
  :commands turn-on-reftex
  :init
  (setq reftex-plug-into-AUCTeX t
        reftex-ref-style-default-list '("Cleveref" "Hyperref" "Fancyref")
        reftex-default-bibliography
        `(,(expand-file-name "phys.bib" write-mode-biblio-dir)))
  (add-hook! (LaTeX-mode latex-mode) 'turn-on-reftex))

(use-package helm-bibtex
  :defer t
  :init
  (setq TeX-auto-save t
        TeX-parse-self t
        bibtex-dialect 'biblatex
        bibtex-align-at-equal-sign t
        bibtex-text-indentation 20)
  (add-hook! bibtex-mode
    (local-set-key (kbd "C-c \\") 'bibtex-fill-entry)
    (setq fill-column 140))
  (add-hook! (LaTeX-mode latex-mode) 'turn-on-auto-fill)

  :config
  (setq helm-bibtex-bibliography
        `(,(expand-file-name "phys.bib" write-mode-biblio-dir))

        helm-bibtex-library-path
        `(,(expand-file-name "phys-pdf" write-mode-biblio-dir))

        helm-bibtex-notes-path (expand-file-name "notes" write-mode-biblio-dir)
        helm-bibtex-notes-extension ".org"

        helm-bibtex-pdf-open-function
        (lambda (fpath) (async-start-process "open-pdf" "/usr/bin/open" nil fpath))))

(provide 'extra-write)
;;; extra-write.el ends here

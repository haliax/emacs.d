;; -*- lexical-binding: t -*-
;;; module-text.el
;; see lib/markdown-defuns.el

(use-package markdown-mode
  :mode ("\\.md$" "\\.markdown$" "/README$")
  :init (add-hook 'markdown-mode-hook #'turn-on-auto-fill)
  :config
  (map! :map markdown-mode-map
        "<backspace>"  nil
        "<M-left>"     nil
        "<M-right>"    nil

        "M-*"  'markdown-insert-list-item
        "M-b"  'markdown-insert-bold
        "M-i"  'markdown-insert-italic
        "M-`"  'polaris/markdown-insert-del

        (:localleader
          :nv "i"   'markdown-insert-image
          :nv "l"   'markdown-insert-link
          :nv "L"   'markdown-insert-reference-link-dwim
          :nv "b"   'markdown-preview)

        ;; TODO: Make context sensitive
        :n "[p"   'markdown-promote
        :n "]p"   'markdown-demote

        :i "M--"  'markdown-insert-hr))

(use-package markdown-toc :after markdown-mode)

(def-company-backend! text-mode (ispell))

(use-package typo
  :config
  (add-hook 'text-mode-hook #'typo-mode)
  (setq typo-language "English"))

(use-package deft
  :defer t
  :init
  (progn
    (setq deft-extensions '("org" "md" "txt")
          deft-text-mode 'org-mode
          deft-use-filename-as-title t)))

(provide 'module-text)
;;; module-text.el ends here

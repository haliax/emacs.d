;; -*- lexical-binding: t -*-
;;; module-media.el

(use-package simple-mpc
  :config
  (evil-set-initial-state 'simple-mpc-mode 'emacs))

(use-package elfeed
  :commands elfeed
  :init
  (setf url-queue-timeout 60)
  (setq elfeed-db-directory (concat polaris-temp-dir "elfeed")
        elfeed-search-filter "@4-weeks +unread "
        elfeed-search-title-max-width 100)

  (defun feeds ()
    "Open a perspective for Elfeed."
    (interactive)
    (persp-switch "feeds")
    (elfeed))

  (evilified-state-evilify-map elfeed-search-mode-map
    :mode elfeed-search-mode
    :eval-after-load elfeed-search
    :bindings
    "c"  'elfeed-db-compact
    "gr" 'elfeed-update
    "gR" 'elfeed-search-update--force
    "gu" 'elfeed-unjam
    "o"  'elfeed-load-opml
    "q"  'quit-window)
  (evilified-state-evilify-map elfeed-show-mode-map
    :mode elfeed-show-mode
    :eval-after-load elfeed-show
    :bindings
    "q" 'quit-window
    (kbd "C-j") 'elfeed-show-next
    (kbd "C-k") 'elfeed-show-prev))


(use-package elfeed-goodies
  :after (elfeed)
  :config
  (elfeed-goodies/setup)
  (evil-define-key 'evilified elfeed-show-mode-map "o" 'elfeed-goodies/show-ace-link)
  (setq elfeed-goodies/entry-pane-size 0.5
        elfeed-goodies/powerline-default-separator 'bar))

(use-package elfeed-org
  :commands elfeed-org
  :config (elfeed-org))

(use-package erc
  :init
  (defun freenode ()
    "Open a perspective for Freenode"
    (interactive)
    (persp-switch "irc")
    (erc-tls :server "irc.frenode.net"))
  (defun nixers ()
    "Open a perspective for Nixers"
    (interactive)
    (persp-switch "irc")
    (erc-tls :server "unix.chat"))
  (setq erc-modules
        '(truncate keep-place log pcomplete netsplit button match
                   notifications track completion readonly networks ring autojoin
                   noncommands irccontrols move-to-prompt stamp menu list)
        erc-server-coding-system '(utf-8 . utf-8))
  :config
  (progn
    (setq erc-server "irc.freenode.net"
          erc-port 6697
          erc-nick "haliax"
          erc-user-full-name user-full-name
          erc-server-reconnect-timeout 60
          ;; erc-server-connect-function 'erc-open-tls-stream
          ;; erc-join-buffer 'bury
          erc-autojoin-timing 'ident
          erc-prompt-for-password nil
          erc-hide-list '("JOIN" "QUIT")
          erc-mode-line-format "%t"
          erc-mode-line-away-status-format " (AWAY %a %H:%M)"
          ;; erc-header-line-format "%n%a on %S [%m,%l] %o"
          erc-header-line-format "%n on %t (%m,%l)"
          ;; erc-timestamp-format-left "\n[%d %B %Y, %A]\n"
          ;; erc-timestamp-intangible nil
          erc-log-file-coding-system 'utf-8
          erc-prompt "ERC ❯"
          erc-paranoid t
          erc-nickserv-identify-mode 'autodetect
          erc-autojoin-channels-alist
          '(("freenode.net" "#emacs" "##programming" "#evil-mode" "#git" "#nixos" "#archlinux"
             "#math" "#bash" "#nixheads" "#nixos" "#haskell" "#clojure" "#emacs-beginners")
            ("rizon.net" "#rice")
            ("unix.chat" "#nixers")))

    (add-hook 'erc-join-hook (lambda () (set (make-variable-buffer-local 'bs-default-configuration) "erc-channels")))
    (add-hook 'erc-mode-hook (lambda () (set (make-variable-buffer-local 'bs-default-configuration) "erc-channels")))

    (defun polaris/erc-quit-part-reason (&rest _)
      (concat "Leaving . Goodbye!"))

    (setq erc-quit-reason 'polaris/erc-quit-part-reason
          erc-part-reason 'polaris/erc-quit-part-reason)

    (setq polaris/erc-away-msg-list
          '("just away" "watching athletics" "watching darts"
            "eating" "i'm not ready to chat" "time to sleep")
          polaris/erc-channel-list
          '("#emacs" "#archlinux" "#emacs-beginners" "#" "#bash"
            "#lisp" "#rice" "#git" "#github" "#/g/technology" "#haskell"
            "#clojure" "#nixos" "#nixers" "#bspwm" "##programming"))

    (add-hook 'erc-mode-hook (lambda () (set (make-variable-buffer-local 'bs-default-configuration) "channels")))

    (erc-services-mode 1)
    (defun erc-list-command ()
      "execute the list command"
      (interactive)
      (insert "/list")
      (erc-send-current-line))
    (setq erc-kill-buffer-on-part t
          erc-kill-queries-on-quit t
          erc-kill-server-buffer-on-quit t)
    (add-hook 'erc-connect-pre-hook (lambda (x) (erc-update-modules)))

    (set-face-attribute
     'erc-prompt-face nil
     :foreground (face-foreground 'mode-line-highlight)
     :weight 'bold)
    ))

(use-package emoji-cheat-sheet-plus
  :diminish emoji-cheat-sheet-plus-display-mode
  :init (add-hook 'erc-mode-hook #'emoji-cheat-sheet-plus-display-mode))

(use-package typo
  :config (add-hook 'erc-mode-hook #'typo-mode))

(use-package erc-hl-nicks
  :config
  (erc-hl-nicks-mode))

(use-package erc-yt
  :init (with-eval-after-load 'erc (add-to-list 'erc-modules 'youtube)))

(use-package erc-image
  :init
  (with-eval-after-load 'erc
    (require 'erc-image)
    (add-to-list 'erc-modules 'image))
  (setq erc-image-inline-rescale 'window))

(use-package erc-pcomplete
  :disabled t
  :config
  (erc-pcomplete-mode)
  (setq erc-pcomplete-nick-postfix ": "))

(use-package erc-spelling
  :disabled t
  :config
  (erc-spelling-mode))

(use-package erc-autoaway
  :config
  (setq erc-auto-discard-away t
        erc-autoaway-idle-seconds 600
        erc-autoaway-use-emacs-idle t
        erc-autoaway-message "I'm away (after %i seconds of idle-time)"))

(use-package erc-track
  :config
  (setq erc-track-exclude-server-buffer t
        erc-track-position-in-mode-line t
        erc-track-shorten-function nil
        erc-track-showcount t
        erc-track-exclude-types
        '("JOIN" "NICK" "PART" "QUIT" "MODE"
          "305" "306"                 ; away messages
          "324"                       ; channel modes
          "328"
          "329"                       ; channel was created on
          "332"                       ; welcome/topic messages
          "333"                       ; set topic
          "353" "477"))
  (erc-track-mode))

(use-package sauron
  :config
  (setq sauron-modules '(sauron-erc sauron-org sauron-notifications)
        sauron-prio-sauron-started 2
        sauron-min-priority 3
        sauron-separate-frame nil
        sauron-nick-insensitivity t
        sauron-max-line-length 180
        sauron-watch-nicks '("haliax"))
  (sauron-start-hidden))

(use-package shr
  :ensure nil
  :config
  (advice-add #'shr-colorize-region :filter-args #'remove-colorize-bg-arg)
  (defun remove-colorize-bg-arg (args)
    "If more than 3 args, remove the last one (corresponding to bg color)."
    (if (> (length args) 3)
        (butlast args)
      args))
  (setq shr-bullet "•"
        shr-color-visible-luminance-min 70
        shr-width 100))


(provide 'module-media)
;;; module-media.el ends here

;; -*- lexical-binding: t -*-
;;; module-nim.el

(use-package nim-mode
  :mode "\\.nim$"
  :init
  (add-hook! nim-mode 'flycheck-mode)
  (def-company-backend! nim-mode (nim yasnippet))
  :config
  (map! :map nim-mode-map "gd" 'nim-goto-sym))

(use-package company-nim
  :after nim-mode)

(use-package flycheck-nim
  :after nim-mode)

(provide 'module-nim)
;;; module-nim.el ends here

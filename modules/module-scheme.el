;;; module-scheme.el

(use-package geiser
  :config
  (setq geiser-active-implementations '(racket)))

(use-package geiser-repl
  :config (evil-set-initial-state 'geiser-repl-mode 'emacs))

(use-package hy-mode
  :mode ("\\.hy\\'" . hy-mode)
  :interpreter "hy")

(provide 'module-scheme)
;;; module-scheme.el ends here

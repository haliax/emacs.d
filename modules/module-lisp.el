;; -*- lexical-binding: t -*-
;;; module-lisp --- all things lisp

(setq eldoc-idle-delay 0.1) ; default is way too long

(associate! emacs-lisp-mode :match "\\(/Cask\\|\\.\\(el\\|gz\\)\\)$")

(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook lisp-interaction-mode-hook))
  (add-hook hook #'turn-on-eldoc-mode))

(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook lisp-interaction-mode-hook))
  (add-hook hook #'highlight-numbers-mode))

(use-package highlight-quoted
  :commands (highlight-quoted-mode)
  :init (add-hook 'emacs-lisp-mode-hook 'highlight-quoted-mode))

(add-hook 'emacs-lisp-mode-hook 'polaris/elisp-init)
(defun polaris/elisp-init ()
  (def-company-backend! emacs-lisp-mode (elisp yasnippet))
  (def-repl! emacs-lisp-mode polaris/elisp-inf-ielm)
  (def-rotate! emacs-lisp-mode
    :symbols (("t" "nil")
              ("let" "let*")
              ("when" "unless")
              ("append" "prepend")
              ("advice-add" "advice-remove")
              ("add-hook" "add-hook!" "remove-hook")))

  ;; Don't affect lisp indentation (only `tab-width')
  (setq editorconfig-indentation-alist
        (delq (assq 'emacs-lisp-mode editorconfig-indentation-alist)
              editorconfig-indentation-alist))

  ;; Real go-to-definition for elisp
  (map! :map emacs-lisp-mode-map :m "gd" 'polaris/elisp-find-function-at-pt)

  (font-lock-add-keywords
   'emacs-lisp-mode `(("(\\(lambda\\)"
                       (1 (polaris/show-as ?λ)))
                      ;; Highlight polaris macros (macros are fontified in emacs 25+)
                      (,(concat
                         "(\\(def-"
                         (regexp-opt '("electric" "project-type" "company-backend"
                                       "builder" "repl" "textobj" "tmp-excmd" "rotate"
                                       "repeat" "yas-mode" "env-command" "docset"))
                         "!\\)")
                       (1 font-lock-keyword-face append))
                      (,(concat
                         "(\\("
                         (regexp-opt '("λ" "in" "map" "after" "shut-up" "add-hook"
                                       "associate" "open-with" "define-org-link"
                                       "define-org-section"))
                         "!\\)")
                       (1 font-lock-keyword-face append))
                      ;; Ert
                      (,(concat
                         "("
                         (regexp-opt '("ert-deftest") t)
                         " \\([^ ]+\\)")
                       (1 font-lock-keyword-face)
                       (2 font-lock-function-name-face))))

  (remove-hook 'emacs-lisp-mode-hook 'polaris/elisp-init))

(add-hook 'emacs-lisp-mode-hook 'polaris/elisp-hook)
(defun polaris/elisp-hook ()
  (setq mode-name "Elisp") ; [pedantry intensifies]

  (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)
  (add-hook 'after-save-hook  'polaris/elisp-auto-compile nil t)

  (let ((header-face 'font-lock-constant-face))
    (push '("Evil Command" "\\(^\\s-*(evil-define-command +\\)\\(\\_<.+\\_>\\)" 2)
          imenu-generic-expression)
    (push '("Evil Operator" "\\(^\\s-*(evil-define-operator +\\)\\(\\_<.+\\_>\\)" 2)
          imenu-generic-expression)
    (push '("Package" "\\(^\\s-*(use-package +\\)\\(\\_<.+\\_>\\)" 2)
          imenu-generic-expression)
    (push '("Spaceline Segment" "\\(^\\s-*(spaceline-define-segment +\\)\\(\\_<.+\\_>\\)" 2)
          imenu-generic-expression)))

;; Add new colors to helm-imenu
(after! helm-imenu
  (defun helm-imenu-transformer (candidates)
    (cl-loop for (k . v) in candidates
             for types = (or (helm-imenu--get-prop k)
                             (list "Function" k))
             for bufname = (buffer-name (marker-buffer v))
             for disp1 = (mapconcat
                          (lambda (x)
                            (propertize
                             x 'face (cond ((string= x "Variables")
                                            'font-lock-variable-name-face)
                                           ((or (string= x "Function")
                                                (string-prefix-p "Evil " x t))
                                            'font-lock-function-name-face)
                                           ((string= x "Types")
                                            'font-lock-type-face)
                                           ((string= x "Package")
                                            'font-lock-negation-char-face)
                                           ((string= x "Spaceline Segment")
                                            'font-lock-string-face))))
                          types helm-imenu-delimiter)
             for disp = (propertize disp1 'help-echo bufname)
             collect
             (cons disp (cons k v)))))

;; Real go-to-definition for elisp
(map! :map emacs-lisp-mode-map :m "gd" 'polaris/elisp-find-function-at-pt)

;; (def-project-type! emacs-ert "ert"
;;   :modes (emacs-lisp-mode)
;;   :match "/test/.+-test\\.el$"
;;   :bind (:localleader
;;           :n "tr" 'polaris/ert-rerun-test
;;           :n "ta" 'polaris/ert-run-all-tests
;;           :n "ts" 'polaris/ert-run-test)
;;   (add-hook 'ert-results-mode-hook 'polaris|hide-mode-line))

(sp-local-pair 'emacs-lisp-mode "'" nil :actions nil)
(sp-local-pair 'emacs-lisp-mode "`" nil :when '(sp-in-string-p))

;; Always keep your code indented
(use-package aggressive-indent
  :diminish " Ⓘ "
  :init
  (dolist (hook '(emacs-lisp-mode-hook lisp-mode-hook clojure-mode-hook))
    (add-hook hook #'aggressive-indent-mode)))

;; SLIME-like navigation in Elisp.
;; jump to elisp definition (function, symbol etc.) and back, show doc
(use-package elisp-slime-nav
  :diminish elisp-slime-nav-mode
  :init
  (dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook lisp-interaction-mode-hook))
    (add-hook hook 'turn-on-elisp-slime-nav-mode))
  (map! :map elisp-slime-nav-mode-map
        :n "C-c ." 'elisp-slime-nav-find-elisp-thing-at-point
        :n "C-c ," 'pop-tag-mark
        :n "C-c d" 'elisp-slime-nav-describe-elisp-thing-at-point))

;; Disable Flycheck (on typical Emacs configs, produces far more false positives than useful warnings).
(use-package flycheck
  :config
  (->> (default-value 'flycheck-disabled-checkers)
       (append '(emacs-lisp emacs-lisp-checkdoc))
       (-uniq)
       (setq-default flycheck-disabled-checkers)))

;; Highlight defined symbols.
(use-package highlight-defined
  :defer t
  :init
  (add-hook! emacs-lisp-mode 'highlight-defined-mode))

(use-package lispy
  :diminish lispy-mode
  :init
  (add-hook 'emacs-lisp-mode-hook (lambda () (lispy-mode 1)))
  (add-hook 'command-history-hook (lambda () (lispy-mode 1)))
  (add-hook 'lisp-interaction-mode-hook (lambda () (lispy-mode 1)))
  (add-hook 'clojure-mode-hook (lambda () (lispy-mode 1)))
  (add-hook 'scheme-mode-hook (lambda () (lispy-mode 1)))
  (add-hook 'lisp-mode-hook (lambda () (lispy-mode 1)))
  :config
  (progn
    (define-key lispy-mode-map-paredit (kbd "DEL") nil) ; Because smartparens delete works better than lispy delete
    (define-key lispy-mode-map (kbd "M-k") 'lispy-kill-sentence)
    (setq lispy-avy-style-symbol 'at-full
          lispy-avy-style-paren 'at-full
          lispy-no-permanent-semantic t
          lispy-helm-columns '(70 100)
          lispy-compat '(edebug cider macrostep)
          lispy-set-key-theme '(oleh special lispy c-digits))))

(use-package lispyville
  :config
  (add-hook 'lispy-mode-hook #'lispyville-mode)
  (lispyville-set-key-theme '(operators (escape insert))))

(use-package macrostep
  :init
  (map! :map emacs-lisp-mode-map
        (:localleader
          :m "m" 'macrostep-expand)
        :m "C-c e" 'macrostep-expand))

;; (use-package bury-successful-compilation
;;   :init (bury-successful-compilation))

(use-package unkillable-scratch
  :init
  (setq unkillable-scratch-behaviour 'bury)
  (unkillable-scratch))

(use-package highlight-parentheses
  :diminish highlight-parentheses-mode
  :init (add-hook! prog-mode 'highlight-parentheses-mode))

(use-package sly
  :config
  (setq inferior-lisp-program (executable-find "sbcl"))
  (push 'sly-repl-ansi-color sly-contribs)
  (push 'sly-indentation sly-contribs)
  (add-hook 'sly-mode-hook #'sly-company-mode)
  (add-hook 'sly-mode-hook #'turn-on-eldoc-mode))

(use-package redshank
  :init
  (add-hook 'sly-mode-hook #'turn-on-redshank-mode))

;; ignore obsolete function warning generated on startup
(let ((byte-compile-not-obsolete-funcs (append byte-compile-not-obsolete-funcs '(preceding-sexp))))
  (require 'eval-sexp-fu))

(provide 'module-lisp)
;;; module-lisp.el ends here

;; -*- lexical-binding: t -*-
;;; module-haskell.el
;; Things to consider adding in the future:
;;   * https://github.com/chrisdone/hindent
;;   * https://github.com/chrisdone/structured-haskell-mode
;;   * https://github.com/alanz/HaRe

;; Main package.
(use-package haskell
  :mode (("\\.hcr\\'" . ghc-core-mode)
         ("\\.dump-simpl\\'" . ghc-core-mode)
         ("\\.ghci\\'" . ghci-script-mode)
         ("\\.cabal\\'" . haskell-cabal-mode)
         ("\\.[gh]s\\'" . haskell-mode)
         ("\\.l[gh]s\\'" . literate-haskell-mode)
         ("\\.hsc\\'" . haskell-mode))
  :interpreter (("runghc" . haskell-mode)
                ("runhaskell" . haskell-mode))
  :init
  (add-hook! haskell-mode '(interactive-haskell-mode flycheck-mode))
  :config
  (def-repl! haskell-mode switch-to-haskell)
  (push ".hi" completion-ignored-extensions)

  (add-hook 'haskell-mode-hook #'turn-on-haskell-indentation) ; Most advanced indentation mode (hi2 was merged into it in March 2015: <https://github.com/haskell/haskell-mode/wiki/Month-in-haskell-mode-March-2015#important-developments>).
  (add-hook 'haskell-mode-hook #'interactive-haskell-mode) ; Use the new interactive mode.
  (add-hook 'haskell-mode-hook #'eldoc-mode) ; Show type signatures in minibuffer (eldoc config is in conf/view/eldoc.el). Uses interactive process when available, otherwise hardcoded list.

  (setq
   ;; Use notify.el (if you have it installed) at the end of running
   ;; Cabal commands or generally things worth notifying.
   haskell-notify-p t
   ;; To enable tags generation on save.
   haskell-tags-on-save t
   ;; Remove annoying error popups
   haskell-interactive-popup-errors nil
   ;; Better import handling
   haskell-process-suggest-remove-import-lines t
   haskell-process-auto-import-loaded-modules t
   ;; Disable haskell-stylish-on-save, as it breaks flycheck highlighting.
   ;; NOTE: May not be true anymore - taksuyu 2015-10-06
   haskell-stylish-on-save nil)

  ;; Customizations suggested on <https://github.com/haskell/haskell-mode/wiki/Haskell-Interactive-Mode-Setup>.
  (setq haskell-process-suggest-remove-import-lines t)
  (setq haskell-process-auto-import-loaded-modules t)
  (setq haskell-process-log t)

  ;; haskell-process-type is set to auto, so setup ghci-ng for either case
  ;; if haskell-process-type == cabal-repl
  (setq haskell-process-args-cabal-repl '("--ghc-option=-ferror-spans" "--with-ghc=ghci-ng"))
  ;; if haskell-process-type == GHCi
  (setq haskell-process-path-ghci "ghci-ng")
  ;; fixes ghci-ng for stack projects
  (setq haskell-process-wrapper-function
        (lambda (args)
          (append args (list "--with-ghc" "ghci-ng"))))

  ;; Use Cabal for the REPL so that projects stay sandboxed instead of polluting the global database.
  (setq haskell-process-type 'cabal-repl))

(use-package inf-haskell
  :commands (inferior-haskell-mode inf-haskell-mode switch-to-haskell)
  :init (evil-set-initial-state 'inferior-haskell-mode 'emacs)
  :config
  (define-key inf-haskell-mode-map (kbd "ESC ESC") 'polaris/popup-close))

;; Auto-configure Flycheck from Cabal.
(use-package flycheck-haskell
  :defer t
  :init
  (with-eval-after-load 'haskell-mode ; No need to load if we're not editing any Haskell file.
    (with-eval-after-load 'flycheck
      (add-hook 'flycheck-mode-hook #'flycheck-haskell-setup))))

;; Additional snippets.
(use-package haskell-snippets
  :defer t)

(use-package ghc
  :init
  (add-hook 'haskell-mode-hook #'ghc-init))

(use-package hindent
  :init
  (add-hook 'haskell-mode-hook #'hindent-mode))

(use-package shm
  :init
  (add-hook 'haskell-mode-hook #'structured-haskell-mode))

(use-package company-cabal
  :init
  (after! company
    (def-company-backend! haskell-mode (cabal))))

(use-package company-ghc
  :init
  (after! company
    (def-company-backend! haskell-mode (ghc))))


(provide 'module-haskell)
;;; module-haskell.el ends here

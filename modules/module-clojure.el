;; -*- lexical-binding: t -*-
;;; module-clojure.el -- Clojure-specific configuration

;;
(use-package align-cljlet
  :init
  (add-hook! clojure-mode (lambda () (require 'align-cljlet)))
  (map! :map clojure-mode
        (:localleader
          :m "a" 'align-cljlet)))

;; Clojure IDE.
(use-package cider
  :init
  (setq cider-stacktrace-default-filters '(tooling dup)
        cider-repl-pop-to-buffer-on-connect nil
        cider-prompt-save-file-on-load nil
        cider-overlays-use-font-lock t
        cider-repl-history-file (expand-file-name "nrepl" polaris-temp-dir)
        cider-repl-use-pretty-printing t
        cider-repl-use-clojure-font-lock t
        nrepl-log-messages t
        cider-eval-result-prefix "→ ")

  (add-hook! clojure-mode 'cider-mode)
  (add-hook! cider-mode 'cider-turn-on-eldoc-mode)
  (add-hook! cider-repl-mode-hook 'smartparens-strict-mode)
  :config
  (push 'cider-stacktrace-mode evil-motion-state-modes)
  (push 'cider-popup-buffer-mode evil-motion-state-modes)
  )

(use-package clojure-mode
  :mode (("\\.clj\\|dtm\\|edn\\'" . clojure-mode)
         ("\\.cljc\\'" . clojurec-mode)
         ("\\.cljx\\'" . clojurex-mode)
         ("\\.cljs\\'" . clojurescript-mode)
         ("\\(?:build\\|profile\\)\\.boot\\'" . clojure-mode))
  :config
  (add-to-list 'flycheck-disabled-checkers 'clojure-cider-typed)
  (add-to-list 'flycheck-disabled-checkers 'clojure-cider-kibit))

;; YASnippet snippets.
(use-package clojure-snippets
  :defer t)

;; Clojure cheatsheet (C-c M-h).
;; See also: CIDER's "C-c C-d g" binding to look up symbol in Grimoire (online Clojure reference).
(use-package clojure-cheatsheet
  :defer t
  :init
  (with-eval-after-load 'clojure-mode
    (defun clojure-cheatsheet-or-error ()
      "Start `clojure-cheatsheet' if there's a nREPL connection (which it needs). Otherwise, error out."
      (interactive)
      (if (and (functionp 'nrepl-current-connection-buffer) ; Future-proofing in case the function name changes.
               (not (nrepl-current-connection-buffer t)))
          (error "An nREPL connection is needed!")
        (call-interactively #'clojure-cheatsheet)))
    (bind-key "C-c M-h" #'clojure-cheatsheet-or-error clojure-mode-map)))

;; Refactoring.
;; Prefix: C-c RET (overrides the binding for macroexpand).
(use-package clj-refactor
  :defer t
  :diminish clj-refactor-mode
  :init
  (add-hook! clojure-mode 'clj-refactor-mode)
  :config
  (cljr-add-keybindings-with-prefix "C-c C-m"))

(provide 'module-clojure)
;;; module-clojure.el ends here

;; -*- lexical-binding: t -*-
;;; module-apple.el

(use-package applescript-mode :mode "\\.applescript$")

;; TODO Set up emacs task runners for fruitstrap
(use-package swift-mode
  :mode "\\.swift$"
  :init
  (add-hook! swift-mode 'flycheck-mode)
  :config
  (after! flycheck (add-to-list 'flycheck-checkers 'swift))
  (after! company
    (require 'company-sourcekit)
    (def-company-backend! swift-mode (sourcekit yasnippet))))

;;
;; LaunchBar: https://www.obdev.at/products/launchbar
;;

(define-minor-mode lb6-mode
  "Launchbar development mode."
  :init-value nil
  :lighter    " lb6"
  :keymap (let ((map (make-sparse-keymap)))
            (map! :map map
                  (:localleader
                    :n "b" 'polaris-lb6-reload))
            map)
  (add-yas-minor-mode! 'lb6-mode))
(associate! lb6-mode :match "\\.lb\\(action\\|ext\\)/. $")

(defun polaris-lb6-reload ()
  (interactive)
  (let ((dir (f-traverse-upwards (lambda (f) (string-suffix-p ".lbaction" f)))))
    (shell-command (format "open '%s'" dir))))

(provide 'module-apple)
;;; module-apple.el ends here

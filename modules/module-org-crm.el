;; -*- lexical-binding: t -*-
;;; module-org-crm.el

(defun polaris|org-crm-init ()
  (define-org-section! cproject "Work")
  (define-org-section! project "Projects")
  (define-org-section! contact "Contacts")
  )

(defun polaris/org-crm-new (type name)
  )

(defun polaris/org-crm-visit-project (&optional name)
  )

(defun polaris/org-crm-visit-contact (&optional name)
  )

(defun polaris/org-crm-visit-invoice (&optional name)
  )

(provide 'module-org-crm)
;;; module-org-crm.el ends here

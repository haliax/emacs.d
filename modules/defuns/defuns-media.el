;;; defuns-media.el

;;;###autoload
(defun polaris/elfeed-show-link-title ()
  "Copy the current entry title and URL as org link to the clipboard."
  (interactive)
  (elfeed-link-title elfeed-show-entry))

;;;###autoload
(defun polaris/elfeed-show-quick-url-note ()
  "Fastest way to capture entry link to org agenda from elfeed show mode"
  (interactive)
  (elfeed-link-title elfeed-show-entry)
  (org-capture nil "n")
  (yank)
  (org-capture-finalize))

;;;###autoload
(defun polaris/elfeed-link-title (entry)
  "Copy the entry title and URL as org link to the clipboard."
  (interactive)
  (let* ((link (elfeed-entry-link entry))
         (title (elfeed-entry-title entry))
         (titlelink (concat "[[" link "][" title "]]")))
    (when titlelink
      (kill-new titlelink)
      (x-set-selection 'PRIMARY titlelink)
      (message "Yanked: %s" titlelink))))

;;;###autoload
(defun polaris/elfeed-search-link-title ()
  "Copy the current entry title and URL as org link to the clipboard."
  (interactive)
  (let ((entries (elfeed-search-selected)))
    (cl-loop for entry in entries
             when (elfeed-entry-link entry)
             do (elfeed-link-title entry))))

;;;###autoload
(defun polaris/elfeed-search-quick-url-note ()
  "In search mode, capture the title and link for the selected
     entry or entries in org aganda."
  (interactive)
  (let ((entries (elfeed-search-selected)))
    (cl-loop for entry in entries
             do (elfeed-untag entry 'unread)
             when (elfeed-entry-link entry)
             do (elfeed-link-title entry)
             do (org-capture nil "n")
             do (yank)
             do (org-capture-finalize)
             (mapc #'elfeed-search-update-entry entries))
    (unless (use-region-p) (forward-line))))

;;;###autoload
(defun polaris/no-fill-lui-setup ()
  (setq fringes-outside-margins t
        right-margin-width 7
        fill-column 80
        wrap-prefix "    ")
  (visual-line-mode)
  (setf (cdr (assoc 'continuation fringe-indicator-alist)) nil)
  (make-local-variable 'overflow-newline-into-fringe)
  (setq overflow-newline-into-fringe nil))

;;;###autoload
(defun polaris/retrieve-irc-password (_)
  (let ((network circe-server-network))
    (with-temp-buffer
      (insert-file-contents-literally polaris-credentials-file)
      (let ((plist (read (buffer-string))))
        (if (string= network "Rizon")
            (plist-get plist :rizon-password)
          (plist-get plist :freenode-password))))))

(provide 'defuns-media)
;;; defuns-media.el ends here

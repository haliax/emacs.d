;;; defuns-org-crm.el --- for my custom org-based CRM

(defun polaris--helm-org (&optional directory)
  (let ((helm-deft-dir-list `(,(or directory default-directory))))
    (helm-deft)))

;;;###autoload
(defun polaris/helm-org ()
  (interactive)
  (polaris--helm-org org-directory))

;;;###autoload
(defun polaris/helm-org-crm-projects ()
  (interactive)
  (polaris--helm-org org-directory-projects))

;;;###autoload
(defun polaris/helm-org-crm-contacts ()
  (interactive)
  (polaris--helm-org org-directory-contacts))

;;;###autoload
(defun polaris/helm-org-crm-invoices ()
  (interactive)
  (polaris--helm-org org-directory-invoices))

;;;###autoload
(defun polaris/helm-org-writing ()
  (interactive)
  (let ((polaris--helm-org-params '()))
    (polaris--helm-org (expand-file-name "writing/" org-directory))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun polaris--org-crm-assert-type (type)
  (unless (memq type '(project contact invoice))
    (user-error "Not a valid type: %s" type)))

(defun polaris--org-crm-new-path (name type)
  (polaris--org-crm-assert-type type)
  (let* ((prefix
          (replace-regexp-in-string
           "/+$" "" (symbol-value (intern (format "org-directory-%ss" type)))))
         (last-file (car-safe (sort (f-glob "*.org" prefix) 'org-string>))))
    (when last-file
      (let* ((old-id (polaris--org-crm-path-to-id last-file type))
             (new-id (format "%04X" (1+ old-id))))
        (if (eq type 'invoice)
            (format "%s/%s-%s.org" prefix (format-time-string "%y%m") new-id)
          (format "%s/%s-%s.org" prefix
                  new-id (replace-regexp-in-string "[][ !@#$%^&*()]" "-" name)))))))

(defun polaris--org-crm-path-to-id (path type)
  (polaris--org-crm-assert-type type)
  (let ((base (f-filename path)))
    (string-to-number
     (if (eq type 'invoice)
         (substring base (1+ (string-match-p "-" base)) (string-match-p ".org" base))
       (substring base 0 (string-match-p "[-.]" base)))
     16)))

(defun polaris--org-crm-id-to-path (id type)
  (polaris--org-crm-assert-type type)
  (let* ((prefix
          (replace-regexp-in-string
           "/+$" "" (symbol-value (intern (format "org-directory-%ss" type))))))
    (car-safe
     (f-glob (format (cond ((eq type 'invoice)
                            "*-%04X.org")
                           ((eq type 'courses)
                            "%s*.org")
                           (t
                            "%04X*.org"))
                     (string-to-number id 16))
             prefix))))

(defun polaris--org-crm (&optional id type new-p)
  (let ((file (if new-p
                  (or (polaris--org-crm-new-path id type)
                      (user-error "path could not be resolved: type(%s) name(%s)" type name))
                (or (polaris--org-crm-id-to-path id type)
                    (user-error "id %s could not be resolved in %s" id type))))
        (old-buffer (current-buffer)))
    (find-file file)
    (with-current-buffer old-buffer
      (when (evil-visual-state-p)
        (org-insert-link
         nil (format "%s:%s" (symbol-name type) (polaris--org-crm-path-to-id file type))
         (buffer-substring-no-properties (region-beginning) (region-end)))))))


;;;###autoload (autoload 'polaris:org-crm-project "defuns-org-crm" nil t)
(evil-define-command polaris:org-crm-project (&optional bang name)
  (interactive "<!><a>")
  (if bang
      (polaris--org-crm name 'project t)
    (polaris/helm-org-crm-projects)))
;;;###autoload (autoload 'polaris:org-crm-contact "defuns-org-crm" nil t)
(evil-define-command polaris:org-crm-contact (&optional bang name)
  (interactive "<!><a>")
  (if bang
      (polaris--org-crm name 'contact t)
    (polaris/helm-org-crm-contacts)))
;;;###autoload (autoload 'polaris:org-crm-invoice "defuns-org-crm" nil t)
(evil-define-command polaris:org-crm-invoice (&optional bang)
  (interactive "<!>")
  (if bang
      (polaris--org-crm nil 'invoice t)
    (polaris/helm-org-crm-invoices)))


(provide 'defuns-org-crm)
;;; defuns-org-crm.el ends here

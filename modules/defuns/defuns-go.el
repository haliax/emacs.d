;;; defuns-go.el

;; TODO Implement polaris:go-get-package
(defun polaris--go-get-package ())

;;;###autoload
(defun polaris:go-test-run-all ()
  (interactive)
  (async-shell-command (format "cd '%s' && go test" (polaris/project-root))))

;;;###autoload
(defun polaris:go-test-run-package ()
  (interactive)
  (error "Not yet implemented")
  (async-shell-command (format "cd '%s' && go test %s" (polaris/project-root) (polaris--go-get-package))))

(provide 'defuns-go)
;;; defuns-go.el ends here

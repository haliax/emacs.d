;;; defuns-markdown.el --- for module-markdown.el

;; Implement strike-through formatting
(defvar polaris--markdown-regex-del
  "\\(^\\|[^\\]\\)\\(\\(~\\{2\\}\\)\\([^ \n	\\]\\|[^ \n	]\\(?:.\\|\n[^\n]\\)*?[^\\ ]\\)\\(\\3\\)\\)")

;;;###autoload
(defun polaris/markdown-insert-del ()
  "Surround region in github strike-through delimiters."
  (interactive)
  (let ((delim "~~"))
    (if (markdown-use-region-p)
        ;; Active region
        (let ((bounds (markdown-unwrap-things-in-region
                       (region-beginning) (region-end)
                       polaris--markdown-regex-del 2 4)))
          (markdown-wrap-or-insert delim delim nil (car bounds) (cdr bounds)))
      ;; Bold markup removal, bold word at point, or empty markup insertion
      (if (thing-at-point-looking-at polaris--markdown-regex-del)
          (markdown-unwrap-thing-at-point nil 2 4)
        (markdown-wrap-or-insert delim delim 'word nil nil)))))

;;;###autoload
(defun polaris/markdown-preview ()
  (interactive)
  (require 'shr)
  (let* ((buf-this (buffer-name (current-buffer)))
         (buf-html (get-buffer-create
                    (format "*md-html (%s)*" buf-this))))
    (markdown-other-window (buffer-name buf-html))
    (shr-render-buffer buf-html)
    (eww-mode)
    (kill-buffer buf-html)))

(provide 'defuns-markdown)
;;; defuns-markdown.el ends here

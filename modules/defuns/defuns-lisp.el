;;; defuns-lisp.el

;;;###autoload
(defun polaris/elisp-find-function-at-pt ()
  (interactive)
  (let ((func (function-called-at-point)))
    (if func (find-function func))))

;;;###autoload
(defun polaris/elisp-find-function-at-pt-other-window ()
  (interactive)
  (let ((func (function-called-at-point)))
    (if func (find-function-other-window func))))

(defun polaris--ert-pre ()
  (save-buffer)
  (eval-buffer))

;;;###autoload
(defun polaris/ert-run-test ()
  (interactive)
  (let (case-fold-search)
    (polaris--ert-pre)
    (aif (thing-at-point 'defun t)
        (if (string-match "(ert-deftest \\([^ ]+\\)" it)
            (ert-run-tests-interactively (substring it (match-beginning 1) (match-end 1)))
          (user-error "Invalid test at point"))
      (user-error "No test found at point"))))

;;;###autoload
(defun polaris/ert-rerun-test ()
  (interactive)
  (let (case-fold-search)
    (polaris--ert-pre)
    (aif (car-safe ert--selector-history)
        (ert-run-tests-interactively it)
      (message "No test found in history, looking for test at point")
      (polaris/ert-run-test))))

;;;###autoload
(defun polaris/ert-run-all-tests ()
  (interactive)
  (ert-delete-all-tests)
  (polaris--ert-pre)
  (ert-run-tests-interactively t))

;;;###autoload
(defun polaris/elisp-auto-compile ()
  (when (let ((file-name (buffer-file-name)))
          (and (f-exists? (f-expand (concat (f-base file-name) ".elc") (f-dirname file-name)))
               (--any? (f-child-of? file-name it)
                       (append (list polaris-core-dir polaris-modules-dir
                                     polaris-core-dir polaris-modules-dir
                                     polaris-private-dir)))))
    (polaris:compile-el)))

;;;###autoload
(defun polaris/elisp-inf-ielm ()
  (ielm)
  (let ((buf (current-buffer)))
    (bury-buffer)
    (pop-to-buffer buf)))

(provide 'defuns-lisp)
;;; defuns-lisp.el ends here

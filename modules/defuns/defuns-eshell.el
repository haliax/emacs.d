;;; defuns-eshell.el ---

(defun polaris--eshell-in-prompt-p (&optional offset)
  (>= (- (point) (or offset 0)) (save-excursion (eshell-bol) (point))))

(defun polaris--eshell-current-git-branch ()
  (let ((branch (car (loop for match in (split-string (shell-command-to-string "git branch") "\n")
                           when (string-match "^\*" match)
                           collect match))))
    (if (not (eq branch nil))
        (concat " [" (substring branch 2) "]")
      "")))

;;;###autoload
(defun polaris/eshell-prompt ()
  (concat (propertize (abbreviate-file-name (eshell/pwd)) 'face 'eshell-prompt)
          (propertize (polaris--eshell-current-git-branch) 'face 'font-lock-function-name-face)
          (propertize " $ " 'face 'font-lock-constant-face)))

;;;###autoload
(defun polaris/eshell-evil-append ()
  (interactive)
  (goto-char (point-max))
  (call-interactively 'evil-append))

;;;###autoload
(defun polaris/eshell-evil-append-maybe ()
  (interactive)
  (if (polaris--eshell-in-prompt-p)
      (call-interactively 'evil-insert)
    (polaris/eshell-append)))

;;;###autoload
(defun polaris/eshell-evil-prepend ()
  (interactive)
  (eshell-bol)
  (call-interactively 'evil-insert))

;;;###autoload
(defun polaris/eshell-evil-prepend-maybe ()
  (interactive)
  (if (polaris--eshell-in-prompt-p)
      (call-interactively 'evil-insert)
    (polaris/eshell-prepend)))

;;;###autoload
(defun polaris/eshell-evil-replace-maybe ()
  (interactive)
  (if (polaris--eshell-in-prompt-p)
      (call-interactively 'evil-replace)
    (user-error "Cannot edit read-only region")))

;;;###autoload
(defun polaris/eshell-evil-replace-state-maybe ()
  (interactive)
  (if (polaris--eshell-in-prompt-p)
      (call-interactively 'evil-replace-state)
    (user-error "Cannot edit read-only region")))

(provide 'defuns-eshell)
;;; defuns-eshell.el ends here

;;; defuns-erc.el

(defun polaris/erc-server-buffer-name ()
  "Return a name of buffer with default server."
  (concat (erc-compute-server) ":"
          (number-to-string (erc-compute-port))))

(defun polaris/erc-server-buffer (&optional noerror)
  "Return the current ERC server buffer.
If NOERROR is non-nil, return nil instead of raising an error if
the server buffer does not exist."
  (or (erc-server-buffer)
      (get-buffer (polaris/erc-server-buffer-name))
      (unless noerror
        (error "No active ERC server buffer"))))

(defun polaris/erc-server-buffer-rename ()
  "Rename current server buffer (make a general name)."
  ;; Sometimes we need to modify names like "irc.freenode.net:7000<2>".
  (interactive)
  (let ((old-name (buffer-name))
        (new-name (polaris/erc-server-buffer-name)))
    (when (string-match (concat (erc-compute-server) ":.*")
                        old-name)
      (rename-buffer new-name)
      (message "Current buffer was renamed from '%s' to '%s'."
               old-name new-name))))

(defun polaris/erc-switch-to-server-buffer ()
  "Switch to ERC buffer with server."
  (interactive)
  (switch-to-buffer (polaris/erc-server-buffer)))

;;;###autoload
(defun polaris/erc-switch-buffer ()
  "Switch to ERC buffer, or start ERC if not already started."
  (interactive)
  (let ((bufs (mapcar #'buffer-name (erc-buffer-list))))
    (if bufs
        (switch-to-buffer (completing-read "ERC buffer: " bufs))
      (erc))))

;;;###autoload
(defun polaris/erc-track-switch-buffer (arg)
  "Same as `erc-track-switch-buffer', but start ERC if not already started."
  (interactive "p")
  (let ((buf (polaris/erc-server-buffer t)))
    (if buf
        (erc-track-switch-buffer arg)
      (erc))))

(defun polaris/erc-get-channel-buffer-list ()
  "Return a list of the ERC-channel-buffers."
  (erc-buffer-filter
   (lambda () (string-match "^#.*" (buffer-name (current-buffer))))))

;;;###autoload
(defun polaris/erc-cycle ()
  "Switch to ERC channel buffer, or run `erc-select'.
When called repeatedly, cycle through the buffers."
  (interactive)
  (let ((buffers (polaris/erc-get-channel-buffer-list)))
    (if buffers
        (progn (when (eq (current-buffer) (car buffers))
                 (bury-buffer)
                 (setq buffers (cdr buffers)))
               (and buffers
                    (switch-to-buffer (car buffers))))
      (call-interactively 'erc-select))))

(defvar polaris/erc-channel-list '("#emacs" "#erc" "#gnus")
  "A list of channels used in `polaris/erc-join-channel'.")

(defun polaris/erc-join-channel (channel &optional key)
  "Join CHANNEL.
Similar to `erc-join-channel', but use `polaris/erc-channel-list'."
  (interactive
   (list
    (let* ((cur-sexp (thing-at-point 'sexp))
           (chn (if (and cur-sexp
                         (eq 0 (string-match-p "#" cur-sexp)))
                    cur-sexp
                  "#")))
      (completing-read "Join channel: " polaris/erc-channel-list nil nil chn))
    (when (or current-prefix-arg erc-prompt-for-channel-key)
      (read-from-minibuffer "Channel key (RET for none): " nil))))
  (with-current-buffer (polaris/erc-server-buffer)
    (erc-cmd-JOIN channel (when (>= (length key) 1) key))))

(defun polaris/erc-quit-server (reason)
  "Disconnect from current server.
Similar to `erc-quit-server', but without prompting for REASON."
  (interactive (list ""))
  (with-current-buffer (polaris/erc-server-buffer)
    (erc-cmd-QUIT reason)))

(defun polaris/erc-ghost-maybe (server nick)
  "Send GHOST message to NickServ if NICK ends with `erc-nick-uniquifier'.
The function is suitable for `erc-after-connect'."
  (when (string-match (format "\\(.*?\\)%s+$" erc-nick-uniquifier) nick)
    (let ((nick-orig (match-string 1 nick))
          (password erc-session-password))
      (erc-message "PRIVMSG" (format "NickServ GHOST %s %s"
                                     nick-orig password))
      (erc-cmd-NICK nick-orig)
      (erc-message "PRIVMSG" (format "NickServ IDENTIFY %s %s"
                                     nick-orig password)))))

(defun polaris/erc-insert-timestamp (string)
  "Insert timestamps in the beginning of the line.

This function is suitable for `erc-insert-timestamp-function'.
It is a sort of combination of `erc-insert-timestamp-left' and
`erc-insert-timestamp-left-and-right'.  Usual
timestamps (`erc-timestamp-format') are inserted in the beginning
of each line and an additional
timestamp (`erc-timestamp-format-left') is inserted only if it
was changed since the last time (by default if the date was
changed)."
  (goto-char (point-min))
  (erc-put-text-property 0 (length string) 'field 'erc-timestamp string)
  (insert string)
  (let ((stamp (erc-format-timestamp (current-time)
                                     erc-timestamp-format-left)))
    (unless (string-equal stamp erc-timestamp-last-inserted-left)
      (goto-char (point-min))
      (erc-put-text-property 0 (length stamp) 'field 'erc-timestamp stamp)
      (insert stamp)
      (setq erc-timestamp-last-inserted-left stamp))))

;;; Away

(defvar polaris/erc-away-msg-list '("just away" "learning emacs" "sleeping")
  "A list of away messages for `polaris/erc-away'.")

(defun polaris/erc-away (&optional reason)
  "Mark the user as being away.
Interactively prompt for reason; with prefix mark as unaway.
Reasons are taken from `polaris/erc-away-msg-list'."
  (interactive
   (list (if current-prefix-arg
             ""
           (completing-read "Reason for AWAY: "
                            polaris/erc-away-msg-list))))
  (with-current-buffer (polaris/erc-server-buffer)
    (erc-cmd-AWAY (or reason ""))))

(defun polaris/erc-away-time ()
  "Return non-nil if the current ERC process is set away.
Similar to `erc-away-time', but no need to be in ERC buffer."
  (with-current-buffer (polaris/erc-server-buffer)
    (erc-away-time)))



(provide 'defuns-erc)
;;; defuns-erc.el ends here

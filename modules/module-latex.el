;; -*- lexical-binding: t -*-
;;; module-latex.el
;; Emacs tip: to type TeX symbols and have them converted to Unicode, press C-\ TeX RET (toggle with C-\).
;; For AUCTeX tips, see <http://tex.stackexchange.com/questions/20843/useful-shortcuts-or-key-bindings-or-predefined-commands-for-emacsauctex>.

(use-package tex
  :defer t
  :init
  (autoload #'LaTeX-math-mode "latex" nil t) ; For use in other modes.
  :config

  (require 'company-auctex)
  (def-company-backend! tex-mode (auctex math-symbols-latex))
  (def-company-backend! latex-mode (auctex math-symbols-latex))

  ;; Indentation: smart tabs.
  (with-eval-after-load 'smart-tabs-mode
    (smart-tabs-add-language-support LaTeX LaTeX-mode-hook
      ((LaTeX-indent-line . LaTeX-indent-level)))
    (smart-tabs-insinuate 'LaTeX)
    (add-hook 'LaTeX-mode-hook #'enable-indent-tabs-mode))

  (add-to-list 'TeX-command-list
               '("XeLaTeX" "%`xelatex -file-line-error -shell-escape%(mode)%' %t"
                 TeX-run-TeX nil t))
  (add-to-list 'TeX-command-list
               '("pdflatex" "%`pdflatex%(mode)%' %t" TeX-run-TeX nil t))
  (add-to-list 'TeX-command-list
               '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
                 :help "Run LaTeXMK on file"))

  ;; Indentation: other.
  (defvaralias 'LaTeX-left-right-indent-level 'LaTeX-indent-level) ; Indent \left and \right normally.
  (setq LaTeX-item-indent 0) ; Don't indent \item additionally (the `itemize' environment will already have its own indentation).
  (setq LaTeX-document-regexp nil)    ; Indent the `document' environment too.
  (setq LaTeX-syntactic-comments nil) ; Don't touch the inside of comments when indenting.

  (setq TeX-auto-save t
        TeX-parse-self t)

  ;; Use the XeTeX engine by default.
  (setq-default TeX-engine 'xetex)

  ;; Output to PDF by default.
  (setq-default TeX-PDF-mode t)

  ;; Auto-save before compiling.
  (setq TeX-save-query nil)


  ;; Don't ask for confirmation when deleting temporary files.
  (setq TeX-clean-confirm nil)

  ;; Use zathura for viewing PDF files.
  (add-to-list 'TeX-view-program-list
               '("zathura"
                 ("zathura" (mode-io-correlate "--page %(outpage)") " %o")))
  (add-to-list 'TeX-view-program-selection '(output-pdf "zathura"))

  ;; Make RET also indent.
  (with-eval-after-load 'evil
    (setq TeX-newline-function #'evil-ret-and-indent))

  ;; Use ";" as prefix for quickly entering math (toggle with C-c ~).
  ;; (The mode also displays a "Math" menu with many symbols -- can be used efficiently with Lacarte.)
  (add-hook 'LaTeX-mode-hook #'LaTeX-math-mode)
  (setq LaTeX-math-abbrev-prefix ";")

  ;; Electric braces after sub- and superscripts.
  (setq TeX-electric-sub-and-superscript t)

  ;; Quickly enter \frac.
  (with-eval-after-load 'latex          ; LaTeX mode is loaded after TeX mode.
    (bind-key "C-c /" (lambda () (interactive) (TeX-insert-macro "frac")) LaTeX-mode-map))

  ;; Improve LaTeX equations with font-lock
  (defface polaris|unimportant-latex-face
    '((t :height 0.7
         :inherit font-lock-comment-face))
    "Face used on less relevant math commands.")

  (font-lock-add-keywords
   'latex-mode
   `((,(rx (or (and "\\" (or (any ",.!;")
                             (and (or "left" "right"
                                      "big" "Big")
                                  symbol-end)))
               (any "_^")))
      0 'polaris|unimportant-latex-face prepend))
   'end)

  ;; Make LaTeX previews bigger.
  ;; (Activate previews in buffer with C-c C-p C-b, clear with C-c C-p C-c C-b).
  (defvar my-latex-preview-scale-factor 1.17
    "Factor by which the default LaTeX preview size (calculated from font size) should be multiplied.")
  (defun my-latex-preview-scale ()
    (* (funcall (preview-scale-from-face))
       my-latex-preview-scale-factor))
  (setq preview-scale-function #'my-latex-preview-scale))

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq bibtex-dialect 'biblatex)
(setq bibtex-align-at-equal-sign t)
(setq bibtex-text-indentation 20)
(add-hook! bibtex-mode
  (local-set-key (kbd "C-c \\") 'bibtex-fill-entry)
  (setq fill-column 140))
(add-hook! latex-mode 'turn-on-auto-fill)
(add-hook! LaTeX-mode 'turn-on-auto-fill)

(defvar biblio-directory (concat polaris-user-dir "docs/biblio/") "docstring")
(use-package reftex
  :diminish reftex-mode
  :config
  (add-hook 'latex-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (setq reftex-plug-into-AUCTeX t
        reftex-ref-style-default-list '("Cleveref" "Hyperref" "Fancyref")
        reftex-default-bibliography
        `(,(expand-file-name "phys.bib" biblio-directory))))

;;; Bibtex

;; NOTE: http://bibdesk.sourceforge.net/

(use-package helm-bibtex
  :defer t
  :config
  (setq helm-bibtex-bibliography
        `(,(expand-file-name "phys.bib" biblio-directory))

        helm-bibtex-library-path
        `(,(expand-file-name "phys-pdf" biblio-directory))

        helm-bibtex-notes-path (expand-file-name "notes" biblio-directory)
        helm-bibtex-notes-extension ".org"

        helm-bibtex-pdf-open-function
        (lambda (fpath) (async-start-process "open-pdf" "/usr/bin/open" nil fpath))))

(require 'smartparens-latex)

(use-package magic-latex-buffer
  :init
  (add-hook! latex-mode 'magic-latex-buffer))



(provide 'module-latex)
;;; module-latex.el ends here

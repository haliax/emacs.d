;;; module-processing.el

(use-package processing-mode
  :when IS-LINUX
  :commands (processing-mode processing-find-sketch)
  :mode "\\.pde$"
  :init
  (add-hook 'processing-compilation-mode-hook 'polaris|hide-mode-line)
  :config
  (def-builder! processing-mode processing-sketch-build)
  (setq processing-location (executable-find "processing-java")
        processing-application-dir "/usr/share/processing"
        processing-sketchbook-dir "~/usr/work/pde"
        processing-output-dir "/tmp")

  (after! quickrun
    (quickrun-add-command
     "processing" `((:command . ,processing-location)
                    (:exec . (lambda () (format "--sketch=%s --output=%s --force --run"
                                           (polaris/project-root) processing-output-dir)))
                    (:description . "Run Processing sketch"))
     :mode 'processing-mode))

  (add-hook! processing-mode
    (setq-local company-backends '((company-keywords
                                    :with
                                    company-yasnippet
                                    company-dabbrev-code)))
    (make-local-variable 'company-keywords-alist)
    (add-to-list 'company-keywords-alist
                 (cons 'processing-mode (append processing-functions
                                                processing-builtins
                                                processing-constants)))))

(provide 'module-processing)
;;; module-processing.el ends here

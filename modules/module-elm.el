;; -*- lexical-binding: t -*-
;;; module-elm.el

(use-package elm-mode
  :mode ("\\.elm\\'" . elm-mode)
  :init
  (add-hook 'elm-mode-hook #'elm-oracle-setup-completion)
  (def-company-backend! elm-mode (elm)))

(provide 'module-elm)
;;; module-elm.el ends here

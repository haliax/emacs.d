;; -*- lexical-binding: t -*-
;;; module-mail.el

(use-package smtpmail-async
  :config
  (setq
   send-mail-function 'async-smtpmail-send-it
   message-send-mail-function 'async-smtpmail-send-it))

(use-package mu4e
  :if (executable-find "mu")
  :commands (mu4e mu4e-compose-new)
  :config
  (progn
    (use-package mu4e-contrib)
    (defun email ()
      "Open a perspective for Email."
      (interactive)
      (persp-switch "mail")
      (mu4e))

    (setq mail-user-agent #'mu4e-user-agent
          mu4e-mu-binary (executable-find "mu")
          mu4e-maildir polaris-mail-dir
          mu4e-attachment-dir (expand-file-name "tmp/down/" polaris-user-dir)
          mu4e-use-fancy-chars            t
          mu4e-get-mail-command           "mbsync -a &>/dev/null" ; isync to fetch email
          mu4e-update-interval            (* 5 60) ; update every 5 min
          mu4e-view-show-images           t
          mu4e-view-show-addresses        t
          mu4e-view-prefer-html           t
          mu4e-headers-skip-duplicates        t
          message-kill-buffer-on-exit         t
          mu4e-hide-index-messages            t
          mu4e-confirm-quit                   nil ; just quit
          mu4e-compose-signature-auto-include t
          message-signature t
          mu4e-compose-signature (concat
                                  "Saludos,\n"
                                  "Axel Parra\n")
          ;; convert html emails properly
          ;; Possible options:
          ;;   - html2text -utf8 -width 72
          ;;   - textutil -stdin -format html -convert txt -stdout
          ;;   - html2markdown | grep -v '&nbsp_place_holder;' (Requires html2text pypi)
          ;;   - w3m -dump -cols 80 -T text/html
          ;;   - view in browser (provided below)
          mu4e-html2text-command 'mu4e-shr2text
          mu4e-auto-retrieve-keys t
          mu4e-compose-dont-reply-to-self t
          mu4e-compose-complete-only-personal t

          mu4e-user-mail-address-list '("apc@openmailbox.org"
                                        "axel.parra.tv@gmail.com")

          ;; to list a lot of mails, more than the default 500
          ;; is reasonable fast, so why not?
          mu4e-headers-results-limit 750

          ;; more cool and practical than the default
          mu4e-headers-from-or-to-prefix '("" . "➜ ")

          mu4e-headers-date-format "%a %b %d %H:%M"
          mu4e-headers-skip-duplicates t
          mu4e-headers-leave-behavior 'apply

          message-send-mail-function #'smtpmail-send-it
          mu4e-compose-signature user-full-name
          mu4e-compose-signature-auto-include t

          mu4e-view-fields '(:from
                             :to
                             :cc
                             :bcc
                             :subject
                             :flags
                             :date
                             :maildir
                             :mailing-list
                             :tags
                             :attachments
                             :signature)

          mu4e-msg2pdf (executable-find "msg2pdf")
          mu4e-view-image-max-width 800
          mu4e-decryption-policy t
          mu4e-headers-visible-flags '(unread read seen draft flagged passed replied trashed attach encrypted signed)
          smtpmail-default-smtp-server "smtp.openmailbox.org"
          smtpmail-smtp-service 587
          smtpmail-stream-type 'starttls)

    (evilified-state-evilify-map mu4e-main-mode-map
      :mode mu4e-main-mode
      :bindings
      (kbd "j") 'mu4e~headers-jump-to-maildir)

    (evilified-state-evilify-map
      mu4e-headers-mode-map
      :mode mu4e-headers-mode
      :bindings
      (kbd "C-j") 'mu4e-headers-next
      (kbd "C-k") 'mu4e-headers-prev
      (kbd "J") (lambda ()
                  (interactive)
                  (mu4e-headers-mark-thread nil '(read))))

    (evilified-state-evilify-map
      mu4e-view-mode-map
      :mode mu4e-view-mode
      :bindings
      (kbd "C-j") 'mu4e-view-headers-next
      (kbd "C-k") 'mu4e-view-headers-prev
      (kbd "J") (lambda ()
                  (interactive)
                  (mu4e-view-mark-thread '(read))))

    (setq mu4e-completing-read-function 'completing-read)

    (defun mu4e-action-view-in-browser-background (msg)
      (let* ((html (mu4e-message-field msg :body-html))
             (txt (mu4e-message-field msg :body-txt))
             (tmpfile (format "%s%x.html" temporary-file-directory (random t))))
        (unless (or html txt)
          (mu4e-error "No body part for this message"))
        (with-temp-buffer
          (insert (or html (concat "<pre>" txt "</pre>")))
          (write-file tmpfile)
          (osx-browse-url (concat "file://" tmpfile) nil nil 'background))))
    (defun mu4e-view-goto-bottom ()
      (interactive)
      (mu4e~view-quit-buffer)
      (end-of-buffer)
      (previous-line)
      (mu4e-headers-view-message))
    (defun mu4e-view-goto-top ()
      (interactive)
      (mu4e~view-quit-buffer)
      (beginning-of-buffer)
      (mu4e-headers-view-message))

    (when (fboundp 'imagemagick-register-types)
      (imagemagick-register-types))

    (add-hook 'mu4e-compose-mode-hook 'epa-mail-mode)
    (add-hook 'mu4e-view-mode-hook 'epa-mail-mode)

    (setq mu4e-contexts
          `(,(make-mu4e-context
              :name "personal"
              :enter-func (lambda () (mu4e-message "Personal"))
              ;; no leave-func
              :match-func (lambda (msg)
                            (and msg (string-match "/openmailbox.org/.*" (mu4e-message-field msg :maildir))))
              :vars '((user-mail-address . "apc@openmailbox.org")
                      ;; mu4e
                      (mu4e-sent-folder   . "/personal/Sent")
                      (mu4e-trash-folder  . "/personal/Trash")
                      (mu4e-drafts-folder . "/personal/Drafts")
                      ;; smtp
                      (smtpmail-smtp-server . "smtp.openmailbox.org")
                      (smtpmail-smtp-user . "apc@openmailbox.org")))
            ,(make-mu4e-context
              :name "work"
              :enter-func (lambda () (mu4e-message "Work"))
              ;; no leave-func
              :match-func (lambda (msg)
                            (and msg (string-match "/gmail.com/.*" (mu4e-message-field msg :maildir))))
              :vars
              '((user-mail-address . "axel.parra.tv@gmail.com")
                ;; mu4e
                (mu4e-drafts-folder . "/gmail.com/[Gmail].Drafts")
                (mu4e-sent-folder   . "/gmail.com/[Gmail].Sent Mail")
                (mu4e-trash-folder  . "/gmail.com/[Gmail].Trash")
                (mu4e-refile-folder  . "/gmail.com/[Gmail].All Mail")
                (mu4e-sent-messages-behavior   . delete)
                ;; smtp
                (smtpmail-starttls-credentials . '(("smtp.gmail.com" 587 nil nil)))
                (smtpmail-default-smtp-server  . "smtp.gmail.com")
                (smtpmail-smtp-server          . "smtp.gmail.com")
                (smtpmail-smtp-service . 587)))))

    ;; decorate mu main view
    (defun polaris/mu4e-main-mode-font-lock-rules ()
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward "\\[\\([a-zA-Z]\\{1,2\\}\\)\\]" nil t)
          (add-text-properties (match-beginning 1) (match-end 1)
                               '(face font-lock-variable-name-face)))))
    (add-hook 'mu4e-main-mode-hook 'polaris/mu4e-main-mode-font-lock-rules)

    ;; columns to show
    (setq mu4e-headers-fields
          '(
            (:human-date . 9)
            (:flags . 6)
            (:mailing-list . 10)
            (:size . 6)
            (:from-or-to . 22)
            (:subject)))

    (setq mu4e-view-actions
          '(
            ;;open an HTML message in the browser
            ("browse mail" . mu4e-action-view-in-browser)
            ;; open pdf
            ("pdf mail" . mu4e-action-view-as-pdf)
            ;; capture message
            ("capture message" . mu4e-action-capture-message)
            ;; retag messages
            ("retag mail" . mu4e-action-retag-message)
            ))

    ;;Search for messages sent by the sender of the message at point
    (defun polaris/search-for-sender (msg)
      (mu4e-headers-search
       (concat "from:" (cdar (mu4e-message-field msg :from)))))

    ;; define 'x' as the shortcut
    (add-to-list 'mu4e-view-actions
                 '("xsearch for sender" . polaris/search-for-sender) t)

    ;; integration with org-contacts
    ;; (setq mu4e-org-contacts-file (expand-file-name "contacts.org" org-directory-contacts))

    (add-to-list 'mu4e-headers-actions
                 '("org-contact-add" . mu4e-action-add-org-contact) t)
    (add-to-list 'mu4e-view-actions
                 '("org-contact-add" . mu4e-action-add-org-contact) t)

    ;; get a pgp key from a message
    ;; from  http://hugoduncan.org/post/snarf-pgp-keys-in-emacs-mu4e/
    (defun polaris/mu4e-view-snarf-pgp-key (&optional msg)
      "get the pgp key for the specified message."
      (interactive)
      (let* ((msg (or msg (mu4e-message-at-point)))
             (path (mu4e-message-field msg :path))
             (cmd (format "%s verify --verbose %s"
                          mu4e-mu-binary
                          (shell-quote-argument path)))
             (output (shell-command-to-string cmd)))
        (let ((case-fold-search nil))
          (when (string-match "key:\\([A-F0-9]+\\)" output)
            (let* ((cmd (format "%s --recv %s"
                                epg-gpg-program (match-string 1 output)))
                   (output (shell-command-to-string cmd)))
              (message output))))))

    (add-to-list 'mu4e-view-actions
                 '("get PGP keys" . polaris/mu4e-view-snarf-pgp-key) t)


    (setq mu4e-bookmarks
          '(("flag:flagged" "Flagged" ?f)
            ("date:48h..now" "Last 2 days" ?l)
            ("date:1h..now" "Last hour" ?h)
            ("flag:attach" "With attachments" ?a)
            ("mime:application/pdf" "With documents" ?d)
            ("size:3M..500M" "Big messages" ?b)
            ("flag:unread AND NOT flag:trashed" "Unread" ?u)
            ("maildir:\"/personal/Inbox\" OR maildir:\"/work/Inbox\"" "All" ?a)))

    ;; Folder shortcuts
    (setq mu4e-maildir-shortcuts
          '(("/personal/Inbox" . ?p)
            ("/work/Inbox" . ?w)))
    ;; simple regexp used to check the message. Tweak to your own need.
    (defvar polaris-message-attachment-regexp "\\(adjunto\\|attach\\)")
    ;; the function that checks the message
    (defun polaris/message-check-attachment nil
      "Check if there is an attachment in the message if I claim it."
      (save-excursion
        (message-goto-body)
        (when (search-forward-regexp polaris-message-attachment-regexp nil t nil)
          (message-goto-body)
          (unless (or (search-forward "<#part" nil t nil)
                      (message-y-or-n-p
                       "No attachment. Send the message ?" nil nil))
            (error "No message sent")))))
    ;; check is done just before sending the message
    (add-hook 'message-send-hook 'polaris/message-check-attachment)

    (defun polaris/mu4e-headers-search-in-new-frame
        (&optional expr prompt edit ignore-history)
      "Execute `mu4e-headers-search' in a new frame."
      (interactive)
      (select-frame (make-frame))
      (mu4e-headers-search expr prompt edit ignore-history))

    ))

(use-package gnus-dired
  :config
  (progn
    ;; make the `gnus-dired-mail-buffers' function also work on
    ;; message-mode derived modes, such as mu4e-compose-mode
    (defun gnus-dired-mail-buffers ()
      "Return a list of active message buffers."
      (let (buffers)
        (save-current-buffer
          (dolist (buffer (buffer-list t))
            (set-buffer buffer)
            (when (and (derived-mode-p 'message-mode)
                       (null message-sent-message-via))
              (push (buffer-name buffer) buffers))))
        (nreverse buffers)))

    (setq gnus-dired-mail-mode 'mu4e-user-agent)
    (add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)))

(use-package mu4e-alert
  :defer t
  :init
  (with-eval-after-load 'mu4e
    (mu4e-alert-enable-mode-line-display)))

(use-package mu4e-maildirs-extension
  :defer t
  :init
  (progn
    (with-eval-after-load 'mu4e (mu4e-maildirs-extension-load))
    (setq mu4e-maildirs-extension-maildir-separator    "*"
          mu4e-maildirs-extension-submaildir-separator "✉"
          mu4e-maildirs-extension-action-text          nil)))

(provide 'module-mail)
;;; module-mail.el ends here

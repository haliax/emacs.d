;; -*- lexical-binding: t -*-
;;; module-elixir.el

(use-package alchemist
  :mode (("\\.elixir\\'" . elixir-mode)
         ("\\.ex\\'"     . elixir-mode)
         ("\\.exs\\'"    . elixir-mode))
  :config
  (add-hook! elixir-mode 'alchemist-mode)
  (setq alchemist-hooks-test-on-save t))

(provide 'module-elixir)
;;; module-elixir.el ends here

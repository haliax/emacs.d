;; -*- lexical-binding: t -*-
;;; module-nix.el

(use-package nix-mode
  :config
  (after! company
    (require 'company-nixos-options)
    (def-company-backend! nix-mode (nixos-options))))

(use-package nixos-options)

(use-package helm-nixos-options
  :init
  (map! :map nix-mode-map
        :m "gd" 'helm-nixos-options))

(provide 'module-nix)
;;; module-nix.el ends here

;; -*- lexical-binding: t -*-
;;; module-org-notebook.el

;; This transforms Emacs org-mode into a notebook application with:
;;     Custom links for class notes
;;     Shortcuts for searching org files
;;     Shortcuts for creating new notes (there's org-capture, but this is suited to my
;;     workflow).
;;     A simpler attachment system with support for drag-and-drop attachments into org.
;;     A simpler, pandoc-powered, export system.

(add-hook 'org-load-hook 'polaris|org-notebook-init t)
(add-hook 'org-load-hook 'polaris|org-attach-init t)
(add-hook 'org-load-hook 'polaris|org-export-init t)

(defvar polaris-org-export-directory (concat org-directory ".export"))
(defvar polaris-org-quicknote-dir    (concat org-directory "Inbox/"))
(defvar org-attach-directory      ".attach/")

;; Tell helm to ignore these directories
;; (after! helm
;;   (mapc (lambda (r) (add-to-list 'helm-boring-file-regexp-list r))
;;         (list "\\.attach$" "\\.export$")))

;;
(defun polaris|org-notebook-init ()
  (define-org-section! course "Classes"
    (lambda (path) (substring path 0 (s-index-of " " path))) "%s*.org")

  (exmap "ocl[ass]" 'polaris:org-search-course))


;;
(defun polaris|org-attach-init ()
  ;; Drag-and-drop support
  (require 'org-download)
  (setq-default org-download-image-dir org-attach-directory
                org-download-heading-lvl nil
                org-download-timestamp "_%Y%m%d_%H%M%S")

  ;; Write download paths relative to current file
  (defun org-download--dir-2 () nil)
  (defun polaris*org-download--fullname (path)
    (f-relative path (f-dirname (buffer-file-name))))
  (advice-add 'org-download--fullname :filter-return 'polaris*org-download--fullname)

  ;; Add another drag-and-drop handler that will handle anything but image files
  (setq dnd-protocol-alist `(("^\\(https?\\|ftp\\|file\\|nfs\\):\\(//\\)?" . polaris/org-download-dnd)
                             ,@dnd-protocol-alist))

  ;; ...auto-delete attachments once all references to it have been deleted.
  (add-hook 'org-mode-hook 'polaris|org-attach-track-init)
  (defun polaris|org-attach-track-init ()
    (setq polaris-org-attachments-list (polaris/org-attachments))
    (add-hook 'after-save-hook 'polaris/org-cleanup-attachments nil t)))


;;
(defun polaris|org-export-init ()
  (setq org-export-backends '(ascii html latex md)
        org-export-with-sub-superscripts '{}
        org-export-with-smart-quotes t
        org-export-with-toc t
        org-export-with-author t
        org-export-headline-levels 4)

  (use-package ox-pandoc)
  (setq org-pandoc-options '((standalone . t) (mathjax . t) (parse-raw . t)))

  ;; Export to a central directory (why isn't this easier?)
  (unless (file-directory-p polaris-org-export-directory)
    (mkdir polaris-org-export-directory))
  (defun polaris*org-export-output-file-name (args)
    (unless (nth 2 args)
      (setq args (append args (list polaris-org-export-directory))))
    args)
  (advice-add 'org-export-output-file-name :filter-args 'polaris*org-export-output-file-name))


;;
;; (defvar polaris-org-tags '())
;; (defun polaris|org-tag-init ()
;;   (async-start
;;    `(lambda ()
;;       (let ((default-directory (polaris/project-root))
;;             (data (s-trim (shell-command-to-string "ag --nocolor --nonumbers '^#\\ TAGS:'")))
;;             (alist '()))
;;         (unless (zerop (length data))
;;           (mapc (lambda (l)
;;                   (let* ((parts (s-split ":" l))
;;                          (file (car parts))
;;                          (tags (s-trim (nth 2 parts))))
;;                     (mapc (lambda (tag)
;;                             (setq tag (substring tag 1))
;;                             (unless (assoc tag alist)
;;                               (push (cons tag (list)) alist))
;;                             (push file (cdr (assoc tag alist))))
;;                           (s-split " " tags))))
;;                 (s-lines data))
;;           alist)))
;;    (lambda (_)
;;      (load (concat php-extras-eldoc-functions-file ".el"))
;;      (message "PHP eldoc updated!")))
;;   )




(provide 'module-org-notebook)
;;; module-org-notebook.el ends here

;; -*- lexical-binding: t -*-
;;; module-org.el

(use-package org
  :mode ("\\.org$" . org-mode)
  :config
  (progn

    ;; Org variables

    (setq-default
     org-directory (expand-file-name "notes/" polaris-user-dir)
     org-default-notes-file (expand-file-name "notes.org" org-directory)
     org-export-coding-system 'utf-8

     ;; ---------- ;;
     ;; Appearance ;;
     ;; ---------- ;;

     org-indent-mode-turns-on-hiding-stars t
     org-adapt-indentation t
     org-blank-before-new-entry '((heading . nil)
                                  (plain-list-item . auto))
     org-cycle-separator-lines 1
     org-cycle-include-plain-lists t
     org-ellipsis " ▼"
     org-entities-user '(("flat" "\\flat" nil "" "" "266D" "♭")
                         ("sharp" "\\sharp" nil "" "" "266F" "♯"))
     org-fontify-done-headline t
     org-fontify-whole-heading-line t
     org-fontify-quote-and-verse-blocks t
     org-footnote-auto-adjust t
     org-hide-emphasis-markers t
     org-hide-leading-stars t
     org-indent-indentation-per-level 1
     org-pretty-entities t
     org-pretty-entities-include-sub-superscripts t
     org-startup-folded t
     org-startup-indented t             ; http://orgmode.org/manual/Clean-view.html
     org-startup-with-inline-images t
     org-tags-column 0
     org-enforce-todo-dependencies t
     org-use-sub-superscripts '{}

     ;; -------- ;;
     ;; Behavior ;;
     ;; -------- ;;

     org-catch-invisible-edits 'smart
     org-hidden-keywords '(title)
     org-log-done 'timestamp
     org-refile-use-outline-path t
     org-special-ctrl-a/e t

     ;; ---------------- ;;
     ;; Sorting/refiling ;;
     ;; ---------------- ;;

     org-archive-location (concat org-directory "/Archived/%s::")
     org-refile-targets '((nil . (:maxlevel . 2))) ; display full path in refile completion

     ;; ------ ;;
     ;; Agenda ;;
     ;; ------ ;;

     org-agenda-files (f-entries org-directory (lambda (path) (f-ext? path ".org")))
     org-agenda-restore-windows-after-quit nil
     org-agenda-skip-unavailable-files nil
     org-agenda-skip-comment-trees nil
     org-agenda-skip-function nil
     org-agenda-dim-blocked-tasks nil
     org-agenda-window-setup 'other-frame ; to get org-agenda to behave with shackle...
     org-agenda-inhibit-startup t
     org-agenda-skip-unavailable-files t
     org-todo-keywords '((sequence "TODO(t)" "|" "DONE(d)" "MOVE(m@)")
                         (sequence "[ ](t)" "[-](p)" "|" "[X](d)")
                         (sequence "IDEA(i)" "NEXT(n)" "ACTIVE(a)" "WAITING(w)" "LATER(l)" "HOLD(h@/!)" "|" "CANCELLED(c)" "STOP(s@/!)")
                         (sequence "UNSENT(u)" "UNPAID(U)" "|" "PAID(p)"))

     ;; ------;;
     ;; Babel ;;
     ;; ----- ;;

     org-confirm-babel-evaluate nil   ; you don't need my permission
     org-src-fontify-natively t       ; make code pretty
     org-src-window-setup 'current-window
     org-src-preserve-indentation t
     org-edit-src-content-indentation 0
     org-src-tab-acts-natively t

     org-tag-alist '(("@home" . ?h)
                     ("@school" . ?s)
                     ("@errand" . ?e)
                     ("@daily" . ?d)
                     ("@projects" . ?r))

     org-capture-templates
     '(("c" "Changelog" entry
        (file+headline (concat (polaris/project-root) "CHANGELOG.org") "Unreleased")
        "* %?")

       ("p" "process-soon" entry (file+headline org-default-todo-file "Todo")
        "* TODO %a %?\nDEADLINE: %(org-insert-time-stamp (org-read-date nil t \"+2d\"))")

       ("t" "todo" entry (file+headline org-default-todo-file "Tasks")
        "* TODO [#A] %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n%a\n")

       ("e" "EVENT (Email; Scheduled)" entry (file+olp org-default-events-file "Events")
        "** EVENT %:subject%?\n  %^T\n  From: %:fromname\n  Subject: %a\n  %i\n"
        :prepend t)

       ("j" "Journal" entry
        (file+datetree (expand-file-name "journal.org" org-directory))
        "** %<%H:%M>: %?\n%i" :prepend t)

       ("c" "Contacts" entry
        (file+headline (expand-file-name "contacts.org" org-directory))
        "* %?")

       ("n" "Notes" entry
        (file+headline org-default-todo-file "Inbox")
        "* %u %?\n%i" :prepend t)

       ("v" "Vocab" entry
        (file+headline (concat org-directory "topics/vocab.org") "Unsorted")
        "** %i%?\n")

       ("x" "Excerpt" entry
        (file+headline (concat org-directory "topics/excerpts.org") "Excerpts")
        "** %u %?\n%i" :prepend t)

       ("q" "Quote" item
        (file+headline (concat org-directory "topics/excerpts.org") "Quotes")
        "+ %i\n  *Source: ...*\n  : @tags" :prepend t)

       ("b" "Blog Feed" entry
        (file+headline (concat user-emacs-directory "elfeed.org") "Blogroll")
        "* %?"))
     )

    ;; ----- ;;
    ;; Babel ;;
    ;; ----- ;;

    (setq org-babel-clojure-backend 'cider
          org-confirm-babel-evaluate nil
          org-babel-default-header-args:R
          '((:session . "*R*")
            (:results . "output")
            (:exports . "both"))
          org-babel-default-header-args:python
          '((:session . "*python*")
            (:results . "output")
            (:exports . "both"))
          org-babel-default-header-args:ipython
          '((:session . "*ipython*")
            (:results . "output")
            (:exports . "both"))
          org-babel-default-header-args:julia
          '((:session . "*julia*")
            (:results . "output")
            (:exports . "both"))
          inferior-julia-program-name "julia")

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((python . t) (ruby . t) (sh . t) (js . t) (css . t) (julia . t)
       (ditaa . t) (plantuml . t) (emacs-lisp . t) (matlab . t) ;; (R . t)
       (latex . t) (calc . t) (lisp . t) (clojure . t) (C . t) (sql . t)
       (http . t) (haskell . t) (rust . t) (go . t) (dot . t)))

    ;; Change the default app for opening pdf files from org
    ;; http://stackoverflow.com/a/9116029/1219634
    (add-to-list 'org-src-lang-modes '("systemverilog" . verilog))
    (add-to-list 'org-src-lang-modes '("dot"           . graphviz-dot))

    ;; Change .pdf association directly within the alist
    (setcdr (assoc "\\.pdf\\'" org-file-apps) "acroread %s")

    (let ((ext-regexp (regexp-opt '("GIF" "JPG" "JPEG" "SVG" "TIF" "TIFF" "BMP" "XPM"
                                    "gif" "jpg" "jpeg" "svg" "tif" "tiff" "bmp" "xpm"))))
      (setq iimage-mode-image-regex-alist
            `((,(concat "\\(`?file://\\|\\[\\[\\|<\\|`\\)?\\([-+./_0-9a-zA-Z]+\\."
                        ext-regexp "\\)\\(\\]\\]\\|>\\|'\\)?") . 2)
              (,(concat "<\\(http://.+\\." ext-regexp "\\)>") . 1))))

    (add-to-list 'org-link-frame-setup '(file . find-file))

    (defun nadvice/org-babel-execute-src-block (old-fun &rest args)
      (let ((language (org-element-property :language (org-element-at-point))))
        (unless (cdr (assoc (intern language) org-babel-load-languages))
          (add-to-list 'org-babel-load-languages (cons (intern language) t))
          (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))
        (apply old-fun args)))

    (advice-add 'org-babel-execute-src-block :around
                #'nadvice/org-babel-execute-src-block)

    ;; ------------ ;;
    ;; Custom faces ;;
    ;; ------------ ;;
    (defface org-list-bullet '((t ())) "Face for list bullets")
    (defvar polaris-org-font-lock-keywords
      `(("^ *\\([-+]\\|[0-9]+[).]\\) "
         (1 'org-list-bullet))
        ("^ *\\(-----+\\)$"
         (1 'org-meta-line))))
    (font-lock-add-keywords 'org-mode polaris-org-font-lock-keywords)

    ;; ------ ;;
    ;; Ispell ;;
    ;; ------ ;;

    ;; Configure `ispell-skip-region-alist' for `org-mode'.
    (make-local-variable 'ispell-skip-region-alist)
    (add-to-list 'ispell-skip-region-alist '(org-property-drawer-re))
    (add-to-list 'ispell-skip-region-alist '("~" "~"))
    (add-to-list 'ispell-skip-region-alist '("=" "="))
    (add-to-list 'ispell-skip-region-alist '("^#\\+BEGIN_SRC" . "^#\\+END_SRC"))

    ;; ---------- ;;
    ;; Encryption ;;
    ;; ---------- ;;

    (require 'epa-file)
    (epa-file-enable)
    (require 'org-crypt)
    (org-crypt-use-before-save-magic)
    (setq org-tags-exclude-from-inheritance '("crypt")
          org-crypt-key user-mail-address
          epa-file-encrypt-to user-mail-address)

    ;; ------------- ;;
    ;; Abbreviations ;;
    ;; ------------- ;;
    (add-hook 'org-mode-hook (lambda () (abbrev-mode 1)))

    ;; ------ ;;
    ;; Images ;;
    ;; ------ ;;
    (add-hook 'org-babel-after-execute-hook #'org-display-inline-images)

    ;; ---------- ;;
    ;; Attachment ;;
    ;; ---------- ;;

    ;; Don't track attachments
    ;; (push (format "/%s.+$" (regexp-quote org-attach-directory)) recentf-exclude)
    ;; Don't clobber recentf with agenda files
    ;; (defun org-is-agenda-file (filename)
    ;; (find (file-truename filename) org-agenda-files :key 'file-truename
    ;; :test 'equal))
    ;; (pushnew 'org-is-agenda-file recentf-exclude)

    ;; ------------- ;;
    ;; Agenda defuns ;;
    ;; ------------- ;;

    ;; http://sachachua.com/blog/2013/01/emacs-org-task-related-keyboard-shortcuts-agenda/
    (defun sacha/org-agenda-done (&optional arg)
      "Mark current TODO as done.
        This changes the line at point, all other lines in the agenda referring to
        the same tree node, and the headline of the tree node in the Org-mode file."
      (interactive "P")
      (org-agenda-todo "DONE"))

    (defun sacha/org-agenda-mark-done-and-add-followup ()
      "Mark the current TODO as done and add another task after it.
        Creates it at the same level as the previous task, so it's better to use
        this with to-do items than with projects or headings."
      (interactive)
      (org-agenda-todo "DONE")
      (org-agenda-switch-to)
      (org-capture 0 "t")
      (org-metadown 1)
      (org-metaright 1))

    (defun sacha/org-agenda-new ()
      "Create a new note or task at the current agenda item.
        Creates it at the same level as the previous task, so it's better to use
        this with to-do items than with projects or headings."
      (interactive)
      (org-agenda-switch-to)
      (org-capture 0))

    (add-hook 'org-agenda-mode-hook
              (lambda ()
                (bind-keys
                 :map org-agenda-mode-map
                 ("x" . sacha/org-agenda-done)
                 ("X" . sacha/org-agenda-mark-done-and-add-followup)
                 ("N" . sacha/org-agenda-new))))

    (define-key org-agenda-mode-map
      "v" 'hydra-org-agenda-view/body)

    (defun org-agenda-cts ()
      (let ((args (get-text-property
                   (min (1- (point-max)) (point))
                   'org-last-args)))
        (nth 2 args)))

    (defhydra hydra-org-agenda-view (:hint nil)
      "
_d_: ?d? day        _g_: time grid=?g? _a_: arch-trees
_w_: ?w? week       _[_: inactive      _A_: arch-files
_t_: ?t? fortnight  _f_: follow=?f?    _r_: report=?r?
_m_: ?m? month      _e_: entry =?e?    _D_: diary=?D?
_y_: ?y? year       _q_: quit          _L__l__c_: ?l?"
      ("SPC" org-agenda-reset-view)
      ("d" org-agenda-day-view
       (if (eq 'day (org-agenda-cts))
           "[x]" "[ ]"))
      ("w" org-agenda-week-view
       (if (eq 'week (org-agenda-cts))
           "[x]" "[ ]"))
      ("t" org-agenda-fortnight-view
       (if (eq 'fortnight (org-agenda-cts))
           "[x]" "[ ]"))
      ("m" org-agenda-month-view
       (if (eq 'month (org-agenda-cts)) "[x]" "[ ]"))
      ("y" org-agenda-year-view
       (if (eq 'year (org-agenda-cts)) "[x]" "[ ]"))
      ("l" org-agenda-log-mode
       (format "% -3S" org-agenda-show-log))
      ("L" (org-agenda-log-mode '(4)))
      ("c" (org-agenda-log-mode 'clockcheck))
      ("f" org-agenda-follow-mode
       (format "% -3S" org-agenda-follow-mode))
      ("a" org-agenda-archives-mode)
      ("A" (org-agenda-archives-mode 'files))
      ("r" org-agenda-clockreport-mode
       (format "% -3S" org-agenda-clockreport-mode))
      ("e" org-agenda-entry-text-mode
       (format "% -3S" org-agenda-entry-text-mode))
      ("g" org-agenda-toggle-time-grid
       (format "% -3S" org-agenda-use-time-grid))
      ("D" org-agenda-toggle-diary
       (format "% -3S" org-agenda-include-diary))
      ("!" org-agenda-toggle-deadlines)
      ("["
       (let ((org-agenda-include-inactive-timestamps t))
         (org-agenda-check-type t 'timeline 'agenda)
         (org-agenda-redo)))
      ("q" (message "Abort") :exit t))

    ;; Org Goto
    (defun polaris/org-goto-override-bindings (&rest _)
      "Override the bindings set by `org-goto-map' function."
      (org-defkey org-goto-map "\C-p" #'outline-previous-visible-heading)
      (org-defkey org-goto-map "\C-n" #'outline-next-visible-heading)
      (org-defkey org-goto-map "\C-f" #'outline-forward-same-level)
      (org-defkey org-goto-map "\C-b" #'outline-backward-same-level)
      org-goto-map)
    (advice-add 'org-goto-map :after #'polaris/org-goto-override-bindings)

    ;; Evil bindings in `org-mode'
    (define-minor-mode evil-org-mode
      "Evil-mode bindings for org-mode."
      :init-value nil
      :lighter    "ⓔ"
      :keymap     (make-sparse-keymap)      ; defines evil-org-mode-map
      :group      'evil-org)

    ;; Evil integration

    (advice-add 'evil-force-normal-state :before 'org-remove-occur-highlights)

    ;; smartparens config
    (sp-with-modes '(org-mode)
      (sp-local-pair "*" "*" :unless '(sp-point-after-word-p sp-point-at-bol-p) :skip-match 'polaris/sp-org-skip-asterisk)
      (sp-local-pair "_" "_" :unless '(sp-point-before-word-p sp-point-after-word-p))
      (sp-local-pair "/" "/" :unless '(sp-point-before-word-p sp-point-after-word-p) :post-handlers '(("[d1]" "SPC")))
      (sp-local-pair "~" "~" :unless '(sp-point-before-word-p sp-point-after-word-p) :post-handlers '(("[d1]" "SPC")))
      (sp-local-pair "=" "=" :unless '(sp-point-before-word-p sp-point-after-word-p) :post-handlers '(("[d1]" "SPC")))

      (sp-local-pair "\\[" "\\]" :post-handlers '(("| " "SPC")))
      (sp-local-pair "\\(" "\\)" :post-handlers '(("| " "SPC")))
      (sp-local-pair "$$" "$$"   :post-handlers '((:add " | ")) :unless '(sp-point-at-bol-p))
      (sp-local-pair "{" nil))

    (evil-org-mode +1)
    ;; (org-indent-mode +1)
    (setq line-spacing 1)

    ;; If saveplace places the point in a folded position, unfold it on load
    (when (outline-invisible-p)
      (ignore-errors
        (save-excursion
          (outline-previous-visible-heading 1)
          (org-show-subtree))))

    (defun polaris|org-update ()
      (when (file-exists-p buffer-file-name)
        (org-update-statistics-cookies t)))

    (add-hook 'before-save-hook 'polaris|org-update nil t)
    (add-hook 'evil-insert-state-exit-hook 'polaris|org-update nil t)

    (visual-line-mode +1)
    (visual-line-mode +1)
    (visual-fill-column-mode +1)
    (setq line-spacing 2)

    (diminish 'org-indent-mode)

    ;; Custom Ex Commands
    (evil-ex-define-cmd "src"      'org-edit-special)
    (evil-ex-define-cmd "refile"   'org-refile)
    (evil-ex-define-cmd "archive"  'org-archive-subtree)
    (evil-ex-define-cmd "agenda"   'org-agenda)
    (evil-ex-define-cmd "todo"     'org-show-todo-tree)
    (evil-ex-define-cmd "link"     'org-link)
    (evil-ex-define-cmd "wc"       'polaris/org-word-count)
    (evil-ex-define-cmd "at[tach]" 'polaris:org-attach)
    (evil-ex-define-cmd "export"   'polaris:org-export)

    ;; Easy Templates
    ;; http://orgmode.org/manual/Easy-Templates.html
    ;; http://oremacs.com/2015/03/07/hydra-org-templates
    ;; https://github.com/abo-abo/hydra/wiki/Org-mode-block-templates

    (defun polaris/org-template-expand (str &optional lang)
      "Expand org template."
      (let (beg old-beg end content)
        ;; Save restriction to automatically undo the upcoming `narrow-to-region'
        (save-restriction
          (when (use-region-p)
            (setq beg (region-beginning))
            (setq end (region-end))
            ;; Note that regardless of the direction of selection, we will always
            ;; have (region-beginning) < (region-end).
            (save-excursion
              ;; If `point' is at `end', exchange point and mark so that now the
              ;; `point' is now at `beg'
              (when (> (point) (mark))
                (exchange-point-and-mark))
              ;; Insert a newline if `beg' is *not* at beginning of the line.
              ;; Example: You have ^abc$ where ^ is bol and $ is eol.
              ;;          "bc" is selected and <e is pressed to result in:
              ;;            a
              ;;            #+BEGIN_EXAMPLE
              ;;            bc
              ;;            #+END_EXAMPLE
              (when (/= beg (line-beginning-position))
                (electric-indent-just-newline 1)
                (setq old-beg beg)
                (setq beg (point))
                ;; Adjust the `end' due to newline
                (setq end (+ end (- beg old-beg)))))
            (save-excursion
              ;; If `point' is at `beg', exchange point and mark so that now the
              ;; `point' is now at `end'
              (when (< (point) (mark))
                (exchange-point-and-mark))
              ;; If the `end' position is at the beginning of a line decrement
              ;; the position by 1, so that the resultant position is eol on
              ;; the previous line.
              (when (= end (line-beginning-position))
                (setq end (1- end)))
              ;; Insert a newline if `point'/`end' is *not* at end of the line.
              ;; Example: You have ^abc$ where ^ is bol and $ is eol.
              ;;          "a" is selected and <e is pressed to result in:
              ;;            #+BEGIN_EXAMPLE
              ;;            a
              ;;            #+END_EXAMPLE
              ;;            bc
              (when (not (looking-at "\\s-*$"))
                (electric-indent-just-newline 1)))
            ;; Narrow to region so that the text surround the region does
            ;; not mess up the upcoming `org-try-structure-completion' eval
            (narrow-to-region beg end)
            (setq content (delete-and-extract-region beg end)))
          (insert str)
          (org-try-structure-completion)
          (when (string= "<s" str)
            (cond
             (lang
              (insert lang)
              (forward-line))
             ((and content (not lang))
              (insert "???")
              (forward-line))
             (t
              )))
          ;; At this point the cursor will be between the #+BEGIN and #+END lines
          (when content
            (insert content)
            (deactivate-mark)))))

    (defhydra hydra-org-template (:color blue
                                         :hint nil)
      "
        org-template:  _c_enter   _s_rc     _e_xample      _r_uby    _t_ext       _L_aTeX:   _I_NCLUDE:
                       _l_atex    _h_tml    _V_erse        _g_o      _p_ython:    _i_ndex:   _H_TML:
                       _a_scii    _q_uote   _E_macs-lisp   _S_hell   _c_lojure:   _A_SCII:
        "
      ("s" (polaris/org-template-expand "<s")) ; #+BEGIN_SRC ... #+END_SRC
      ("E" (polaris/org-template-expand "<s" "emacs-lisp"))
      ("r" (polaris/org-template-expand "<s" "ruby"))
      ("g" (polaris/org-template-expand "<s" "go"))
      ("p" (polaris/org-template-expand "<s" "python"))
      ("S" (polaris/org-template-expand "<s" "sh"))
      ("t" (polaris/org-template-expand "<s" "text"))
      ("C" (polaris/org-template-expand "<s" "clojure"))
      ("e" (polaris/org-template-expand "<e")) ; #+BEGIN_EXAMPLE ... #+END_EXAMPLE
      ("x" (polaris/org-template-expand "<e")) ; #+BEGIN_EXAMPLE ... #+END_EXAMPLE
      ("q" (polaris/org-template-expand "<q")) ; #+BEGIN_QUOTE ... #+END_QUOTE
      ("V" (polaris/org-template-expand "<v")) ; #+BEGIN_VERSE ... #+END_VERSE
      ("c" (polaris/org-template-expand "<c")) ; #+BEGIN_CENTER ... #+END_CENTER
      ("l" (polaris/org-template-expand "<l")) ; #+BEGIN_EXPORT latex ... #+END_EXPORT
      ("L" (polaris/org-template-expand "<L")) ; #+LaTeX:
      ("h" (polaris/org-template-expand "<h")) ; #+BEGIN_EXPORT html ... #+END_EXPORT
      ("H" (polaris/org-template-expand "<H")) ; #+HTML:
      ("a" (polaris/org-template-expand "<a")) ; #+BEGIN_EXPORT ascii ... #+END_EXPORT
      ("A" (polaris/org-template-expand "<A")) ; #+ASCII:
      ("i" (polaris/org-template-expand "<i")) ; #+INDEX: line
      ("I" (polaris/org-template-expand "<I")) ; #+INCLUDE: line
      ("<" self-insert-command "<")
      ("o" nil "quit"))

    (defun polaris/org-template-maybe ()
      "Insert org-template if point is at the beginning of the line, or is a
    region is selected. Else call `self-insert-command'."
      (interactive)
      (let ((regionp (use-region-p)))
        (if (or regionp
                (and (not regionp)
                     (looking-back "^")))
            (hydra-org-template/body)
          (self-insert-command 1))))

    (bind-keys
     :map org-mode-map
     ("<"   . polaris/org-template-maybe))

    ;; -------- ;;
    ;; Bindings ;;
    ;; -------- ;;
    (define-key org-mode-map (kbd "RET") nil)
    (define-key org-mode-map (kbd "C-j") nil)
    (define-key org-mode-map (kbd "C-k") nil)
    ;; http://endlessparentheses.com/emacs-narrow-or-widen-dwim.html
    ;; Pressing `C-x C-s' while editing org source code blocks saves and exits
    ;; the edit.
    (with-eval-after-load 'org-src
      (bind-key "C-x C-s" #'org-edit-src-exit org-src-mode-map))
    (map! (:map org-mode-map
            :i [remap polaris/inflate-space-maybe] 'org-self-insert-command
            :i "RET" 'org-return-indent)

          (:map evil-org-mode-map
            :ni "A-l" 'org-metaright
            :ni "A-h" 'org-metaleft
            :ni "A-k" 'org-metaup
            :ni "A-j" 'org-metadown
            ;; Expand tables (or shiftmeta move)
            :ni "A-L" 'polaris/org-table-append-field-or-shift-right
            :ni "A-H" 'polaris/org-table-prepend-field-or-shift-left
            :ni "A-K" 'polaris/org-table-prepend-row-or-shift-up
            :ni "A-J" 'polaris/org-table-append-row-or-shift-down

            :i  "C-L" 'polaris/org-table-next-field
            :i  "C-H" 'polaris/org-table-previous-field
            :i  "C-K" 'polaris/org-table-previous-row
            :i  "C-J" 'polaris/org-table-next-row

            :i  "C-e" 'org-end-of-line
            :i  "C-a" 'org-beginning-of-line
            :i  "<tab>"   'polaris/org-indent
            :i  "<S-tab>" 'polaris/org-dedent

            :nv "j"   'evil-next-visual-line
            :nv "k"   'evil-previous-visual-line
            :v  "<S-tab>" 'polaris/yas-insert-snippet

            :i  "M-a" (λ! (evil-visual-state) (org-mark-element))
            :n  "M-a" 'org-mark-element
            :v  "M-a" 'mark-whole-buffer

            :ni "<M-return>"   (λ! (polaris/org-insert-item 'below))
            :ni "<S-M-return>" (λ! (polaris/org-insert-item 'above))

            :i  "M-b" (λ! (polaris/org-surround "*")) ; bold
            :i  "M-u" (λ! (polaris/org-surround "_")) ; underline
            :i  "M-i" (λ! (polaris/org-surround "/")) ; italics
            :i  "M-`" (λ! (polaris/org-surround "+")) ; strikethrough

            :v  "M-b" "S*"
            :v  "M-u" "S_"
            :v  "M-i" "S/"
            :v  "M-`" "S+"

            (:leader
              :n ";"  'helm-org-in-buffer-headings
              :n "oa" 'polaris/org-attachment-reveal)

            (:localleader
              :n  "/"  'org-sparse-tree
              :n  "?"  'org-tags-view

              :n  "n"  (λ! (if (buffer-narrowed-p) (widen) (org-narrow-to-subtree)))
              :n  "e"  'org-edit-special
              :n  "="  'org-align-all-tags
              :nv "l"  'org-insert-link
              :n  "L"  'org-store-link
              :n  "x"  'polaris/org-remove-link
              ;; :n  "w"  'writing-mode
              :n  "v"  'variable-pitch-mode
              :n  "SPC" 'polaris/org-toggle-checkbox
              :n  "RET" 'org-archive-subtree

              :n  "a"  'org-agenda
              :n  "A"  'polaris:org-attachment-list

              :n  "d"  'org-time-stamp
              :n  "D"  'org-time-stamp-inactive
              :n  "i"  'polaris/org-toggle-inline-images-at-point
              :n  "t"  (λ! (org-todo (if (org-entry-is-todo-p) 'none 'todo)))
              :v  "t"  (λ! (evil-ex-normal evil-visual-beginning evil-visual-end "\\t"))
              :n  "T"  'org-todo
              :n  "s"  'org-schedule
              :n  "r"  'org-refile
              :n  "R"  (λ! (org-metaleft) (org-archive-to-archive-sibling)) ; archive to parent sibling
              )

            ;; TODO Improve folding bindings
            :n  "za"  'org-cycle
            :n  "zA"  'org-shifttab
            :n  "zm"  (λ! (outline-hide-sublevels 1))
            :n  "zr"  'outline-show-all
            :n  "zo"  'outline-show-subtree
            :n  "zO"  'outline-show-all
            :n  "zc"  'outline-hide-subtree
            :n  "zC"  (λ! (outline-hide-sublevels 1))
            :n  "zd"  (lambda (&optional arg) (interactive "p") (outline-hide-sublevels (or arg 3)))

            :m  "]]"  (λ! (call-interactively 'org-forward-heading-same-level) (org-beginning-of-line))
            :m  "[["  (λ! (call-interactively 'org-backward-heading-same-level) (org-beginning-of-line))
            :m  "]l"  'org-next-link
            :m  "[l"  'org-previous-link

            :n  "RET" 'polaris/org-dwim-at-point

            :m  "gh"  'outline-up-heading
            :m  "gj"  'org-forward-heading-same-level
            :m  "gk"  'org-backward-heading-same-level
            :m  "gl"  (λ! (call-interactively 'outline-next-visible-heading) (show-children))

            :n  "go"  'org-open-at-point
            :n  "gO"  (λ! (let ((org-link-frame-setup (append '((file . find-file-other-window)) org-link-frame-setup))
                                (org-file-apps '(("\\.org$" . emacs)
                                                 (t . "open \"%s\""))))
                            (call-interactively 'org-open-at-point)))

            :n  "gQ"  'org-fill-paragraph
            :m  "$"   'org-end-of-line
            :m  "^"   'org-beginning-of-line
            :n  "<"   'org-metaleft
            :n  ">"   'org-metaright
            :v  "<"   (λ! (org-metaleft)  (evil-visual-restore))
            :v  ">"   (λ! (org-metaright) (evil-visual-restore))
            :n  "-"   'org-cycle-list-bullet
            :n  "<tab>" 'org-cycle
            :n  [tab] 'org-cycle)

          (:map org-src-mode-map
            :n  "<escape>" (λ! (message "Exited") (org-edit-src-exit)))

          (:after org-agenda
            (:map org-agenda-mode-map
              :e "<escape>" 'org-agenda-Quit
              :e "C-j" 'org-agenda-next-item
              :e "C-k" 'org-agenda-previous-item
              :e "C-n" 'org-agenda-next-item
              :e "C-p" 'org-agenda-previous-item)))

    (defface org-block-background nil "")
    (defun org-fontify-meta-lines-and-blocks-1 (limit)
      "Fontify #+ lines and blocks."
      (let ((case-fold-search t))
        (if (re-search-forward
             "^\\([ \t]*#\\(\\(\\+[a-zA-Z]+:?\\| \\|$\\)\\(_\\([a-zA-Z]+\\)\\)?\\)[ \t]*\\(\\([^ \t\n]*\\)[ \t]*\\(.*\\)\\)\\)"
             limit t)
            (let ((beg (match-beginning 0))
                  (block-start (match-end 0))
                  (block-end nil)
                  (lang (match-string 7))
                  (beg1 (line-beginning-position 2))
                  (dc1 (downcase (match-string 2)))
                  (dc3 (downcase (match-string 3)))
                  end end1 quoting block-type ovl)
              (cond
               ((and (match-end 4) (equal dc3 "+begin"))
                ;; Truly a block
                (setq block-type (downcase (match-string 5))
                      quoting (member block-type org-protecting-blocks))
                (when (re-search-forward
                       (concat "^[ \t]*#\\+end" (match-string 4) "\\>.*")
                       nil t)  ;; on purpose, we look further than LIMIT
                  (setq end (min (point-max) (match-end 0))
                        end1 (min (point-max) (1- (match-beginning 0))))
                  (setq block-end (match-beginning 0))
                  (when quoting
                    (org-remove-flyspell-overlays-in beg1 end1)
                    (remove-text-properties beg end
                                            '(display t invisible t intangible t)))
                  (add-text-properties
                   beg end '(font-lock-fontified t font-lock-multiline t))
                  (add-text-properties beg beg1 '(face org-meta-line))
                  (org-remove-flyspell-overlays-in beg beg1)
                  (add-text-properties	; For end_src
                   end1 (min (point-max) (1+ end)) '(face org-meta-line))
                  (org-remove-flyspell-overlays-in end1 end)
                  (cond
                   ((and lang (not (string= lang "")) org-src-fontify-natively)
                    (org-src-font-lock-fontify-block lang block-start block-end)
                      ;;;;;;; EDIT
                    ;; remove old background overlays
                    (mapc (lambda (ov)
                            (if (eq (overlay-get ov 'face) 'org-block-background)
                                (delete-overlay ov)))
                          (overlays-at (/ (+ beg1 block-end) 2)))
                    ;; add a background overlay
                    (setq ovl (make-overlay beg1 block-end))
                    (overlay-put ovl 'face 'org-block-background)
                    (overlay-put ovl 'evaporate t)) ; make it go away when empty
                   ;; (add-text-properties beg1 block-end '(src-block t)))
                      ;;;;;;; /EDIT
                   (quoting
                    (add-text-properties beg1 (min (point-max) (1+ end1))
                                         '(face org-block))) ; end of source block
                   ((not org-fontify-quote-and-verse-blocks))
                   ((string= block-type "quote")
                    (add-text-properties beg1 (min (point-max) (1+ end1)) '(face org-quote)))
                   ((string= block-type "verse")
                    (add-text-properties beg1 (min (point-max) (1+ end1)) '(face org-verse))))
                  (add-text-properties beg beg1 '(face org-block-begin-line))
                  (add-text-properties (min (point-max) (1+ end)) (min (point-max) (1+ end1))
                                       '(face org-block-end-line))
                  t))
               ((string-match-p
                 (format "^\\+%s+:$"
                         (regexp-opt '("title" "author" "email" "date" "address" "location" "contact"
                                       "project" "country" "city" "created" "issued" "paid" "currency")))
                 dc1)
                ;; (member dc1 '("+title:" "+author:" "+email:" "+date:" "+address:" "+location:" "+contact:" "+project:"))
                (org-remove-flyspell-overlays-in
                 (match-beginning 0)
                 (if (equal "+title:" dc1) (match-end 2) (match-end 0)))
                (add-text-properties
                 beg (match-end 3)
                 (if (member (intern (substring dc1 1 -1)) org-hidden-keywords)
                     '(font-lock-fontified t invisible t)
                   '(font-lock-fontified t face org-document-info-keyword)))
                (add-text-properties
                 (match-beginning 6) (min (point-max) (1+ (match-end 6)))
                 (if (string-equal dc1 "+title:")
                     '(font-lock-fontified t face org-document-title)
                   '(font-lock-fontified t face org-document-info))))
               ((equal dc1 "+caption:")
                (org-remove-flyspell-overlays-in (match-end 2) (match-end 0))
                (remove-text-properties (match-beginning 0) (match-end 0)
                                        '(display t invisible t intangible t))
                (add-text-properties (match-beginning 1) (match-end 3)
                                     '(font-lock-fontified t face org-meta-line))
                (add-text-properties (match-beginning 6) (+ (match-end 6) 1)
                                     '(font-lock-fontified t face org-block))
                t)
               ((member dc3 '(" " ""))
                (org-remove-flyspell-overlays-in beg (match-end 0))
                (add-text-properties
                 beg (match-end 0)
                 '(font-lock-fontified t face font-lock-comment-face)))
               (t ;; just any other in-buffer setting, but not indented
                (org-remove-flyspell-overlays-in (match-beginning 0) (match-end 0))
                (remove-text-properties (match-beginning 0) (match-end 0)
                                        '(display t invisible t intangible t))
                (add-text-properties beg (match-end 0)
                                     '(font-lock-fontified t face org-meta-line))
                t))))))

    ))


;; Org Cliplink
;; https://github.com/rexim/org-cliplink
(use-package org-cliplink
  :defer t
  :bind (:map org-mode-map
          ;; "C-c C-l" is bound to `org-insert-link' by default
          ;; "C-c C-L" is bound to `org-cliplink'
          ("C-c C-S-l" . org-cliplink)))

(use-package org-bullets
  :config
  (setq org-bullets-bullet-list '("■" "◆" "▲" "▶" "●"))
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; LaTeX
(use-package ox-latex
  :defer t
  :config
  (progn
    (setq org-latex-compiler "xelatex"
          org-latex-preview-ltxpng-directory (concat polaris-temp-dir "ltxpng/")
          org-latex-remove-logfiles nil
          org-latex-prefer-user-labels t
          org-latex-create-formula-image-program 'imagemagick
          org-startup-with-latex-preview t
          org-highlight-latex-and-related '(latex)
          org-latex-default-figure-position "H"
          org-format-latex-options (plist-put org-format-latex-options :scale 1.2)
          org-latex-image-default-width nil
          org-latex-hyperref-template
          (concat "\\hypersetup{\n"
                  "pdfauthor={%a},\n"
                  "pdftitle={%t},\n"
                  "pdfkeywords={%k},\n"
                  "pdfsubject={%d},\n"
                  "pdfcreator={%c},\n"
                  "pdflang={%L},\n"
                  ;; Get rid of the red boxes drawn around the links
                  "colorlinks,\n"
                  "citecolor=black,\n"
                  "filecolor=black,\n"
                  "linkcolor=blue,\n"
                  "urlcolor=blue\n"
                  "}")
          org-latex-packages-alist
          '(
            ;; % 0 paragraph indent, adds vertical space between paragraphs
            ;; http://en.wikibooks.org/wiki/LaTeX/Paragraph_Formatting
            ("" "parskip")
            ;; Graphics package for more complicated figures
            ("" "tikz")
            ("" "caption")
            ;; Packages suggested to be added for previewing latex fragments
            ;; http://orgmode.org/worg/org-tutorials/org-latex-preview.html
            ("mathscr" "eucal")
            ("" "latexsym")))

    ;; Prevent tables/figures from one section to float into another section
    ;; http://tex.stackexchange.com/a/282/52678
    (add-to-list 'org-latex-packages-alist '("section" "placeins"))

    ;; Prevent an image from floating to a different location
    ;; http://tex.stackexchange.com/a/8633/52678
    (add-to-list 'org-latex-packages-alist '("" "float"))
    ;; "H" option is from the `float' package.
    ;; That prevents the images from floating around.
    (setq org-latex-default-figure-position "H") ; figures are NOT floating
    ;; (setq org-latex-default-figure-position "htb") ; default - figures are floating

    ;; using minted
    ;; https://github.com/gpoore/minted
    (setq org-latex-listings 'minted)   ; default nil
    (add-to-list 'org-latex-packages-alist '("" "minted"))

    ;; minted package options (applied to embedded source codes)
    (setq org-latex-minted-options
          '(("linenos")
            ("numbersep"   "5pt")
            ("frame"       "none") ; box frame is created by the mdframed package
            ("framesep"    "2mm")
            ;; ("fontfamily"  "zi4") ; required only when using pdflatex
                                        ; instead of xelatex
            ;; minted 2.0 specific features
            ("breaklines")              ; line wrapping within code blocks
            ))

    ;; Run xelatex multiple times to get the cross-references right
    (setq org-latex-pdf-process '("xelatex -shell-escape %f"
                                  "xelatex -shell-escape %f"
                                  "xelatex -shell-escape %f"
                                  "xelatex -shell-escape %f"))))

(provide 'module-org)
;;; module-org.el ends here

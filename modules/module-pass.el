;; -*- lexical-binding: t -*-
;;; module-pass.el

(use-package pass
  :commands (pass)
  :config
  (evil-set-initial-state 'pass-mode 'emacs))

(provide 'module-pass)
;;; module-pass.el ends here

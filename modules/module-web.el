;; -*- lexical-binding: t -*-
;; module-web.el

(add-hook! (sass-mode scss-mode less-css-mode)
  '(polaris|hl-line-off hs-minor-mode))

(push '("css" "scss" "sass" "less" "styl") projectile-other-file-alist)

(use-package haml-mode :mode "\\.haml$")

(use-package stylus-mode :mode "\\.styl$"
  :config (push '("styl" "css") projectile-other-file-alist))

(use-package less-css-mode :mode "\\.less$"
  :config (push '("less" "css") projectile-other-file-alist))

(use-package sass-mode :mode "\\.sass$"
  :config
  (def-company-backend! sass-mode (css))
  (push '("sass" "css") projectile-other-file-alist))

(use-package scss-mode
  :mode "\\.scss$"
  :preface (require 'css-mode)
  :init (setq scss-compile-at-save nil)
  :config
  (def-company-backend! scss-mode (css))
  (push '("scss" "css") projectile-other-file-alist)
  (sp-local-pair 'scss-mode "/*" "*/" :post-handlers '(("[d-3]||\n[i]" "RET") ("| " "SPC")))

  (map! :map scss-mode-map
        :n "M-r" 'polaris/web-refresh-browser
        (:localleader :nv ";" 'polaris/append-semicolon)
        (:leader
          :n ";" 'helm-css-scss
          :n ":" 'helm-css-scss-multi))

  (after! emr
    (emr-declare-command 'polaris/scss-toggle-inline-or-block
      :title "toggle inline/block"
      :modes 'scss-mode
      :predicate (lambda () (not (use-region-p))))))

(use-package jade-mode
  :mode "\\.jade$"
  :config (require 'sws-mode))

(use-package slim-mode :mode "\\.slim$")

(use-package web-mode
  :mode ("\\.p?html?$"
         "\\.\\(tpl\\|blade\\)\\(\\.php\\)?$"
         "\\.erb$"
         "\\.as[cp]x$"
         "\\.mustache$"
         "wp-content/themes/.+/.+\\.php$")
  :init
  (add-hook 'web-mode-hook #'turn-off-smartparens-mode)
  :config
  (setq web-mode-markup-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-style-padding 2
        web-mode-script-padding 2
        web-mode-block-padding 2

        web-mode-enable-current-column-highlight t
        web-mode-enable-current-element-highlight t
        web-mode-enable-element-content-fontification t
        web-mode-enable-element-tag-fontification t
        web-mode-enable-html-entities-fontification t
        web-mode-enable-inlays t
        web-mode-enable-sql-detection t
        web-mode-enable-block-face t
        web-mode-enable-part-face t)
  (push '("html" "jade") projectile-other-file-alist)

  (map! :map web-mode-map :i "SPC" 'self-insert-command)
  (after! nlinum
    ;; Fix blank line numbers after unfolding
    (advice-add 'web-mode-fold-or-unfold :after 'nlinum--flush))

  (map! :map web-mode-map
        "M-/" 'web-mode-comment-or-uncomment
        :n  "M-r" 'polaris/web-refresh-browser

        :n  "za" 'web-mode-fold-or-unfold
        (:localleader :n "t" 'web-mode-element-rename)

        :nv "]a" 'web-mode-attribute-next
        :nv "[a" 'web-mode-attribute-previous
        :nv "]t" 'web-mode-tag-next
        :nv "[t" 'web-mode-tag-previous
        :nv "]T" 'web-mode-element-child
        :nv "[T" 'web-mode-element-parent))

;;
(use-package emmet-mode
  :commands (emmet-mode)
  :diminish emmet-mode
  :init
  (add-hook! (scss-mode web-mode html-mode haml-mode nxml-mode) 'emmet-mode)
  (defvar emmet-mode-keymap (make-sparse-keymap))
  :config
  (setq emmet-move-cursor-between-quotes t)
  (map! :map emmet-mode-keymap
        :v "M-e" 'emmet-wrap-with-markup
        :i "M-e" 'emmet-expand-yas
        :i "M-E" 'emmet-expand-line))

;;
(def-project-type! jekyll ":{"
  :modes (web-mode scss-mode html-mode markdown-mode yaml-mode)
  :match "/\\(\\(css\\|_\\(layouts\\|posts\\|sass\\)\\)/.+\\|.+.html\\)$"
  :files ("config.yml" "_layouts/")
  (add-hook! mode
    (when (eq major-mode 'web-mode)
      (web-mode-set-engine "django"))))

(def-project-type! wordpress "wp"
  :modes (php-mode web-mode css-mode scss-mode sass-mode)
  :match "/wp-\\(\\(content\\|admin\\|includes\\)/\\)?.+$"
  :files ("wp-config.php" "wp-content/"))

;; TODO Add stylus-mode

(provide 'module-web)
;;; module-web.el ends here
